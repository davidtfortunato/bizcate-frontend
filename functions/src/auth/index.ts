import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {FirestoreCollectionNames} from '../data';

/**
 * on Create user
 */
export const onCreateUser = functions.auth.user().onCreate((user) => {
  const profile = {
    uid: user.uid,
    name: user.displayName,
    phoneNumber: user.phoneNumber,
    email: user.email,
    photoUrl: user.photoURL,
    role: 'CLIENT' // Default value
  };
  admin
    .firestore()
    .collection(FirestoreCollectionNames.userProfile)
    .add(profile);
});

