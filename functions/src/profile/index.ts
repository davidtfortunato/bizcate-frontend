import * as functions from 'firebase-functions';

export const saveProfileAPI = functions.https.onRequest((req, res) => {
  console.log('saveProfileAPI: ' + req);
  res.send(JSON.stringify(req.body));
});

export const saveProfile = functions.database.ref('/UserProfile').onCreate(async event => {
  console.log('saveProfile: ' + event);
});
