import {GeneralKeys} from '../data/general-keys';

export enum ModuleType { TASKS, MESSAGE}

export function getDetailsLink(moduleType: ModuleType, id: string) {
  let url = '';
  switch (moduleType) {
    case ModuleType.TASKS:
      url = '/#/tasks/' + id;
      break;
    case ModuleType.MESSAGE:
      url = '/#/messages?conversationId=' + id;
      break;
  }

  // Add qparam from_notif
  url += url.indexOf('?') >= 0 ? '&' : '?'
    + GeneralKeys.QPARAM_FROM_NOTIF + '=true';

  return url;
}
