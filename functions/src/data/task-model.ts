import {StorageFileModel} from './storage-model';
import {ServiceModel} from './skills-model';
import {AppMapLocation} from './location-model';
import {TaskOfferModel} from './task-offer-model';
import {ScheduleAvailabilityPeriod, ScheduleStatus} from './calendar-model';
import {ServiceSkill} from './professional-profile-model';

export enum ReviewLevel {
  LEVEL_1 = 1,
  LEVEL_2 = 2,
  LEVEL_3 = 3,
  LEVEL_4 = 4,
  LEVEL_5 = 5,
}

export enum TaskStatus {
  OPEN = 'open',
  ASSIGNED = 'assigned',
  DONE = 'done',
}

export enum TaskExtraStatus {
  NONE = 'none',
  IN_PROGRESS = 'in_progress',
  EXPIRED = 'expired',
  COMPLETED = 'completed'
}

export enum TaskBudgetType {HOURLY = 'hourly', TOTAL = 'total'}

export interface TaskModel {
  id?: string;
  uid: string; // User id
  details: TaskDetails;
  skills: ServiceSkill[];
  location: TaskLocation;
  budget: TaskBudget;
  uploadedFiles: StorageFileModel[];
  status: TaskStatus;
  extraStatus?: TaskExtraStatus;
  postedTime?: number;
  userInfo?: TaskUserInfo;
  acceptedOffer?: TaskOfferModel;
  schedule?: TaskSchedule;
  scheduledStatus?: ScheduleStatus;
  reviews?: {
    clientReview?: TaskUserReview;
    professionalReview?: TaskUserReview;
    isFinished?: boolean;
  };
  taskOffersData?: TaskOfferModel[];
}

export interface TaskUserReview {
  taskId: string;
  publisherUid: string;
  receivedUid: string;
  date: number;
  level: number;
  taskTitle: string;
  message: string;
  origin: TaskReviewOrigin;
  isVisible?: boolean;
}

export enum TaskReviewOrigin { FROM_PROFESSIONAL, FROM_CLIENT}

export interface TaskLocation {
  mapLocation: AppMapLocation;
  addressLocation?: string;
  isRemote: boolean;
}

export interface TaskDetails {
  title: string;
  description: string;
}

export interface TaskBudget {
  expectedBudget: string;
  budgetType: TaskBudgetType; // TOTAL | HOURLY
  dueDate: string;
  currency?: string;
}

export interface TaskUserInfo {
  name: string;
  photoUrl: string;
}

export interface TaskSchedule {
  availability?: ScheduleAvailabilityPeriod[];
  scheduledBlocks?: ScheduleAvailabilityPeriod[];
  startDate?: number;
  endDate?: number;
}



