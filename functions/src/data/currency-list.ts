export const CURRENCIES_LIST: Currency[] = [
  // Euro
  {
    isoCode: 'EUR',
    symbol: '€'
  },
  // USD
  {
    isoCode: 'USD',
    symbol: '$'
  },
  // GBP
  {
    isoCode: 'GBP',
    symbol: '£'
  }
];

export const DEFAULT_CURRENCY = CURRENCIES_LIST[0];

export interface Currency {
  isoCode: string;
  symbol: string;
}
