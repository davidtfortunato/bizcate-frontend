export interface UserConversationModel {
  id?: string;
  userUIDA: string;
  userA?: UserInfos;
  userUIDB: string;
  userB?: UserInfos;
  lastMessage?: UserMessageModel;
  userAReadAt?: number;
  userBReadAt?: number;
};

/**
 * User infos to use on the conversation
 */
export interface UserInfos {
  name?: string;
  photoUrl?: string;
}

export interface UserMessageModel {
  id?: string;
  conversationId?: string; // In the case the message doesn't have a conversationId yet
  userReceiverUID: string;
  userSenderUID: string;
  date: number;
  message: string;
}
