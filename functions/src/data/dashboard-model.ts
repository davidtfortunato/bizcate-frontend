import {TaskModel} from './task-model';
import {TaskOfferModel} from './task-offer-model';
import {UserConversationModel} from './messages-model';

export interface DashboardClientModel {
  uid: string;

  listMyOpenTasks: TaskModel[];
  listMyCompletedTasks: TaskModel[];
  listReceivedOffers: TaskOfferModel[];
  listReceivedMessages: UserConversationModel[];
}

export interface DashboardProfessionalModel {
  uid: string;

  // Data as a client
  dashboardClient: DashboardClientModel;

  // Profesisonal Data
  listMyPendingOffers: TaskOfferModel[];
  listTasksAssigned: TaskModel[]; // List of tasks that are assigned to the professional but wasn't yet finished

}


