import {StorageFileModel} from './storage-model';

export interface ProfessionalPortfolioModel {
  id?: string;
  uid: string; // User id
  thumbnailFile?: StorageFileModel;
  thumbnailImageUrl?: string;
  title: string;
  description: string;
  url: string;
  publishedDate: number;
}
