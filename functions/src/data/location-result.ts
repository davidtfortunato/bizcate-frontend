export interface LocationResult {
  id: string;
  name: string;
  iso2: string;
  country: string;
  admin1: string; // Region Capital
  lat: number;
  lon: number;
  timezone: string;
  postcodes: string[];
  featureCode: string;
}

export function getLocationLabel(location: LocationResult)
{
  return location.name + ', ' + location.admin1 + ', ' + location.country;
}
