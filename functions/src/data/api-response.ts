export const responseMessages = {
  // 1xxx Critical errors
  1000: 'Error validating request body (From Joi)',
  1001: 'Request not authorized for none Admin users',
  1002: 'User not found',
  1003: 'Profile of the current user doesn\'t exist',
  1004: 'Error updating the user profile',
  1005: 'Invalid JWtoken',
  1006: 'Unable to create a conversation',
  1007: 'Unable to login because the UserProfile is not valid',
  1008: 'The message data is invalid',
  1010: 'User not logged in',
  1111: 'General error', // Usually get a custom message
  1999: 'Invalid request',
  // 2xxx Errors
  2000: 'Your email is not valid or doesn\'t exist',
  2001: 'Your password is not valid',
  2002: 'Email already registered',
  // 3xxx Warning
  // 4xxx Success
  4000: 'Success',
  4001: 'User logged in with success',
  4002: 'User deleted with success',
  4003: 'Message sent with success'
};

const PRINT_ERRORS = true;
const PRINT_RESPONSES = false;

export interface ApiResponse<T extends any> {
  responseCode: number; // Internal response code
  responseData?: T; // Body response,
  errorData?: any;
  message?: string; // Message in the case it needs some extra information
  displayMessage?: boolean; // if the Message sent should be displayed to the User
  isSuccess: boolean; // If the request was success or not
}

export const getResponse = function (responseCode, responseData = null, displayMessage = false, message = null) {
  const isSuccess = responseCode >= 4000;
  return {
    responseCode: responseCode,
    responseData: isSuccess ? responseData : null,
    errorData: !isSuccess ? responseData : null,
    message: message ? message : responseMessages[responseCode],
    displayMessage: displayMessage,
    isSuccess: responseCode >= 4000
  };
};

export const getQuickResponse = function (isSuccess: boolean, responseData = null) {
  if ((!isSuccess && PRINT_ERRORS) || PRINT_RESPONSES) {
    // Print responses
    console.log('Response: ' + JSON.stringify(responseData));
  }
  return getResponse(isSuccess ? 4000 : 1999, responseData);
};
