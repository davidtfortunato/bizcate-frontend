import {TaskModel} from './task-model';

export interface TaskOfferModel {
  id?: string;
  taskId: string;
  taskData: TaskModel;
  userInfo: UserOfferInfo;
  valueOffered: number;
  message: string;
  offerDate: number;
  replies: OfferReplies[];
  isAccepted: boolean;
}

export interface UserOfferInfo {
  uid: string;
  name: string;
  photoUrl: string;
}

export interface OfferReplies {
  userInfo: UserOfferInfo;
  message: string;
  date: number;
}
