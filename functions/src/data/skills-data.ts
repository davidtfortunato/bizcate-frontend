import {CategorySkill, ServiceModel} from './skills-model';


/*** CATEGORIES *****/
export const Categories:{[id: string]: CategorySkill} = {
  /** HOUSE CARING */
  '1': {
    id: '1',
    name: 'House caring',
    label: {
      'pt': 'Manutenção de habitação'
    },
    iconUrl: 'https://bitbucket.org/bizcateapp/bizcate-assets/raw/master/assets/ic_category_house.png',
    matIcon: 'home'
  },
  /** Software Development */
  '2': {
    id: '2',
    name: 'Software Development',
    iconUrl: 'https://bitbucket.org/bizcateapp/bizcate-assets/raw/master/assets/ic_category_softwaredev.png',
    matIcon: 'computer'
  }
  // TODO Complete all categories
};

/**
 *
 * @param categoryId
 * @param locale only the language not the region
 */
export function getLabelCategory(categoryId: string, locale: string) {
  let label = '';

  if (categoryId) {
    const category = Categories[categoryId];

    if (category) {

      // Check if have localized labels
      if (category.label)
      {
        let finalLocale = locale;
        // check if have the locale language
        if (locale.indexOf('-') >= 0) {
          // Contains region, get only the language
          finalLocale = locale.substring(0, locale.indexOf('-'));
        }
        label = category[finalLocale];
      }

      // If the label is undefined, then use name for label
      if (!label) {
        label = category.name;
      }

    }
  }
  return label;
}


/***********************************************/
/************** SKILLS DATA ********************/

export const Skills:{[id: string]: ServiceModel} = {
  /** CATEGORY:Software development */
  '1': {
    id: '1',
    categoryId: '2',
    name: 'Web Developer',
    label: {
      'pt': 'Programador Web'
    }
  },
  /** CATEGORY:  House caring */
  '2': {
    id: '2',
    categoryId: '1',
    name: 'Cleaning House',
    label: {
      'pt': 'Limpeza da habitação'
    }
  },
  '3': {
    id: '3',
    categoryId: '1',
    name: 'Garden',
    label: {
      'pt': 'Jardinagem'
    }
  }
  // TODO Complete all professionalSkills
};

/**
 * Get Label of the serviceName. Fallback to the name in the case doesn't exist any label for the location
 * @param skillId
 * @param locale
 */
export function getSkillLabel(skillId: string, locale: string) {
  let label = '';

  if (skillId) {
    const skill = Skills[skillId];

    if (skill) {

      // Check if have localized labels
      if (skill.label)
      {
        let finalLocale = locale;
        // check if have the locale language
        if (locale.indexOf('-') >= 0) {
          // Contains region, get only the language
          finalLocale = locale.substring(0, locale.indexOf('-'));
        }
        label = skill[finalLocale];
      }

      // If the label is undefined, then use name for label
      if (!label) {
        label = skill.name;
      }
    }
  }
  return label;
}

/**
 * Get all the professionalSkills available for a category
 * @param categoryId
 */
export function getCategorySkills(categoryId: string): ServiceModel[] {
  const listSkills: ServiceModel[] = [];

  Object.keys(Skills).forEach(skillId => {
    const skill = Skills[skillId];
    if (skill.categoryId === categoryId)
    {
      listSkills.push(skill);
    }
  });
  return listSkills;
}

/**
 * Get Skill details
 * @param skillId
 */
export function getSkillDetails(skillId: string): {skill: ServiceModel, category: CategorySkill} {
  const skill = Skills[skillId];
  if (skill != null)
  {
    return {
      skill,
      category: Categories[skill.categoryId]
    }
  }
  else {
    return null;
  }
}


export function getSkillCategories() {
  const categories = [];

  Object.keys(Categories).forEach(key => {
    categories.push(Categories[key]);
  });
  return categories;
}

export function getSkillLevels() {
  return ['1', '2', '3', '4', '5+'];
}
