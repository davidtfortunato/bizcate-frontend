export interface CategorySkill {
  id: string;
  name: string; // Fallback when there isn't a localized label
  label?: {
    [localLang: string]: string;
  };
  iconUrl: string;
  matIcon: string;
}

export interface ServiceModel {
  id: string;
  categoryId: string;
  name: string;
  label?: {
    [localLang: string]: string;
  };
}

