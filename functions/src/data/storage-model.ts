export interface StorageFileModel {
  id?: string;
  uid: string; // Author of the file
  filePath: string;
  fileUrl: string;
  publishedDate: number;
}
