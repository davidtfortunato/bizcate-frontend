import {TaskModel, TaskStatus} from './task-model';
import {ServiceModel} from './skills-model';
import {ServiceSkill} from './professional-profile-model';

export interface TaskBrowseFilters {
  locationType: TaskLocationType;
  price: PriceRange;
  taskStatus: TaskStatus;
  onlyMyWorkAreas: boolean;
  onlyMySkills: boolean;
  updatedAt?: number;
  categories?: string[];
}

export enum TaskLocationType { REMOTE = 'remote', PERSON = 'person', ALL = 'all'}

export interface PriceRange {
  minPrice: number;
  maxPrice: number;
  maxPriceLimit: number;
}

export function isTaskFitsFilters(task: TaskModel, taskBrowserFilters: TaskBrowseFilters, skills?: ServiceSkill[]): boolean {

  // Check if match the location
  if (taskBrowserFilters.locationType !== TaskLocationType.ALL) {
    // Just remote positions and this task isn't for remote
    if (taskBrowserFilters.locationType === TaskLocationType.REMOTE && !task.location.isRemote) {
      return false;
    }

    // Just in person tasks and this task doesn't have any location defined and accept remote
    if (taskBrowserFilters.locationType === TaskLocationType.PERSON && (task.location.isRemote && !task.location.mapLocation)) {
      return false;
    }
  }

  // Price range
  if (taskBrowserFilters.price.maxPriceLimit > 0
    && Number(task.budget.expectedBudget) > taskBrowserFilters.price.maxPriceLimit) {
    // Exceeds the max price
    return false;
  }

  // Task status
  if (task.status !== taskBrowserFilters.taskStatus) {
    return false;
  }

  // Have professionalSkills defined. Should match at least one serviceName
  console.log('Skills: ' + JSON.stringify(skills));
  if (skills && skills.length > 0 && taskBrowserFilters.onlyMySkills) {
    let foundSkill = false;
    for (let taskSkill of task.skills) {
      for (let professionalSkill of skills) {
        if (taskSkill
          && taskSkill.serviceName
          && professionalSkill
          && professionalSkill.serviceName
          && taskSkill.serviceName.categoryId === professionalSkill.serviceName.categoryId
          && taskSkill.serviceName.id === professionalSkill.serviceName.id) {
          foundSkill = true;
        }
      }
    }

    // If didn't found any serviceName, then should return false
    if (!foundSkill) {
      return false;
    }
  }

  return true;
}
