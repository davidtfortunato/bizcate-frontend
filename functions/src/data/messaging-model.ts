export interface FCMUserToken {
  locale: string;
  token: string;
}
