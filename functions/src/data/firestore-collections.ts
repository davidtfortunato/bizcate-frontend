export const FirestoreCollectionNames = {
  userProfile: 'UserProfile',
  userConversations: 'UserConversations',
  userMessages: 'UserMessages',
  userProfessionalProfile: 'UserProfessionalProfile',
  userProWorkableAreas: 'UserProWorkableAreas',
  taskLocations: 'TaskLocations',
  storageFilesRegistries: 'StorageFilesRegistries',
  professionalPortfolioRegistries: 'ProfessionalPortfolioRegistries',
  tasksList: 'TasksList',
  taskOffers: 'TaskOffers',
  centralUserData: 'CentralUserData'
};
