export interface PeriodDayModel {
  periodDay: PeriodDayTime;
  dayWeek: WeekDay;
}

export declare enum WeekDay {
  Sunday = 0,
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6
}

export function getPeriodDayByType(periodDay: PeriodDayType): PeriodDayTime {
  switch (periodDay) {
    case PeriodDayType.MORNING:
      return {type: periodDay, startHour: 8, endHour: 12};
    case PeriodDayType.AFTERNOON:
      return {type: periodDay, startHour: 12, endHour: 16};
    case PeriodDayType.EVENING:
      return {type: periodDay, startHour: 16, endHour: 20};
    case PeriodDayType.NIGHT:
      return {type: periodDay, startHour: 20, endHour: 24};
  }
}

export enum PeriodDayType {
  MORNING,
  AFTERNOON,
  EVENING,
  NIGHT
}

export interface PeriodDayTime {
  type: PeriodDayType;
  startHour: number,
  endHour: number;
}
