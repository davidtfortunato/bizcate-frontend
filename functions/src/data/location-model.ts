export interface UserWorkableArea {
  uid: string;
  mapArea: MapAreaDraw;
}

export interface MapAreaDraw {
  id: number;
  radius: number;
  location: AppMapLocation;
}

export interface AppMapLocation {
  longitude: number;
  latitude: number;
}
