export interface ScheduleAvailabilityPeriod {
  day: number;
  period: PeriodType;
  eventType: EventType;
  startHour: number;
  endHour: number;
  id: string;
}

export enum ScheduleStatus {
  SELECTING_AVAILABILITY_PHASE1 = 'selecting_availabilty',
  SELECTING_TIME_PHASE2 = 'selecting_time',
  SCHEDULED_PHASE3 = 'scheduled'
}

export enum EventType {
  HOURLY = 'hourly',
  PERIOD_DAY = 'period_day'
}

export enum PeriodType {
  MORNING = 'morning', // 8am - 12am
  AFTERNOON = 'afternoon', // 12am - 6pm
  EVENING = 'evening' // 6pm - 10pm
}

export class PeriodConst {
  static START_MORNING = 7; // am
  static END_MORNING = 12; // am
  static START_AFTERNOON = 12;// am
  static END_AFTERNOON = 18 // pm
  static START_EVENING = 18 // pm
  static END_EVENING = 24 // pm
}
