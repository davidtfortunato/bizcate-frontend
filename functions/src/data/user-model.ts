import {TaskUserReview} from './task-model';
import {LocationResult} from './location-result';

export interface AuthUserModel {
  uid?: string;
  displayName?: string;
  photoUrl?: string;
  email?: string;
  emailVerified?: boolean;
  phoneNumber?: string;
  isAnonymous: boolean;
}

export interface UserProfileModel {
  id?: string;
  uid: string;
  email?: string;
  name?: string;
  birthday?: string;
  gender?: 'M' | 'F';
  address?: string;
  country?: string;
  city?: string;
  userLocation?: LocationResult;
  phoneNumber?: string;
  photoUrl?: string;
  vatNumber?: string;
  linkedinUrl?: string;
  facebookUrl?: string;
  professionalProfileId?: string;
  role: UserProfileRoleType;
  taskReviews?: TaskUserReview[];
  reviewsAverage?: number;
}


export enum UserProfileRoleType {CLIENT = 'CLIENT', CLIENT_PROFESSIONAL = 'CLIENT_PROFESSIONAL', ADMIN = 'ADMIN'}

export enum UserRegisterState {
  LOADING_USER_DATA,
  NOT_LOGGEDIN,
  REGISTERED_NOT_VALIDATED_EMAIL,
  REGISTERED_NOT_VALIDATED_PHONE,
  REGISTERED_NO_PROFILE,
  REGISTER_COMPLETED
}

export enum UserLoggedinState {
  INITIALIZING_SESSION, LOGGEDIN, ANONYMOUS
}

export function isUserProfileFinished(userProfile: UserProfileModel) {
  return userProfile
    && userProfile.city && userProfile.city.length > 0
    && userProfile.phoneNumber && userProfile.phoneNumber.length > 0
    && userProfile.country && userProfile.country.length > 0
    && userProfile.vatNumber && userProfile.vatNumber.length > 0
    && userProfile.gender
    && userProfile.address && userProfile.vatNumber.length > 0;
}
