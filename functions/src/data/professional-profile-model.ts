import {CategorySkill, ServiceModel} from './skills-model';
import {UserWorkableArea} from './location-model';
import {TaskUserReview} from './task-model';
import {LocationResult} from './location-result';
import {Currency} from './currency-list';

export interface UserProfessionalProfile {
  id?: string;
  uid: string;
  isActive: boolean;
  logoUrl: string;
  name: string;
  description: string;
  address: string;
  country: string;
  city?: string;
  vatNumber: string
  phoneNumber: {
    number: string;
    validated: boolean;
  };
  documents: ProfessionalDocument[];
  workableAreas?: UserWorkableArea[]; // Deprecated
  location?: LocationResult;
  remoteWork: boolean;
  websiteUrls?: string[];
  availableServices: ProfessionalServiceAvailable[];
  professionalSkills: ServiceSkill[];
  taskReviews?: TaskUserReview[];
  reviewsAverage?: number;
  priceHourlyRate?: BizcaterPriceHourlyRate;
}

export interface BizcaterPriceHourlyRate {
  priceHourly: number;
  currency: Currency;
}

export interface ProfessionalDocument {
  name: string;
  fileUrl: string;
  validated: boolean;
}

export interface ProfessionalServiceAvailable {
  service: ServiceSkill;
  priceHour: number; // Price/hour
  currency: Currency;
}

export interface ServiceSkill {
  category: CategorySkill;
  serviceName: ServiceModel;
  professionalExperience: '1' | '2' | '3' | '4' | '5+';
}
