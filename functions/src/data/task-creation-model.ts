import {ServiceSkill} from './professional-profile-model';
import {LocationResult} from './location-result';
import {PeriodDayModel} from './period-day-time';

export interface TaskCreationModel {
  uid: string;
  title: string;
  description?: string;
  professionalService: ServiceSkill;
  taskLocation?: LocationResult;
  isRemoteTask?:boolean;
  listWeekAvailability?: PeriodDayModel[], // Availability of the user during the week to execute the task
  dueDate?: number;
  estimatedDuration?: number; // hours
  lastUpdate: number;
}
