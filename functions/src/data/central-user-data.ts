import {UserProfileModel} from './user-model';
import {TaskModel} from './task-model';
import {TaskOfferModel} from './task-offer-model';
import {UserProfessionalProfile} from './professional-profile-model';
import {TaskCreationModel} from './task-creation-model';

/**
 * All the personal data of the user should be updated in this model data. The data is updated by the trigger
 * of the collections with the data relevant to this user
 */
export interface CentralUserData {
  docId?: string;
  uid: string;
  userProfile: UserProfileModel;
  clientData?: CentralClientData;
  professionalData?: CentralProfessionalData;
  professionalProfileData?: UserProfessionalProfile;
  taskCreationPending?: TaskCreationModel; // If the user started creating a task and is pending
  // TODO Subscription plan
}

/**
 * Data just for the client user type
 */
export interface CentralClientData {
  myTasks: TaskModel[];
  myOffers: TaskOfferModel[];
  // TODO Agenda
}

export interface CentralProfessionalData {
  myOffersSent: TaskOfferModel[];
  listClients: ClientInfo[];
  // TODO Agenda
}


/**
 * Info of the client with already worked with
 */
export interface ClientInfo {
  userProfile: UserProfileModel;
  notes: string;
  // TODO Some other relevant information
}

