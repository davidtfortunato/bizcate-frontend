import * as functions from 'firebase-functions';
import {FirestoreCollectionNames, UserProfessionalProfile, UserProfileModel} from '../data';
import {CentralUserData} from '../data/central-user-data';
import * as admin from 'firebase-admin';
import {TaskModel} from '../data/task-model';
import {TaskOfferModel} from '../data/task-offer-model';

/**
 * On Create central user data when a user profile is created
 */
export const onUserProfileCreate = functions
  .firestore
  .document(FirestoreCollectionNames.userProfile + '/{userId}')
  .onCreate((snapshot, context) => {
    const userProfileData: UserProfileModel = snapshot.data() as UserProfileModel;
    const uid = userProfileData.uid;

    return new Promise((resolve, reject) => {
      createCentralDataDocument(uid, userProfileData)
        .then(createdResponse => {
          resolve(createdResponse);
        }).catch(err => reject(err));
    });
  });

/**
 * When the user profile is updated should create the central data object or update it if already exists
 */
export const onUserProfileUpdate = functions
  .firestore
  .document(FirestoreCollectionNames.userProfile + '/{userId}')
  .onUpdate((snapshot, context) => {
    const userProfileData: UserProfileModel = snapshot.after.data() as UserProfileModel;
    const uid = userProfileData.uid;

    return new Promise((resolve, reject) => {
      getCentralUserData(uid)
        .then((doc) => {
          if (doc && !doc.empty) {
            const dataDoc = doc.docs[0];
            admin
              .firestore()
              .collection(FirestoreCollectionNames.centralUserData)
              .doc(dataDoc.id)
              .update({'userProfile': userProfileData})
              .then(updatedResponse => {
                resolve(updatedResponse);
              }).catch(err => reject(err));

          } else {
            // If not exist yet, then should create a user central data
            createCentralDataDocument(uid, userProfileData)
              .then(createdResponse => {
                resolve(createdResponse);
              }).catch(err => reject(err));
          }
        }).catch(err => reject(err));
    });
  });


/**
 * When a professional profile is added or updated, should update the central user data with the new data
 */
export const onProfessionalProfileUpdate = functions
  .firestore
  .document(FirestoreCollectionNames.userProfessionalProfile + '/{userId}')
  .onWrite((change, context) => {
    let professionalProfileData: UserProfessionalProfile = null;
    let uid = null;

    console.log('onWriteProfessional - before: '
      + (change.before && change.before.data() ? JSON.stringify(change.before.data()) : 'null')
      + ' after: ' + (change.after && change.after.data() ? JSON.stringify(change.after.data()) : 'null'));

    if (change.after && change.after.data()) {
      professionalProfileData = change.after.data() as UserProfessionalProfile;
      uid = professionalProfileData.uid;
    }
    else if (change.before && change.before.data()) {
      professionalProfileData = null;
      // It was deleted
      uid = (change.before.data() as UserProfessionalProfile).uid;
    }

    if (uid) {
      return new Promise((resolve, reject) => {
        getCentralUserData(uid).then(querySnap => {
          const centralData = !querySnap.empty ? querySnap.docs[0] : null;

          if (centralData) {
            admin
              .firestore()
              .collection(FirestoreCollectionNames.centralUserData)
              .doc(centralData.id)
              .update({'professionalProfileData': professionalProfileData})
              .then(updatedResponse => {
                resolve(updatedResponse);
              }).catch(err => reject(err));
          } else {
            reject('Central document not found');
          }
        });
      });
    } else {
      return {message: 'Professional profile not exist'};
    }
  });

/**
 * Update central data of client when a task is updated
 */
export const onTaskUpdate = functions
  .firestore
  .document(FirestoreCollectionNames.tasksList + '/{taskId}')
  .onWrite((change, context) => {
    const taskData: TaskModel = change.after.data() as TaskModel;
    const docId = change.after.id;


    if (taskData) {
      return new Promise((resolve, reject) => {
        // Update evolved users

        // Update the creator of the task
        getCentralUserData(taskData.uid).then(dataQuery => {
          const centralData = !dataQuery.empty ? dataQuery.docs[0] : null;
          if (centralData) {
            const centralUserObject: CentralUserData = centralData.data() as CentralUserData;

            // First remove the old task data from the central data
            let updatedMyTasks = centralUserObject.clientData.myTasks.filter(t => t.id !== docId);
            // add the new updated data
            updatedMyTasks.push({id: docId, ...taskData});

            admin
              .firestore()
              .collection(FirestoreCollectionNames.centralUserData)
              .doc(centralData.id)
              .update({
                clientData: {
                  myTasks: updatedMyTasks
                }
              })
              .then(updatedResponse => {
                resolve(updatedResponse);
              }).catch(err => reject(err));
          }
        });
      });
    } else {
      return {message: 'Task model doesn\'t exists'};
    }
  });

/**
 * Update central data of professional when a task offer is updated
 */
export const onTaskOfferUpdateProfessional = functions
  .firestore
  .document(FirestoreCollectionNames.taskOffers + '/{taskOfferId}')
  .onWrite((change, context) => {
    let taskOfferData: TaskOfferModel = null;
    let isDeleteData = false;
    let docId = change.after.id;

    console.log('Offer ' + docId + ' changed');

    if (change.after && change.after.data()) {
      taskOfferData = change.after.data() as TaskOfferModel;
      isDeleteData = false;
      console.log('Offer updated: ' + change.after.id);
    }
    else if (change.before && change.before.data()) {
      taskOfferData = change.before.data() as TaskOfferModel;
      isDeleteData = true;
      console.log('Offer deleted: ' + change.after.id);
    }

    console.log('TaskOfferData: ' + JSON.stringify(taskOfferData));

    if (taskOfferData && taskOfferData.userInfo) {
      return new Promise((resolve, reject) => {
        // Update professional Central data
        getCentralUserData(taskOfferData.userInfo.uid).then(querySnap => {
          const centralDoc = !querySnap.empty && querySnap.docs ? querySnap.docs[0] : null;
          let centralDataObject = centralDoc.data() as CentralUserData;

          // Update central data of the professional
          if (centralDataObject) {
            if (!centralDataObject.professionalData) {
              // Initialize professional data
              centralDataObject = {
                ...centralDataObject,
                professionalData: {
                  myOffersSent: [],
                  listClients: []
                }
              };
            }

            let updatedProfessionalOffers = centralDataObject
              .professionalData
              .myOffersSent
              .filter(offer => offer.id !== docId);
            // If is to delete the offer, doesn't add it again to the list
            if (!isDeleteData) {
              updatedProfessionalOffers.push({id: docId, ...taskOfferData});
            }

            const updatedCentralData: CentralUserData = {
              ...centralDataObject,
              professionalData: {
                ...centralDataObject.professionalData,
                myOffersSent: updatedProfessionalOffers
              }
            };

            console.log('updatedProfessionalOffers: ' + JSON.stringify(updatedProfessionalOffers));
            // Update central data
            admin
              .firestore()
              .collection(FirestoreCollectionNames.centralUserData)
              .doc(centralDoc.id)
              .update({'professionalData': updatedCentralData.professionalData})
              .then(updatedResponse => {
                console.log('Updated response: ' + JSON.stringify(updatedResponse));
                resolve(updatedResponse);
              }).catch(err => {
              console.log('Updated failed: ' + err);
              reject(err);
            });
          }
        });
      });
    } else {
      return {message: 'Task offer not found'};
    }
  });


/**
 * Update central data of client when a task offer is updated
 */
export const onTaskOfferUpdateClient = functions
  .firestore
  .document(FirestoreCollectionNames.taskOffers + '/{taskOfferId}')
  .onWrite((change, context) => {
    let taskOfferData: TaskOfferModel = null;
    let isDeleteData = false;
    let docId = change.after.id;

    console.log('Offer ' + docId + ' changed');

    if (change.after && change.after.data()) {
      taskOfferData = change.after.data() as TaskOfferModel;
      isDeleteData = false;
      console.log('Offer updated: ' + change.after.id);
    }
    else if (change.before && change.before.data()) {
      taskOfferData = change.before.data() as TaskOfferModel;
      isDeleteData = true;
      console.log('Offer deleted: ' + change.after.id);
    }

    if (taskOfferData) {
      return new Promise((resolve, reject) => {
        // Update client Central data
        getCentralUserData(taskOfferData.taskData.uid).then(querySnap => {
          const centralDoc = !querySnap.empty && querySnap.docs ? querySnap.docs[0] : null;
          let centralDataObject = centralDoc.data() as CentralUserData;

          // Update central data of the professional
          if (centralDataObject) {

            let updatedClientOffers = centralDataObject
              .clientData
              .myOffers
              .filter(offer => offer.id !== docId);
            // If is to delete the offer, doesn't add it again to the list
            if (!isDeleteData) {
              updatedClientOffers.push({id: docId, ...taskOfferData});
            }

            centralDataObject = {
              ...centralDataObject,
              clientData: {
                ...centralDataObject.clientData,
                myOffers: updatedClientOffers
              }
            };

            // Update central data
            admin
              .firestore()
              .collection(FirestoreCollectionNames.centralUserData)
              .doc(centralDoc.id)
              .update({
                'clientData': centralDataObject.clientData
              })
              .then(updatedResponse => {
                resolve(updatedResponse);
              }).catch(err => reject(err));
          }
        });
      });
    } else {
      return {message: 'Task offer not found'};
    }
  });


/**** UTILS FUNCTIONS ***/

/**
 * Query for the central user data of the respective uid
 * @param uid
 */
export function getCentralUserData(uid: string) {
  return admin
    .firestore()
    .collection(FirestoreCollectionNames.centralUserData)
    .where('uid', '==', uid).limit(1)
    .get();
}

/**
 * Create a central data document related with the user profile
 * @param uid
 * @param userProfile
 */
export function createCentralDataDocument(uid: string, userProfile: UserProfileModel) {
  console.log('Creating CentralData for: ' + uid);

  // If not exist yet, then should create a user central data
  const centralUserData: CentralUserData = {
    uid: uid,
    userProfile: userProfile,
    clientData: {
      myTasks: [],
      myOffers: []
    },
    professionalData: {
      myOffersSent: [],
      listClients: []
    }
  };

  // Update on DB
  return admin
    .firestore()
    .collection(FirestoreCollectionNames.centralUserData)
    .add(centralUserData);
}
