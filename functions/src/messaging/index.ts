import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import MessagingPayload = admin.messaging.MessagingPayload;
import {FCMUserToken} from '../data/messaging-model';

export const sendPushNotification = functions.https.onCall(async (data: PushNotificationBody, context) => {
    return sendPushNotificationFunction(data);
  }
);


/**
 * Function to execute sending a notification
 * @param data
 */
export function sendPushNotificationFunction(data: PushNotificationBody) {
  return new Promise(async (resolve, reject) => {
    const payload: MessagingPayload = {
      notification: {
        title: data.title ? data.title : 'Bizcate',
        body: data.message,
        icon: 'https://cdn3.iconfinder.com/data/icons/2018-social-media-logotypes/1000/2018_social_media_popular_app_logo_youtube-512.png',
        clickAction: data.link
      }
    };

    // Send the notifications
    if (data.targetsUid) {
      data.targetsUid.forEach(userId => {
        admin.database()
          .ref(`/fcmTokens/${userId}`)
          .once('value')
          .then(token => token.val() as FCMUserToken)
          .then((userFacmToken: FCMUserToken) => {
            return admin.messaging().sendToDevice(userFacmToken.token, payload);
          })
          .then(res => {
            console.log('Sent successfully', res);
            resolve({
              status: 'success',
              response: res
            });
          }).catch(err => {
          reject({
            status: 'failed',
            error: err,
            uid: userId
          });
        });
      });
    }
  });
}

/*export function generateDetailLink() {

}*/

export interface PushNotificationBody {
  title?: string;
  message: string;
  targetsUid: string[];
  link: string;
}
