import * as functions from 'firebase-functions';
import {FirestoreCollectionNames, UserMessageModel} from '../data';
import {sendPushNotificationFunction} from '../messaging';
import {getDetailsLink, ModuleType} from '../utils';

/**
 * Called when a new offer is added
 */
export const messageSentOnCreate = functions.firestore
  .document(FirestoreCollectionNames.userConversations + '/{conversationId}/' + FirestoreCollectionNames.userMessages + '/{messageId}')
  .onCreate((snapshot, context) => {
    return new Promise((resolve, reject) => {
      const newMessage: UserMessageModel = snapshot.data() as UserMessageModel;

      const targets = [];

      targets.push(newMessage.userReceiverUID);

      sendPushNotificationFunction({
        title: 'New message',
        message: newMessage.message,
        link: getDetailsLink(ModuleType.MESSAGE, newMessage.conversationId),
        targetsUid: targets
      }).then(res => res).catch(err => err);

      resolve(newMessage);
    });
  });
