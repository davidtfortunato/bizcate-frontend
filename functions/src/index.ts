import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

// Functions
import * as AuthService from './auth';
import * as TaskService from './tasks';
import * as MessagingService from './messaging';
import * as MessagesService from './messages';
import * as DashboardService from './dashboard';
import * as CentralUserDataService from './centraluserdata';

// Init App
admin.initializeApp(functions.config().firebase);

// FIXME Remove this dummy functions
/*export const saveProfile = profile.saveProfile;
export const saveProfileAPI = profile.saveProfileAPI;*/

// Auth Functions
export const onCreateUser = AuthService.onCreateUser;

// Central Data
export const centralDataOnCreateUserProfile = CentralUserDataService.onUserProfileCreate;
export const centralDataOnupdateUserProfile = CentralUserDataService.onUserProfileUpdate;
export const centralDataOnProfessionalProfileUpdate = CentralUserDataService.onProfessionalProfileUpdate;
export const centralDataOnTaskUpdate = CentralUserDataService.onTaskUpdate;
export const centralDataOnTaskOfferUpdateProfessional = CentralUserDataService.onTaskOfferUpdateProfessional;
export const centralDataOnTaskOfferUpdateClient = CentralUserDataService.onTaskOfferUpdateClient;

// Tasks Functions
export const forceUpdateAllOffersTask = TaskService.forceUpdateAllOffersTask;
export const taskUpdated = TaskService.taskUpdated;
export const acceptTaskOffer = TaskService.acceptTaskOffer;
export const updatingScheduledTasks = TaskService.updatingScheduledTasks;
export const publishTaskReview = TaskService.publishTaskReview;
export const taskOfferCreated = TaskService.taskOfferCreated;
export const getMultipleTasksById = TaskService.getMultipleTasksById;

// Messages Functions
export const messageSentOnCreate = MessagesService.messageSentOnCreate;

// Cloud Messaging
export const sendPushNotification = MessagingService.sendPushNotification;

// Dashboard functions
export const getUserClientDashboard = DashboardService.getUserClientDashboard;
export const getUserProfessionalDashboard = DashboardService.getUserProfessionalDashboard;

