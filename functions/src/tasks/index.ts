import * as functions from 'firebase-functions';
import {TaskExtraStatus, TaskModel, TaskReviewOrigin, TaskStatus, TaskUserReview} from '../data/task-model';
import * as admin from 'firebase-admin';
import {FirestoreCollectionNames, UserProfessionalProfile, UserProfileModel} from '../data';
import {ScheduleStatus} from '../data/calendar-model';
import {TaskOfferModel} from '../data/task-offer-model';
import {sendPushNotificationFunction} from '../messaging';
import {getDetailsLink, ModuleType} from '../utils';


/**
 * This is a method to help fix wrong updated data on the Task Offers
 * WARNING: Should not be used dynamically.
 */
export const forceUpdateAllOffersTask = functions.https.onRequest(async (req, res) => {

  // Get All Tasks
  const listTaskDocs = await admin
    .firestore()
    .collection(FirestoreCollectionNames.tasksList)
    .get() ;

  if (listTaskDocs && !listTaskDocs.empty) {
    listTaskDocs.forEach(async taskDoc => {
      const taskData: TaskModel = taskDoc.data() as TaskModel;
      taskData.id = taskDoc.id;

      const updatedOffers = await updateOffersUpdatedTask(taskDoc.id, taskData);
    });
  }

  res.send({message: 'Updating ' + listTaskDocs.size + ' tasks'});
});

/**
 * Listen to changes on the task update and update the offers with the updated data
 */
export const taskUpdated = functions.firestore
  .document(FirestoreCollectionNames.tasksList + '/{taskId}')
  .onWrite(async (change, context) => {
    // Get updated task data
    const taskData: TaskModel = change.after.data() as TaskModel;

    if (taskData)
    {
      // if the task data doesn't have an id, should be added
      if (!taskData.id)
      {
        await admin
          .firestore()
          .collection(FirestoreCollectionNames.taskOffers)
          .doc(change.after.id).update({id: change.after.id})
      }

      const updatedOffers = await updateOffersUpdatedTask(change.after.id, taskData);
    }
  });

/**
 * Function to update on DB the updated task data on each offer
 * @param taskData
 */
async function updateOffersUpdatedTask(taskId: string, taskData: TaskModel) {
  console.log('Updating task: ' + taskData.details.title);

  // Update offers that are pointing to this task
  const taskOffers = await admin
    .firestore()
    .collection(FirestoreCollectionNames.taskOffers)
    .where('taskId', '==', taskId)
    .get();


  console.log('Updating task: ' + taskData.details.title + ' Offers found: ' + taskOffers.size);

  if (taskOffers && !taskOffers.empty) {
    taskOffers.forEach(doc => {
      let taskOfferData: TaskOfferModel = doc.data() as TaskOfferModel;
      taskOfferData.id = doc.id;

      console.log('Updating offer with new TaskData: ' + taskOfferData.id);

      // Update with new task data
      taskOfferData = {
        ...taskOfferData,
        taskData: taskData
      };

      // Update on DB
      admin
        .firestore()
        .collection(FirestoreCollectionNames.taskOffers)
        .doc(taskOfferData.id).update(taskOfferData);
    });
  }
  return taskOffers;
}

/**
 * Validations
 * - Look for tasks not COMPLETED
 * - Verify if the dueDate is before than today and no schedule update to EXPIRED
 * - If the scheduled startDate is before than today and endDate after today, update to IN_PROGRESS
 * - If the scheduled endDate is after today update to complete
 */
export const updatingScheduledTasks = functions.https.onRequest(async (req, res) => {

  const listUpdatedTasks: { oldData: TaskModel, updatedData: TaskModel }[] = [];

  // Current time
  const currentTime = admin.database.ServerValue.TIMESTAMP;


  // Update expired tasks
  let taskDocs = await admin
    .firestore()
    .collection(FirestoreCollectionNames.tasksList)
    .where('status', '==', TaskStatus.OPEN)
    .get();

  if (taskDocs && !taskDocs.empty) {

    taskDocs.forEach(task => {
      const taskData: TaskModel = task.data() as TaskModel;
      taskData.id = task.id;

      if (!taskData.schedule && Number(taskData.budget.dueDate) < currentTime) {

        const updatedTask: TaskModel = {
          ...taskData,
          status: TaskStatus.DONE,
          extraStatus: TaskExtraStatus.EXPIRED
        };
        // Notify is expired
        notifyTaskParticipants('A task has expired', taskData);

        // Add to response
        listUpdatedTasks.push({
          oldData: taskData,
          updatedData: updatedTask
        });

        // Update it on firestore
        admin
          .firestore()
          .collection(FirestoreCollectionNames.tasksList)
          .doc(task.id).update(updatedTask);

      }
    });
  }

  // Update assigned tasks
  taskDocs = await admin
    .firestore()
    .collection(FirestoreCollectionNames.tasksList)
    .where('status', '==', TaskStatus.ASSIGNED)
    .get();

  if (taskDocs && !taskDocs.empty) {
    taskDocs.forEach(task => {

      const taskData: TaskModel = task.data() as TaskModel;
      taskData.id = task.id;
      let updatedTask: TaskModel = null;

      // Check if is expired
      if (!taskData.schedule && Number(taskData.budget.dueDate) < currentTime) {
        updatedTask = {
          ...taskData,
          status: TaskStatus.DONE,
          extraStatus: TaskExtraStatus.EXPIRED
        };

        // Notify is expired
        notifyTaskParticipants('A task has expired', taskData);

      } else if (taskData.schedule) {

        // Is already scheduled

        // Verify if is in progress
        if (taskData.schedule.startDate < currentTime && taskData.schedule.endDate > currentTime
          && taskData.extraStatus !== TaskExtraStatus.IN_PROGRESS) {
          // Is between start date and end date so is in progress
          updatedTask = {
            ...taskData,
            extraStatus: TaskExtraStatus.IN_PROGRESS
          };
          // Notify is started
          notifyTaskParticipants('A task has started', taskData);

        } else if (taskData.schedule.endDate < currentTime) {
          // Is after the endData but it stills assigned, means that should be marked as DONE & COMPLETED
          updatedTask = {
            ...taskData,
            status: TaskStatus.DONE,
            extraStatus: TaskExtraStatus.COMPLETED
          };

          // Notify is complete
          notifyTaskParticipants('A task is completed', taskData);
        }
      }

      // Check if should update the task
      if (updatedTask) {
        // Add to response
        listUpdatedTasks.push({
          oldData: taskData,
          updatedData: updatedTask
        });

        // Update it on firestore
        admin
          .firestore()
          .collection(FirestoreCollectionNames.tasksList)
          .doc(task.id).update(updatedTask);
      }

    });
  }

  const responseBody = {
    message: 'Tasks Updated: ' + listUpdatedTasks.length,
    updatedList: listUpdatedTasks
  };

  console.log('Response: ' + JSON.stringify(responseBody));
  res.status(200).send(responseBody);
});

function notifyTaskParticipants(message: string, taskData: TaskModel) {
  const targets = [];
  targets.push(taskData.uid);
  if (taskData.acceptedOffer) {
    targets.push(taskData.acceptedOffer.userInfo.uid);
  }

  sendPushNotificationFunction({
    message: message,
    link: getDetailsLink(ModuleType.TASKS, taskData.id),
    targetsUid: targets
  }).then(res => res).catch(err => err);
}

export const getMultipleTasksById = functions.https.onCall(async (data: GetMultipleTasksBody, context) => {
  return new Promise(async (resolve, reject) => {
    if (data && data.taskIds) {
      const tasks: TaskModel[] = [];

      data.taskIds.forEach(async(taskId) => {
        const doc = await admin
          .firestore()
          .collection(FirestoreCollectionNames.tasksList)
          .doc(taskId).get();

        if (doc && doc.exists) {
          tasks.push({
            ...doc.data() as TaskModel,
            id: doc.id
          });
        }
      });

      resolve(tasks);
    } else {
      reject('Invalid tasksIds on body');
    }
  });
});


/**
 * To accept an offer should follow the next steps:
 * - The task have to still be on "open" status
 * - Change Task status to "assigned"
 * - remove other offers
 * - update the accepted offer with the Task data updated
 */
export const acceptTaskOffer = functions.https.onCall(async (data: AcceptTaskBody, context) => {
  if (!context.auth && !data.uid) return {status: 'error', code: 401, message: 'Not signed in'};


  return new Promise(async (resolve, reject) => {

    const uid = context.auth ? context.auth.uid : data.taskId;
    const taskId = data.taskId;
    const taskOfferId = data.taskOfferId;

    if (!uid && !taskId && !taskOfferId) {
      reject('Invalid data. Uid or taskid or taskOfferId is not valid');
      return;
    }

    // Check if is the user is the owner of the task
    let taskDataDocument = await admin.firestore().collection(FirestoreCollectionNames.tasksList).doc(taskId).get();
    if (taskDataDocument.exists && (taskDataDocument.data() as TaskModel).uid !== uid) {
      reject('This user is not authorized to accept this task');
      return;
    }

    // Try to find task
    let offerUpdateResponse = await admin.firestore().collection(FirestoreCollectionNames.taskOffers).doc(taskOfferId).update({'isAccepted': true});
    let taskOfferDoc = await admin.firestore().collection(FirestoreCollectionNames.taskOffers).doc(taskOfferId).get();

    // Remove the other offers
    let removedTaskOffersDoc = await admin
      .firestore()
      .collection(FirestoreCollectionNames.taskOffers)
      .where('taskId', '==', taskId)
      .where('isAccepted', '==', false)
      .get();
    if (removedTaskOffersDoc && removedTaskOffersDoc.docs.length > 0) {
      removedTaskOffersDoc.forEach(async (taskOfferRemoveDoc) => {
        notifyOfferRejected(taskOfferRemoveDoc.data() as TaskOfferModel);
        const response = await admin.firestore().collection(FirestoreCollectionNames.taskOffers).doc(taskOfferRemoveDoc.id).delete();
        console.log('Removed taskOffer: ' + JSON.stringify(taskOfferRemoveDoc.data()));
      });
    }

    let taskDocument = await admin
      .firestore()
      .collection(FirestoreCollectionNames.tasksList)
      .doc(taskId).update({
        status: TaskStatus.ASSIGNED,
        acceptedOffer: {...taskOfferDoc.data(), id: taskOfferDoc.id},
        scheduledStatus: ScheduleStatus.SELECTING_AVAILABILITY_PHASE1
      });
    notifyOfferAccepted(taskOfferDoc.data() as TaskOfferModel);

    // find a user by data.uid and return the result
    resolve(taskDocument);
  });

});

function notifyOfferAccepted(offerData: TaskOfferModel) {
  const targets = [];
  targets.push(offerData.userInfo.uid);

  sendPushNotificationFunction({
    message: 'Your offer was accepted',
    link: getDetailsLink(ModuleType.TASKS, offerData.taskId),
    targetsUid: targets
  }).then(res => res).catch(err => err);
}

function notifyOfferRejected(offerData: TaskOfferModel) {
  const targets = [];
  targets.push(offerData.userInfo.uid);

  sendPushNotificationFunction({
    message: 'Your offer was rejected',
    link: getDetailsLink(ModuleType.TASKS, offerData.taskId),
    targetsUid: targets
  }).then(res => res).catch(err => err);
}

export const publishTaskReview = functions.https.onCall((data: PublishTaskReviewBody, context) => {
  if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'};
  if (!data.taskData || !data.taskReview)
    return {
      status: 'error',
      code: 400,
      message: 'The body doesn\'t have all the information needed'
    };
  if (data.taskData.uid !== context.auth.uid && data.taskData.acceptedOffer.userInfo.uid !== context.auth.uid)
    return {
      status: 'error',
      code: 401,
      message: 'This is neither the Client and neither the Professional. Is not authorize to publish a review'
    };

  return new Promise(async (resolve, reject) => {

    // Task Offer
    let taskDataDoc = await admin.firestore().collection(FirestoreCollectionNames.tasksList).doc(data.taskData.id).get();
    let taskDataUpdated: TaskModel = {
      ...taskDataDoc.data() as TaskModel,
      id: taskDataDoc.id
    };

    // If doesn't exist yet, initialize reviews property
    if (!taskDataUpdated.reviews) taskDataUpdated.reviews = {};

    // Check if the review should be visible
    switch (data.taskReview.origin) {
      case TaskReviewOrigin.FROM_CLIENT:
        if (taskDataUpdated.reviews.professionalReview) {
          // Already have the professional review, so can set as visible both reviews
          taskDataUpdated.reviews.professionalReview.isVisible = true;
          taskDataUpdated.reviews.clientReview = {
            ...data.taskReview,
            isVisible: true
          };
        } else {
          // Should not be visible yet
          taskDataUpdated.reviews.clientReview = {
            ...data.taskReview,
            isVisible: false
          };
        }
        break;
      case TaskReviewOrigin.FROM_PROFESSIONAL:
        if (taskDataUpdated.reviews.clientReview) {
          // Already have the client review, so can set as visible both reviews
          taskDataUpdated.reviews.clientReview.isVisible = true;
          taskDataUpdated.reviews.professionalReview = {
            ...data.taskReview,
            isVisible: true
          };
        } else {
          // Should not be visible yet
          taskDataUpdated.reviews.professionalReview = {
            ...data.taskReview,
            isVisible: false
          };
        }
        break;
    }

    // Update Task
    const response = await admin.firestore().collection(FirestoreCollectionNames.tasksList).doc(data.taskData.id).update(taskDataUpdated);

    // Check if the Reviews should be added to the Client & Professional
    if (taskDataUpdated.reviews.professionalReview
      && taskDataUpdated.reviews.professionalReview.isVisible
      && taskDataUpdated.reviews.clientReview
      && taskDataUpdated.reviews.clientReview.isVisible) {

      // Update Client User Profile
      const clientUserProfileDoc = await admin
        .firestore()
        .collection(FirestoreCollectionNames.userProfile)
        .where('uid', '==', taskDataUpdated.uid).get();

      if (!clientUserProfileDoc.empty) {
        const clientProfile: UserProfileModel = {
          ...clientUserProfileDoc.docs[0].data() as UserProfileModel,
          id: clientUserProfileDoc.docs[0].id
        };

        if (!clientProfile.taskReviews) {
          clientProfile.taskReviews = [];
        }

        clientProfile.taskReviews.push(taskDataUpdated.reviews.professionalReview);

        // Reviews average
        let total = 0;
        clientProfile.taskReviews.forEach(review => {
          total += review.level;
        });
        clientProfile.reviewsAverage = total / clientProfile.taskReviews.length;


        await admin
          .firestore()
          .collection(FirestoreCollectionNames.userProfile).doc(clientProfile.id).update(clientProfile);
      }

      // Update Professional Profile
      const professionalProfileDoc = await admin
        .firestore()
        .collection(FirestoreCollectionNames.userProfessionalProfile)
        .where('uid', '==', taskDataUpdated.acceptedOffer.userInfo.uid).get();

      if (!professionalProfileDoc.empty) {
        const professionalProfile: UserProfessionalProfile = {
          ...professionalProfileDoc.docs[0].data() as UserProfessionalProfile
        };

        if (!professionalProfile.taskReviews) {
          professionalProfile.taskReviews = [];
        }

        professionalProfile.taskReviews.push(taskDataUpdated.reviews.professionalReview);

        // Reviews average
        let total = 0;
        professionalProfile.taskReviews.forEach(review => {
          total += review.level;
        });
        professionalProfile.reviewsAverage = total / professionalProfile.taskReviews.length;


        await admin
          .firestore()
          .collection(FirestoreCollectionNames.userProfessionalProfile)
          .doc(professionalProfileDoc.docs[0].id)
          .update(professionalProfile);
      }
    }

    // Notify User Review
    notifyNewUserReview(data.taskReview);

    resolve(response);
  });
});

function notifyNewUserReview(newUserReview: TaskUserReview) {
  const targets = [];
  targets.push(newUserReview.receivedUid);

  sendPushNotificationFunction({
    message: 'You received a user review',
    targetsUid: targets,
    link: getDetailsLink(ModuleType.TASKS, newUserReview.taskId)
  }).then(res => res).catch(err => err);
}


/**
 * Called when a new offer is added
 */
export const taskOfferCreated = functions.firestore
  .document(FirestoreCollectionNames.taskOffers + '/{offerId}')
  .onCreate((snapshot, context) => {
    return new Promise((resolve, reject) => {
      const newOffer: TaskOfferModel = snapshot.data() as TaskOfferModel;
      const targets = [];

      targets.push(newOffer.taskData.uid);

      sendPushNotificationFunction({
        message: 'New offer on task: ' + newOffer.taskData.details.title,
        link: getDetailsLink(ModuleType.TASKS, newOffer.taskId),
        targetsUid: targets
      }).then(res => {
        resolve(newOffer);
      }).catch(err => reject(err));
    });
  });

export interface AcceptTaskBody {
  uid?: string;
  taskId: string;
  taskOfferId: string;
}

export interface PublishTaskReviewBody {
  taskData: TaskModel;
  taskReview: TaskUserReview;
}

export interface GetMultipleTasksBody {
  taskIds: string[];
};
