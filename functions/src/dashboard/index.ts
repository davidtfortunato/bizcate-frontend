import * as functions from 'firebase-functions';
import {DashboardClientModel, DashboardProfessionalModel} from '../data/dashboard-model';
import * as admin from 'firebase-admin';
import {FirestoreCollectionNames, UserConversationModel} from '../data';
import {TaskModel, TaskStatus} from '../data/task-model';
import {TaskOfferModel} from '../data/task-offer-model';


export const getUserClientDashboard = functions.https.onCall(async (req, context) => {

  if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'};

  return new Promise(async (resolve, reject) => {

    const uid = context.auth.uid;

    // First generate the client dashboard
    const clientDashboard: DashboardClientModel = await getUserClientDashboardData(uid);

    // Return dashboard data
    resolve(clientDashboard);
  });
});

export const getUserProfessionalDashboard = functions.https.onCall(async (req, context) => {

  if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'};

  return new Promise(async (resolve, reject) => {

    const uid = context.auth.uid;

    // First generate the client dashboard
    const clientDashboard: DashboardClientModel = await getUserClientDashboardData(uid);
    const professionalDashboard: DashboardProfessionalModel = {
      dashboardClient: clientDashboard,
      uid: clientDashboard.uid,
      listMyPendingOffers: [],
      listTasksAssigned: []
    };

    // If the user is a professional, should get also the data for the Professional dashboard

    // Get Professional pending offers
    professionalDashboard.listMyPendingOffers = await getUserProfessionalPendingOffers(uid);

    // Get Professional Assigned Tasks
    professionalDashboard.listTasksAssigned = await getUserProfessionalTasksAssigned(uid);

    // Return dashboard data
    resolve(professionalDashboard);
  });
});

async function getUserClientDashboardData(uid: string): Promise<DashboardClientModel> {
  return new Promise<DashboardClientModel>(async (resolve, reject) => {
    // First generate the client dashboard
    const clientDashboard: DashboardClientModel = {
      uid: uid,
      listMyCompletedTasks: [],
      listMyOpenTasks: [],
      listReceivedMessages: [],
      listReceivedOffers: []
    };

    // Get list of my open tasks
    clientDashboard.listMyOpenTasks = await getUserTasks(uid, TaskStatus.OPEN);

    // Get list of tasks completed
    clientDashboard.listMyCompletedTasks = await getUserTasks(uid, TaskStatus.DONE);

    // Get received offers
    clientDashboard.listReceivedOffers = await getUserReceivedPendingOffers(uid);

    // Get last received messages from conversations
    clientDashboard.listReceivedMessages = await getLastConversationsReceived(uid);

    resolve(clientDashboard);
  });
}

/**
 * The the user open tasks
 * @param uid
 */
async function getUserTasks(uid: string, taskStatys: TaskStatus): Promise<TaskModel[]> {
  return new Promise<TaskModel[]>(async (resolve, reject) => {
    const listTasks = [];

    const taskDocs = await admin
      .firestore()
      .collection(FirestoreCollectionNames.tasksList)
      .where('uid', '==', uid)
      .where('status', '==', taskStatys)
      .get();

    if (taskDocs && !taskDocs.empty) {
      taskDocs.forEach(taskDoc => {
        const taskData: TaskModel = taskDoc.data() as TaskModel;
        taskData.id = taskDoc.id;
        listTasks.push(taskData);
      });
    }

    resolve(listTasks);
  });
}

/**
 * The the user open tasks
 * @param uid
 */
async function getUserProfessionalTasksAssigned(uid: string): Promise<TaskModel[]> {
  return new Promise<TaskModel[]>(async (resolve, reject) => {
    const listTasks = [];

    const taskDocs = await admin
      .firestore()
      .collection(FirestoreCollectionNames.tasksList)
      .where('status', '==', TaskStatus.ASSIGNED)
      .where('acceptedOffer.userInfo.uid', '==', uid)
      .get();

    if (taskDocs && !taskDocs.empty) {
      taskDocs.forEach(taskDoc => {
        const taskData: TaskModel = taskDoc.data() as TaskModel;
        taskData.id = taskDoc.id;
        listTasks.push(taskData);
      });
    }

    resolve(listTasks);
  });
}

async function getUserReceivedPendingOffers(uid: string): Promise<TaskOfferModel[]> {
  return new Promise<TaskOfferModel[]>(async (resolve, reject) => {
    const listOffers = [];

    const taskOfferDocs = await admin
      .firestore()
      .collection(FirestoreCollectionNames.taskOffers)
      .where('taskData.uid', '==', uid)
      .get();

    if (taskOfferDocs && !taskOfferDocs.empty) {
      taskOfferDocs.forEach(taskOfferDoc => {
        const taskOfferData: TaskOfferModel = taskOfferDoc.data() as TaskOfferModel;
        taskOfferData.id = taskOfferDoc.id;
        listOffers.push(taskOfferData);
      });
    }

    resolve(listOffers);
  });
}

async function getUserProfessionalPendingOffers(uid: string): Promise<TaskOfferModel[]> {
  return new Promise<TaskOfferModel[]>(async (resolve, reject) => {
    const listOffers = [];

    const taskOfferDocs = await admin
      .firestore()
      .collection(FirestoreCollectionNames.taskOffers)
      .where('userInfo.uid', '==', uid)
      .get();

    if (taskOfferDocs && !taskOfferDocs.empty) {
      taskOfferDocs.forEach(taskOfferDoc => {
        const taskOfferData: TaskOfferModel = taskOfferDoc.data() as TaskOfferModel;
        taskOfferData.id = taskOfferDoc.id;
        listOffers.push(taskOfferData);
      });
    }

    resolve(listOffers);
  });
}

/**
 * Get the last user conversations where the user received a new message
 * @param uid
 */
async function getLastConversationsReceived(uid: string): Promise<UserConversationModel[]> {
  return new Promise<UserConversationModel[]>(async (resolve, reject) => {
    const listConversations = [];

    const userConversationsDocs = await admin
      .firestore()
      .collection(FirestoreCollectionNames.userConversations)
      .where('lastMessage.userReceiverUID', '==', uid)
      .get();

    if (userConversationsDocs && !userConversationsDocs.empty) {
      userConversationsDocs.forEach(doc => {
        const userConversationData = doc.data() as UserConversationModel;
        userConversationData.id = doc.id;
        listConversations.push(userConversationData);
      });
    }

    resolve(listConversations);
  });
}
