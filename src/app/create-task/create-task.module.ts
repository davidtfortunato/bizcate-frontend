import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {CreateTaskContainerComponent} from './create-task-container.component';
import {RouterModule, Routes} from '@angular/router';
import {CreateTaskInitialComponent} from './create-task-initial/create-task-initial.component';
import {MatCheckboxModule, MatIconModule, MatInputModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {CreateTaskStepsContainerComponent} from './create-task-steps/create-task-steps-container.component';
import {Step1TaskDescriptionComponent} from './create-task-steps/step1-task-description/step1-task-description.component';
import {TaskLocationComponent} from './create-task-steps/step1-task-description/task-location/task-location.component';
import {TaskDurationComponent} from './create-task-steps/step1-task-description/task-duration/task-duration.component';
import {TaskDescriptionComponent} from './create-task-steps/step1-task-description/task-description/task-description.component';

const ROUTES: Routes = [
  {path: '', component: CreateTaskContainerComponent, pathMatch: 'full'},
  {path: 'creation-steps', component: CreateTaskStepsContainerComponent, pathMatch: 'full'}
];

@NgModule({
  providers: [],
  declarations: [CreateTaskContainerComponent,
    CreateTaskInitialComponent,
    CreateTaskStepsContainerComponent,
    Step1TaskDescriptionComponent,
    TaskLocationComponent,
    TaskDurationComponent,
    TaskDescriptionComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule,
    RouterModule.forChild(ROUTES),
    MatInputModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ],
  entryComponents: []
})
export class CreateTaskModule {
}
