import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../../core/language/language.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-task-duration',
  templateUrl: './task-duration.component.html',
  styleUrls: ['./task-duration.component.scss']
})
export class TaskDurationComponent implements OnInit {
  @Input() defaultDurationHours: number = 1;
  @Output() onDurationHoursChanged = new EventEmitter<number>();

  // Labels
  labelTitle: string;
  labelFormPlaceholder: string;
  labelFormHint: string;
  labelHoursLabel: string;

  // Form Control
  durationFormControl = new FormControl('', Validators.required);

  constructor(private language: LanguageService) {
  }


  ngOnInit(): void {

    // Labels
    this.labelTitle = this.language.getLanguage().CREATE_TASK_STEP1_DURATION_TITLE;
    this.labelFormPlaceholder = this.language.getLanguage().CREATE_TASK_STEP1_DURATION_FORM_PLACEHOLDER;
    this.labelFormHint = this.language.getLanguage().CREATE_TASK_STEP1_DURATION_FORM_HINT;
    this.labelHoursLabel = this.language.getLanguage().GENERIC_TIME_HOURS;

    this.durationFormControl.valueChanges.subscribe(durationChanged => {
      this.onDurationHoursChanged.emit(durationChanged);
    });
  }

}
