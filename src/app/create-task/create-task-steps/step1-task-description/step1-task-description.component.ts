import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskCreationModel} from '../../../../../functions/src/data/task-creation-model';
import {LanguageService} from '../../../core/language/language.service';
import {LocationResult} from '../../../../../functions/src/data/location-result';
import {PlatformService} from '../../../shared/platform';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-create-task-step1-description',
  templateUrl: './step1-task-description.component.html',
  styleUrls: ['./step1-task-description.component.scss']
})
export class Step1TaskDescriptionComponent implements OnInit {
  @Input() creationTaskData: TaskCreationModel;
  @Output() onCreationChanges = new EventEmitter<TaskCreationModel>();
  @Output() onContinueClick = new EventEmitter<boolean>();

  // Labels
  labelHeaderTitle: string;
  labelHeaderSubtitle: string;
  labelContinueButton: string;

  // Platform
  isMobile$: Observable<boolean>;

  constructor(private language: LanguageService, private platform: PlatformService) {
  }

  ngOnInit() {
    // Header labels
    this.labelHeaderTitle = this.language.getLanguage().CREATE_TASK_STEP1_HEADER_TITLE;
    this.labelHeaderSubtitle = this.language.getLanguage().CREATE_TASK_STEP1_HEADER_SUBTITLE;
    this.labelContinueButton = this.language.getLanguage().GENERIC_CONTINUE;

    this.isMobile$ = this.platform.isMobileSize(true);
  }

  onTaskLocationChanged(taskLocation: LocationResult) {
    this.creationTaskData.taskLocation = taskLocation;
    this.onCreationChanges.emit(this.creationTaskData);
  }

  onIsRemoteTaskChanged(isRemoteTask: boolean) {
    this.creationTaskData.isRemoteTask = isRemoteTask;
    this.onCreationChanges.emit(this.creationTaskData);
  }

  onTaskDurationChanged(durationHours: number) {
    this.creationTaskData.estimatedDuration = durationHours;
    this.onCreationChanges.emit(this.creationTaskData);
  }

  onTextDescriptionChanged(taskDescription: string) {
    this.creationTaskData.description = taskDescription;
    this.onCreationChanges.emit(this.creationTaskData);
  }
}
