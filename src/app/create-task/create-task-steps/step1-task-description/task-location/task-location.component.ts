import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LocationResult} from '../../../../../../functions/src/data/location-result';
import {LanguageService} from '../../../../core/language/language.service';
import {MatCheckboxChange} from '@angular/material/typings/esm5/checkbox';

@Component({
  selector: 'app-create-task-location',
  templateUrl: './task-location.component.html',
  styleUrls: ['./task-location.component.scss']
})
export class TaskLocationComponent implements OnInit {
  @Input() defaultIsRemoteTask: boolean = false;
  @Input() defaultLocation: LocationResult;
  @Output() taskLocationChanged = new EventEmitter<LocationResult>();
  @Output() isRemoteTaskChanged = new EventEmitter<boolean>();

  // Label
  labelTitle: string;
  labelRemoteWork: string;

  constructor(private language: LanguageService) {
  }


  ngOnInit(): void {

    // set labels
    this.labelTitle = this.language.getLanguage().CREATE_TASK_STEP1_MIDDLE_TITLE_LOCATION;
    this.labelRemoteWork = this.language.getLanguage().CREATE_TASK_STEP1_LOCATION_REMOTE;

  }

  onCitySelected(locationSelected: LocationResult) {
    this.taskLocationChanged.emit(locationSelected);
  }

  onRemoteWorkChange(isRemoteChecked: MatCheckboxChange) {
    this.defaultIsRemoteTask = isRemoteChecked.checked;
    this.isRemoteTaskChanged.emit(this.defaultIsRemoteTask);
  }

}
