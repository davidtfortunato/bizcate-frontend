import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../../core/language/language.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-task-description',
  templateUrl: './task-description.component.html',
  styleUrls: ['./task-description.component.scss']
})
export class TaskDescriptionComponent implements OnInit {
  @Input() defaultDescription: string;
  @Output() onTextDescriptionChanged = new EventEmitter<string>();

  // Label
  labelTitle: string;
  labelFieldPlaceholder: string;
  labelFieldHint: string;
  labelFieldExplanation: string;

  // Form Control
  descriptionFormControl = new FormControl('', Validators.required);

  constructor(private language: LanguageService) {
  }


  ngOnInit(): void {

    // Set labels
    this.labelTitle = this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION_TITLE;
    this.labelFieldPlaceholder = this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION_PLACEHOLDER
    this.labelFieldHint = this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION_HINT;
    this.labelFieldExplanation = this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION_FIELD_EXPLANATION;

    this.descriptionFormControl.valueChanges.subscribe(durationChanged => {
      this.onTextDescriptionChanged.emit(durationChanged);
    });
  }

}
