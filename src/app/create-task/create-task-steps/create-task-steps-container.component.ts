import {Component, OnInit} from '@angular/core';
import {HeaderActionButton} from '../../shared/components/title-header/title-header.component';
import {LanguageService} from '../../core/language/language.service';
import {Observable} from 'rxjs';
import {CentralUserData} from '../../../../functions/src/data/central-user-data';
import {HeaderStepsData} from '../../shared/root/root.component';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {AppStoreService} from '../../store/app-store.service';
import {select} from '@ngrx/store';
import {getCentralUserData} from '../../core/central-data/store/selectors';
import {TaskCreationModel} from '../../../../functions/src/data/task-creation-model';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';

@Component({
  selector: 'app-create-task-steps',
  templateUrl: './create-task-steps-container.component.html'
})
export class CreateTaskStepsContainerComponent implements OnInit {

  // Data
  centralUserData$: Observable<CentralUserData>;
  localUpdatedCreationTaskData: TaskCreationModel;

  // UI
  headerTitle: { title: string, subtitle: string };
  headerActionButtons: HeaderActionButton[] = [];

  // Steps Data
  headerStepsData: HeaderStepsData;

  constructor(private language: LanguageService,
              private navigationLinks: NavigationLinksService,
              private appStoreService: AppStoreService,
              private centralDataStoreService: CentralUserDataStoreService) {
  }

  ngOnInit(): void {

    // Listening to central user data
    this.centralUserData$ = this.appStoreService.getStore().pipe(select(getCentralUserData));

    this.headerTitle = {
      title: this.language.getLanguage().TASKS_MODULE_TITLE,
      subtitle: this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION
    };

    this.headerStepsData = {
      currentIndex: 0,
      listSteps: [{
        index: 0,
        label: this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION
      }, {
        index: 1,
        label: this.language.getLanguage().CREATE_TASK_STEP2_BROWSE_BIZCATERS
      }, {
        index: 2,
        label: this.language.getLanguage().CREATE_TASK_STEP3_REVIEW_TASK_DETAILS
      }]
    };

    this.headerActionButtons = [
      {
        id: '2',
        label: this.language.getLanguage().GENERIC_BACK,
        styleColor: 'ORANGE',
        state: 'enabled'
      },
      {
        id: '1',
        label: this.language.getLanguage().GENERIC_CONTINUE,
        styleColor: 'GREEN',
        state: 'enabled'
      }];

    // Set first step
    this.updateCurrentStepIndex(0);
  }

  onActionButtonClick(button: HeaderActionButton) {
    if (button.id === '1') {
      // Continue button
      this.doNextStep();
    } else if (button.id === '2') {
      // Back step
      this.doBackStep();
    }
  }

  doNextStep() {
    if (this.getCurrentStepIndex() < (this.headerStepsData.listSteps.length - 1)) {
      // It still can go to the next step
      this.updateCurrentStepIndex(this.getCurrentStepIndex() + 1);
    }
  }

  doBackStep() {
    if (this.getCurrentStepIndex() > 0) {
      // It still can go to the next step
      this.updateCurrentStepIndex(this.getCurrentStepIndex() - 1);
    } else {
      // Get back to inital creation task screen
      this.navigationLinks.openTaskCreate();
    }
  }

  /**
   * Update current step and the respective title
   */
  updateCurrentStepIndex(index: number) {
    this.headerStepsData = {
      ...this.headerStepsData,
      currentIndex: index
    };
    switch (this.getCurrentStepIndex()) {
      case 0:
        this.headerTitle.subtitle = this.language.getLanguage().CREATE_TASK_STEP1_DESCRIPTION;
        break;
      case 1:
        this.headerTitle.subtitle = this.language.getLanguage().CREATE_TASK_STEP2_BROWSE_BIZCATERS;
        break;
      case 2:
        this.headerTitle.subtitle = this.language.getLanguage().CREATE_TASK_STEP3_REVIEW_TASK_DETAILS;
        break;
    }

    this.updateTaskCreationDataOnStore();
  }

  getCurrentStepIndex() {
    return this.headerStepsData.currentIndex;
  }

  /**
   * Update task on the store (and will also update on the API side)
   */
  updateTaskCreationDataOnStore() {
    if (this.localUpdatedCreationTaskData) {
      this.centralDataStoreService.updateCreatingTaskData(this.localUpdatedCreationTaskData);
      this.localUpdatedCreationTaskData = null; // after updated, clean the local changes to avoid update data that wasn't changed
    }
  }

  // Callbacks

  /**
   * Callback when some changes happen on the creation data
   * @param creationTaskData
   */
  onTaskCreationChanged(creationTaskData: TaskCreationModel) {
    this.localUpdatedCreationTaskData = creationTaskData;
  }
}
