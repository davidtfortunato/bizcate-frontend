import {Component, OnInit} from '@angular/core';
import {HeaderActionButton} from '../shared/components/title-header/title-header.component';
import {LanguageService} from '../core/language/language.service';
import {Observable} from 'rxjs';
import {CentralUserData} from '../../../functions/src/data/central-user-data';
import {CentralUserDataStoreService} from '../core/central-data/store/central-user-data-store.service';
import {select} from '@ngrx/store';
import {getCentralUserData} from '../core/central-data/store/selectors';

@Component({
  selector: 'app-create-task-container',
  templateUrl: './create-task-container.component.html'
})
export class CreateTaskContainerComponent implements OnInit {

  // Data
  centralUserData$: Observable<CentralUserData>;

  // UI
  headerTitle: { title: string, subtitle: string };
  headerActionButtons: HeaderActionButton[] = [];

  constructor(private language: LanguageService, private centralStoreService: CentralUserDataStoreService) {
  }

  ngOnInit(): void {
    this.centralUserData$ = this.centralStoreService.getStore().pipe(select(getCentralUserData));

    this.headerTitle = {
      title: this.language.getLanguage().TASKS_MODULE_TITLE,
      subtitle: this.language.getLanguage().TASK_MODULE_CREATE_SUBTITLE
    };

  }

}
