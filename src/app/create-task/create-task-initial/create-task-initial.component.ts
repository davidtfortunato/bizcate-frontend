import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {CentralUserData} from '../../../../functions/src/data/central-user-data';
import {LanguageService} from '../../core/language/language.service';
import {AppAssets} from '../../shared/app.assets';
import {ServiceSkill} from '../../../../functions/src/data';
import {FormControl, Validators} from '@angular/forms';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';
import {TaskCreationModel} from '../../../../functions/src/data/task-creation-model';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';

@Component({
  selector: 'app-create-task-initial',
  templateUrl: './create-task-initial.component.html',
  styleUrls: ['./create-task-initial.component.scss']
})
export class CreateTaskInitialComponent implements OnInit, OnChanges {
  @Input() centralUserData: CentralUserData;
  @Output() onIsValidToContinue = new EventEmitter<boolean>();

  // Labels
  labelTitle: string;
  labelSubtitle: string;
  labelInputTaskTitle: string;
  labelContinueButton: string;

  // Form data
  formTaskCreationData: TaskCreationModel;
  titleFormControl: FormControl = new FormControl('', Validators.required);
  isValidToContinue = false;
  serviceSkillSelected: ServiceSkill;
  inputTitleTask: string;

  // Icons
  icCreateTask = AppAssets.ic_create_task_initial;

  constructor(private language: LanguageService,
              private storeCentralDataService: CentralUserDataStoreService,
              private linksNavigation: NavigationLinksService) {
  }

  ngOnInit(): void {

    // Labels
    this.labelTitle = this.language.getLanguage().CREATE_TASK_INITIAL_TITLE;
    this.labelSubtitle = this.language.getLanguage().CREATE_TASK_INITIAL_SUBTITLE;
    this.labelInputTaskTitle = this.language.getLanguage().CREATE_TASK_WHAT_YOU_NEED;
    this.labelContinueButton = this.language.getLanguage().CREATE_TASK_START_CREATE_BTN;

    this.titleFormControl.valueChanges.subscribe(titleChanged => {
      this.onTitleChanged(titleChanged);
    });

  }

  ngOnChanges() {
    if (this.centralUserData && this.centralUserData.taskCreationPending) {
      this.formTaskCreationData = this.centralUserData.taskCreationPending;
      this.inputTitleTask = this.formTaskCreationData.title;
      this.serviceSkillSelected = this.formTaskCreationData.professionalService;
      this.updateIsValidToContinue();
    }
  }

  getDefaultServiceSelection() {
    return this.centralUserData && this.centralUserData.taskCreationPending
      ? this.centralUserData.taskCreationPending.professionalService : null;
  }

  /**
   * Get the central user data
   */
  getPendingTaskTitle() {
    return this.centralUserData && this.centralUserData.taskCreationPending ? this.centralUserData.taskCreationPending.title : '';
  }

  onServiceSkillChanged(skillService: ServiceSkill) {
    this.serviceSkillSelected = skillService;
    this.updateIsValidToContinue();
  }

  onTitleChanged(title: string) {
    this.inputTitleTask = title;
    this.updateIsValidToContinue();
  }

  /**
   * Update the state of continue button
   */
  updateIsValidToContinue() {
    const hasSkillSelected = this.serviceSkillSelected
      && this.serviceSkillSelected.category
      && this.serviceSkillSelected.serviceName
      && this.serviceSkillSelected.professionalExperience;

    this.isValidToContinue = hasSkillSelected && this.inputTitleTask && this.inputTitleTask.length > 5;

    this.onIsValidToContinue.emit(this.isValidToContinue);
  }

  onContinueClick() {
    if (this.isValidToContinue) {
      // Update Creation Data
      this.formTaskCreationData = {
        ...this.centralUserData.taskCreationPending,
        uid: this.centralUserData.uid,
        title: this.inputTitleTask,
        professionalService: this.serviceSkillSelected,
        lastUpdate: new Date().getTime()
      };

      this.storeCentralDataService.updateCreatingTaskData(this.formTaskCreationData);

      // Navigate to the next step on creating a task
      this.linksNavigation.openCreateTaskStepsScreen();
    }
  }
}
