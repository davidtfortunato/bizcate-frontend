import {Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';

import * as fromAppRoot from '../../store/app.reducer';
import {getRootIsDrawerOpened} from '../../store/app.selectors';
import {PlatformService} from '../platform';
import {HeaderActionButton} from '../components/title-header/title-header.component';
import {UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';
import {
  getUserLoggedInState,
  getUserProfessionalProfile,
  getUserProfile,
  getUserRegisterState
} from '../../auth/store/selectors';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {UserLoggedinState, UserRegisterState} from '../../../../functions/src/data/user-model';
import {AppStoreService} from '../../store/app-store.service';
import {StepperItem} from '../components/stepper-horizontal/stepper-horizontal.component';

export interface HeaderStepsData {
  currentIndex: number;
  listSteps: StepperItem[];
}

@Component({
  selector: 'app-root-container',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit, OnChanges, OnDestroy {
  @Input() headerTitle: { title?: string, subtitle?: string };
  @Input() actionButtons: HeaderActionButton[];
  @Input() requireUserAuthenticated = true; // By default all the components require the user to be authenticated
  @Input() headerWithSteps: HeaderStepsData;
  @Output() actionButtonClick = new EventEmitter<HeaderActionButton>();
  isDrawerOpened$: Observable<boolean>;
  isMobile = false;

  // Observables
  userProfile$: Observable<UserProfileModel>;
  userProfessionalProfile$: Observable<UserProfessionalProfile>;
  userRegisterState$: Observable<UserRegisterState>;
  userLoggedinState$: Observable<UserLoggedinState>;

  // Subscriptions
  subUserAuthenticated: Subscription;


  @ViewChild('bottombtnscontainer', {static: false}) bottomButtonsContainer: ElementRef;

  constructor(private store: Store<fromAppRoot.State>,
              private platformSercice: PlatformService,
              private navigation: NavigationLinksService,
              private appStoreService: AppStoreService) {
  }

  ngOnInit() {
    this.isMobile = this.platformSercice.isMobileSize();
    this.platformSercice.deviceChanged.subscribe(deviceChanged => {
      this.isMobile = this.platformSercice.isMobileSize();
    });

    this.isDrawerOpened$ = this.store.pipe(select(getRootIsDrawerOpened));
    this.userProfile$ = this.store.pipe(select(getUserProfile));
    this.userProfessionalProfile$ = this.store.pipe(select(getUserProfessionalProfile));
  }

  ngOnChanges() {
    console.log('requireUserAuthenticated: ' + this.requireUserAuthenticated);

    if (this.requireUserAuthenticated) {
      // If this component requires the user to be authenticated, then should check the auth state here
      this.userLoggedinState$ = this.store.pipe(select(getUserLoggedInState));
      this.userRegisterState$ = this.store.pipe(select(getUserRegisterState));
      this.subUserAuthenticated = this.userRegisterState$.subscribe(userRegisterState => {

        if (userRegisterState === UserRegisterState.LOADING_USER_DATA || !userRegisterState) {
          this.appStoreService.displayProgressBarById('loadingUserData');
        } else {
          this.appStoreService.hideProgressBar('loadingUserData');
          if (userRegisterState === UserRegisterState.NOT_LOGGEDIN) {
            // It's not authenticated or validated. Will redirect to the login page
            this.navigation.openLogin();
          }
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.subUserAuthenticated) {
      this.subUserAuthenticated.unsubscribe();
      this.subUserAuthenticated = null;
    }
  }

  onActionButtonClick(actionButton: HeaderActionButton) {
    this.actionButtonClick.emit(actionButton);
  }

  getBottomPadding() {
    if (this.bottomButtonsContainer) {
      return this.bottomButtonsContainer.nativeElement.offsetHeight;
    } else {
      return 0;
    }
  }

  /**
   * Check if should display the header steps instead of the toolbar
   */
  hasHeaderWithSteps() {
    // Display header with steps if received in the input
    return this.headerWithSteps && this.headerWithSteps.listSteps.length > 0;
  }

}
