import { NgModule } from '@angular/core';
import { PortalModule } from '@angular/cdk/portal';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatGridListModule,
  MatMenuModule,
  MatInputModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatTooltipModule,
  MatFormFieldModule, MatIconModule, MatProgressBarModule,
  // MatBottomSheetModule
} from '@angular/material';

// import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatGridListModule,
    MatMenuModule,
    MatInputModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatTooltipModule,
    PortalModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressBarModule
    // MatMomentDateModule,
    // MatBottomSheetModule
  ]
})
export class MaterialModule {}
