import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {RootComponent} from './root/root.component';
import {ActionButtonComponent} from './components/action-button/action-button.component';
import {SnackBarComponent} from './components/snack-bar/snack-bar.component';
import {EmptyListComponent} from './components/empty-list/empty-list.component';
import {AvatarPhotoUploadComponent} from './components/avatar-photo-upload/avatar-photo-upload.component';
import {AvatarPhotoComponent} from './components/avatar-photo/avatar-photo.component';
import {SidenavBarComponent} from './components/sidenav-bar/sidenav-bar.component';
import {SidenavOptionComponent} from './components/sidenav-bar/sidenav-option/sidenav-option.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {TitleHeaderComponent} from './components/title-header/title-header.component';
import {ContainerCardComponent} from './components/container-card/container-card.component';
import {StarsLevelComponent} from './components/stars-level/stars-level.component';
import {UploadPhotoComponent} from './components/upload-photo/upload-photo.component';
import {ContainerTitleComponent} from './components/container-title/container-title.component';
import {MiddleTitleComponent} from './components/middle-title/middle-title.component';
import {UserReviewComponent} from './components/user-review/user-review.component';
import {SkillItemComponent} from './components/skill-item/skill-item.component';
import {PortfolioItemComponent} from './components/portfolio-item/portfolio-item.component';
import {AddSkillComponent} from './components/add-skill/add-skill.component';
import {StoragePhotoItemComponent} from './components/storage-photo-item/storage-photo-item.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {CheckableItemComponent} from './components/checkable-item/checkable-item.component';
import {ShareContainerComponent} from './components/share-container/share-container.component';
import {ShareItemComponent} from './components/share-container/share-item/share-item.component';
import {DialogPhotoComponent} from './components/storage-photo-item/dialog-photo/dialog-photo.component';
import {BorderContainerComponent} from './components/border-container/border-container.component';
import {LineIconTextComponent} from './components/line-icon-text/line-icon-text.component';
import {CalendarEventsComponent} from './components/calendar-events/calendar-events.component';
import {CalendarHeaderComponent} from './components/calendar-events/header/calendar-header.component';
import {FilterButtonToggleComponent} from './components/filters/filter-button-toggle/filter-button-toggle.component';
import {FilterSliderComponent} from './components/filters/filter-slider/filter-slider.component';
import {SwitchContainerComponent} from './components/switch/switch-container.component';
import {SidenavOptionSelectableComponent} from './components/sidenav-bar/sidenav-option/sidenav-option-selectable/sidenav-option-selectable.component';
import {UserAvatarComponent} from './components/user-avatar/user-avatar.component';
import {CityFormComponent} from './components/city-form/city-form.component';
import {MatStepperModule} from '@angular/material';
import {StepperHorizontalComponent} from './components/stepper-horizontal/stepper-horizontal.component';

export const COMPONENTS = [
  ToolbarComponent, RootComponent, ActionButtonComponent, SnackBarComponent, EmptyListComponent, AvatarPhotoUploadComponent,
  AvatarPhotoComponent,
  SidenavBarComponent, SidenavOptionComponent, ProgressBarComponent, TitleHeaderComponent, ContainerCardComponent, StarsLevelComponent,
  UploadPhotoComponent, ContainerTitleComponent, MiddleTitleComponent, UserReviewComponent, SkillItemComponent, PortfolioItemComponent,
  AddSkillComponent, StoragePhotoItemComponent, NotFoundComponent, CheckableItemComponent, ShareContainerComponent, ShareItemComponent,
  DialogPhotoComponent, BorderContainerComponent, LineIconTextComponent, CalendarEventsComponent,
  CalendarHeaderComponent, FilterButtonToggleComponent, FilterSliderComponent, SwitchContainerComponent, SidenavOptionSelectableComponent,
  UserAvatarComponent, CityFormComponent, StepperHorizontalComponent
];
