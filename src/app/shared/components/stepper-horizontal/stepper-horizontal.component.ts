import {Component, Input, OnInit} from '@angular/core';

export interface StepperItem {
  index: number;
  label: string;
}

@Component({
  selector: 'app-shared-stepper-horizontal',
  templateUrl: './stepper-horizontal.component.html',
  styleUrls: ['./stepper-horizontal.component.scss']
})
export class StepperHorizontalComponent implements OnInit {
  @Input() listStepItems: StepperItem[] = [];
  @Input() currentStep = 1;


  ngOnInit(): void {
  }
}
