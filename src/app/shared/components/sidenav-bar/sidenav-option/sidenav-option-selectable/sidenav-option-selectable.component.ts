import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {SidenavOptionData} from '../sidenav-option.component';
import {logger} from 'codelyzer/util/logger';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidenav-option-selectable',
  templateUrl: './sidenav-option-selectable.component.html',
  styleUrls: ['./sidenav-option-selectable.component.scss']
})
export class SidenavOptionSelectableComponent implements OnChanges {
  @Input() optionData: SidenavOptionData;

  @Output() click = new EventEmitter<SidenavOptionData>();

  // UI
  isSelected = false;

  constructor(private router: Router) {
  }

  ngOnChanges() {
    this.isSelected = this.isSamePath();
  }

  getOptionClass() {
    return {
      'isSelected': this.isSelected
    };
  }

  optionClick() {
    this.click.emit(this.optionData);
  }

  /**
   * Is the same path
   */
  private isSamePath(): boolean {
    if (this.optionData && this.optionData.isSelectable) {
      return this.router.url === this.getPathString();
    } else {
      logger.debug('Doesn\'t have the current params or the optionData');
      return false;
    }
  }

  /**
   * Get the final path in a string
   */
  private getPathString() {
    let path = '';
    this.optionData.path.forEach(value => {
      path += '/' + value;
    });
    return path;
  }


}
