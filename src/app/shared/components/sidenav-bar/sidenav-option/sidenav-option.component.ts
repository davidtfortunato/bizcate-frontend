import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {logger} from 'codelyzer/util/logger';
import {Router, UrlSegment} from '@angular/router';
import {NavigationLinksService} from '../../../../core/navigation/navigation-links.service';

export interface SidenavOptionData {
  id: string;
  label: string;
  path: string[];
  isSelectable: boolean;
  invisible?: boolean;
}

@Component({
  selector: 'app-sidenav-option',
  templateUrl: './sidenav-option.component.html',
  styleUrls: ['./sidenav-option.component.scss']
})
export class SidenavOptionComponent implements OnChanges {
  @Input() optionData: SidenavOptionData;


  constructor(private router: Router, private linkNavigation: NavigationLinksService) {
  }

  ngOnChanges() {
  }

  optionClick(sidenavOptionData: SidenavOptionData) {
    this.linkNavigation.openLink(sidenavOptionData.path);
  }
}
