import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {AppAssets} from '../../app.assets';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {SidenavOptionData} from './sidenav-option/sidenav-option.component';
import {LanguageService} from '../../../core/language/language.service';
import {UserProfessionalProfile, UserProfileModel} from '../../../../../functions/src/data';
import {UserLoggedinState, UserProfileRoleType, UserRegisterState} from '../../../../../functions/src/data/user-model';
import {AppUIParams} from '../../app.ui-params';
import {AuthStoreService} from '../../../auth/store/auth-store.service';

@Component({
  selector: 'app-sidenav-bar',
  templateUrl: './sidenav-bar.component.html',
  styleUrls: ['./sidenav-bar.component.scss']
})
export class SidenavBarComponent implements OnInit, OnChanges {

  @Input() userProfile: UserProfileModel;
  @Input() userLoggedinState: UserLoggedinState;
  @Input() userRegisterState: UserRegisterState;
  @Input() professionalProfile: UserProfessionalProfile;

  constructor(private linkNavigation: NavigationLinksService,
              private language: LanguageService,
              private authService: AuthStoreService) {
  }

  // Add Task Data
  addTaskLabel: string;

  // Login Button
  loginBtnLabel: string;

  // Logout Button
  logoutBtnLabel: string;
  logoutBtnColor: string;

  // Types
  userRegisterStateTypes = UserRegisterState;
  userLoggedinStateTypes = UserLoggedinState;

  // Data
  appLogoUrl = AppAssets.logo_white;
  userListOptions: SidenavOptionData[] = [
    {
      id: '0',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_HOME,
      path: this.linkNavigation.getInitialScreenPath(),
      isSelectable: true
    },
    {
      id: '1',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_PROFILE,
      path: this.linkNavigation.getEditProfile(),
      isSelectable: true
    },
    {
      id: '3',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_MESSAGES,
      path: this.linkNavigation.getMessagesPath(),
      isSelectable: true
    },
    {
      id: '4',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_BROWSE_TASKS,
      path: this.linkNavigation.getBrowseTasks(),
      isSelectable: true
    },
    {
      id: '5',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_MY_TASKS,
      path: this.linkNavigation.getMyTasksList(),
      isSelectable: true
    }
  ];

  // Professional List options
  professionalListOptions: SidenavOptionData[] = [
    {
      id: '2',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_PROFESSIONAL_PROFILE,
      path: this.linkNavigation.getEditProfessionalProfile(),
      isSelectable: true
    },
    {
      id: '6',
      label: this.language.getLanguage().SIDENAV_BAR_OPTION_MY_PROFESSIONAL_TASKS,
      path: this.linkNavigation.getMyProfessionalTasksList(),
      isSelectable: true
    }];

  ngOnInit() {
    this.addTaskLabel = this.language.getLanguage().GENERIC_ADD_TASK_BUTTON;

    this.loginBtnLabel = this.language.getLanguage().GENERIC_LOGIN;

    this.logoutBtnLabel = this.language.getLanguage().GENERIC_LOGOUT;
    this.logoutBtnColor = AppUIParams.actionButtonRedColor;
  }

  ngOnChanges() {
    console.log('Register state: ' + this.userRegisterState);
  }


  onLogoClick() {
    this.linkNavigation.logoClick();
  }

  onAddTaskClick() {
    this.linkNavigation.openTaskCreate();
  }

  onLoginClick() {
    this.linkNavigation.openLogin();
  }

  onLogoutClick() {
    this.authService.logout();
  }

  isProfessionalProfile() {
    return this.userProfile && this.professionalProfile && this.userProfile.role === UserProfileRoleType.CLIENT_PROFESSIONAL;
  }
}
