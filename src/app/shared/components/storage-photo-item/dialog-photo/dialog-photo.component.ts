import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-dialog-photo',
  templateUrl: './dialog-photo.component.html',
  styleUrls: ['./dialog-photo.component.scss']
})
export class DialogPhotoComponent implements OnInit {
  @Input() photoUrl: string;
  @Output() closeDialog = new EventEmitter();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {
    if (!this.photoUrl && this.data) {
      this.photoUrl = this.data.photoUrl;
    }
    console.log('PhotoUrl: ' + this.photoUrl);
  }

  onCloseDialog() {
    this.closeDialog.emit();
  }
}
