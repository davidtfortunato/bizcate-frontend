import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StorageFileModel} from '../../../../../functions/src/data/storage-model';
import {StorageApiService} from '../../../core/api/storage-api.service';
import {AddPortfolioComponent} from '../../../professional/edit-professional-profile/portfolio-form/add-portfolio/add-portfolio.component';
import {MatDialog} from '@angular/material';
import {PlatformService} from '../../platform';
import {DialogPhotoComponent} from './dialog-photo/dialog-photo.component';

@Component({
  selector: 'app-storage-photo-item',
  templateUrl: './storage-photo-item.component.html',
  styleUrls: ['./storage-photo-item.component.scss']
})
export class StoragePhotoItemComponent implements OnInit {
  @Input() file: StorageFileModel;
  @Input() height = 200;
  @Input() width = 200;
  @Input() hasRemoveButton = false;
  @Output() removeClick = new EventEmitter<StorageFileModel>();

  constructor(private storageAPI: StorageApiService, private dialog: MatDialog, private platform: PlatformService) {
  }

  ngOnInit() {
  }

  onFileRemove() {
    this.removeClick.emit(this.file);
    this.storageAPI.removeFile(this.file.filePath);
  }

  onOpenFullscreen() {
    const dialogRef = this.dialog.open(DialogPhotoComponent, {
      width: this.platform.isMobileSize() ? '95%' : '60%',
      data: {
        photoUrl: this.file.fileUrl
      }
    });

    const closeSub = dialogRef.componentInstance.closeDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    const closeDialog = dialogRef.afterClosed().subscribe(result => {
      closeSub.unsubscribe();
      closeDialog.unsubscribe();
    });
  }

}
