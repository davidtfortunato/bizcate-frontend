import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppAssets} from '../../app.assets';

@Component({
  selector: 'app-avatar-photo',
  templateUrl: './avatar-photo.component.html',
  styleUrls: ['./avatar-photo.component.scss']
})
export class AvatarPhotoComponent implements OnInit {
  @Input() photoURL: string;
  @Output() click = new EventEmitter();

  defaultAvatarUrl: string;

  ngOnInit() {
    this.defaultAvatarUrl = AppAssets.ic_avatar;
  }

  onClick() {
    this.click.emit();
  }
}
