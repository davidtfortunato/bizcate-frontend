import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';
import {LanguageService} from '../../../core/language/language.service';
import {StorageApiService} from '../../../core/api/storage-api.service';
import {AppStoreService} from '../../../store/app-store.service';
import {SnackBarService} from '../snack-bar/snack-bar.service';
import {StorageFileModel} from '../../../../../functions/src/data/storage-model';
import {logger} from 'codelyzer/util/logger';

@Component({
  selector: 'app-avatar-photo-upload',
  templateUrl: './avatar-photo-upload.component.html',
  styleUrls: ['./avatar-photo-upload.component.scss']
})
export class AvatarPhotoUploadComponent implements OnInit {
  @Input() defaultPhotoUrl: string;
  @Input() uploadText: string;
  @Input() fileName: string;
  @Output() uploadedPhotoUrl = new EventEmitter();

  // UI
  actionButtonBgColor = AppUIParams.actionButtonBlueColor;

  constructor(private language: LanguageService,
              private firebaseUtils: StorageApiService,
              private appStoreService: AppStoreService,
              private snackbarService: SnackBarService) {
  }

  ngOnInit() {
    this.uploadText = this.language.getLanguage().GENERIC_UPLOAD_PHOTO;
  }

  uploadFile(file) {
    if (this.fileName && file) {
      if (file && file.length > 0) {
        this.firebaseUtils
          .uploadUserFile(file[0],
            this.fileName,
            (storageFileModel: StorageFileModel) => {
              this.appStoreService.hideProgressBar();
              this.uploadedPhotoUrl.emit(storageFileModel.fileUrl);
            },
            (error: any) => {
              // upload failed
              this.appStoreService.hideProgressBar();
              this.snackbarService.displayMessage(this.language.getLanguage().ERROR_GENERIC_UPLOAD_PHOTO, 'error');
            },
            (percentage: number) => {
              logger.debug('AvatarPhotoUploadComponent', 'Upload Progress: ' + percentage);
              this.appStoreService.displayProgressBar('determine', percentage);
            }
          );
      }
    } else {
      logger.error('AvatarPhotoUploadComponent', 'File or filename is not valid');
    }
  }
}
