import {Component, Input, OnInit} from '@angular/core';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {LanguageService} from '../../../core/language/language.service';
import {AppUIParams} from '../../app.ui-params';

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {
  @Input() avatarUrl: string;
  @Input() userName: string;
  @Input() uid: string;
  @Input() isProfessionalProfile = false;

  // Professional Label
  proLabelText: string;
  proLabelTextColor: string;
  proLabelBgColor: string;

  constructor(private navigationLink: NavigationLinksService, private languages: LanguageService) {
  }

  ngOnInit() {
    this.proLabelText = this.languages.getLanguage().GENERIC_PRO_LABEL;
    // this.proLabelTextColor = AppUIParams.actionButtonRedColor;
    // this.proLabelBgColor = AppUIParams.actionButtonRedColorOpacity;
    this.proLabelTextColor = AppUIParams.actionButtonWhiteColor;
    this.proLabelBgColor = AppUIParams.actionButtonRedColor;
  }


  onProfileClick() {
    if (this.isProfessionalProfile) {
      this.navigationLink.openProfessionalProfile(this.uid);
    } else {
      this.navigationLink.openUserProfile(this.uid);
    }
  }
}
