import {Component, OnInit} from '@angular/core';

import {AppStoreService} from '../../../store/app-store.service';
import {PlatformService} from '../../platform';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../../store';
import {Observable} from 'rxjs';
import {ProgressBarInterface} from '../progress-bar/progress-bar.component';
import {getProgressBarData} from '../../../store/app.selectors';
import {AppUIParams} from '../../app.ui-params';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {AppAssets} from '../../app.assets';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  // Data
  progressBarDisplay$: Observable<ProgressBarInterface>;

  // UI
  hasMenuButton = false;

  // UI Params
  paramsAddButtonBackground = AppUIParams.actionButtonBlueColor;

  // Assets
  appAssets = AppAssets;

  constructor(private appStoreService: AppStoreService,
              private platformService: PlatformService,
              private store: State<fromStore.State>,
              private navigationService: NavigationLinksService) {
  }

  ngOnInit() {
    this.progressBarDisplay$ = this.store.pipe(select(getProgressBarData));
    this.hasMenuButton = this.platformService.isMobileSize();
    this.platformService.deviceChanged.subscribe(deviceChange => {
      this.hasMenuButton = this.platformService.isMobileSize();
    });
  }

  onMenuToggle() {
    this.appStoreService.toggleDrawerOpening();
  }

  onAddTaskClick() {
    this.navigationService.openTaskCreate();
  }
}
