import {Component, Input, OnInit} from '@angular/core';

export interface ProgressBarInterface {
  id?: string;
  display: boolean;
  percentage: number;
  type: 'determine' | 'indetermine';
  blocking?: boolean; // if the loading should block other actions
}

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() progressBarData: ProgressBarInterface;

  spinnerConfigs = {
    text: 'Loading...',
    spinnerOptions: {
      color: '#0078D2'
    }
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}
