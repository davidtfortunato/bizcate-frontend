import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {AppAssets} from '../../app.assets';

@Component({
  selector: 'app-share-container',
  templateUrl: './share-container.component.html',
  styleUrls: ['./share-container.component.scss']
})
export class ShareContainerComponent implements OnInit {
  @Input() title: string;
  @Input() link: string;

  // Label
  labelShareTitle: string;

  // UI
  icFacebookUrl: string;
  icTwitterUrl: string;
  icLinkedinUrl: string;

  constructor(private languages: LanguageService) {
  }


  ngOnInit() {
    this.labelShareTitle = this.languages.getLanguage().GENERIC_SHARE_TITLE;

    this.icFacebookUrl = AppAssets.ic_facebook;
    this.icTwitterUrl = AppAssets.ic_twitter;
    this.icLinkedinUrl = AppAssets.ic_linkedin;
  }

  generateFacebookUrl() {
    return 'https://www.facebook.com/sharer/sharer.php?u=' + this.link;
  }

  generateTwitterUrl() {
    return `http://twitter.com/share?text=${this.title}&url=${this.link}`;
  }

  generateLinkedinUrl() {
    return `https://www.linkedin.com/shareArticle?mini=true&url=${this.link}&title=${this.title}&summary=${this.title}&source=Bizcate.com`;
  }
}
