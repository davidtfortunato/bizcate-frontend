import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-share-item',
  templateUrl: './share-item.component.html',
  styleUrls: ['./share-item.component.scss']
})
export class ShareItemComponent implements OnInit{
  @Input() url: string;
  @Input() iconUrl: string;
  @Input() title: string;

  constructor() {

  }


  ngOnInit() {

  }

  onClick() {
    window.open(this.url, '_blank');
  }
}
