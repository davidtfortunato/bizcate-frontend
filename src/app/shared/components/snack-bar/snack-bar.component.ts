import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {

  @Input() message: string;
  @Input() messageType: 'SUCCESS' | 'WARNING' | 'ERROR' = 'SUCCESS';

  ngOnInit(): void {
  }

  getColor() {
    switch (this.messageType) {
      case 'ERROR': return '#FF9494';

      case 'WARNING': return '#EED202';

      default:
      case 'SUCCESS': return '#4BB543';

    }
  }

}
