import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {SnackBarComponent} from './snack-bar.component';

@Injectable()
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {}

  displayMessage(message: string, messageType: 'success' | 'warning' | 'error' = 'error') {
    this.snackBar.open(message, null, {
      panelClass: [`snack-bar-${messageType}`],
      duration: 2000,
      horizontalPosition: 'center'
    });
  }

}
