import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';
import {
  CategorySkill, getCategorySkills,
  getLabelCategory,
  getSkillCategories, getSkillLabel,
  getSkillLevels,
  ServiceSkill,
  ServiceModel, ProfessionalServiceAvailable
} from '../../../../../functions/src/data';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';
import {SnackBarService} from '../snack-bar/snack-bar.service';

@Component({
  selector: 'app-add-skill-form',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.scss']
})
export class AddSkillComponent implements OnInit, OnChanges {

  @Input() listSkills: ProfessionalServiceAvailable[];
  @Input() withLevel = true;
  @Input() withAddButton = true;
  @Input() defaultSelection: ServiceSkill = null;
  @Output() skillToAdd = new EventEmitter<ServiceSkill>();
  @Output() serviceSkillChanged = new EventEmitter<ServiceSkill>();

  // Data
  categories: CategorySkill[];
  categorySkills: ServiceModel[];
  skillLevels: string[];

  // Placeholders
  labelCategories: string;
  labelSkills: string;
  labelLevels: string;

  // Form controllers
  form: FormGroup;
  categoryFormControl: FormControl;
  categorySkillsFormControl: FormControl;
  skillLevelsFormControl: FormControl;

  // UI
  categoryIconColor: string;

  constructor(@Inject(LOCALE_ID) protected localeId: string,
              private formBuilder: FormBuilder,
              private language: LanguageService,
              private snackBar: SnackBarService) {
  }

  ngOnInit() {
    // Initialize form
    this.initForm();

    // Init Labels
    this.labelCategories = this.language.getLanguage().PROFESSIONAL_PROFILE_LABEL_CATEGORY;
    this.labelSkills = this.language.getLanguage().PROFESSIONAL_PROFILE_LABEL_SKILL;
    this.labelLevels = this.language.getLanguage().PROFESSIONAL_PROFILE_LABEL_SKILL_LEVEL;

    // Init Data
    this.categories = getSkillCategories();
    this.categorySkills = [];
    this.skillLevels = getSkillLevels();

    this.categoryIconColor = AppUIParams.actionButtonOrangeBackgroundColor;
  }

  ngOnChanges(changes: SimpleChanges) {
    // Check if should select by default
    if (changes && this.defaultSelection && changes.defaultSelection) {
      this.setDefaultSelection(this.defaultSelection);
    }
  }

  /**
   * Initialize Form to add a new serviceName
   */
  private initForm() {

    // Init Form control
    this.categoryFormControl = new FormControl('', Validators.required);
    this.categorySkillsFormControl = new FormControl('', Validators.required);
    this.skillLevelsFormControl = this.withLevel ? new FormControl('', Validators.required) : null;

    // Init Form
    this.form = this.formBuilder.group({
      category: this.categoryFormControl,
      skill: this.categorySkillsFormControl,
      level: this.skillLevelsFormControl
    });
    this.form.valueChanges.subscribe(val => {
      this.onFormChanges(val);
    });

    this.categoryFormControl.valueChanges.subscribe(value => {
      // Reset serviceName selection when the category change
      this.categorySkillsFormControl.reset();
      this.skillLevelsFormControl.reset();
    });

    // Select default selection values (from cache)
    if (this.defaultSelection) {
      this.setDefaultSelection(this.defaultSelection);
    }
  }

  /**
   * Set the default selection skills
   * @param defaultSelection
   */
  setDefaultSelection(defaultSelection: ServiceSkill) {
    if (defaultSelection) {
      // Select the default skill service
      this.categoryFormControl.setValue(defaultSelection.category);

      // Update skills to be selectable
      this.categorySkills = getCategorySkills(defaultSelection.category.id);
      setTimeout(() => {
        // Wait for the options being displayed on list skills
        this.categorySkillsFormControl.setValue(defaultSelection.serviceName, {emitEvent: true, emitViewToModelChange: true});

        this.skillLevelsFormControl.setValue(defaultSelection.professionalExperience, {emitEvent: true, emitViewToModelChange: true});
      }, 500);
    }
  }

  /**
   * Verify if the serviceName should be enabled
   */
  isSkillEnabled(skill: ServiceModel) {
    // Check if the serviceName was already selected
    if (this.listSkills) {
      for (const selectedSkill of this.listSkills) {
        if (selectedSkill.service.serviceName.id === skill.id && selectedSkill.service.serviceName.categoryId === skill.categoryId) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Get Label of category
   */
  getCategoryLabel(category: CategorySkill) {
    if (category) {
      return getLabelCategory(category.id, this.localeId);
    } else {
      return '';
    }
  }

  /**
   * Get Label of a serviceName
   */
  getSkillLabel(skill: ServiceModel) {
    if (skill) {
      return getSkillLabel(skill.id, this.localeId);
    } else {
      return '';
    }
  }

  onFormChanges(changes) {
    // Update Skills list
    if (changes.category) {
      this.categorySkills = getCategorySkills(changes.category.id);
    }

    this.serviceSkillChanged.emit({
      category: this.categoryFormControl.value,
      serviceName: this.categorySkillsFormControl.value,
      professionalExperience: this.skillLevelsFormControl ? this.skillLevelsFormControl.value : '1'
    });
  }

  onAddClick() {
    console.log('onAddClick');
    // Add Skill
    if (this.form.valid) {
      this.skillToAdd.emit({
        category: this.categoryFormControl.value,
        serviceName: this.categorySkillsFormControl.value,
        professionalExperience: this.skillLevelsFormControl ? this.skillLevelsFormControl.value : '1'
      });
      this.form.reset();
    } else {
      // Alert the form is not valid
      this.snackBar.displayMessage(this.language.getLanguage().ERROR_FORM_INVALID);
      this.form.markAsTouched();
    }
  }
}
