import {Component, Input, OnChanges} from '@angular/core';
import {UserRegisterState} from '../../../../../functions/src/data/user-model';

@Component({
  selector: 'app-container-card',
  templateUrl: './container-card.component.html',
  styleUrls: ['./container-card.component.scss']
})
export class ContainerCardComponent implements OnChanges {
  @Input() userStateType: UserRegisterState;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() titleAlign: 'LEFT' | 'CENTER' | 'RIGHT' = 'LEFT';

  // Class
  containerClass: Object;

  constructor() {}

  ngOnChanges() {
    this.containerClass = {
      'toLeft': this.titleAlign === 'LEFT',
      'toCenter': this.titleAlign === 'CENTER',
      'toRight': this.titleAlign === 'RIGHT'
    };
  }
}
