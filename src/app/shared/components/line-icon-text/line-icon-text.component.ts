import {Component, Input} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';

@Component({
  selector: 'app-line-icon-text',
  templateUrl: './line-icon-text.component.html',
  styleUrls: ['./line-icon-text.component.scss']
})
export class LineIconTextComponent {
  @Input() text;
  @Input() textColor = '#000000';
  @Input() icon;
  @Input() iconColor = AppUIParams.iconColor;
}
