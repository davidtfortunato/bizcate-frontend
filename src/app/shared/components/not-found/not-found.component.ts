import {Component, Input, OnInit} from '@angular/core';
import {AppAssets} from '../../app.assets';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-notfound-container',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  @Input() message: string;
  @Input() iconUrl: string = AppAssets.ic_not_found;

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    if (!this.message) {
      this.message = this.languages.getLanguage().ERROR_GENERIC_NOT_FOUND;
    }
  }

}
