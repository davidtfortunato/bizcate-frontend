import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {CalendarEvent} from 'angular-calendar';
import {ScheduleAvailabilityPeriod} from '../../../../../functions/src/data/calendar-model';
import {LanguageService} from '../../../core/language/language.service';
import {CalendarHelperService} from './calendar-helper.service';

export interface AppCalendarPeriod {
  period: ScheduleAvailabilityPeriod;
  data?: any;
  isSelected: boolean;
}

export enum ViewType {
  MONTH = 'month',
  WEEK = 'week',
  DAY = 'day'
}

@Component({
  selector: 'app-calendar-events',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar-events.component.html',
  styleUrls: ['./calendar-events.component.scss']
})
export class CalendarEventsComponent implements OnInit {
  @Input() view: ViewType = ViewType.WEEK;
  @Input() inputListEvents: AppCalendarPeriod[];
  @Input() editable: boolean = false;
  @Output() updateListEvents = new EventEmitter<AppCalendarPeriod[]>();

  // View Date
  viewDate: Date = new Date();

  // Data
  listEventsData: AppCalendarPeriod[];
  listCalendarEvents: CalendarEvent[];

  constructor(private language: LanguageService, private calendarHelper: CalendarHelperService) {

  }

  ngOnInit() {
    // Generate events;
    if (this.inputListEvents) {
      this.listEventsData = this.inputListEvents;
      this.generateViewCalendarEvents();
    }
  }

  private generateViewCalendarEvents() {
    this.listCalendarEvents = [];
    this.listEventsData.forEach((value) => {
      this.listCalendarEvents.push(this.calendarHelper.convertCalendarPeriodToCalendarEvent(value));
    });
  }

  eventClicked({event}: { event: CalendarEvent }): void {
    if (this.editable) {
      this.updateEventClick(event);
    }
  }

  updateEventClick(event: CalendarEvent) {
    this.listEventsData.forEach(calendarEvent => {
      if (calendarEvent.period.id === event.id) {
        calendarEvent.isSelected = !calendarEvent.isSelected;
      }
    });
    console.log('Update Event Click');
    this.generateViewCalendarEvents();
    this.updateListEvents.emit(this.listEventsData);
  }

}
