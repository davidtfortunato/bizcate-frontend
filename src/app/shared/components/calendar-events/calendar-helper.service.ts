import {Injectable} from '@angular/core';
import {EventType, PeriodConst, PeriodType, ScheduleAvailabilityPeriod} from '../../../../../functions/src/data/calendar-model';
import {AppCalendarPeriod} from './calendar-events.component';
import {CalendarEvent} from 'angular-calendar';
import {LanguageService} from '../../../core/language/language.service';
import {AppUIParams} from '../../app.ui-params';

@Injectable()
export class CalendarHelperService {

  constructor(private language: LanguageService) {

  }

  generateAppCalendarEvent(date: Date, period: PeriodType, isSelected: boolean, data?: any): AppCalendarPeriod {
    // clean hours, minutes
    date.setHours(0, 0, 0, 0);
    const startEndHour = this.getPeriodStartEndHour(period);

    return {
      period: {
        day: date.getTime(),
        period: period, eventType: EventType.PERIOD_DAY, id: String(date.getTime()) + '_' + period,
        startHour: startEndHour.startHour,
        endHour: startEndHour.endHour
      },
      isSelected,
      data
    };
  }

  /**
   * Generate a list of periods between 2 dates
   */
  generateAvailablePeriods(startDate: Date, endDate: Date): AppCalendarPeriod[] {
    const listPeriods: AppCalendarPeriod[] = [];

    for (const d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
      // Generate all periods
      listPeriods.push(this.generateAppCalendarEvent(d, PeriodType.MORNING, false, null));
      listPeriods.push(this.generateAppCalendarEvent(d, PeriodType.AFTERNOON, false, null));
      listPeriods.push(this.generateAppCalendarEvent(d, PeriodType.EVENING, false, null));
    }

    return listPeriods;
  }

  /**
   *
   */
  generateSelectableHours(periods: ScheduleAvailabilityPeriod[]): AppCalendarPeriod[] {
    const listPeriods: AppCalendarPeriod[] = [];
    let initHour = 0;
    let endHour = 0;

    if (periods) {
      periods.forEach(period => {
        switch (period.period) {
          case PeriodType.MORNING:
            initHour = PeriodConst.START_MORNING;
            endHour = PeriodConst.END_MORNING;
            break;
          case PeriodType.AFTERNOON:
            initHour = PeriodConst.START_AFTERNOON;
            endHour = PeriodConst.END_AFTERNOON;
            break;
          case PeriodType.EVENING:
            initHour = PeriodConst.START_EVENING;
            endHour = PeriodConst.END_EVENING;
            break;
        }

        // Add the hours for the Period
        for (let i = initHour; i < endHour; i++) {
          listPeriods.push({
            period: {
              day: period.day,
              period: period.period,
              eventType: EventType.HOURLY,
              id: period.id + '_' + i,
              startHour: i,
              endHour: 1 + i
            },
            isSelected: false
          });
        }

      });
    }

    return listPeriods;
  }

  generateScheduledEvents(periods: ScheduleAvailabilityPeriod[]): AppCalendarPeriod[] {
    const listPeriods: AppCalendarPeriod[] = [];
    if (periods) {
      periods.forEach(period => {
        listPeriods.push({
          period: period,
          isSelected: true
        });
      });
    }
    return listPeriods;
  }

  getPeriodStartEndHour(period: PeriodType): { startHour: number, endHour: number } {
    let data = null;

    switch (period) {
      case PeriodType.MORNING:
        data = {
          startHour: PeriodConst.START_MORNING,
          endHour: PeriodConst.END_MORNING
        };
        break;
      case PeriodType.AFTERNOON:
        data = {
          startHour: PeriodConst.START_AFTERNOON,
          endHour: PeriodConst.END_AFTERNOON
        };
        break;
      case PeriodType.EVENING:
        data = {
          startHour: PeriodConst.START_EVENING,
          endHour: PeriodConst.END_EVENING
        };
        break;
    }

    return data;
  }

  /**
   * Convert Calendar period to a Calendar Event
   */
  convertCalendarPeriodToCalendarEvent(calendarPeriod: AppCalendarPeriod): CalendarEvent {
    // init data
    const startDate = new Date(calendarPeriod.period.day);
    const endDate = new Date(calendarPeriod.period.day);
    let title = '';

    // Set start/end hour
    startDate.setHours(calendarPeriod.period.startHour);
    endDate.setHours(calendarPeriod.period.endHour);

    if (calendarPeriod.period.eventType === EventType.PERIOD_DAY) {
      switch (calendarPeriod.period.period) {
        case PeriodType.MORNING:
          title = this.language.getLanguage().GENERIC_PERIOD_MORNING;
          break;
        case PeriodType.AFTERNOON:
          title = this.language.getLanguage().GENERIC_PERIOD_AFTERNOON;
          break;
        case PeriodType.EVENING:
          title = this.language.getLanguage().GENERIC_PERIOD_EVENING;
          break;
      }

      if (calendarPeriod.isSelected) {
        title = title + ' - ' + this.language.getLanguage().GENERIC_SELECTED;
      }
    } else if (calendarPeriod.period.eventType === EventType.HOURLY) {
      title = calendarPeriod.isSelected
        ? this.language.getLanguage().GENERIC_SELECTED.toUpperCase()
        : this.language.getLanguage().GENERIC_AVAILABLE;
    }

    return {
      title: title,
      color: {
        primary: calendarPeriod.isSelected ? AppUIParams.actionButtonGreenColor : AppUIParams.actionButtonGrayColor,
        secondary: calendarPeriod.isSelected ? AppUIParams.actionButtonGreenColorOpacity : AppUIParams.actionButtonGrayColorOpacity
      },
      id: calendarPeriod.period.id,
      start: startDate,
      end: endDate
    };
  }

}
