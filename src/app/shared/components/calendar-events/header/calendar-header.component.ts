import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {LanguageService} from '../../../../core/language/language.service';

@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.scss']
})
export class CalendarHeaderComponent implements OnInit {
  @Input() view: string;

  @Input() viewDate: Date;

  @Output() viewChange: EventEmitter<string> = new EventEmitter();

  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

  // Data
  locale: string;
  labelToday: string;

  constructor(private language: LanguageService) {

  }

  ngOnInit() {
    this.locale = this.language.locale;
    this.labelToday = this.language.getLanguage().GENERIC_TODAY;
  }


  onPreviousClick() {
    console.log('On previous click');
  }

  onNextClick() {
    console.log('On next click');
    this.viewDateChange.emit()
  }

}
