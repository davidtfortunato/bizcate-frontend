import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-stars-level',
  templateUrl: './stars-level.component.html',
  styleUrls: ['./stars-level.component.scss']
})
export class StarsLevelComponent implements OnInit {
  @Input() level: number;
  @Input() type: 'SHORT' | 'EXPANDED';
  @Input() isEditable: boolean = false;
  @Output() onLevelSelected = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {

  }

  /**
   * Function when a level is selected
   */
  onLevelSelectedClick(level: number) {
    if (this.isEditable) {
      this.level = level;
      this.onLevelSelected.emit(this.level);
    }
  }

}
