import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface ToggleButtonData {
  label: string;
  id: string;
  isDefault?: boolean;
}

@Component({
  selector: 'app-filter-toggle-selector',
  templateUrl: './filter-button-toggle.component.html',
  styleUrls: ['./filter-button-toggle.component.scss']
})
export class FilterButtonToggleComponent implements OnInit {

  @Input() buttons: ToggleButtonData[];

  @Output() toggleChangeId = new EventEmitter<string>();

  // Data
  selectedButtonId: string;

  ngOnInit() {
    if (!this.selectedButtonId) {
      this.selectedButtonId = this.getDefaultValue();
    }

  }

  onToggleChange(buttonSelectedId: string) {
    if (!this.selectedButtonId || this.selectedButtonId !== buttonSelectedId) {
      this.selectedButtonId = buttonSelectedId;
      this.toggleChangeId.emit(this.selectedButtonId);
    }
  }

  getDefaultValue(): string {
    let defaultValue = '';
    if (this.buttons) {
      this.buttons.forEach(button => {
        if (button.isDefault) {
          defaultValue = button.id;
        }
      });
    }
    return defaultValue;
  }

}
