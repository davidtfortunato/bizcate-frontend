import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-filter-slider',
  templateUrl: './filter-slider.component.html',
  styleUrls: ['./filter-slider.component.scss']
})
export class FilterSliderComponent implements OnInit {
  @Input() maxValue: number = 10;
  @Input() minValue: number = 0;
  @Input() defaultValue: number;

  @Output() valueChanged = new EventEmitter<number>();

  // Data
  currentSelection: number;

  ngOnInit() {
    if (!this.defaultValue || this.defaultValue < 0) {
      this.defaultValue = this.maxValue;
    }
  }

  onValueChanged(value: number) {
    if (value !== this.currentSelection) {
      this.currentSelection = value;
      this.valueChanged.emit(this.currentSelection);
    }
  }

}
