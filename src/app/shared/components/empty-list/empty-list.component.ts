import {Component, Input, OnInit} from '@angular/core';
import {AppAssets} from '../../app.assets';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-empty-list',
  templateUrl: './empty-list.component.html',
  styleUrls: ['./empty-list.component.scss']
})
export class EmptyListComponent implements OnInit {
  @Input() data: {message: string, iconURL: string};
  @Input() message: string;
  @Input() iconUrl: string = AppAssets.ic_empty_list;

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    if (!this.message) {
      this.message = this.language.getLanguage().GENERIC_EMPTY_LIST;
    }

    if (this.data) {
      this.iconUrl = this.data.iconURL;
      this.message = this.data.message;
    }
  }

}
