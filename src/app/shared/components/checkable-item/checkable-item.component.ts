import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';



@Component({
  selector: 'app-checkable-item',
  templateUrl: './checkable-item.component.html',
  styleUrls: ['./checkable-item.component.scss']
})
export class CheckableItemComponent implements OnInit {
  @Input() text: string;
  @Input() isSelected = false;
  @Input() data: any;
  @Input() size: 'SMALL' | 'MEDIUM' | 'LARGE' = 'MEDIUM';

  // UI
  @Input() textColor = AppUIParams.actionButtonGrayColor;
  @Input() textSelectedColor = AppUIParams.actionButtonGreenColor;
  @Input() backgroundColor =  AppUIParams.actionButtonGrayColorOpacity;
  @Input() backgroundSelectedColor = AppUIParams.actionButtonGreenColorOpacity;

  @Output() itemClicked = new EventEmitter<any>();

  containerClass: object;

  constructor() {}

  ngOnInit() {
    this.containerClass = {
      sizeSmall: this.size === 'SMALL',
      sizeMedium: this.size === 'MEDIUM',
      sizeLarge: this.size === 'LARGE'
    };
  }

  onClickItem() {
    this.itemClicked.emit(this.data);
  }

}
