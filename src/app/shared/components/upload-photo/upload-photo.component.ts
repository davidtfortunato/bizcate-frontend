import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.scss']
})
export class UploadPhotoComponent implements OnChanges {
  @Input() defaultImageSrc: string;
  @Input() height: number;
  @Output() imageSrcChanged = new EventEmitter();
  @Output() fileToUpdate = new EventEmitter();

  // Current Image src
  imageSrc: string;
  overlayClass: Object;

  ngOnChanges(changes) {
    if (this.defaultImageSrc) {
      this.imageSrc = this.defaultImageSrc;
    }

    this.overlayClass = {
      'hasimage': !!this.imageSrc
    };
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      this.fileToUpdate.emit(file);
      const reader = new FileReader();
      reader.onload = e => {
        this.updateImageSrc((reader.result as string));
      };

      reader.readAsDataURL(file);
    }
  }

  updateImageSrc(url: string) {
    this.imageSrc = url;
    this.imageSrcChanged.emit(this.imageSrc);

    this.overlayClass = {
      'hasimage': !!this.imageSrc
    };
  }


}
