import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {AppUIParams} from '../../app.ui-params';
import {PlatformService} from '../../platform';
import {getLabelCategory, getSkillLabel, ServiceSkill} from '../../../../../functions/src/data';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-skill-item',
  templateUrl: './skill-item.component.html',
  styleUrls: ['./skill-item.component.scss']
})
export class SkillItemComponent implements OnInit {
  @Input() professionalSkill: ServiceSkill;
  @Input() hasRemoveButton = false;
  @Input() hasLevelSkill = true;
  @Output() skillRemoved = new EventEmitter<ServiceSkill>();

  // UI Params
  categoryIconColor: string;
  isMobile$: Observable<boolean>;

  // Labels
  labelYearsExp: string;

  constructor(@Inject(LOCALE_ID) protected localeId: string, private platform: PlatformService, private language: LanguageService) {
  }

  ngOnInit() {
    this.labelYearsExp = this.language.getLanguage().PROFESSIONAL_PROFILE_LABEL_SKILL_LEVEL;

    this.categoryIconColor = AppUIParams.actionButtonOrangeBackgroundColor;
    this.isMobile$ = this.platform.isMobileSize(true);
  }

  /**
   * Get Label of category
   */
  getCategoryLabel() {
    if (this.professionalSkill) {
      return getLabelCategory(this.professionalSkill.category.id, this.localeId);
    } else {
      return '';
    }
  }

  /**
   * Get Label of a serviceName
   */
  getSkillLabel() {
    if (this.professionalSkill) {
      return getSkillLabel(this.professionalSkill.serviceName.id, this.localeId);
    } else {
      return '';
    }
  }

  onSkillRemove() {
    this.skillRemoved.emit(this.professionalSkill);
  }

}
