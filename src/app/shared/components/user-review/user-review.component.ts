import {Component, Inject, Input, LOCALE_ID, OnChanges, OnInit} from '@angular/core';
import {formatDate} from '@angular/common';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-user-review',
  templateUrl: './user-review.component.html',
  styleUrls: ['./user-review.component.scss']
})
export class UserReviewComponent implements OnInit {

  @Input() avatarUrl: string;
  @Input() reviewLevel: number;
  @Input() reviewTitle: string; // Review title (maybe will be the task title)
  @Input() reviewLink: string; // Supposedly will be the task link
  @Input() reviewerName: string;
  @Input() reviewerUID: string;
  @Input() message: string;
  @Input() reviewDate: number;
  @Input() isVisible: boolean;

  constructor(@Inject(LOCALE_ID) public locale: string,
              private navigationService: NavigationLinksService,
              private languages: LanguageService) {
  }

  ngOnInit() {

  }

  getDateText() {
    if (this.reviewDate > 0) {
      return formatDate(new Date(this.reviewDate), 'M/d/yy, h:mm:ss a', this.locale);
    } else {
      return this.languages.getLanguage().GENERIC_NOT_AVAILABLE_SHORT;
    }

  }

  getReviewerLink() {
    return this.navigationService.parseArrayLinkToHref(this.navigationService.getUserProfile(this.reviewerUID));
  }

  getHeaderClass() {
    return {
      'app-overlay-blurred': !this.isVisible
    };
  }

}
