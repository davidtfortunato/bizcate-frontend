import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnInit, Output} from '@angular/core';
import {formatDate} from '@angular/common';
import {ProfessionalStoreService} from '../../../professional/store/professional-store.service';
import {ProfessionalPortfolioModel} from '../../../../../functions/src/data/portfolio-model';

@Component({
  selector: 'app-portfolio-item',
  templateUrl: './portfolio-item.component.html',
  styleUrls: ['./portfolio-item.component.scss']
})
export class PortfolioItemComponent implements OnInit {

  @Input() portfolioItem: ProfessionalPortfolioModel;
  @Input() hasRemoveButton = false;
  @Output() onRemoveClick = new EventEmitter<string>();

  constructor(@Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
  }

  getPublishedDate(): string {
    if (this.portfolioItem.publishedDate) {
      return formatDate(new Date(this.portfolioItem.publishedDate), 'dd/MM/yyyy', this.locale);
    } else {
      return '';
    }

  }

  onPostfolioRemove() {
    this.onRemoveClick.emit(this.portfolioItem.id);
  }
}
