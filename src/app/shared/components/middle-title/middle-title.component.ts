import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-middle-title',
  templateUrl: './middle-title.component.html',
  styleUrls: ['./middle-title.component.scss']
})
export class MiddleTitleComponent implements OnInit {
  @Input() title: string;
  @Input() fontSize: number;
  @Input() hasSeparator = true;
  @Input() titleAlign: 'LEFT' | 'CENTER' | 'RIGHT' = 'LEFT';

  ngOnInit() {

  }

}
