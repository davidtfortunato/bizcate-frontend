import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {AppMapLocation} from '../../../../../functions/src/data';
import {getLocationLabel, LocationResult} from '../../../../../functions/src/data/location-result';
import {LanguageService} from '../../../core/language/language.service';
import {GeolocationService} from '../../../core/location/geolocation.service';
import {SearchLocationService} from '../../../core/location/search-location.service';
import {MatAutocompleteSelectedEvent} from '@angular/material';

@Component({
  selector: 'app-shared-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.scss']
})
export class CityFormComponent implements OnInit, OnChanges {
  @Input() defaultSelectedCity: LocationResult;
  @Input() enabledFields: boolean = true;
  @Output() citySelected = new EventEmitter<LocationResult>();

  // Data
  currentLocation$: Observable<AppMapLocation>;
  citySearchResults$: Observable<LocationResult[]>;
  lastSelectedCity: LocationResult;

  // Labels
  countryPlaceholder: string;
  cityPlaceholder: string;

  // Form controllers
  form: FormGroup;
  countryFormControl = new FormControl({value: '', disabled: true}, Validators.required);
  cityFormControl = new FormControl('', Validators.required);

  // Subscription
  subCurrentLocation: Subscription;

  constructor(private language: LanguageService, private geoLocation: GeolocationService,
              private formBuilder: FormBuilder, private searchLocation: SearchLocationService) {
  }


  ngOnInit(): void {
    // Placeholders
    this.countryPlaceholder = this.language.getLanguage().AUTH_REGISTER_COUNTRY_PLACEHOLDER;
    this.cityPlaceholder = this.language.getLanguage().AUTH_REGISTER_CITY_PLACEHOLDER;

    // Current location
    this.currentLocation$ = this.geoLocation.getCurrentLocation();
    this.subCurrentLocation = this.currentLocation$.subscribe(currentLocation => {
      if (currentLocation && !this.defaultSelectedCity) {
        const subsSearchByCoords = this
          .searchLocation
          .searchLocationByCoords(currentLocation.latitude, currentLocation.longitude)
          .subscribe((locations: LocationResult[]) => {
            if (subsSearchByCoords) {
              subsSearchByCoords.unsubscribe();
            }

            if (locations && locations.length > 0) {
              this.selectCity(locations[0]);
            }

          });
      }

      if (this.subCurrentLocation) {
        this.subCurrentLocation.unsubscribe();
      }

    });

    this.initForm();
  }


  onSelectedCity(selectedCity: MatAutocompleteSelectedEvent) {
    const location: LocationResult = selectedCity.option.value;
    this.selectCity(location);
  }

  selectCity(location: LocationResult) {
    if (location && (!this.lastSelectedCity || this.lastSelectedCity.id !== location.id)) {
      this.lastSelectedCity = location;
      // Update forms
      this.cityFormControl.setValue(location.name);
      this.countryFormControl.setValue(location.country);

      this.citySelected.emit(location);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.defaultSelectedCity) {
      this.selectCity(this.defaultSelectedCity);
    }

    // Update the form to enabled/disable the city form
    if (this.cityFormControl) {
      if (this.enabledFields)
      {
        this.cityFormControl.enable();
      } else {
        this.cityFormControl.disable();
      }
    }
  }

  private initForm() {

    // Init Form
    this.form = this.formBuilder.group({
      country: this.countryFormControl,
      city: this.cityFormControl
    });

    // Listen to city name changes
    this.cityFormControl.valueChanges.subscribe(value => {
      this.citySearchResults$ = this.searchLocation.searchLocation(value);
    });
  }

  getLocationLabel(location: LocationResult): string
  {
    return getLocationLabel(location);
  }
}
