import {Component, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-container-title',
  templateUrl: './container-title.component.html',
  styleUrls: ['./container-title.component.scss']
})
export class ContainerTitleComponent implements OnChanges {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() titleAlign: 'LEFT' | 'CENTER' | 'RIGHT' = 'LEFT';

  // Class
  containerClass: Object;

  constructor() {}

  ngOnChanges() {
    this.containerClass = {
      'toLeft': this.titleAlign === 'LEFT',
      'toCenter': this.titleAlign === 'CENTER',
      'toRight': this.titleAlign === 'RIGHT'
    };
  }
}
