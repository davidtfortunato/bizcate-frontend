import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';

@Component({
  selector: 'app-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.scss']
})
export class ActionButtonComponent implements OnInit {

  @Input() text: string;
  @Input() textColor = '#FFF';
  @Input() backgroundColor = AppUIParams.actionButtonBlueColor;
  @Input() style: 'rounded' | 'rectangle' | 'circle' = 'rounded';
  @Input() state: 'enabled' | 'loading' | 'disabled' = 'enabled';
  @Input() iconMat: string;
  @Input() iconUrl: string; // not implemented yet
  @Input() fontSize = 15;
  @Input() btnTopBottomPadding = 12;
  @Input() styleColor: 'ORANGE' | 'GREEN' | 'RED' | 'WHITE' | 'BLUE';

  @Output() onClick = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    if (this.styleColor)
    {
      switch (this.styleColor) {
        case 'GREEN':
          this.textColor = AppUIParams.actionButtonWhiteColor;
          this.backgroundColor = AppUIParams.actionButtonGreenColor;
          break;
        case 'ORANGE':
          this.textColor = AppUIParams.actionButtonWhiteColor;
          this.backgroundColor = AppUIParams.actionButtonOrangeBackgroundColor;
          break;
        case 'RED':
          this.textColor = AppUIParams.actionButtonWhiteColor;
          this.backgroundColor = AppUIParams.actionButtonRedColor;
          break;
        case 'WHITE':
          this.textColor = AppUIParams.actionButtonBlackColor;
          this.backgroundColor = '#FFFFFF';
          break;
        case 'BLUE':
          this.textColor = AppUIParams.actionButtonWhiteColor;
          this.backgroundColor = AppUIParams.actionButtonBlueColor;
          break;
      }
    }
  }

  /**
   * Get the State class to handle the Button state
   */
  getButtonStateClass() {
    return {
      'enabled': this.state === 'enabled',
      'loading': this.state === 'loading',
      'disabled': this.state === 'disabled',
      'rounded': this.style === 'rounded',
      'rectangle': this.style === 'rectangle',
      'circle': this.style === 'circle'
    };
  }

  /**
   * Method when the click event occurs
   */
  onButtonClick() {
    if (this.state === 'enabled') {
      this.onClick.emit();
    }
  }
}
