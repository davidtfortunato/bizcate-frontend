import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUIParams} from '../../app.ui-params';
import {LanguageService} from '../../../core/language/language.service';

export interface SwitchItemData {
  id: string;
  label: string;
}

@Component({
  selector: 'app-shared-switcher',
  templateUrl: './switch-container.component.html',
  styleUrls: ['./switch-container.component.scss']
})
export class SwitchContainerComponent implements OnInit {

  // Data
  @Input() switchItems: SwitchItemData[];
  @Input() defaultSelectedId: string;

  // UI
  @Input() textColor = AppUIParams.actionButtonGrayColor;
  @Input() textSelectedColor = AppUIParams.actionButtonGreenColor;
  @Input() backgroundColor =  AppUIParams.actionButtonGrayColorOpacity;
  @Input() backgroundSelectedColor = AppUIParams.actionButtonGreenColorOpacity;

  // Output
  @Output() switchChanged = new EventEmitter<SwitchItemData>();

  // Current selection
  currentSelection: SwitchItemData;

  constructor() {}

  ngOnInit() {

    // Select the default selection
    if (!this.currentSelection) {
      // If has the default id defined, try to select it
      if (this.defaultSelectedId) {
        this.switchItems.forEach(item => {
          if (item.id === this.defaultSelectedId) {
            this.checkableClicked(item);
            return;
          }
        });
      } else {
        // otherwise, select the first on the list
        this.checkableClicked(this.switchItems[0]);
      }
    }
  }

  checkableClicked(clickedSwitchItem: SwitchItemData) {
    if (!this.currentSelection || this.currentSelection.id !== clickedSwitchItem.id) {
      this.currentSelection = clickedSwitchItem;
      this.switchChanged.emit(this.currentSelection);
    }
  }

}
