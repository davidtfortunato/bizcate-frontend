import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface HeaderActionButton {
  id: string;
  label: string;
  backgroundColor?: string;
  textColor?: string;
  state?: 'enabled' | 'loading' | 'disabled';
  styleColor?: 'ORANGE' | 'GREEN' | 'RED' | 'WHITE' | 'BLUE';
}

@Component({
  selector: 'app-title-header',
  templateUrl: './title-header.component.html',
  styleUrls: ['./title-header.component.scss']
})
export class TitleHeaderComponent implements OnInit {
  @Input() isMobile: boolean;
  @Input() title: String;
  @Input() subtitle: String;
  @Input() actionButtons: HeaderActionButton[];
  @Output() actionButtonClick = new EventEmitter<HeaderActionButton>();

  constructor() { }

  ngOnInit() {

  }

  /**
   * Event when a button was clicked
   * @param actionButtonData
   */
  onButtonClick(actionButtonData: HeaderActionButton) {
    this.actionButtonClick.emit(actionButtonData);
  }

}
