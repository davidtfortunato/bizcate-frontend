import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {COMPONENTS} from './index';
import {MaterialModule} from './material/material.module';
import {SnackBarService} from './components/snack-bar/snack-bar.service';
import {MatButtonToggleModule, MatCardModule, MatSliderModule, MatStepperModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogPhotoComponent} from './components/storage-photo-item/dialog-photo/dialog-photo.component';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {CalendarHelperService} from './components/calendar-events/calendar-helper.service';
import {NgxLoadingModule} from 'ngx-loading';

@NgModule({
  providers: [SnackBarService, CalendarHelperService],
  declarations: [COMPONENTS],
  exports: [COMPONENTS],
  imports: [
    RouterModule,
    MaterialModule,
    CommonModule,
    MatCardModule,
    MatButtonToggleModule,
    MatSliderModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgxLoadingModule
  ],
  entryComponents: [
    DialogPhotoComponent
  ]
})
export class SharedModule {
}
