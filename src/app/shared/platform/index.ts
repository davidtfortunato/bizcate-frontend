export { PlatformService, MOBILE_BREAKPOINT, TABLET_BREAKPOINT, BIG_SCREEN_BREAKPOINT } from './platform.service';
export { DeviceType, DeviceTypeUp, Orientation } from './types';
