import {Injectable, Inject, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser, DOCUMENT} from '@angular/common';
import {ɵgetDOM as getDOM} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {ReplaySubject, Observable, combineLatest} from 'rxjs';
import {distinctUntilChanged, map} from 'rxjs/operators';

import {DeviceType, Orientation} from './types';
import {WindowRefService} from './window.service';

export const MOBILE_BREAKPOINT = 736;
export const TABLET_BREAKPOINT = 1135;
export const BIG_SCREEN_BREAKPOINT = 1919;

@Injectable({providedIn: 'root'})
export class PlatformService {
  get deviceType(): DeviceType {
    return this._deviceType;
  }

  /**
   * Event fired when device type has changed (according to screen size)
   */
  get deviceChanged() {
    return this.deviceSubject.pipe(distinctUntilChanged());
  }

  /**
   * Event fired when orientation has changed
   */
  get orientationChanged() {
    return this.orientationSubject.pipe(distinctUntilChanged());
  }

  /**
   * Event fired on desktop size when BIG_SCREEN_BREAKPOINT is passed
   */
  get bigScreenChanged() {
    return this.bigScreenSubject.pipe(distinctUntilChanged());
  }

  /**
   * Event fired when either device has changed, BIG_SCREEN_BREAKPOINT is passed, or orientation has changed
   */
  get platformChanged() {
    return combineLatest(this.deviceChanged, this.orientationChanged, this.bigScreenChanged).pipe(distinctUntilChanged());
  }

  /**
   * Indicates if device is in landscape mode
   */
  get isLandscape(): boolean {
    return this.orientation === 'landscape';
  }

  /**
   * Indicates if device is in portrait mode
   */
  get isPortrait(): boolean {
    return this.orientation === 'portrait';
  }

  /**
   * Indicates if we are a browser app
   */
  get isBrowser(): boolean {
    return isPlatformBrowser(this.platformId);
  }

  safeAreaInsetTop = 0;

  /** Whether the current platform is Apple iOS */
  readonly isIOS: boolean;

  /** Whether the current OS is Windows */
  readonly isWindows: boolean;

  /** Whether the current browser is IE11 */
  readonly isIE11: boolean;

  /** Whether the current browser is Safari */
  readonly isSafari: boolean;

  /**
   * Indicates if the preview is rendered from a native and PWA backend. (Old offers)
   */
  readonly isNativeAndPWAPlanPreviewApp: boolean;

  private orientation: Orientation;
  private _deviceType: DeviceType;
  private deviceSubject = new ReplaySubject<DeviceType>(1);
  private orientationSubject = new ReplaySubject<Orientation>(1);
  private deviceCookieName = 'gbdevice';
  private bigScreen: boolean;
  private bigScreenSubject = new ReplaySubject<boolean>(1);
  private dom = getDOM();

  constructor(
    public windowRef: WindowRefService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(DOCUMENT) public doc: Document
  ) {
    this.update();

    this.isIOS = this.isBrowser && !!/iPhone|iPad|iPod/i.exec(windowRef.nativeWindow.navigator.userAgent);
    if (this.isIOS) {
      this.dom.addClass(this.doc.body, 'is-ios');
    }
    this.isWindows = this.isBrowser && !!/Windows/i.exec(this.windowRef.nativeWindow.navigator.userAgent);

    this.isIE11 = this.isBrowser && !!/(msie|trident)/i.exec(windowRef.nativeWindow.navigator.userAgent);
    this.isSafari = this.isBrowser
      && !!/Safari/i.exec(windowRef.nativeWindow.navigator.userAgent)
      && !/Chrome/i.exec(windowRef.nativeWindow.navigator.userAgent);
  }

  /**
   * Updates platform information
   */
  update() {
    let newDeviceType: DeviceType;
    if (this.windowRef.nativeWindow.innerWidth <= MOBILE_BREAKPOINT) {
      newDeviceType = 'mobile';
    } else if (this.windowRef.nativeWindow.innerWidth <= TABLET_BREAKPOINT) {
      newDeviceType = 'tablet';
    } else {
      newDeviceType = 'desktop';
    }

    this.bigScreen = this.windowRef.nativeWindow.innerWidth > BIG_SCREEN_BREAKPOINT;
    this.orientation = this.windowRef.nativeWindow.innerWidth < this.windowRef.nativeWindow.innerHeight ? 'portrait' : 'landscape';

    if (this.deviceType !== newDeviceType) {
      this._deviceType = newDeviceType;
    }

    this.deviceSubject.next(this._deviceType);
    this.orientationSubject.next(this.orientation);
    this.bigScreenSubject.next(this.bigScreen);

    this.initSafeAreaInsetTop();
  }

  isTabletSize(asObservable = false): any {
    return asObservable ? this.deviceChanged.pipe(map(device => device === 'tablet')) : this._deviceType === 'tablet';
  }

  isMobileSize(asObservable = false): any {
    return asObservable ? this.deviceChanged.pipe(map(device => device === 'mobile')) : this._deviceType === 'mobile';
  }

  isDesktopSize(asObservable = false): any {
    return asObservable ? this.deviceChanged.pipe(map(device => device === 'desktop')) : this._deviceType === 'desktop';
  }

  isBigScreenSize(): boolean {
    return this._deviceType === 'desktop' && this.bigScreen;
  }

  isMobileOrTablet(): Observable<boolean> {
    return this.deviceChanged.pipe(map(device => device !== 'desktop'));
  }

  /**
   * Get the safe-area-inset-top environment variable when it's not possible to use css
   */
  initSafeAreaInsetTop() {
    if (this.isBrowser) {
      const div = this.doc.createElement('div');
      div.style.setProperty('height', 'env(safe-area-inset-top)');
      this.doc.body.appendChild(div);
      this.safeAreaInsetTop = div.clientHeight;
      this.doc.body.removeChild(div);
    }
  }
}
