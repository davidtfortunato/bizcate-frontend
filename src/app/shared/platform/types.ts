export type DeviceType = 'mobile' | 'tablet' | 'desktop';
export type DeviceTypeUp = 'MOBILE' | 'TABLET' | 'DESKTOP';
export type Orientation = 'portrait' | 'landscape';
