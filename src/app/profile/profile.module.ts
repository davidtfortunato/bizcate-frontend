import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {MapModule} from '../map/map.module';
import {ProfileUserContainerComponent} from './client-profile/profile-user-container.component';
import {ProfileProfessionalContainerComponent} from './professional-profile/profile-professional-container.component';
import {AppRoutes} from '../core/navigation/routes';
import {ProfileComponent} from './profile.component';
import {PublicProfileUserComponent} from './client-profile/content/public-profile-user.component';
import {ProfileHeaderPhotoComponent} from './shared/header-photo/header-photo.component';
import {MatIconModule} from '@angular/material';
import {ProfileSocialNetworksComponent} from './shared/social-networks/social-networks.component';
import {ReviewsContainerComponent} from './shared/reviews-container/reviews-container.component';
import {UserInfosComponent} from './shared/user-infos/user-infos.component';
import {PublicProfileProfessionalComponent} from './professional-profile/content/public-profile-professional.component';
import {ProfileSkillsContainerComponent} from './shared/skills-container/skills-container.component';
import {ProfilePortfolioContainerComponent} from './shared/portfolio-container/portfolio-container.component';

const ROUTES: Routes = [
  {path: '', component: ProfileComponent, pathMatch: 'full'},
  {path: 'user/:' + AppRoutes.USERID, component: ProfileUserContainerComponent, pathMatch: 'full'},
  {path: 'pro/:' + AppRoutes.USERID, component: ProfileProfessionalContainerComponent, pathMatch: 'full'}
];

const COMPONENTS = [ProfileComponent,
  ProfileUserContainerComponent,
  ProfileProfessionalContainerComponent,
  PublicProfileUserComponent,
  ProfileHeaderPhotoComponent,
  ProfileSocialNetworksComponent,
  ReviewsContainerComponent,
  UserInfosComponent,
  PublicProfileProfessionalComponent,
  ProfileSkillsContainerComponent,
  ProfilePortfolioContainerComponent];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    MapModule,
    MatIconModule
  ],
  declarations: [
    COMPONENTS
  ],
  providers: [],
  entryComponents: []
})
export class ProfileModule {
}
