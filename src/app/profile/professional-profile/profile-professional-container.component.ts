import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {ApiResponse, UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';
import {ActivatedRoute} from '@angular/router';
import {ProfessionalApiService, UserAPIService} from '../../core/api';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {AppRoutes} from '../../core/navigation/routes';
import {ProfessionalPortfolioModel} from '../../../../functions/src/data/portfolio-model';
import {PortfolioApiService} from '../../core/api/portfolio-api.service';

@Component({
  selector: 'app-profile-professional-container',
  templateUrl: './profile-professional-container.component.html',
  styleUrls: ['./profile-professional-container.component.scss']
})
export class ProfileProfessionalContainerComponent implements OnInit, OnDestroy {

  // Data
  userData$: Observable<UserProfileModel>;
  professionalProfile$: Observable<ApiResponse<UserProfessionalProfile>>;
  portfolioList$: Observable<ApiResponse<ProfessionalPortfolioModel[]>>;

  // Route params
  paramUserId: string;

  // Route Subs
  routeSub: Subscription;

  constructor(private route: ActivatedRoute,
              private userAPI: UserAPIService,
              private navigationService: NavigationLinksService,
              private professionalAPI: ProfessionalApiService,
              private portfolioAPI: PortfolioApiService) {
  }

  ngOnInit() {

    // Get UID from the URL
    this.routeSub = this.route.params.subscribe(params => {
      this.paramUserId = params[AppRoutes.USERID];

      if (!this.paramUserId) {
        // Navigate to the root
        this.navigationService.openHome();
      } else {
        // Get user data
        this.userData$ = this.userAPI.getProfileData(this.paramUserId);
        this.professionalProfile$ = this.professionalAPI.getProfessionalProfileByUid(this.paramUserId);
        this.portfolioList$ = this.portfolioAPI.getProfessionalPortfolioByUid(this.paramUserId);
      }
    });
  }

  getProfessionalProfile(response: ApiResponse<UserProfessionalProfile>) {
    return response && response.isSuccess ? response.responseData : null;
  }

  getPortfolioResponse(response: ApiResponse<ProfessionalPortfolioModel[]>) {
    return response && response.isSuccess ? response.responseData : null;
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
