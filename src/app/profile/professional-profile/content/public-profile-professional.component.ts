import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {UserProfessionalProfile, UserProfileModel} from '../../../../../functions/src/data';
import {HeaderActionButton} from '../../../shared/components/title-header/title-header.component';
import {LanguageService} from '../../../core/language/language.service';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {AppUIParams} from '../../../shared/app.ui-params';
import {ProfessionalPortfolioModel} from '../../../../../functions/src/data/portfolio-model';

@Component({
  selector: 'app-public-profile-professional',
  templateUrl: './public-profile-professional.component.html',
  styleUrls: ['./public-profile-professional.component.scss']
})
export class PublicProfileProfessionalComponent implements OnInit, OnChanges {

  @Input() userProfile: UserProfileModel;
  @Input() professionalProfile: UserProfessionalProfile;
  @Input() portfolioList: ProfessionalPortfolioModel[];

  // Labels
  headerTitle: { title?: string, subtitle?: string } = {};

  // UI
  headerActionButtons: HeaderActionButton[];

  constructor(private language: LanguageService,
              private navigationService: NavigationLinksService) {

  }

  ngOnInit() {
    this.headerTitle.title = this.language.getLanguage().USER_PROFILE_SCREEN_TITLE;

    this.headerActionButtons = [
      {
        id: '1',
        label: this.language.getLanguage().GENERIC_SEND_MESSAGE,
        backgroundColor: AppUIParams.actionButtonGreenColor,
        textColor: AppUIParams.actionButtonWhiteColor
      },
      {
        id: '2',
        label: this.language.getLanguage().GENERIC_REQUEST_QUOTE,
        backgroundColor: AppUIParams.actionButtonOrangeBackgroundColor,
        textColor: AppUIParams.actionButtonWhiteColor
      }
    ];
  }

  ngOnChanges() {
    if (this.professionalProfile) {
      this.headerTitle.subtitle = this.professionalProfile.name;
    }
  }

  onActionButtonClick(actionButton: HeaderActionButton) {
    if (actionButton.id === '1') {
      this.navigationService.openSendMessage(this.professionalProfile.uid);
    } else if (actionButton.id === '2') {
      this.navigationService.openRequestQuote(this.professionalProfile.uid);
    }
  }

  getUserLocation() {
    let locationBuilder = '';
    if (this.userProfile.city) {
      locationBuilder = this.userProfile.city;
    }

    if (this.userProfile.country) {
      if (this.userProfile.city) {
        locationBuilder += ', ';
      }
      locationBuilder += this.userProfile.country;
    }

    return locationBuilder;
  }

  getProfessionalLocation() {
    let locationBuilder = '';
    if (this.professionalProfile) {
      if (this.professionalProfile.city) {
        locationBuilder = this.professionalProfile.city;
      }

      if (this.professionalProfile.country) {
        if (this.professionalProfile.city) {
          locationBuilder += ', ';
        }
        locationBuilder += this.professionalProfile.country;
      }
    }
    return locationBuilder;
  }
}
