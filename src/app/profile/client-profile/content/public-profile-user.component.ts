import {Component, Input, OnInit} from '@angular/core';
import {UserProfessionalProfile, UserProfileModel} from '../../../../../functions/src/data';
import {LanguageService} from '../../../core/language/language.service';
import {HeaderActionButton} from '../../../shared/components/title-header/title-header.component';
import {AppUIParams} from '../../../shared/app.ui-params';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';

@Component({
  selector: 'app-public-profile-user',
  templateUrl: './public-profile-user.component.html',
  styleUrls: ['./public-profile-user.component.scss']
})
export class PublicProfileUserComponent implements OnInit {

  @Input() userProfile: UserProfileModel;
  @Input() professionalProfile: UserProfessionalProfile;

  // Labels
  headerTitle: {title?: string, subtitle?: string} = {};

  // UI
  headerActionButtons: HeaderActionButton[];

  constructor(private language: LanguageService, private navigationService: NavigationLinksService) {

  }

  ngOnInit() {
    this.headerTitle.title = this.language.getLanguage().USER_PROFILE_SCREEN_TITLE;
    this.headerTitle.subtitle = this.userProfile.name;

    this.headerActionButtons = [
      {
        id: '1',
        label: this.language.getLanguage().GENERIC_SEND_MESSAGE,
        backgroundColor: AppUIParams.actionButtonGreenColor,
        textColor: AppUIParams.actionButtonWhiteColor
      }
    ];
  }

  onActionButtonClick(actionButton: HeaderActionButton) {
    if (actionButton.id === '1') {
      this.navigationService.openSendMessage(this.userProfile.uid);
    }
  }

  getUserLocation() {
    let locationBuilder = '';
    if (this.userProfile.city) {
      locationBuilder = this.userProfile.city;
    }

    if (this.userProfile.country) {
      if (this.userProfile.city) {
        locationBuilder += ', ';
      }
      locationBuilder += this.userProfile.country;
    }

    return locationBuilder;
  }

  getProfessionalLocation() {
    let locationBuilder = '';
    if (this.professionalProfile) {
      if (this.professionalProfile.city) {
        locationBuilder = this.professionalProfile.city;
      }

      if (this.professionalProfile.country) {
        if (this.professionalProfile.city) {
          locationBuilder += ', ';
        }
        locationBuilder += this.professionalProfile.country;
      }
    }
    return locationBuilder;
  }
}
