import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfessionalApiService, UserAPIService} from '../../core/api';
import {Observable, Subscription} from 'rxjs';
import {ApiResponse, UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';
import {AppRoutes} from '../../core/navigation/routes';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';

@Component({
  selector: 'app-profile-user-container',
  templateUrl: './profile-user-container.component.html',
  styleUrls: ['./profile-user-container.component.scss']
})
export class ProfileUserContainerComponent implements OnInit, OnDestroy {

  // Data
  userData$: Observable<UserProfileModel>;
  professionalProfile$: Observable<ApiResponse<UserProfessionalProfile>>;

  // Route params
  paramUserId: string;

  // Route Subs
  routeSub: Subscription;

  constructor(private route: ActivatedRoute,
              private userAPI: UserAPIService,
              private navigationService: NavigationLinksService,
              private professionalAPI: ProfessionalApiService) {
  }

  ngOnInit() {

    // Get UID from the URL
    this.routeSub = this.route.params.subscribe(params => {
      this.paramUserId = params[AppRoutes.USERID];

      if (!this.paramUserId) {
        // Navigate to the root
        this.navigationService.openHome();
      } else {
        // Get user data
        this.userData$ = this.userAPI.getProfileData(this.paramUserId);
        this.professionalProfile$ = this.professionalAPI.getProfessionalProfileByUid(this.paramUserId);
      }
    });
  }

  getProfessionalProfile(response: ApiResponse<UserProfessionalProfile>) {
    return response && response.isSuccess ? response.responseData : null;
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
