import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';

@Component({
  selector: 'app-profile-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {
  @Input() type: 'PROFESSIONAL' | 'USER' = 'USER';
  @Input() avatarUrl: string;
  @Input() name: string;
  @Input() location: string;
  @Input() description: string;
  @Input() uid: string;


  // Labels
  labelTitle: string;

  constructor(private language: LanguageService, private navigationService: NavigationLinksService) {

  }

  ngOnInit() {
    this.labelTitle = this.type === 'USER'
      ? this.language.getLanguage().PUBLIC_PROFILE_USER_INFOS_TITLE
      : this.language.getLanguage().PUBLIC_PROFILE_PROFESSIONAL_INFOS_TITLE;
  }

  openProfileDetails() {
     return this.type === 'PROFESSIONAL'
       ? this.navigationService.openProfessionalProfile(this.uid) : this.navigationService.openUserProfile(this.uid);
  }
}
