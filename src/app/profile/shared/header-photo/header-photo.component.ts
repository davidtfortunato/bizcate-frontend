import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile-header-photo',
  templateUrl: './header-photo.component.html',
  styleUrls: ['./header-photo.component.scss']
})
export class ProfileHeaderPhotoComponent implements OnInit {

  @Input() photUrl: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() description: string;

  constructor() {
  }

  ngOnInit() {

  }

}
