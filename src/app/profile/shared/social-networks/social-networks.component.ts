import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {AppAssets} from '../../../shared/app.assets';

@Component({
  selector: 'app-profile-social-networks',
  templateUrl: './social-networks.component.html',
  styleUrls: ['./social-networks.component.scss']
})
export class ProfileSocialNetworksComponent implements OnInit {

  @Input() facebookUrl: string;
  @Input() linkedinUrl: string;

  // Labels
  labelTitle: string;

  // Icons
  icFacebookUrl: string = AppAssets.ic_facebook_gray;
  icLinkedinUrl: string = AppAssets.ic_linkedin_gray;

  constructor(private language: LanguageService) {

  }

  ngOnInit() {
    this.labelTitle = this.language.getLanguage().PUBLIC_PROFILE_SOCIAL_NETWORKS_TITLE;
  }

}
