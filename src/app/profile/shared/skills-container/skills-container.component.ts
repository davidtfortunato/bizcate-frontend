import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {ServiceSkill} from '../../../../../functions/src/data';

@Component({
  selector: 'app-profile-skills-container',
  templateUrl: './skills-container.component.html',
  styleUrls: ['./skills-container.component.scss']
})
export class ProfileSkillsContainerComponent implements OnInit {

  @Input() skills: ServiceSkill[];

  // Title Label
  labelTitle: string;

  constructor(private language: LanguageService) {

  }

  ngOnInit() {
    this.labelTitle = this.language.getLanguage().PUBLIC_PROFILE_SKILLS_TITLE;
  }

}
