import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-profile-reviews-container',
  templateUrl: './reviews-container.component.html',
  styleUrls: ['./reviews-container.component.scss']
})
export class ReviewsContainerComponent implements OnInit {

  // Labels
  labelTitle: string;

  constructor(private language: LanguageService) {

  }

  ngOnInit() {
    this.labelTitle = this.language.getLanguage().PUBLIC_PROFILE_USER_REVIEWS_TITLE;
  }
}
