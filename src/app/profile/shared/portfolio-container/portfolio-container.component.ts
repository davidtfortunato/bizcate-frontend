import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {ProfessionalPortfolioModel} from '../../../../../functions/src/data/portfolio-model';

@Component({
  selector: 'app-profile-portfolio-container',
  templateUrl: './portfolio-container.component.html',
  styleUrls: ['./portfolio-container.component.scss']
})
export class ProfilePortfolioContainerComponent implements OnInit {

  @Input() portfolioList: ProfessionalPortfolioModel[];

  // Title Label
  labelTitle: string;

  constructor(private language: LanguageService) {

  }

  ngOnInit() {
    this.labelTitle = this.language.getLanguage().PUBLIC_PROFILE_PORTFOLIO_TITLE;
  }

}
