import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';

import {AppStoreService} from './store/app-store.service';
import * as fromAuthStore from './auth/store/reducer';
import {AppUIParams} from './shared/app.ui-params';
import {PlatformService} from './shared/platform';
import {MessagingService} from './core/messaging/messaging.service';
import {CentralUserDataService} from './core/central-data/central-user-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./core/styles/common.scss', './app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'bizcate-frontend';

  // UI Params
  backgroundColor: String;

  constructor(private store: Store<fromAuthStore.State>,
              private appStoreService: AppStoreService,
              private platformService: PlatformService,
              private msgService: MessagingService) {
  }

  ngOnInit() {
    this.backgroundColor = AppUIParams.appBackground;

    // Init Store
    this.appStoreService.initApp();

    // Messaging Service
    this.msgService.getPermission();
    this.msgService.receiveMessage();
  }

  @HostListener('window:resize')
  onResize() {
    this.platformService.update();
  }
}
