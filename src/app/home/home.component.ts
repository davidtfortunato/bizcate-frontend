import {Component, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {select, State} from '@ngrx/store';

import {AuthStoreService} from '../auth/store/auth-store.service';
import {Router} from '@angular/router';
import * as fromStore from '../store';
import {getIsLoggedIn, getUserLoggedInState} from '../auth/store/selectors';
import {NavigationLinksService} from '../core/navigation/navigation-links.service';
import {AppAssets} from '../shared/app.assets';
import {AppUIParams} from '../shared/app.ui-params';
import {UserLoggedinState} from '../../../functions/src/data/user-model';
import {AppStoreService} from '../store/app-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userLoggedinState$: Observable<UserLoggedinState>;

  // UI Params
  backgroundColor: String;

  // Assets
  appAssets = AppAssets;

  // Subscriptions
  subIsLoggedIn: Subscription;
  subUserLoggedState: Subscription;

  constructor(private authService: AuthStoreService,
              private router: Router,
              private store: State<fromStore.State>,
              private navigationService: NavigationLinksService,
              private appStoreService: AppStoreService) {
  }

  ngOnInit() {

    // Redirects depending on user states
    this.userLoggedinState$ = this.store.pipe(select(getUserLoggedInState));
    this.subUserLoggedState = this.userLoggedinState$.subscribe(state => {
      switch (state) {
        case UserLoggedinState.LOGGEDIN:
          this.navigationService.openInitialScreen();
          this.appStoreService.hideProgressBar();
          break;
        case UserLoggedinState.ANONYMOUS:
          this.navigationService.openLogin();
          this.appStoreService.hideProgressBar();
          break;
        case UserLoggedinState.INITIALIZING_SESSION:
          this.appStoreService.displayProgressBar('indetermine');
          break;
      }

      if (state !== UserLoggedinState.INITIALIZING_SESSION && this.subIsLoggedIn) {
        this.subIsLoggedIn.unsubscribe();
      }
    });

    this.backgroundColor = AppUIParams.appBackground;
  }

  onLogout() {
    this.authService.logout();
  }

  onLogin() {
    this.navigationService.openLogin();
  }

  onEditProfile() {
    this.navigationService.openEditProfile();
  }

  onMessages() {
    this.navigationService.openUserConversations();
  }
}
