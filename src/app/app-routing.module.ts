import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren:  './home/home.module#HomeModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'messages', loadChildren: './messages/messages.module#MessagesModule' },
  { path: 'professional', loadChildren: './professional/professional.module#ProfessionalModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
  { path: 'tasks', loadChildren: './tasks/tasks.module#TasksModule' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
  { path: 'create-task', loadChildren: './create-task/create-task.module#CreateTaskModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, onSameUrlNavigation: 'reload'})
    // ,{ enableTracing: true })
    ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
