import {Component, Inject, Input, LOCALE_ID, OnChanges} from '@angular/core';
import {AuthUserModel, UserConversationModel, UserProfileModel} from '../../../../../functions/src/data/index';
import {UserInfos} from '../../../../../functions/src/data/messages-model';
import {formatDate} from '@angular/common';
import {MessagesStoreService} from '../../store/messages-store.service';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {AppAssets} from '../../../shared/app.assets';

@Component({
  selector: 'app-conversation-item',
  templateUrl: './conversation-item.component.html',
  styleUrls: ['./conversation-item.component.scss']
})
export class ConversationItemComponent implements OnChanges {

  @Input() userConversation: UserConversationModel;
  @Input() currentUser: AuthUserModel | UserProfileModel;
  @Input() isConversationSelected: Boolean = false;
  @Input() displayLastMessage: Boolean = true;

  // The other user letter
  otherUserLetter: 'A' | 'B' | 'UNKNOWN';
  otherUserInfos: UserInfos;

  constructor(@Inject(LOCALE_ID) public locale: string, private navigation: NavigationLinksService) {
  }

  ngOnChanges() {
    this.otherUserLetter = this.getOtherUserLetter();
    this.otherUserInfos = this.getOtherUserInfos();
  }

  getUserConversationPhotoURL() {
    return this.otherUserInfos.photoUrl ? this.otherUserInfos.photoUrl : AppAssets.ic_avatar;
  }

  /**
   * Get if the other user is the letter A or B
   */
  getOtherUserLetter(): 'A' | 'B' | 'UNKNOWN' {
    if (this.currentUser != null && this.userConversation != null) {
      if (this.userConversation.userUIDA === this.currentUser.uid) {
        return 'B';
      } else {
        return 'A';
      }
    }
    return 'UNKNOWN';
  }

  /**
   * In this case will get the User data that is not the current one logged in
   */
  getOtherUserInfos(): UserInfos {
    switch (this.otherUserLetter) {
      case 'A':
        return this.userConversation.userA;
      case 'B':
        return this.userConversation.userB;
      default:
        return {name: '', photoUrl: ''};
    }
  }

  getOtherUserUID() {
    return this.otherUserLetter === 'A' ? this.userConversation.userUIDA : this.userConversation.userUIDB;
  }

  getLastMessageTime(): string {
    return formatDate(this.userConversation.lastMessage.date, 'M/d/yy, h:mm:ss a', this.locale);
  }

  getBackgroundClass() {
    return {
      'isSelected': this.isConversationSelected
    };
  }

  onOpenConversation() {
    this.navigation.openConversationDetails(this.userConversation.id, this.getOtherUserUID());
  }

}
