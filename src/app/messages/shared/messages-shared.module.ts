import {NgModule} from '@angular/core';
import {ConversationItemComponent} from './conversation-item/conversation-item.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [CommonModule,
    SharedModule],
  exports: [ConversationItemComponent],
  declarations: [ConversationItemComponent],
  providers: []
})
export class MessagesSharedModule {
}
