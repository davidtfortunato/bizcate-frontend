import {Component, Inject, Input, LOCALE_ID, OnInit} from '@angular/core';
import {AuthUserModel, UserConversationModel, UserMessageModel} from '../../../../../functions/src/data';
import {formatDate} from '@angular/common';
import {AppAssets} from '../../../shared/app.assets';

@Component({
  selector: 'app-conversation-message-item',
  templateUrl: './conversation-message-item.component.html',
  styleUrls: ['./conversation-message-item.component.scss']
})
export class ConversationMessageItemComponent implements OnInit {

  @Input() conversation: UserConversationModel;
  @Input() message: UserMessageModel;
  @Input() currentUser: AuthUserModel;

  constructor(@Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {

  }

  getAvatarAlign(): 'LEFT' | 'RIGHT' {
    return (this.currentUser.uid === this.message.userSenderUID) ? 'LEFT' : 'RIGHT';
  }

  getMessageContainerClass() {
    return {
      'isSender': this.getAvatarAlign() === 'LEFT',
      'isReceiver': this.getAvatarAlign() === 'RIGHT',
    };
  }

  getUserConversationPhotoURL() {
    if (this.getAvatarAlign() === 'LEFT') {
      return this.currentUser.photoUrl ? this.currentUser.photoUrl : AppAssets.ic_avatar;
    } else if (this.conversation) {
      return this.getOtherUserInfos().photoUrl ? this.getOtherUserInfos().photoUrl : AppAssets.ic_avatar;
    } else {
      return AppAssets.ic_avatar;
    }
  }

  getOtherUserInfos() {
    if (this.currentUser.uid === this.conversation.userUIDA) {
      return this.conversation.userB;
    } else {
      return this.conversation.userA;
    }
  }

  getMessageDate() {
    return formatDate(this.message.date, 'M/d/yy, h:mm:ss a', this.locale);
  }
}
