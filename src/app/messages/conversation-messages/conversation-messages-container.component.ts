import {AfterViewChecked, Component, ElementRef, HostListener, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiResponse, AuthUserModel, UserConversationModel, UserMessageModel, UserProfileModel} from '../../../../functions/src/data';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getConversationById, getConversationMessages} from '../store/selectors';
import {MessagesStoreService} from '../store/messages-store.service';
import {PlatformService} from '../../shared/platform';

@Component({
  selector: 'app-conversation-messages',
  templateUrl: './conversation-messages-container.component.html',
  styleUrls: ['./conversation-messages-container.component.scss']
})
export class ConversationMessagesContainerComponent implements OnInit, OnChanges, AfterViewChecked {

  @Input() conversationId: string;
  @Input() otherUserUID: string; // In the case of a null conversationId, having this ID means that should create a new conversation
  @Input() currentUser: AuthUserModel;

  // Elements
  @ViewChild('container', {static: false}) container: ElementRef;
  @ViewChild('sendcontainer', {static: false}) sendContainer: ElementRef;
  @ViewChild('scrollMe', {static: false}) myScrollContainer: ElementRef;

  // UI
  containerWidth = 0;
  sendMessageHeight = 0;
  listMessagesHeight = 0;

  // Data
  listMessages$: Observable<UserMessageModel[]>;
  conversation$: Observable<UserConversationModel>;

  constructor(private store: State<fromStore.State>,
              private messageStoreService: MessagesStoreService,
              private platformService: PlatformService) {}

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.conversationId) {
      this.messageStoreService.loadConversationMessages(this.conversationId);
      this.listMessages$ = this.store.pipe(select(getConversationMessages(this.conversationId)));
      this.conversation$ = this.store.pipe(select(getConversationById(this.conversationId)));
    } else if (this.otherUserUID) {
      // Start a new conversation with this user
      // FOR NOW IS WORKING. It will naturally create a new conversation. So, doesn't need to specifically create a new one
    }
    this.containerWidth = this.container.nativeElement.offsetWidth;
    this.sendMessageHeight = this.sendContainer.nativeElement.offsetHeight;
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  onSendMessage(message) {
    this.messageStoreService.sendMessage({
      conversationId: this.conversationId,
      userReceiverUID: this.otherUserUID,
      userSenderUID: this.currentUser.uid,
      date: Date.now(),
      message: message
    });
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
      console.log('Error: ' + err);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.containerWidth = this.container.nativeElement.offsetWidth;
    this.sendMessageHeight = this.sendContainer.nativeElement.offsetHeight;
  }

  getMessagesContainerHeight() {
    if (this.container != null && this.sendContainer) {
      const messagesHeight = this.sendContainer.nativeElement.offsetTop - this.container.nativeElement.offsetTop;
      return messagesHeight;
    } else {
      return 0;
    }
  }

  onListChanged() {
    this.scrollToBottom();
  }

  getBottomMargin() {
    if (this.platformService.isMobileSize()) {
      return 10;
    } else {
      return this.sendMessageHeight;
    }
  }

}
