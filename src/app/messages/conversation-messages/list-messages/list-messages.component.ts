import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {AuthUserModel, UserConversationModel, UserMessageModel} from '../../../../../functions/src/data';
import {AppAssets} from '../../../shared/app.assets';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-conversation-list-messages',
  templateUrl: './list-messages.component.html',
  styleUrls: ['./list-messages.component.scss']
})
export class ListMessagesComponent implements OnInit, OnChanges {
  @Input() listMessages: UserMessageModel[];
  @Input() currentUser: AuthUserModel;
  @Input() conversation: UserConversationModel;
  @Output() listChanged = new EventEmitter();

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes['listMessages']) {
      this.listChanged.emit();
    }
  }

  hasMessages() {
    return this.listMessages && this.listMessages.length > 0;
  }



  getEmptyData() {
    return {
      message: this.language.getLanguage().USER_CONVERSATION_MESSAGES_EMPTY,
      iconURL: AppAssets.ic_empty_conversations
    };
  }
}
