import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-message-textbox',
  templateUrl: './message-textbox.component.html',
  styleUrls: ['./message-textbox.component.scss']
})
export class MessageTextboxComponent implements OnInit {

  @Output() sendMessage = new EventEmitter<string>();

  // Place Holders
  placeholderWriteMessage: string;

  // Form
  form: FormGroup;
  messageFormControl: FormControl = new FormControl('');

  constructor(private languageService: LanguageService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.placeholderWriteMessage = this.languageService.getLanguage().USER_CONVERSATION_PLACEHOLDER_WRITE_A_MESSAGE;

    // Initialize Form
    this.initForm();
  }

  private initForm() {
    this.messageFormControl.setValidators(Validators.required);

    // Init Form
    this.form = this.formBuilder.group({
      message: this.messageFormControl
    });
  }

  onSendMessage() {
    if (this.messageFormControl.valid) {
      this.sendMessage.emit(this.messageFormControl.value);
      this.messageFormControl.setValue('');
      this.messageFormControl.reset();
    }
  }

}
