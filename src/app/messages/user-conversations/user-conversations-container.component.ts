import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {State} from '@ngrx/store';

import * as fromStore from '../../store';
import {AuthUserModel, UserConversationModel, UserProfileModel} from '../../../../functions/src/data';
import {MessagesStoreService} from '../store/messages-store.service';
import {DeviceType, PlatformService} from '../../shared/platform';
import {LanguageService} from '../../core/language/language.service';
import {AppAssets} from '../../shared/app.assets';

@Component({
  selector: 'app-user-conversations',
  templateUrl: './user-conversations-container.component.html',
  styleUrls: ['./user-conversations-container.component.scss']
})
export class UserConversationsContainerComponent implements OnInit, OnChanges {

  @Input() userConversations: UserConversationModel[];
  @Input() currentUser: AuthUserModel;
  @Input() deviceType: DeviceType;
  @Input() selectedConversationID: string;
  @Input() otherUserUID: string;

  constructor(private messagesStoreService: MessagesStoreService,
              private platformService: PlatformService,
              private language: LanguageService) {
  }

  ngOnInit() {
    // Initialize Messages
    // this.messagesStoreService.init();
    this.messagesStoreService.loadUserConversations();
  }

  ngOnChanges() {
    console.log('Device Type changed: ' + this.deviceType);
  }

  displayListConversations() {
    return (!this.selectedConversationID && !this.otherUserUID) || !this.platformService.isMobileSize();
  }

  hasConversations() {
    return this.userConversations && this.userConversations.length > 0;
  }

  getUserConversationSelected() {
    let selectedUserConversation = null;
    if (this.userConversations && this.selectedConversationID) {
      this.userConversations.forEach(userConversation => {
        if (userConversation.id === this.selectedConversationID) {
          selectedUserConversation = userConversation;
        }
      });
    }
    return selectedUserConversation;
  }

  getEmptyData() {
    return {
      message: this.language.getLanguage().USER_CONVERSATIONS_EMPTY,
      iconURL: AppAssets.ic_empty_conversations
    };
  }

  getConversationListClassObject() {
    return {
      'noneConversationSelected' : !this.selectedConversationID
    };
  }
}
