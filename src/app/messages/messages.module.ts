import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MessagesComponent} from './messages.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {EffectsModule} from '@ngrx/effects';
import {Effects} from './store/effects';
import {ConversationMessagesContainerComponent} from './conversation-messages/conversation-messages-container.component';
import {UserConversationsContainerComponent} from './user-conversations/user-conversations-container.component';
import {MessagesStoreService} from './store/messages-store.service';
import {MessageTextboxComponent} from './conversation-messages/message-textbox/message-textbox.component';
import {ListMessagesComponent} from './conversation-messages/list-messages/list-messages.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material';
import {ConversationMessageItemComponent} from './conversation-messages/message-item/conversation-message-item.component';
import {MessagesSharedModule} from './shared/messages-shared.module';

const ROUTES: Routes = [
  {path: '', component: MessagesComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    CommonModule,
    SharedModule,
    MessagesSharedModule,
    EffectsModule.forFeature([Effects]),
    RouterModule.forChild(ROUTES)
  ],
  declarations: [MessagesComponent,
    UserConversationsContainerComponent,
    ConversationMessagesContainerComponent,
    MessageTextboxComponent,
    ListMessagesComponent,
    ConversationMessageItemComponent],
  providers: [MessagesStoreService]
})
export class MessagesModule {
}
