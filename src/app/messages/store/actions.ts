import {Action} from '@ngrx/store';
import {ApiResponse} from '../../../../functions/src/data';
import {UserConversationModel, UserMessageModel} from '../../../../functions/src/data';

export enum ActionTypes {
  Init = '[Messages] Init Messages',

  // Load User Conversations
  LoadUserConversations = '[Messages] Load User Conversations',
  LoadUserConversationsResult = '[Messages] Load User Conversations Result',

  // Load Conversation Messages
  LoadConversationMessages = '[Messages] Load Conversation Messages',
  LoadConversationMessagesResult = '[Messages] Load Conversation Messages Result',

  // Send Message
  SendUserMessage = '[Messages] Send User Message',
  SendUserMessageResult = '[Messages] Send User Message Result',

  // Create New Conversation
  CreateUserConversation = '[Messages] Create User Conversation',
  CreateUserConversationResult = '[Messages] Create User Conversation Result'

}

export class Init implements Action {
  readonly type = ActionTypes.Init;
}

// Load User Conversations
export class LoadUserConversations implements Action {
  readonly type = ActionTypes.LoadUserConversations;
}

export class LoadUserConversationsResult implements Action {
  readonly type = ActionTypes.LoadUserConversationsResult;

  constructor(public payload: {response: ApiResponse<UserConversationModel[]>}) {}
}

// Load Conversation Messages
export class LoadConversationMessages implements Action {
  readonly type = ActionTypes.LoadConversationMessages;
  constructor(public payload: {userConversationId: string}) {}
}

export class LoadConversationMessagesResult implements Action {
  readonly type = ActionTypes.LoadConversationMessagesResult;

  constructor(public payload: {response: ApiResponse<{ conversationId: string, messages: UserMessageModel[]}>}) {}
}

// Send a new message
export class SendUserMessage implements Action {
  readonly type = ActionTypes.SendUserMessage;

  constructor(public payload: {messageData: UserMessageModel}) {}
}

export class SendUserMessageResult implements Action {
  readonly type = ActionTypes.SendUserMessageResult;

  constructor(public payload: {response: ApiResponse<UserConversationModel>}) {}
}

// Create new user conversation
export class CreateUserConversation implements Action {
  readonly type = ActionTypes.CreateUserConversation;

  constructor(public payload: {otherUserUID: string}) {}
}

export class CreateUserConversationResult implements Action {
  readonly type = ActionTypes.CreateUserConversationResult;

  constructor(public payload: {response: ApiResponse<UserConversationModel>, otherUserUID: string}) {}
}

export type Actions = Init
  | LoadUserConversations
  | LoadUserConversationsResult
  | LoadConversationMessages
  | LoadConversationMessagesResult
  | SendUserMessage
  | SendUserMessageResult
  | CreateUserConversation
  | CreateUserConversationResult;
