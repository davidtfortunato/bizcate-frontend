import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as actions from './actions';
import {catchError, concatMap, map, mergeMap, share, switchMap} from 'rxjs/operators';
import {LoadUserConversations} from './actions';
import {MessagesApiService} from '../../core/api/messages-api.service';
import {of} from 'rxjs';
import {getQuickResponse} from '../../../../functions/src/data';
import {NoAction} from '../../store/app.actions';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';

@Injectable()
export class Effects {

  @Effect()
  init$ = this.actions$.pipe(
    ofType<actions.Init>(actions.ActionTypes.Init),
    map(action => action),
    concatMap(action => {
      return of(new LoadUserConversations());
    })
  );

  @Effect()
  loadUserConversations$ = this.actions$
    .pipe(
      ofType<actions.LoadUserConversations>(actions.ActionTypes.LoadUserConversations),
      map(action => action),
      switchMap(action => {
        return this.messagesService.getUserConversations()
          .pipe(map(response => {
              return new actions.LoadUserConversationsResult({response});
            }),
            catchError(err => of(new actions.LoadUserConversationsResult({response: getQuickResponse(false, err)}))));
      }));

  @Effect()
  loadConversationMessages$ = this.actions$
    .pipe(
      ofType<actions.LoadConversationMessages>(actions.ActionTypes.LoadConversationMessages),
      switchMap(action => {
        return this.messagesService.getConversationMessages(action.payload.userConversationId)
          .pipe(map(response => {
              return new actions.LoadConversationMessagesResult({response});
            }),
            catchError(err => of(new actions.LoadConversationMessagesResult({response: getQuickResponse(false, err)}))));
      }));

  @Effect()
  sendMessage$ = this.actions$
    .pipe(
      ofType<actions.SendUserMessage>(actions.ActionTypes.SendUserMessage),
      map(action => action),
      switchMap(action => {
        return this.messagesService.sendMessage(action.payload.messageData).pipe(
          map(response => new actions.SendUserMessageResult({response}))
        );
      })
    );

  @Effect()
  sendMessageResult$ = this.actions$.pipe(
    ofType<actions.SendUserMessageResult>(actions.ActionTypes.SendUserMessageResult),
    mergeMap(action => {
      const dispatchActions = [];

      if (action.payload.response.isSuccess) {
        dispatchActions.push(new actions.LoadUserConversations());
        dispatchActions.push(new actions.LoadConversationMessages({userConversationId: action.payload.response.responseData.id}));
      } else {
        dispatchActions.push(new NoAction());
      }

      return dispatchActions;
      }
    )
  );

  // # Create Conversation
  @Effect()
  createUserConversation$ = this.actions$
    .pipe(
      ofType<actions.CreateUserConversation>(actions.ActionTypes.CreateUserConversation),
      map(action => action),
      switchMap(action => {
        return this.messagesService.createUserConversationWithUserUID(action.payload.otherUserUID).pipe(
          map(response => new actions.CreateUserConversationResult({response, otherUserUID: action.payload.otherUserUID}))
        );
      })
    );

  @Effect({dispatch: false})
  createUserConversationResult$ = this.actions$
    .pipe(
      ofType<actions.CreateUserConversationResult>(actions.ActionTypes.CreateUserConversationResult),
      map(action => {
        if (action.payload.response.isSuccess && action.payload.response.responseData.id) {
          // Navigate to the new conversation
          this.linksService.openConversationDetails(action.payload.response.responseData.id, action.payload.otherUserUID);
        }
      })
    );

  constructor(private actions$: Actions, private messagesService: MessagesApiService, private linksService: NavigationLinksService) {
  }
}
