import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AngularFireAuth} from '@angular/fire/auth';
import * as actions from './actions';
import {UserMessageModel} from '../../../../functions/src/data';
import {State} from '../../store';

@Injectable()
export class MessagesStoreService {

  constructor(private store: Store<State>, private afAuth: AngularFireAuth) { }

  init() {
    this.store.dispatch(new actions.Init());
  }

  loadUserConversations() {
    this.store.dispatch(new actions.LoadUserConversations());
  }

  sendMessage(message: UserMessageModel) {
    this.store.dispatch(new actions.SendUserMessage({messageData: message}));
  }

  loadConversationMessages(conversationId) {
    this.store.dispatch(new actions.LoadConversationMessages({userConversationId: conversationId}));
  }
}
