import {UserConversationModel, UserMessageModel} from '../../../../functions/src/data';
import * as actions from './actions';
import {ActionTypes} from './actions';

export interface State {
  conversations?: UserConversationModel[];
  messagesByConversation?: { [conversationId: string]: UserMessageModel[]};
}

export const initialState: State = {
  conversations: [],
  messagesByConversation: {}
};


export function reducer(state = initialState, action: actions.Actions): State {
  switch (action.type) {
    case ActionTypes.LoadUserConversationsResult:
      if (action.payload.response.isSuccess) {
        return {
          ...state,
          conversations: action.payload.response.responseData
        };
      }
      break;
    case ActionTypes.LoadConversationMessagesResult:
      if (action.payload.response.isSuccess) {
        return {
          ...state,
          messagesByConversation: {
            ...state.messagesByConversation,
            [action.payload.response.responseData.conversationId]: action.payload.response.responseData.messages
          }
        };
      }
  }

  return state;
}


// Pure Functions
export const getConversations = (state: State) => state.conversations;
export const getMessagesByConversation = (state: State) => state.messagesByConversation;
