import {createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromStore from './reducer';
import {UserConversationModel} from '../../../../functions/src/data';

export const selectMessagesState = createFeatureSelector<fromStore.State>('messages');

// Getters
export const getUserConversations = createSelector(selectMessagesState, fromStore.getConversations);
export const getMessagesByConversation = createSelector(selectMessagesState, fromStore.getMessagesByConversation);
export const getConversationMessages = (conversationId) => createSelector(getMessagesByConversation, (conversations) => {
  return conversations[conversationId];
});
export const getConversationById = (conversationId) => createSelector(getUserConversations, (conversations) => {
  let conversationFound: UserConversationModel = null;
  if (conversations) {
    conversations.forEach(conversation => {
      if (conversation.id === conversationId) {
        conversationFound = conversation;
      }
    });
  }
  return conversationFound;
});
