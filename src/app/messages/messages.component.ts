import {Component, HostListener, OnChanges, OnInit} from '@angular/core';
import {MessagesApiService} from '../core/api/messages-api.service';
import {MessagesStoreService} from './store/messages-store.service';
import {Observable} from 'rxjs';
import {select, State} from '@ngrx/store';
import {getIsLoggedIn, getUser} from '../auth/store/selectors';
import * as fromStore from '../store';
import {AuthUserModel, UserConversationModel, UserProfileModel} from '../../../functions/src/data';
import {getUserConversations} from './store/selectors';
import {DeviceType, PlatformService} from '../shared/platform';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';
import {LanguageService} from '../core/language/language.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  // Query params
  conversationIDSelected$: Observable<string>;
  otherUserUIDSelected$: Observable<string>;

  // Data
  isUserLogged$: Observable<boolean>;
  userConversations$: Observable<UserConversationModel[]>;
  currentUserData$: Observable<AuthUserModel>;
  deviceType$: Observable<DeviceType>;

  // Titles
  title: {title, subtitle};

  constructor(private messagesStoreService: MessagesStoreService,
              private messagesAPIService: MessagesApiService,
              private store: State<fromStore.State>,
              private platformService: PlatformService,
              private route: ActivatedRoute,
              private language: LanguageService) {
  }

  ngOnInit() {
    // QParams
    this.conversationIDSelected$ = this.route.queryParams.pipe(map(qparams => {
      return qparams['conversationId'];
    }));
    this.otherUserUIDSelected$ = this.route.queryParams.pipe(map(qparams => {
      return qparams['otherUserUID'];
    }));

    this.isUserLogged$ = this.store.pipe(select(getIsLoggedIn));
    this.userConversations$ = this.store.pipe(select(getUserConversations));
    this.currentUserData$ = this.store.pipe(select(getUser));
    this.deviceType$ = this.platformService.deviceChanged;
    this.title = {
      title: this.language.getLanguage().USER_CONVERSATION_TITLE,
      subtitle: this.language.getLanguage().USER_CONVERSATION_SUBTITLE
    };
  }
}
