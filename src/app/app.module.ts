import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {MatInputModule} from '@angular/material';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {reducers, metaReducers} from './store/index';
import {environment} from '../environments/environment';
import {AppEffects} from './store/app.effects';
import {CoreModule} from './core/core.module';
import {AppStoreService} from './store/app-store.service';
import {ApiRequestService} from './core/data/api-request.service';
import {RouterModule} from '@angular/router';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {WindowRefService} from './shared/platform/window.service';
import {PlatformService} from './shared/platform';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {LeafletDrawModule} from '@asymmetrik/ngx-leaflet-draw';
import {AngularFireFunctionsModule} from '@angular/fire/functions';
import { NgxLoadingModule } from 'ngx-loading';
import {CentralUserDataModule} from './core/central-data/central-user-data.module';

@NgModule({
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot([AppEffects]),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    CommonModule,
    CoreModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    CentralUserDataModule.forRoot()
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AppStoreService,
    ApiRequestService,
    WindowRefService,
    PlatformService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
