import * as actions from './app.actions';
import {ProgressBarInterface} from '../shared/components/progress-bar/progress-bar.component';


export interface State {
  root: {
    drawerOpened: boolean;
    progressBar: ProgressBarInterface;
  };
}

export const initialState: State = {
  root: {
    drawerOpened: false,
    progressBar: {
      display: false,
      percentage: 0,
      type: 'determine'
    }
  }
};

export function reducer(state = initialState, action: actions.Actions): State {
  switch (action.type) {
    case actions.ActionTypes.ToggleDrawerOpening:
      return {
        ...state,
        root: {
          ...state.root,
          drawerOpened: !state.root.drawerOpened
        }
      };

    case actions.ActionTypes.CloseSideNavbar:
      return {
        ...state,
        root: {
          ...state.root,
          drawerOpened: false
        }
      };

    case actions.ActionTypes.DisplayProgressBar:
      return {
        ...state,
        root: {
          ...state.root,
          progressBar: {
            percentage: action.payload.percentage,
            display: true,
            type: action.payload.type
          }
        }
      };

    case actions.ActionTypes.HideProgressBar:
      return {
        ...state,
        root: {
          ...state.root,
          progressBar: {
            percentage: 0,
            display: false,
            type: 'determine'
          }
        }
      };
    default:
      return state;
  }
}


// Getters
export const getRoot = (state: State) => state.root;
