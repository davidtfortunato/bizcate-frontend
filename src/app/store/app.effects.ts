import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';

import * as appActions from './app.actions';
import {map} from 'rxjs/operators';
import  * as fromAuth from '../auth/store/actions';
import {NavigationLinksService} from '../core/navigation/navigation-links.service';
import {CentralUserDataService} from '../core/central-data/central-user-data.service';

@Injectable()
export class AppEffects {

  @Effect()
  initApp$ = this.actions$.pipe(
    ofType(appActions.ActionTypes.InitApp),
    map(action => {
      this.centralUserDataService.initCentralDataService();

      // Init Auth
      return new fromAuth.Init();
    })
  );

  @Effect({dispatch: false})
  reloadPage$ = this.actions$.pipe(
    ofType(appActions.ActionTypes.ReloadPage),
    map(action => {
      this.navigationService.reloadPage();
    })
  );

  constructor(private actions$: Actions, private navigationService: NavigationLinksService,
              private centralUserDataService: CentralUserDataService) {
  }
}
