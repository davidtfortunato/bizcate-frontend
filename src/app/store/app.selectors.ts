import {createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromAppStore from './app.reducer';

export const selectAppState = createFeatureSelector<fromAppStore.State>('app');

// Getters
export const getRoot = createSelector(selectAppState, fromAppStore.getRoot);
export const getRootIsDrawerOpened = createSelector(selectAppState, getRoot, (root) => root.root.drawerOpened);
export const getProgressBarData = createSelector(selectAppState, getRoot, (root) => root.root.progressBar);
