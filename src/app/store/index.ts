import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';

import { environment } from '../../environments/environment';

// Reducers
import * as fromRoot from './app.reducer';
import * as fromAuth from '../auth/store/reducer';
import * as fromMessages from '../messages/store/reducer';
import * as fromProfessional from '../professional/store/reducer';
import * as fromCreateTask from '../tasks/create/store/reducer';
import * as fromDetailsTask from '../tasks/details/store/reducer';
import * as fromBrowseTasks from '../tasks/browsetasks/store/reducer';
import * as fromCentralUserData from '../core/central-data/store/reducer';

export interface State {
  app: fromRoot.State;
  auth: fromAuth.State;
  messages: fromMessages.State;
  professional: fromProfessional.State;
  createTask: fromCreateTask.State;
  taskDetails: fromDetailsTask.State;
  browseTasks: fromBrowseTasks.State;
  centralData: fromCentralUserData.State;
}

export const reducers: ActionReducerMap<State> = {
  app: fromRoot.reducer,
  auth: fromAuth.reducer,
  messages: fromMessages.reducer,
  professional: fromProfessional.reducer,
  createTask: fromCreateTask.reducer,
  taskDetails: fromDetailsTask.reducer,
  browseTasks: fromBrowseTasks.reducer,
  centralData: fromCentralUserData.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
