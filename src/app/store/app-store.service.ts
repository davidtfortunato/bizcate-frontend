import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';

import * as appActions from './app.actions';
import * as fromRoot from './app.reducer';
import {ProgressBarInterface} from '../shared/components/progress-bar/progress-bar.component';
import {CentralUserData} from '../../../functions/src/data/central-user-data';

@Injectable()
export class AppStoreService {
  constructor(private store: Store<fromRoot.State>) {
  }

  getStore() {
    return this.store;
  }

  initApp() {
    this.store.dispatch(new appActions.InitApp());
  }

  /**
   * Toggle the Drawer Opening
   */
  toggleDrawerOpening() {
    this.store.dispatch(new appActions.ToggleDrawerOpening());
  }

  closeSideNavbar() {
    this.store.dispatch(new appActions.CloseSideNavbar());
  }

  displayProgressBarByData(progressBarData: ProgressBarInterface) {
    this.store.dispatch(new appActions.DisplayProgressBar(progressBarData));
  }

  displayProgressBarById(id: string) {
    this.displayProgressBar('indetermine', -1, id);
  }

  displayProgressBar(type: 'indetermine' | 'determine', percentage?: number, id?: string) {
    this.store.dispatch(new appActions.DisplayProgressBar({
      type, percentage, display: true, id: id
    }));
  }

  hideProgressBar(id?: string) {
    this.store.dispatch(new appActions.HideProgressBar());
  }

  reloadPage() {
    this.store.dispatch(new appActions.ReloadPage());
  }
}
