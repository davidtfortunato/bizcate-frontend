import {Action} from '@ngrx/store';
import {ProgressBarInterface} from '../shared/components/progress-bar/progress-bar.component';
import {CentralUserData} from '../../../functions/src/data/central-user-data';

export enum ActionTypes {
  InitApp = '[App] Init App',
  ToggleDrawerOpening = '[App] Toggle Drawer opening', // Toggle the Drawer to display it and close it
  CloseSideNavbar = '[App] Close Sidenavbar', // Toggle the Drawer to display it and close it

  // Progress Bar
  DisplayProgressBar = '[App] Display Progress bar',
  HideProgressBar = '[App] Hide progress bar',

  // Refresh Page
  ReloadPage = '[App] Refresh page',

  // ACTIONS NOT USED
  NoAction = '[App] ActionNotUsed' // This is useful when we don't have an action to return in a Side Effect
}

export class InitApp implements Action {
  readonly type = ActionTypes.InitApp;
}

export class NoAction implements Action {
  readonly type = ActionTypes.NoAction;
}

export class ToggleDrawerOpening implements Action {
  readonly type = ActionTypes.ToggleDrawerOpening;
}

export class CloseSideNavbar implements Action {
  readonly type = ActionTypes.CloseSideNavbar;
}

export class DisplayProgressBar implements Action {
  readonly type = ActionTypes.DisplayProgressBar;

  constructor(public payload: ProgressBarInterface) {
  }
}

export class HideProgressBar implements Action {
  readonly type = ActionTypes.HideProgressBar;
}

export class ReloadPage {
  readonly type = ActionTypes.ReloadPage;
}

export type Actions = InitApp
  | NoAction
  | ToggleDrawerOpening
  | CloseSideNavbar
  | DisplayProgressBar
  | HideProgressBar
  | ReloadPage;
