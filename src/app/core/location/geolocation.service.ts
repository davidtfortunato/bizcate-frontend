import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppMapLocation} from '../../../../functions/src/data';

@Injectable()
export class GeolocationService {

  getCurrentLocation(): Observable<AppMapLocation> {
    return Observable.create(observer => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position => {
          if (position) {
            observer.next({
              longitude: position.coords.longitude,
              latitude: position.coords.latitude
            });
          } else {
            observer.next({ longitude: -8.41286, latitude: 40.19103});
          }
        }));
      } else {
        alert('Geolocation is not supported by this browser.');
        observer.next(null);
      }
    });
  }
}
