import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {LocationResult} from '../../../../functions/src/data/location-result';

const API_BASE_URL = 'https://www.meteoblue.com/en/server/search/query3?query=';

export interface LocationAPIResponse {
  query: string;
  results: LocationResult[];
}

@Injectable()
export class SearchLocationService {
  constructor(private http: HttpClient) {
  }

  /**
   * Search for location
   */
  searchLocation(query: string): Observable<LocationResult[]> {
    const url = API_BASE_URL + query;
    return this
      .http
      .request('GET', url)
      .pipe(map((response: LocationAPIResponse) => {
        const locationsResponse = [];
        if (response && response.results) {
          response.results.forEach(result => {
            result = {
              ...result,
              postcodes: [] // The post codes are useless
            };
            locationsResponse.push(result);
          });
        }
        return locationsResponse;
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  searchLocationByCoords(lat: number, lng: number): Observable<LocationResult[]> {
    return this.searchLocation(lat + '%20' + lng);
  }
}
