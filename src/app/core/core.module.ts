import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpUrlEncodingCodec} from '@angular/common/http';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';

import {ApiRequestService} from './data/api-request.service';
import {HeaderInterceptor} from './utils/header-interceptor';
import {Logger} from './utils/logger';
import {LocalStorageService} from './data/local-storage-service';
import {AuthModule} from '../auth/auth.module';
import {LanguageService} from './language/language.service';
import {NavigationLinksService} from './navigation/navigation-links.service';
import {StorageApiService} from './api/storage-api.service';
import {API_SERVICE} from './api';
import {GeolocationService} from './location/geolocation.service';
import {SearchLocationService} from './location/search-location.service';
import {UtilsService} from './utils/utils.service';
import {MessagingService} from './messaging/messaging.service';
import {TaskLocationApiService} from './api/task-location-api.service';
import {CentralUserDataModule} from './central-data/central-user-data.module';


@NgModule({
  declarations: [],
  imports: [HttpClientModule,
    AuthModule,
    AngularFireAuthModule,
    AngularFirestoreModule],
  exports: [
    CentralUserDataModule
  ],
  providers: [ApiRequestService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true
    },
    Logger,
    LocalStorageService,
    LanguageService,
    NavigationLinksService,
    StorageApiService,
    GeolocationService,
    SearchLocationService,
    UtilsService,
    MessagingService,
    TaskLocationApiService,
    API_SERVICE]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
      throw new Error('You shall not run!');
    }
  }
}
