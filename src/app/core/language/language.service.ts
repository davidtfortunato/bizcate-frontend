import {Inject, Injectable, LOCALE_ID, OnChanges} from '@angular/core';
import {BaseLanguageModel} from './base-language-model';
import {LocalStorageService} from '../../core/data/local-storage-service';
import {CountryPtPtLanguage} from './country-pt-pt-language';
import {CountryEnEnLanguage} from './country-en-en-language';

@Injectable()
export class LanguageService implements OnChanges {
  private language: BaseLanguageModel;

  constructor(private localStorage: LocalStorageService, @Inject(LOCALE_ID) protected _localeId: string) {
  }

  ngOnChanges() {
    this.initLanguage();
  }

  private initLanguage() {
    // Init Language
    console.log('Locale: ' + this._localeId);
    if (this._localeId.startsWith('pt')) { // includes all Portuguese language
      this.language = new CountryPtPtLanguage();
    } else {
      this.language = new CountryEnEnLanguage();
    }
  }


  get locale(): string {
    return this._localeId;
  }

  /**
   * Get the current language
   */
  getLanguage(): BaseLanguageModel {
    if (!this.language) {
      this.initLanguage();
    }
    return this.language;
  }
}
