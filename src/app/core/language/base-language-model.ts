import {LanguageModelInterface} from './language-model-interface';

export abstract class BaseLanguageModel implements LanguageModelInterface {

  // AUTH
  AUTH_LOGIN_PLACEHOLDER = 'Email';
  abstract AUTH_PASSWORD_PLACEHOLDER = 'Password';
  AUTH_ERROR_PASSWORD_MIN_LENGTH = 'The password should have at least 8 chars';
  AUTH_ERROR_CONFIRMPASSWORD_NOT_SAME = 'The confirm password is not the same';
  AUTH_ERROR_FIELD_REQUIRED = 'This field is required';
  AUTH_ERROR_EMAIL_INVALID = 'The email is not valid. Please, insert a valid email';
  AUTH_ERROR_PROFILE_NOT_COMPLETED = 'You must complete all the form fields to finish your registration';

  AUTH_LOGIN_LABEL = 'Login or register your account';
  AUTH_LOGIN_VALIDATE_ACCOUNT_LABEL = 'Account not validated yet';
  AUTH_LOGIN_VALIDATE_PHONENUMBER_LABEL = 'Validate your phone number';
  AUTH_VALIDATE_EMAIL = 'Please, check your email inbox to validate your account';
  AUTH_VALIDATE_EMAIL_RESEND_VALIDATION = 'Resend email';
  AUTH_VALIDATE_PHONE_NUMBER = 'We need to know what is your phone number';
  AUTH_VALIDATE_PHONE_SEND_SMSCODE = 'Send SMS Code';
  AUTH_VALIDATE_PHONE_SET_SMS_CODE = 'You received an SMS code. Please, set it here to validate your phone number';
  AUTH_VALIDATE_PHONE_SET_SMS_CODE_PLACEHOLDER = 'SMS Code';
  AUTH_VALIDATE_PHONE_SET_VALIDATE_CODE_BTN = 'Validate code';
  AUTH_VALIDATE_PHONE_ERROR_INVALID_CODE = 'The code is invalid. Please, use the code you received on your phone';
  AUTH_VALIDATE_PHONE_ERROR_ALREADY_USED = 'This phone number is already in use. Please, contact our support team: geral@bizcate.com';
  AUTH_REGISTER_LABEL = 'Register';
  AUTH_REGISTER_USERNAME_PLACEHOLDER = 'Username';
  AUTH_REGISTER_NAME_PLACEHOLDER = 'Name';
  AUTH_REGISTER_BIRTHDAY_PLACEHOLDER = 'Birthday';
  AUTH_REGISTER_GENDER_PLACEHOLDER = 'Gender:';
  AUTH_REGISTER_GENDER_MALE_PLACEHOLDER = 'Male';
  AUTH_REGISTER_GENDER_FEMALE_PLACEHOLDER = 'Female';
  AUTH_REGISTER_USERTYPE_PLACEHOLDER = 'User type';
  AUTH_REGISTER_PASSWORD_PLACEHOLDER = 'Confirm password';
  AUTH_REGISTER_ADDRESS_PLACEHOLDER = 'Address';
  AUTH_REGISTER_COUNTRY_PLACEHOLDER = 'Country';
  AUTH_REGISTER_CITY_PLACEHOLDER = 'City';
  AUTH_REGISTER_PHONENUMBER_PLACEHOLDER = 'Phone number';
  AUTH_REGISTER_VATNUMBER_PLACEHOLDER = 'VAT number';
  AUTH_REGISTER_DESCRIPTION_PLACEHOLDER = 'Description';
  AUTH_REGISTER_LINKEDINURL_PLACEHOLDER = 'Linkedin Url';
  AUTH_REGISTER_FACEBOOKURL_PLACEHOLDER = 'Facebook Url';
  AUTH_REGISTER_PROFESSIONAL_EDIT_PROFILE = 'Edit Professional';
  AUTH_REGISTER_PROFESSIONAL_ADD_PROFILE = 'Be a Bizcater';

  AUTH_LOGIN_SUCCESS = 'Welcome [X]';
  AUTH_REGISTERED_SUCCESS = 'Your account was created with success';

  // END AUTH

  // Profile

  USER_PROFILE_SCREEN_TITLE = 'Profile';
  USER_PROFILE_SCREEN_SUBTITLE = 'Edit your profile';
  USER_PROFILE_CONTAINER_TITLE = 'Personal information';
  USER_PROFILE_UPDATED_SUCCESS = 'Your profile was updated with success';

  // END PROFILE

  // Professional Profile

  PROFESSIONAL_PROFILE_SCREEN_TITLE = 'Profile';
  PROFESSIONAL_PROFILE_SCREEN_SUBTITLE = 'Edit professional profile';
  PROFESSIONAL_PROFILE_FORM_TITLE = 'Professional information';
  PROFESSIONAL_PROFILE_FORM_SUBTITLE = 'You should set your professional information here. This info will be used in your professional ' +
    'profile.';
  PROFESSIONAL_PROFILE_LOCATION_TITLE = 'Work location';
  PROFESSIONAL_PROFILE_LOCATION_SUBTITLE = 'Define your work location. This will allow you to receive tasks from your work area.';
  PROFESSIONAL_PROFILE_REMOTEWORK_LABEL = 'I accept remote tasks';

  PROFESSIONAL_PROFILE_SKILLS_TITLE = 'Professional Skills';
  PROFESSIONAL_PROFILE_SKILLS_SUBTITLE = 'Set your best skills. This is the way clients will find you to do their tasks. And also to you ' +
    'to filter the tasks that probably you are interested to work in.';
  PROFESSIONAL_PROFILE_SKILLS_EMPTY = 'You don\' have any skill selected yet. Add your best skills to attract new customers';

  PROFESSIONAL_PROFILE_PORTFOLIO_TITLE = 'Portfolio';
  PROFESSIONAL_PROFILE_PORTFOLIO_SUBTITLE = 'Show to your possible customers what you already did. This is a good way to attract new ' +
    'customers.';
  PROFESSIONAL_PROFILE_PORTFOLIO_ADD_PORTFOLIO = 'Add portfolio';

  PROFESSIONAL_PROFILE_LABEL_CATEGORY = 'Category';
  PROFESSIONAL_PROFILE_LABEL_SKILL = 'Skill';
  PROFESSIONAL_PROFILE_LABEL_SKILL_LEVEL = 'Years exp.';
  PROFESSIONAL_PROFILE_PRICE_HOURLY = 'Price hourly rate';

  // END Professional Profile

  // MESSAGES

  USER_CONVERSATIONS_EMPTY = 'You don\'t have any conversation started';
  USER_CONVERSATION_MESSAGES_EMPTY = 'You don\'t have any message sent to this user. Send him a message now';
  USER_CONVERSATION_PLACEHOLDER_WRITE_A_MESSAGE = 'Write your message';
  USER_CONVERSATION_TITLE = 'Conversations';
  USER_CONVERSATION_SUBTITLE = 'Your messages';

  // END MESSAGES

  // GENERIC
  GENERICE_SUPPORT_EMAIL = 'geral@bizcate.com';
  GENERIC_SAVE_LABEL = 'Save';
  GENERIC_CANCEL_LABEL = 'Cancel';
  GENERIC_ERROR_SAVING = 'Failed on saving data';
  GENERIC_UPLOAD_PHOTO = 'Upload photo';
  GENERIC_SAVED_SUCCESS = 'Saved with success';
  GENERIC_DELETED_SUCCESS = 'Deleted with success';
  GENERIC_SEND_MESSAGE = 'Send message';
  GENERIC_REQUEST_QUOTE = 'Request quote';
  GENERIC_SHARE_TITLE = 'Share';
  GENERIC_TIME_HOURS = 'hours';
  GENERIC_TIME_HOUR = 'hour';
  GENERIC_TODAY = 'Today';
  GENERIC_PERIOD_MORNING = 'Morning';
  GENERIC_PERIOD_AFTERNOON = 'Afternoon';
  GENERIC_PERIOD_EVENING = 'Evening';
  GENERIC_SELECTED = 'Selected';
  GENERIC_AVAILABLE = 'Available';
  GENERIC_SCHEDULE_TASK_EXECUTION = 'Schedule execution';
  GENERIC_NOT_AVAILABLE_SHORT = 'N/A';
  GENERIC_PUBLISH = 'Publish';
  GENERIC_PUBLISH_SUCCESS = 'Published with success';
  GENERIC_EMPTY_LIST = 'This list is empty';
  GENERIC_ADD_TASK_BUTTON = 'Add task';
  GENERIC_LOGOUT = 'LOGOUT';
  GENERIC_LOGIN = 'LOGIN';
  GENERIC_PRO_LABEL = 'Pro';
  GENERIC_CONTINUE = 'Continue';
  GENERIC_BACK = 'Back';

  // GENERIC FORM
  GENERIC_FORM_TITLE = 'Title';
  GENERIC_FORM_DESCRIPTION = 'Description';
  GENERIC_FORM_LINK = 'Link';
  GENERIC_FORM_PUBLISHED_DATE = 'Published Date';
  GENERIC_FORM_ERROR_FIELD_REQUIRED = 'This field is required';

  // END GENERIC

  // GENERIC ERRORS
  ERROR_GENERIC_FIELD_REQUIRED = 'This field is required';
  ERROR_GENERIC_UPLOAD_PHOTO = 'Uploading photo failed. Please, try again';
  ERROR_FORM_INVALID = 'Some field in the form is invalid';
  ERROR_GENERIC_REQUEST = 'Your request failed';
  ERROR_GENERIC_SAVING_FAILED = 'Failed saving your data';
  ERROR_GENERIC_DELETED_FAILED = 'Failed deleting this document';
  ERROR_GENERIC_NOT_FOUND = 'The page you\'re looking for was not found';
  ERROR_GENERIC_OPENING_PAGE = 'We can\'t open the requested page';
  // END GENERIC ERRORS

  // Generic Toast Messages
  TOAST_MESSAGE_SAVED_SUCCESS = 'Saved with success';

  // SIDENAV BAR Option
  SIDENAV_BAR_OPTION_HOME = 'Home';
  SIDENAV_BAR_OPTION_PROFILE = 'Profile';
  SIDENAV_BAR_OPTION_PROFESSIONAL_PROFILE = 'Professional';
  SIDENAV_BAR_OPTION_MESSAGES = 'Messages';
  SIDENAV_BAR_OPTION_BROWSE_TASKS = 'Browse Tasks';
  SIDENAV_BAR_OPTION_MY_TASKS = 'My Tasks';
  SIDENAV_BAR_OPTION_MY_PROFESSIONAL_TASKS = 'My Offers';

  // END SIDENAV BAR OPTION

  // PUBLIC PROFILE
  PUBLIC_PROFILE_SOCIAL_NETWORKS_TITLE = 'Social networks';
  PUBLIC_PROFILE_SKILLS_TITLE = 'Skills';
  PUBLIC_PROFILE_PORTFOLIO_TITLE = 'Portfolio';
  PUBLIC_PROFILE_USER_REVIEWS_TITLE = 'User Reviews';
  PUBLIC_PROFILE_USER_INFOS_TITLE = 'User details';
  PUBLIC_PROFILE_PROFESSIONAL_INFOS_TITLE = 'Professional details';

  // TASKS
  TASKS_MODULE_TITLE = 'Tasks';
  TASK_MODULE_CREATE_SUBTITLE = 'Create new task';
  TASKS_CREATE_HEADER_BUTTON_CREATE_TASK = 'Create task';
  TASK_CREATE_DETAILS_TITLE = 'Details of your task';
  TASK_CREATE_DETAILS_SUBTITLE = 'Set the details of your task with the title and the description. Then add the skills the professional ' +
    'needs to have to perform the task';
  TASK_CREATE_DETAILS_TASK_TITLE = 'What do you need done?';
  TASK_CREATE_DETAILS_TASK_DESCRIPTION = 'Describe as much as possible the task that you want to be done';
  TASK_CREATE_DETAILS_TASK_SKILLS_TITLE = 'Skills for the task';
  TASK_CREATE_LOCATION_TITLE = 'Where';
  TASK_CREATE_LOCATION_SUBTITLE = 'Tell the professional where is supposed to execute the task. use the icons on the right to ' +
    'draw the area where the task will be done.';
  TASK_CREATE_BUDGET_TITLE = 'Budget and when to do';
  TASK_CREATE_BUDGET_SUBTITLE = 'Setup the budget you are expecting to spend in this task. Also, say to the professional when is the best' +
    ' time and day to execute the task.';
  TASK_CREATE_UPLOAD_FILES_TITLE = 'Upload photos';
  TASK_CREATE_UPLOAD_FILES_SUBTITLE = 'Upload relevant photos to help the professional to understand ' +
    'better what is suppose to do';
  TASK_CREATE_REMOTE_WORK = 'This task can be done remotely?';
  TASK_CREATE_SEARCH_LOCATION = 'Search other location';
  TASK_CREATE_BUDGET_TYPE_HOURLY = 'Hourly';
  TASK_CREATE_BUDGET_TYPE_TOTAL = 'Total';
  TASK_CREATE_PLACEHOLDER_BUDGET_VALUE = 'Set the expected budget';
  TASK_CREATE_PLACEHOLDER_DUEDATE = 'When do you want this task to be done';

  TASK_DETAILS_TITLE = 'Details';
  TASK_DETAILS_NOT_FOUND = 'We didn\'t found the task you\'re looking for. Maybe was already deleted';
  TASK_DETAILS_STATUS_OPEN = 'Open';
  TASK_DETAILS_STATUS_ASSIGNED = 'Assigned';
  TASK_DETAILS_STATUS_DONE = 'Completed';
  TASK_DETAILS_STATUS_EXPIRED = 'Expired';
  TASK_DETAILS_STATUS_IN_PROGRESS = 'In Progress';
  TASK_DETAILS_SCHEDULE_TITLE = 'Schedule task execution';
  TASK_DETAILS_SCHEDULE_AVAILABILITY_PHASE1 = 'Select availability';
  TASK_DETAILS_SCHEDULE_SCHEDULE_DAY_PHASE2 = 'Schedule day';
  TASK_DETAILS_SCHEDULE_SHOW_CALENDAR = 'Show on calendar';
  TASK_DETAILS_SCHEDULE_SCHEDULED_PHASE3 = 'Scheduled';
  TASK_DETAILS_SCHEDULE_PHASE1_CLIENT_DESCRIPTION = 'Select the time periods you want that the task execution happen, for the ' +
    'professional choose one of them to execute the task';
  TASK_DETAILS_SCHEDULE_PHASE1_PROFESSIONAL_DESCRIPTION = 'Client is now selecting his availability to execute the task. You will be ' +
    'notified after client select his availability for then you choose which one fits you better.';
  TASK_DETAILS_SCHEDULE_PHASE2_CLIENT_DESCRIPTION = 'Professional is now choosing the date and time to execute the task. You will be ' +
    'notified when it\'s ready';
  TASK_DETAILS_SCHEDULE_PHASE2_PROFESSIONAL_DESCRIPTION = 'Choose the time and date that you want to execute the task and define the ' +
    'time will take to execute the task';
  TASK_DETAILS_SCHEDULE_PHASE3_DESCRIPTION = 'The task is scheduled for this date.';
  TASK_DETAILS_SCHEDULE_DURATION = 'Duration: [HOURS] hours';
  TASK_DETAILS_POSTEDBY = 'Posted by';
  TASK_DETAILS_POSTED_AT = 'Posted at ';
  TASK_DETAILS_DUEDATE_NOT_DEFINED = 'Doesn\'t have a specific due date for this task';
  TASK_DETAILS_DUEDATE = 'Due date';
  TASK_DETAILS_LISTSKILLS = 'List skills required';
  TASK_DETAILS_DESCRIPTION = 'Description';
  TASK_DETAILS_BUDGET_TITLE = 'Task budget';
  TASK_DETAILS_BUDGET_MAKE_OFFER = 'Make an offer';
  TASK_DETAILS_BUDGET_PER_HOUR = 'hour';
  TASK_DETAILS_LOCATION_TITLE = 'Task location';
  TASK_DETAILS_LOCATION_REMOTE_WORK = 'This work can be done remotely';
  TASK_DETAILS_PHOTOS_TITLE = 'Task photos';
  TASK_DETAILS_TASK_REVIEWS = 'Task Reviews';
  TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WITHOUT_CLIENT = 'The task is finished, now is time to give your review your work and how ' +
    'your client handled the task execution.';
  TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WITH_CLIENT = 'The task is finished and your client already published a review about you. Review' +
    'your client too to let you see your review';
  TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WAITING_CLIENT = 'Thank you to have published your review. Your review will only be visible to ' +
    'the client after his review';
  TASK_DETAILS_REVIEW_HINT_CLIENT_WITHOUT_PROFESSIONAL = 'The task is finished. Is now time to give your review about the works done by ' +
    'the professional you hired.';
  TASK_DETAILS_REVIEW_HINT_CLIENT_WITH_PROFESSIONAL = 'The task is finished and the professional already published a review about you. ' +
    'Review the professional too to let you see your review';
  TASK_DETAILS_REVIEW_HINT_CLIENT_WAITING_PROFESSIONAL = 'Thank you to have published your review. Your review will only be visible to ' +
    'the professional after his review';
  TASK_DETAILS_REVIEW_HINT_ALL_REVIEWED = '';
  TASK_DETAILS_REVIEW_BTN_POST_REVIEW = 'Post your review';
  TASK_DETAILS_REVIEW_FORM_MESSAGE = 'Write your review about the execution of the task';
  TASK_DETAILS_REVIEW_LEVEL_EVALUATION = 'Evaluate your experience';

  TASK_OFFER_MAKE_AN_OFFER_TITLE = 'Make an offer';
  TASK_OFFER_OFFER_VALUE = 'Offer budget';
  TASK_OFFER_WRITE_A_MESSAGE = 'Message';
  TASK_OFFER_WRITE_A_MESSAGE_HINT = 'Write a message to your client. A good message can increase the trust with the client..';
  TASK_OFFER_ERROR_TASK_NOT_OPEN = 'The task isn\'t open anymore';
  TASK_OFFER_ERROR_TASK_DOESNT_EXIST = 'The task doesn\'t exist';
  TASK_OFFER_SUCCESS_SENT = 'Your offer was sent';
  TASK_OFFER_TITLE = 'Offers';
  TASK_OFFER_ASSIGNEE_TITLE = 'Offer Assignee';
  TASK_OFFER_BTN_ACCEPT_LABEL = 'Accept offer';
  TASK_OFFER_BTN_REJECT_LABEL = 'Reject offer';
  TASK_OFFER_BTN_REMOVE_LABEL = 'Remove my offer';
  TASK_OFFER_ITEM_VALUE_OFFERED = 'Quote: ';

  TASK_LIST_TITLE = 'Browse tasks';
  TASK_BROWSE_SEARCH_AREA_BTN = 'Search in this location';
  TASK_BROWSE_FILTER_BTN = 'Filter';
  TASK_BROWSE_FILTER_DIALOG = 'Filter your search';
  TASK_BROWSE_FILTER_TITLE_LOCATION = 'Location';
  TASK_BROWSE_FILTER_TITLE_PRICE_RANGE = 'Price range';
  TASK_BROWSE_FILTER_TITLE_STATUS = 'Status';
  TASK_BROWSE_FILTER_TITLE_WORKABLE_AREAS = 'Just my workable areas';
  TASK_BROWSE_FILTER_TITLE_TASK_SKILLS = 'Required skills';
  TASK_BROWSE_FILTER_ONLY_MY_SKILLS = 'Only with my skills';
  TASK_BROWSE_FILTER_LOCATION_REMOTE = 'Remote';
  TASK_BROWSE_FILTER_LOCATION_PERSON = 'Personally';
  TASK_BROWSE_FILTER_LOCATION_ALL = 'All';
  TASK_BROWSE_EMPTY_LIST = 'No tasks found with the current filters';

  // MY TASKS
  MY_TASKS_MODULE_TITLE = 'My Tasks';
  MY_TASKS_SWITCH_TITLE_CHANGE_STATUS = 'Select task status to filter the list';

  // MY PROFESSIONAL TASKS
  MY_PROFESSIONAL_TASKS_SUBTITLE = 'Professional tasks';
  MY_PROFESSIONAL_TASKS_SWITCH_TITLE_CHANGE_STATUS = 'Select offer status to filter the list';
  MY_PROFESSIONAL_TASKS_SWITCH_PENDING_OFFERS = 'Pending response';
  MY_PROFESSIONAL_TASKS_SWITCH_ACCEPTED_OFFERS_PENDING_SCHEDULE = 'Accepted and waiting to schedule';
  MY_PROFESSIONAL_TASKS_SWITCH_SCHEDULED_TASKS = 'Accepted and scheduled';
  MY_PROFESSIONAL_TASKS_SWITCH_COMPLETED_TASKS = 'Finished tasks';

  // DASHBOARD
  DASHBOARD_MODULE_TITLE = 'Dashboard';
  DASHBOARD_MODULE_SUBTITLE_CLIENT = 'Client';
  DASHBOARD_MODULE_SUBTITLE_PROFESSIONAL = 'Professional';
  DASHBOARD_TITLE_MY_OPEN_TASKS = 'My open tasks';
  DASHBOARD_TITLE_MY_COMPLETED_TASKS = 'My completed tasks';
  DASHBOARD_TITLE_RECEIVED_OFFERS = 'Received offers';
  DASHBOARD_TITLE_RECEIVED_MESSAGES = 'Recent messages';
  DASHBOARD_TITLE_MY_PENDING_OFFERS_SENT = 'My pending offers';
  DASHBOARD_TITLE_MY_ASSIGNED_TASKS = 'My assigned tasks';

  // Create Task Initial
  CREATE_TASK_INITIAL_TITLE = 'Book your next task';
  CREATE_TASK_INITIAL_SUBTITLE = 'Tell us about your task. We use these details to show Bizcaters in your area who fit your needs';
  CREATE_TASK_WHAT_YOU_NEED = 'Tell us what you need?';
  CREATE_TASK_START_CREATE_BTN = 'Continue task creation';
  CREATE_TASK_STEP1_DESCRIPTION = 'Task description';
  CREATE_TASK_STEP2_BROWSE_BIZCATERS = 'Browse for Bizcaters';
  CREATE_TASK_STEP3_REVIEW_TASK_DETAILS = 'Review task details';
  CREATE_TASK_STEP1_HEADER_TITLE = 'Describe your task';
  CREATE_TASK_STEP1_HEADER_SUBTITLE = 'Now it\'s time to you give us some more details to we start looking for the right Bizcaters for your task';
  CREATE_TASK_STEP1_MIDDLE_TITLE_LOCATION = 'Location of your task';
  CREATE_TASK_STEP1_DURATION_TITLE = 'Estimated duration of your task in hours?';
  CREATE_TASK_STEP1_DURATION_FORM_PLACEHOLDER = 'How many hours you think it will take?';
  CREATE_TASK_STEP1_DURATION_FORM_HINT = 'X hours';
  CREATE_TASK_STEP1_DESCRIPTION_TITLE = 'Tell us the details of your task';
  CREATE_TASK_STEP1_DESCRIPTION_PLACEHOLDER = 'Describe exactly what you need to be done';
  CREATE_TASK_STEP1_DESCRIPTION_HINT = 'Ex: I need to clean my small apartment with 2 rooms.';
  CREATE_TASK_STEP1_DESCRIPTION_FIELD_EXPLANATION = 'Start the conversation and tell your Bizcater what you need done. This helps us to show you only qualified and available Bizcaters for the job.';
  CREATE_TASK_STEP1_LOCATION_REMOTE = 'It\'s a remote task?';

}
