import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as actions from './actions';
import {map, withLatestFrom} from 'rxjs/operators';
import {NoAction} from '../../../store/app.actions';
import {CentralUserDataService} from '../central-user-data.service';
import {select} from '@ngrx/store';
import {AppStoreService} from '../../../store/app-store.service';
import {getCentralUserData} from './selectors';
import {CentralUserDataApiService} from '../central-user-data-api.service';
import {APIUpdateCentralUserData, UpdateCreatingTaskData} from './actions';

@Injectable()
export class Effects {

  @Effect()
  initAuth$ = this.actions$.pipe(
    ofType(actions.ActionTypes.InitCentralUserData),
    map(action => {
      // Init CentralUserData
      this.centralUserDataService.initCentralDataService();
      return new NoAction();
    })
  );

  @Effect({dispatch: false})
  apiUpdateCentralUserData = this.actions$.pipe(
    ofType<APIUpdateCentralUserData>(actions.ActionTypes.APIUpdateCentralUserData),
    map((action) => {
      this.centralDataAPIService.updateCentralUserData(action.payload);
      return new NoAction();
    })
  );

  @Effect()
  updateCreatingTask = this.actions$.pipe(
    ofType<UpdateCreatingTaskData>(actions.ActionTypes.UpdateCreatingTaskData),
    withLatestFrom(this.appStoreService.getStore().pipe(select(getCentralUserData))),
    map(([action, centralUserData]) => {
      return new APIUpdateCentralUserData({
        ...centralUserData,
        taskCreationPending: action.payload
      });
    })
  );


  constructor(private actions$: Actions,
              private centralUserDataService: CentralUserDataService,
              private appStoreService: AppStoreService,
              private centralDataAPIService: CentralUserDataApiService) {

  }
}
