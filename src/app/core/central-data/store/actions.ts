import {CentralUserData} from '../../../../../functions/src/data/central-user-data';
import {TaskCreationModel} from '../../../../../functions/src/data/task-creation-model';

export enum ActionTypes {
  // Central User Data
  InitCentralUserData = '[CentralUserData] Init central user data',
  UpdateCentralUserData = '[CentralUserData] Update Central User Data',
  CleanCentralUserData = '[CentralUserData] Clean Central User Data',
  APIUpdateCentralUserData = '[CentralUserData] API Update Central User Data',

  // Creating Task
  UpdateCreatingTaskData = '[CentralUserData] Update Creating Task Data',
  CleanCreatingTaskData = '[CentralUserData] Clean Creating Task Data'
}

export class InitCentralUserData {
  readonly type = ActionTypes.InitCentralUserData;
}

export class UpdateCentralUserData {
  readonly type = ActionTypes.UpdateCentralUserData;

  constructor(public payload: CentralUserData) {
  }
}

export class APIUpdateCentralUserData {
  readonly type = ActionTypes.APIUpdateCentralUserData;

  constructor(public payload: CentralUserData) {
  }
}

export class CleanCentralUserData {
  readonly type = ActionTypes.CleanCentralUserData;
}

// Creating task data
export class UpdateCreatingTaskData {
  readonly type = ActionTypes.UpdateCreatingTaskData;

  constructor(public payload: TaskCreationModel) {
  }
}

export class CleanCreatingTaskData {
  readonly type = ActionTypes.CleanCreatingTaskData;
}

export type Actions = InitCentralUserData
  | UpdateCentralUserData
  | CleanCentralUserData
  | APIUpdateCentralUserData
  | UpdateCreatingTaskData
  | CleanCreatingTaskData;
