import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromStore from './reducer';
import {TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

export const selectCentralDataState = createFeatureSelector<fromStore.State>('centralData');

// Getters
export const getCentralUserData = createSelector(selectCentralDataState, fromStore.getCentralUserData, centralData => {
  return centralData ? centralData.centralUserData : null;
});


// Client Getters
// Get Client Tasks created
export const getClientMyTasks = createSelector(getCentralUserData, centralUserData => {
  if (centralUserData
    && centralUserData.clientData
    && centralUserData.clientData.myTasks) {
    return centralUserData.clientData.myTasks;
  } else {
    return [];
  }
});

// Get Client Offers
export const getClientMyOffers = createSelector(getCentralUserData, centralUserData => {
  if (centralUserData && centralUserData.clientData && centralUserData.clientData.myOffers) {
    return centralUserData.clientData.myOffers;
  } else {
    return [];
  }
});

// Get Client Open Tasks
export const getClientMyOpenedTasks = createSelector(getClientMyTasks, (tasks) => {
  if (tasks && tasks.length > 0) {
    return tasks.filter((task: TaskModel) => task.status !== TaskStatus.DONE);
  } else {
    return [];
  }
});

// Get Client Open Tasks
export const getClientMyPendingTasks = createSelector(getClientMyTasks, (tasks) => {
  if (tasks && tasks.length > 0) {
    return tasks.filter((task: TaskModel) => task.status === TaskStatus.OPEN);
  } else {
    return [];
  }
});

// Get Client Open Tasks
export const getClientMyAssignedTasks = createSelector(getClientMyTasks, (tasks) => {
  if (tasks && tasks.length > 0) {
    return tasks.filter((task: TaskModel) => task.status === TaskStatus.ASSIGNED);
  } else {
    return [];
  }
});

// Get Client Completed Tasks
export const getClientMyDoneTasks = createSelector(getClientMyTasks, (tasks) => {
  if (tasks && tasks.length > 0) {
    return tasks.filter((task: TaskModel) => task.status === TaskStatus.DONE);
  } else {
    return [];
  }
});

// Professional Getters
export const getProfessionalOffersSent = createSelector(getCentralUserData, centralUserData => {
  if (centralUserData && centralUserData.professionalData) {
    return centralUserData.professionalData.myOffersSent;
  } else {
    return [];
  }
});

// Get Pending offers sent
export const getProfessionalOffersPending = createSelector(getProfessionalOffersSent, (offers: TaskOfferModel[]) => {
  if (offers) {
    return offers.filter(offer => offer.taskData.status === TaskStatus.OPEN);
  } else {
    return [];
  }
});

// Get Professional Tasks assigned
export const getProfessionalAssignedOffers = createSelector(getProfessionalOffersSent, (offers: TaskOfferModel[]) => {
  if (offers) {
    return offers.filter(offer => offer.taskData.status === TaskStatus.ASSIGNED);
  } else {
    return [];
  }
});

// Get Professional Tasks Done
export const getProfessionalFinishedOffers = createSelector(getProfessionalOffersSent, (offers: TaskOfferModel[]) => {
  if (offers) {
    return offers.filter(offer => offer.taskData.status === TaskStatus.DONE);
  } else {
    return [];
  }
});


export const getProfessionalAssignedTasks = createSelector(getProfessionalAssignedOffers, (offers: TaskOfferModel[]) => {
  if (offers) {
    const tasks: TaskModel[] = [];
    offers.forEach(offer => tasks.push(offer.taskData));
    return tasks;
  } else {
    return [];
  }
});


// Get Professional Offers accepted and pending to schedule
export const getAcceptedWaitingScheduleTasks = createSelector(getProfessionalAssignedOffers, (offers: TaskOfferModel[]) => {
  if (offers) {
    console.log('getAcceptedWaitingScheduleTasks: ' + offers.length);
    return offers.filter(offer => !offer.taskData.schedule);
  } else {
    return [];
  }
});

// Get Professional Offers accepted and pending to schedule
export const getAcceptedScheduledTasks = createSelector(getProfessionalAssignedOffers, (offers: TaskOfferModel[]) => {
  if (offers) {
    return offers.filter(offer => offer.taskData.schedule);
  } else {
    return [];
  }
});
