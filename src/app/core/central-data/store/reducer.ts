import {CentralUserData} from '../../../../../functions/src/data/central-user-data';
import {Actions, ActionTypes} from './actions';

export interface State {
  centralUserData: CentralUserData;
}

export const initialState: State = getInitState();

export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.UpdateCentralUserData:
      return {
        ...state,
        centralUserData: action.payload
      };
    case ActionTypes.CleanCentralUserData:
      return {
        centralUserData: null
      };
    case ActionTypes.UpdateCreatingTaskData:
      return {
        ...state,
        centralUserData: {
          ...state.centralUserData,
          taskCreationPending: action.payload
        }
      };
    case ActionTypes.CleanCreatingTaskData:
      return {
        ...state,
        centralUserData: {
          ...state.centralUserData,
          taskCreationPending: null
        }
      };
    default:
      return state;
  }
}

function getInitState(): State {
  return {
    centralUserData: null
  };
}

// Pure Functions
export const getCentralUserData = (state: State) => state.centralUserData;
