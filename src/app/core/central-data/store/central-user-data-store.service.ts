import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../store/app.reducer';
import {CentralUserData} from '../../../../../functions/src/data/central-user-data';
import {CleanCentralUserData, CleanCreatingTaskData, UpdateCentralUserData, UpdateCreatingTaskData} from './actions';
import {TaskCreationModel} from '../../../../../functions/src/data/task-creation-model';

@Injectable()
export class CentralUserDataStoreService {
  constructor(private store: Store<fromRoot.State>) {
  }

  updateCentralUserData(centralUserData: CentralUserData) {
    this.store.dispatch(new UpdateCentralUserData(centralUserData));
  }

  cleanCentralUserData() {
    this.store.dispatch(new CleanCentralUserData());
  }

  updateCreatingTaskData(taskData: TaskCreationModel) {
    this.store.dispatch(new UpdateCreatingTaskData(taskData));
  }

  cleanCreatingTaskData() {
    this.store.dispatch(new CleanCreatingTaskData());
  }

  getStore() {
    return this.store;
  }
}
