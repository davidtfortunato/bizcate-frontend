import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {AuthUserModel} from '../../../../functions/src/data';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getUser} from '../../auth/store/selectors';
import {CentralUserDataApiService} from './central-user-data-api.service';
import {AppStoreService} from '../../store/app-store.service';
import {CentralUserData} from '../../../../functions/src/data/central-user-data';
import {CentralUserDataStoreService} from './store/central-user-data-store.service';

@Injectable()
export class CentralUserDataService {

  userData$: Observable<AuthUserModel>;

  // Subcribed data
  userDataReceived: AuthUserModel;

  constructor(private afs: AngularFirestore,
              private store: State<fromStore.State>,
              private centralUserDataApi: CentralUserDataApiService,
              private centralUserDataStore: CentralUserDataStoreService) {

  }

  initCentralDataService() {
    if (!this.userData$) {
      this.userData$ = this.store.pipe(select(getUser));
      this.userData$.subscribe((userData: AuthUserModel) => {
        if (userData && userData.uid && !userData.isAnonymous) {
          if (!this.userDataReceived || this.userDataReceived.uid !== userData.uid) {
            this.userDataReceived = userData;
            console.log('On userdata received: ' + userData.uid);
            this.centralUserDataApi.getCentralUserData(userData.uid).onSnapshot(centralUserData => {
              if (centralUserData && !centralUserData.empty) {
                // Update store state with the central user data
                const centralUserDataDoc: CentralUserData = {
                  ...centralUserData.docs[0].data() as CentralUserData,
                  docId: centralUserData.docs[0].id
                };
                this.centralUserDataStore.updateCentralUserData(centralUserDataDoc);
              } else {
                this.centralUserDataStore.cleanCentralUserData();
              }
            }, err => {
              console.log(`Encountered error: ${err}`);
            });
          }
        }
        else {
          this.centralUserDataStore.cleanCentralUserData();
        }
      });
    }
  }
}
