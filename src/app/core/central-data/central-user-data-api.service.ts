import {Injectable} from '@angular/core';
import {AngularFirestore, DocumentData} from '@angular/fire/firestore';
import {FirestoreCollectionNames} from '../../../../functions/src/data/index';
import {TaskCreationModel} from '../../../../functions/src/data/task-creation-model';
import {CentralUserData} from '../../../../functions/src/data/central-user-data';

@Injectable()
export class CentralUserDataApiService {

  constructor(private afs: AngularFirestore) {
  }

  getCentralUserData(uid: String) {
    return this.afs.firestore.collection(FirestoreCollectionNames.centralUserData).where('uid', '==', uid);
  }

  updateCentralUserData(centralUserData: CentralUserData) {
    if (centralUserData) {
      if (centralUserData.docId) {
        return this
          .afs
          .firestore
          .collection(FirestoreCollectionNames.centralUserData).doc(centralUserData.docId).set(centralUserData as DocumentData);
      }
    }
  }
}
