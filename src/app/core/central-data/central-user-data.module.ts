import {ModuleWithProviders, NgModule} from '@angular/core';
import {CentralUserDataApiService} from './central-user-data-api.service';
import {CentralUserDataService} from './central-user-data.service';
import {EffectsModule} from '@ngrx/effects';
import {Effects} from './store/effects';
import {CentralUserDataStoreService} from './store/central-user-data-store.service';

@NgModule({
  providers: [
    CentralUserDataApiService,
    CentralUserDataService,
    CentralUserDataStoreService],
  declarations: [],
  imports: [
    EffectsModule.forFeature([Effects])
  ]
})
export class CentralUserDataModule {
  static forRoot(): ModuleWithProviders {
    return {ngModule: CentralUserDataModule, providers: []};
  }
}
