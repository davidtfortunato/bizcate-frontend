import {Injectable} from '@angular/core';
import {ApiRequestService} from '../data/api-request.service';
import {Observable, Subject} from 'rxjs';
import {ApiResponse, getQuickResponse, UserProfessionalProfile} from '../../../../functions/src/data/index';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {UserProfileModel, UserProfileRoleType} from '../../../../functions/src/data/user-model';
import {AuthAPIService} from './auth-api.service';
import {AuthStoreService} from '../../auth/store/auth-store.service';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {FirestoreCollectionNames} from '../../../../functions/src/data/index';
import {AngularFireStorage} from '@angular/fire/storage';
import {first, map} from 'rxjs/operators';
import {UploadTask} from '@angular/fire/storage/interfaces';

@Injectable()
export class UserAPIService {
  // API Paths
  private static BASE_CONTEXT_URL = '/profile';
  private static PROFILE_UPDATE = '/update';


  private userProfileCollectionRef: AngularFirestoreCollection<UserProfileModel>;


  constructor(private apiService: ApiRequestService,
              private afs: AngularFirestore,
              private authService: AuthStoreService,
              private db: AngularFireDatabase,
              private afAuth: AngularFireAuth) {
    this.userProfileCollectionRef = this.afs.collection(FirestoreCollectionNames.userProfile);
  }

  getUser() {
    return this.afAuth.user.pipe(first()).toPromise();
  }

  setProfessionalProfileID(professionalId: string) {
    return Observable.create(observer => {
      this.afs.collection(FirestoreCollectionNames.userProfile)
        .ref
        .where('uid', '==', this.afAuth.auth.currentUser.uid)
        .get()
        .then((queryResponse) => {
          if (!queryResponse.empty) {
            const profileData: UserProfileModel = queryResponse.docs[0].data() as UserProfileModel;
            profileData.professionalProfileId = professionalId;
            this.userProfileCollectionRef.doc(queryResponse.docs[0].id).update(profileData)
              .then(value => {
                observer.next(getQuickResponse(true, profileData));
              }, err => {
                observer.next(getQuickResponse(false, err));
              });
          }
        });
    });
  }

  /**
   * PATCH /profile/update
   * @param profileData Body to send to the api
   */
  patchSaveProfile(profileData: UserProfileModel): Observable<ApiResponse<any>> {
    const response = new Subject<ApiResponse<any>>();
    this.afs.collection(FirestoreCollectionNames.userProfile)
      .ref
      .where('uid', '==', this.afAuth.auth.currentUser.uid)
      .get()
      .then((queryResponse) => {
        if (!queryResponse.empty) {
          this.userProfileCollectionRef.doc(queryResponse.docs[0].id)
            .update(profileData)
            .then(value => {
              response.next({
                responseCode: 4000,
                responseData: profileData,
                isSuccess: true
              });
            }, err => {
              response.next({
                responseCode: 1000,
                responseData: err,
                isSuccess: false
              });
            });
        } else {
          // Should create new user profile
          profileData.uid = this.afAuth.auth.currentUser.uid;
          this
            .afs
            .collection<UserProfileModel>(FirestoreCollectionNames.userProfile)
            .add(profileData).then(addProfileResponse => {
            response.next({
              responseCode: 4000,
              responseData: profileData,
              isSuccess: true
            });
          }).catch(err => response.next({
            responseCode: 1000,
            responseData: err,
            isSuccess: false
          }));
        }
      });

    return response;
  }

  getProfileData(uid): Observable<UserProfileModel> {
    console.log('Get user: ' + uid);
    return Observable.create(observer => {
      this.afs.collection(FirestoreCollectionNames.userProfile)
        .ref
        .where('uid', '==', uid)
        .get()
        .then(response => {
          if (response && !response.empty) {
            const userProfile: UserProfileModel = response.docs[0].data() as UserProfileModel;
            userProfile.id = response.docs[0].id;
            observer.next(userProfile);
          } else {
            observer.next(null);
          }
        }).catch(err => observer.next(null));
    });
  }

  getProfessionalProfileData(uid): Observable<UserProfessionalProfile> {
    console.log('Get user: ' + uid);
    return Observable.create(observer => {
      this.afs.collection(FirestoreCollectionNames.userProfessionalProfile)
        .ref
        .where('uid', '==', uid)
        .get()
        .then(response => {
          if (response && !response.empty) {
            const userProfessionalProfile: UserProfessionalProfile = response.docs[0].data() as UserProfessionalProfile;
            userProfessionalProfile.id = response.docs[0].id;
            observer.next(userProfessionalProfile);
          } else {
            observer.next(null);
          }
        }).catch(err => observer.next(null));
    });
  }

  getUserProfile(): Observable<UserProfileModel> {
    return Observable.create(observer => {
      this.getUser().then(userAuth => {
        if (userAuth) {
          const uid = userAuth.uid;
          this.getProfileData(uid)
            .pipe(first())
            .toPromise().then(userProfile => {
            observer.next(userProfile);
          }).catch(err => observer.next(null));
        } else {
          observer.next();
        }
      }).catch(err => observer.next(null));
    });
  }

  /**
   * Update the user Professional type (if is just a client or is also a Professional)
   */
  updateUserRoleType(roleType: UserProfileRoleType): Observable<ApiResponse<UserProfileModel>> {
    return Observable.create(observer => {
      this.getUserProfile().pipe(first()).toPromise().then(userProfile => {
        const updateData = {
          ...userProfile,
          role: roleType
        };
        if (userProfile) {
          userProfile.role = roleType;
          this.userProfileCollectionRef.doc(userProfile.id)
            .update(updateData)
            .then(value => {
              observer.next(getQuickResponse(true, updateData));
            }, err => {
              observer.next(getQuickResponse(false, err));
            });
        } else {
          observer.next(getQuickResponse(false, 'Didn\'t found the user profile'));
        }
      });
    });
  }
}
