import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserAPIService} from './user-api.service';
import {FirestoreCollectionNames, UserWorkableArea} from '../../../../functions/src/data';
import {GeoFire} from 'geofire';
import * as firebase from 'firebase/app'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable()
export class WorkableAreasApiService {

  geoFire: GeoFire;

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth, private userAPIService: UserAPIService) {
    /// Reference database location for GeoFire
    const firebaseRef = firebase.database().ref().child(FirestoreCollectionNames.userProWorkableAreas);
    this.geoFire = new GeoFire(firebaseRef);
  }

  private getUserAreaKey(userWorkableArea: UserWorkableArea) {
    return userWorkableArea.uid + '_' + userWorkableArea.mapArea.id;
  }

  /**
   * Add user workable areas
   * @param userWorkableAreas
   */
  addUserWorkableAreas(userWorkableAreas: UserWorkableArea[]) {
    if (userWorkableAreas) {
      userWorkableAreas.forEach(userWorkableArea => {
        this.addUserWorkableArea(userWorkableArea);
      });
    }
  }

  /**
   * Add User workable area
   * @param userWorkableArea
   */
  addUserWorkableArea(userWorkableArea: UserWorkableArea) {
    const key = this.getUserAreaKey(userWorkableArea);
    this.geoFire
      .set(key, [userWorkableArea.mapArea.location.latitude, userWorkableArea.mapArea.location.longitude])
      .then(response => {
        console.log('addUserWorkableArea: ' + key);
      }).catch(err => {
      console.log('addUserWorkableArea: ' + key);
    });
  }

  /**
   * Remove workable areas
   * @param keys
   */
  removeWorkableAreas(userWorkableAreas: UserWorkableArea[]) {
    if (userWorkableAreas) {
      userWorkableAreas.forEach(userArea => {
        const key = this.getUserAreaKey(userArea);
        this.geoFire.remove(key).then(response => {
          console.log('Removed workable key: ' + key);
        }).catch(err => {
          console.log('Removed workable error: ' + key);
        });
      });
    }
  }

  /// Queries database for nearby locations
  /// Maps results to the hits BehaviorSubject
  /*getLocations(radius: number, coords: Array<number>) {
    this.geoFire.query({
      center: coords,
      radius: radius
    })
      .on('key_entered', (key, location, distance) => {
        let hit = {
          location: location,
          distance: distance
        };

        let currentHits = this.hits.value
        currentHits.push(hit)
        this.hits.next(currentHits);
      })
  }*/

}
