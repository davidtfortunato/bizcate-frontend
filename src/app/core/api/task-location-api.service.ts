import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserAPIService} from './user-api.service';
import {AppMapLocation, FirestoreCollectionNames, UserWorkableArea} from '../../../../functions/src/data';
import {GeoCallbackRegistration, GeoFire, GeoQuery} from 'geofire';
import * as firebase from 'firebase/app';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {GeolocationService} from '../location/geolocation.service';
import {Observable} from 'rxjs';

export interface GeoQueryResponse {
  taskId: string;
  distance: number;
  type: 'in' | 'out';
}

@Injectable()
export class TaskLocationApiService {

  geoFire: GeoFire;

  _geoQuery: GeoQuery;

  constructor(private geoLocationService: GeolocationService) {
    /// Reference database location for GeoFire
    const firebaseRef = firebase.database().ref().child(FirestoreCollectionNames.taskLocations);
    this.geoFire = new GeoFire(firebaseRef);
  }

  private getTaskLocationKey(task: TaskModel) {
    return task.id;
  }


  /**
   * Add a geo location related with the task
   */
  addTaskLocation(taskData: TaskModel) {
    if (taskData
      && taskData.location
      && taskData.location.mapLocation
      && taskData.location.mapLocation.longitude
      && taskData.location.mapLocation.latitude) {
      const key = this.getTaskLocationKey(taskData);
      this.geoFire
        .set(key, [taskData.location.mapLocation.latitude, taskData.location.mapLocation.longitude])
        .then(response => {
          console.log('addUserWorkableArea: ' + key);
        }).catch(err => {
        console.log('addUserWorkableArea: ' + key);
      });
    }
  }

  /**
   * Remove task location
   * @param keys
   */
  removeTaskLocation(task: TaskModel) {
    if (task) {
      this.geoFire.remove(this.getTaskLocationKey(task)).then(response => {
        console.log('Removed task location key: ' + response);
      }).catch(err => {
        console.log('Removed task location key error: ' + err);
      });
    }
  }

  /**
   * Get Locations
   */
  getLocationsBaseCurrentLocation(radius: number) {
    const sub = this.geoLocationService
      .getCurrentLocation()
      .subscribe(location => {
        if (sub) {
          sub.unsubscribe();
        }

        if (location) {
          this.getLocations(radius, {latitude: location.latitude, longitude: location.longitude});
        }

      });
  }

  /**
   * Get geo query object to handle the callbacks
   * @param radius
   * @param center
   */
  getGeoQueryTaskArea(radius: number, center: AppMapLocation): GeoQuery {
    if (radius > 0 && center) {
      const coords = [center.latitude, center.longitude];
      const geoQuery = this.geoFire.query({
        center: coords,
        radius: radius
      });
      return geoQuery;
    }
    return null;
  }

  /**
   * Get tasks inside of a area location
   * @param radius
   * @param center
   * @param isToInit: if is the first time to search in the area or is to update the criteria
   */
  getLocations(radius: number, center: AppMapLocation, isToInit: boolean = true): Observable<GeoQueryResponse> {
    return Observable.create(observe => {
      if (radius > 0 && center) {
        const coords = [center.latitude, center.longitude];

        if (this._geoQuery) {
          // Check if should init the query or update the criteria
          if (isToInit) {
            this._geoQuery.cancel();
            this._geoQuery = null;
          } else {
            // update query criteria
            this._geoQuery.updateCriteria({
              center: coords,
              radius: radius
            });
          }
        }

        if (!this._geoQuery) {
          this._geoQuery = this.geoFire.query({
            center: coords,
            radius: radius
          });
        }

        // if is the first time
        if (isToInit) {
          // On Key entered
          this._geoQuery.on('key_entered', (key, location, distance) => {
            console.log('onKeyEntered: ' + key);
            observe.next({
              taskId: key,
              distance: distance,
              type: 'in'
            });
          });

          // on exited from area
          this._geoQuery.on('key_exited', (key, location, distance) => {
            console.log('onKeyExited: ' + key);
            observe.next({
              taskId: key,
              distance: distance,
              type: 'out'
            });
          });
        }
      }
    });
  }

}
