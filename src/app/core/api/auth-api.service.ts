import {Injectable} from '@angular/core';
import {ApiRequestService} from '../data/api-request.service';
import {Observable, Subject} from 'rxjs';
import {ApiResponse} from '../../../../functions/src/data/index';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserAPIService} from './user-api.service';
import {first} from 'rxjs/operators';
import {UserProfileRoleType} from '../../../../functions/src/data/user-model';

@Injectable()
export class AuthAPIService {

  // API Paths
  private static BASE_CONTEXT_URL = '/auth';
  private static AUTH_LOGIN = '/login';
  private static AUTH_REGISTER = '/register';
  private static AUTH_ACCOUNT = '/account';

  constructor(private apiService: ApiRequestService, private afAuth: AngularFireAuth, private userAPIService: UserAPIService) {
  }

  /**
   * POST /auth/login
   * @param email Email to login user
   * @param password Password
   */
  doLogin(email: string, password: string): Observable<ApiResponse<any>> {

    const response = new Subject<ApiResponse<any>>();
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res => {
      return response.next({
        responseCode: 4000,
        responseData: res,
        isSuccess: true
      }), err => {
        return response.next({
          responseCode: 1000,
          responseData: res,
          isSuccess: false
        });
      };
    });
    return response;
  }

  /**
   * Register a new User
   */
  doRegisterUser(email: string, password: string, displayName: string): Observable<ApiResponse<any>> {
    const response = new Subject<ApiResponse<any>>();

    this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(res => {

        const currentUser = this.afAuth.auth.currentUser;
        this.afAuth.auth.currentUser.updateProfile({displayName, photoURL: currentUser.photoURL ? currentUser.photoURL : ''}).then(resUpdate => {

          // Update Profile
          this.userAPIService.patchSaveProfile({
            uid: currentUser.uid,
            name: displayName,
            email: email,
            role: UserProfileRoleType.CLIENT // By default
          });

          response.next({
            responseCode: 4000,
            responseData: resUpdate,
            isSuccess: true
          });
        }, err => {
          response.next({
            responseCode: 1000,
            responseData: err,
            isSuccess: false
          });
        });
      }, err => {
        response.next({
          responseCode: 1000,
          responseData: err,
          isSuccess: false
        });
      }
    );
    return response;
  }

  /**
   * Execute the Logout request
   */
  doLogout() {
    const response = new Subject<ApiResponse<any>>();
    this.afAuth.auth.signOut().then(res => {
      return response.next({
        responseCode: 4000,
        responseData: res,
        isSuccess: true
      });
    }, err => {
      return response.next({
        responseCode: 1000,
        responseData: err,
        isSuccess: true
      });
    });
    return response;
  }

  /**
   * GET /auth/account - If the user is logged in, will return the user account and the profile
   */
  getUserAccount() {
    return this.afAuth.authState;
  }

  getUser() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

}
