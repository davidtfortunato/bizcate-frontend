import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {
  FirestoreCollectionNames,
  getQuickResponse,
  getResponse,
  UserConversationModel,
  UserMessageModel, UserProfileModel
} from '../../../../functions/src/data/index';
import {Observable, Subject} from 'rxjs';
import {ApiResponse} from '../../../../functions/src/data/index';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable()
export class MessagesApiService {

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
  }

  /**
   * Get Conversation messages
   */
  getConversationMessages(conversationId: string): Observable<ApiResponse<{ conversationId: string, messages: UserMessageModel[] }>> {
    return Observable.create((observer) => {
      const listMessages = [];

      this.afs
        .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
        .ref.doc(conversationId)
        .collection(FirestoreCollectionNames.userMessages)
        .orderBy('date')
        .get().then(response => {
        // Add messages in the list
        if (!response.empty) {
          response.forEach(messageRes => listMessages.push({id: messageRes.id, ...messageRes.data()}));
        }
        observer.next(getQuickResponse(true, {conversationId: conversationId, messages: listMessages}));
      }).catch(err => getQuickResponse(false, err));
    });
  }

  /**
   * Get Current user conversations
   */
  getUserConversations(): Observable<ApiResponse<UserConversationModel[]>> {
    return Observable.create((observer) => {
      const userUID = this.afAuth.auth.currentUser.uid;

      console.log('Getting conversations of user: ' + userUID);

      if (userUID) {
        // Get Conversations of user
        const listConversations = [];
        const userUIDARef = this.afs
          .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
          .ref.where('userUIDA', '==', userUID);
        const userUIDBRef = this.afs
          .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
          .ref.where('userUIDB', '==', userUID);

        // Start to get Conversations where UID is equal in userA and then get where is in userB
        userUIDARef.get().then(userUIDARefData => {
          // Add Data into list conversations
          userUIDARefData.forEach(document => {
            listConversations.push({id: document.id, ...document.data()});
          });

          // Return List conversations
          observer.next(getQuickResponse(true, listConversations));

          // Get Conversations where UID is on B
          userUIDBRef.get().then(userUIDBRefData => {
            // Add conversations data
            userUIDBRefData.forEach(document => {
              listConversations.push({id: document.id, ...document.data()});
            });

            // Return List conversations
            observer.next(getQuickResponse(true, listConversations));
          }).catch(err => observer.next(getQuickResponse(false, err)));

        }).catch(err => observer.next(getQuickResponse(false, err)));
      } else {
        observer.next(getResponse(1010));
      }
    });
  }

  /**
   * Send Message to a user
   */
  sendMessage(message: UserMessageModel): Observable<ApiResponse<UserConversationModel>> {
    if (message.conversationId) {
      return Observable.create((observer) => {
        // Search for conversation
        this.afs
          .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
          .doc(message.conversationId).ref
          .get().then(conversation => {
          console.log('Found Conversation: ' + JSON.stringify(conversation.data()));
          this.addMessageInConversation(message,
            conversation.data() as UserConversationModel,
            conversation.id, observer);
        }).catch(err => observer.next(getQuickResponse(false, err)));
      });
    } else {
      return this.createUserConversation(message.userSenderUID, message.userReceiverUID, message);
    }
  }

  createUserConversationWithUserUID(otherUserUID: string) {
    return this.createUserConversation(this.afAuth.auth.currentUser.uid, otherUserUID);
  }

  /**
   * Create a Conversation between users
   * @param message If the conversation is created by sending a new message, this one should be added immediatly to the conversation
   */
  createUserConversation(userUIDA: string, userUIDB: string, message?: UserMessageModel): Observable<ApiResponse<UserConversationModel>> {
    return Observable.create((observer) => {
      // Create a new conversation
      const usersConversation: UserConversationModel = {
        userUIDA: userUIDA,
        userUIDB: userUIDB,
        userAReadAt: Date.now(),
        userBReadAt: -1
      };

      // UserData from Auth
      usersConversation.userA = {
        name: this.afAuth.auth.currentUser.displayName,
        photoUrl: this.afAuth.auth.currentUser.photoURL ? this.afAuth.auth.currentUser.photoURL : ''
      };

      // Find User B and then add the new conversation Data
      this.afs
        .collection<UserProfileModel>(FirestoreCollectionNames.userProfile)
        .ref.where('uid', '==', message.userReceiverUID)
        .get().then(queryResponse => {

        // Found UserB
        if (!queryResponse.empty) {
          // Adding a new conversation
          const userProfileB = queryResponse.docs[0];
          // Set UserB Data
          usersConversation.userB = {
            name: userProfileB.data().name,
            photoUrl: userProfileB.data().photoUrl ? userProfileB.data().photoUrl : ''
          };

          // Add Conversation
          this.afs.collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
            .add(usersConversation)
            .then(value => {
              // On added a conversation
              if (message) {
                this.addMessageInConversation(message, usersConversation, value.id, observer);
              } else {
                observer.next(getQuickResponse(true, value));
              }
            }).catch(err => {
            // On adding a conversation failed
            observer.next(getQuickResponse(false, err));
          });
        } else {
          observer.next(getQuickResponse(false, 'User Profile B not found'));
        }
      });
    });
  }

  /**
   * Add a message in the UserConversation document
   */
  private addMessageInConversation(message: UserMessageModel,
                                   userConversation: UserConversationModel,
                                   conversationId: string,
                                   response: Subject<ApiResponse<UserConversationModel>>) {
    // If doesn't have conversationID yet, add it
    if (!message.conversationId) {
      message = {...message, conversationId: conversationId};
    }
    userConversation.lastMessage = message;
    userConversation.id = conversationId;

    // Update Conversations Collections && UserMessages Collection
    this.afs
      .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
      .doc(conversationId)
      .update(userConversation)
      .then(value => {
        // Conversation was correctly updated
        response.next(getQuickResponse(true,
          userConversation));
      }).catch(err => response.next(getQuickResponse(false, err)));

    this.afs
      .collection<UserConversationModel>(FirestoreCollectionNames.userConversations)
      .doc(conversationId)
      .collection<UserMessageModel>(FirestoreCollectionNames.userMessages)
      .add(message)
      .then(newMessageAdded => {
        // The message was correctly added
      }).catch(err => console.log('Got some error: ' + err.toString()));
  }


}
