import {Injectable} from '@angular/core';
import {ProfessionalPortfolioModel} from '../../../../functions/src/data/portfolio-model';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {ApiResponse, FirestoreCollectionNames, getQuickResponse, UserWorkableArea} from '../../../../functions/src/data';
import {Observable} from 'rxjs';
import {UserAPIService} from './user-api.service';

@Injectable()
export class PortfolioApiService {

  constructor(private afs: AngularFirestore,
              private afAuth: AngularFireAuth,
              private userAPI: UserAPIService) {
  }

  /**
   * Add a Professional Portfolio entrance
   * @param porfolio Data to add to the Portfolio collection
   */
  addProfessionalPortfolio(porfolio: ProfessionalPortfolioModel): Observable<ApiResponse<ProfessionalPortfolioModel>> {
    // Make sure it have the right uid
    porfolio.uid = this.afAuth.auth.currentUser.uid;

    return Observable.create((observer) => {
      this
        .afs
        .collection(FirestoreCollectionNames.professionalPortfolioRegistries)
        .add(porfolio)
        .then(value => {
          observer.next(getQuickResponse(true, porfolio));
        }).catch(err => {
        observer.next(getQuickResponse(false, err));
      });
    });
  }

  /**
   * Remove a professional portfolio entry
   * @param id id of the document to remove
   */
  removeProfessionalPortfolio(id: string): Observable<ApiResponse<ProfessionalPortfolioModel[]>> {
    return Observable.create(observer => {
      this
        .afs
        .collection(FirestoreCollectionNames.professionalPortfolioRegistries)
        .doc(id)
        .delete()
        .then(value => {
          // After delete, respond with the list of portfolio
          this.getProfessionalPortfolio().subscribe(response => {
            observer.next(response);
          });
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  getProfessionalPortfolioByUid(uid: string): Observable<ApiResponse<ProfessionalPortfolioModel[]>>  {
    return Observable.create(observer => {
      this
        .afs
        .collection(FirestoreCollectionNames.professionalPortfolioRegistries)
        .ref.where('uid', '==', uid)
        .get().then(queryResponse => {
        const portfolioList: ProfessionalPortfolioModel[] = [];
        if (queryResponse && !queryResponse.empty) {
          for (const doc of queryResponse.docs) {
            const portfolio: ProfessionalPortfolioModel = {
              ...doc.data() as ProfessionalPortfolioModel,
              id: doc.id
            };
            portfolioList.push(portfolio);
          }
        }
        observer.next(getQuickResponse(true, portfolioList));
      }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Get Professional portfolio
   */
  getProfessionalPortfolio(): Observable<ApiResponse<ProfessionalPortfolioModel[]>> {
    return Observable.create(observer => {
      this.userAPI.getUser().then(user => {
        if (user) {
          this
            .afs
            .collection(FirestoreCollectionNames.professionalPortfolioRegistries)
            .ref.where('uid', '==', user.uid)
            .get().then(queryResponse => {
              const portfolioList: ProfessionalPortfolioModel[] = [];
              if (queryResponse && !queryResponse.empty) {
                for (const doc of queryResponse.docs) {
                  const portfolio: ProfessionalPortfolioModel = {
                    ...doc.data() as ProfessionalPortfolioModel,
                    id: doc.id
                  };
                  portfolioList.push(portfolio);
                }
              }
              observer.next(getQuickResponse(true, portfolioList));
          }).catch(err => observer.next(getQuickResponse(false, err)));
        } else {
          observer.next(getQuickResponse(false, 'User not valid'));
        }
      }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  deletePortfolio(docId): Observable<ApiResponse<string>> {
    return Observable.create(observer => {
      this
        .afs
        .collection(FirestoreCollectionNames.professionalPortfolioRegistries)
        .doc(docId)
        .delete()
        .then(value => {
          observer.next(getQuickResponse(true, docId));
        }).catch(err => observer.next(getQuickResponse(false, docId)));
    });
  }

}
