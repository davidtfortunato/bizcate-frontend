import {WorkableAreasApiService} from './workable-areas-api.service';

export * from './auth-api.service';
export * from './messages-api.service';
export * from './professional-api.service';
export * from './user-api.service';

import {AuthAPIService} from './auth-api.service';
import {MessagesApiService} from './messages-api.service';
import {ProfessionalApiService} from './professional-api.service';
import {UserAPIService} from './user-api.service';
import {PortfolioApiService} from './portfolio-api.service';
import {TasksApiService} from './tasks-api.service';
import {DashboardApiService} from './dashboard-api.service';


export const API_SERVICE = [
  AuthAPIService,
  MessagesApiService,
  ProfessionalApiService,
  UserAPIService,
  WorkableAreasApiService,
  PortfolioApiService,
  TasksApiService,
  DashboardApiService
];
