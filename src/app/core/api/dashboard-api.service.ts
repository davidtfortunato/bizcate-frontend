import {Injectable} from '@angular/core';
import {AngularFireFunctions} from '@angular/fire/functions';
import {Observable} from 'rxjs';
import {ApiResponse, getQuickResponse} from '../../../../functions/src/data';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {DashboardClientModel, DashboardProfessionalModel} from '../../../../functions/src/data/dashboard-model';

@Injectable()
export class DashboardApiService {

  constructor(private fns: AngularFireFunctions) {
  }


  getUserClientDashboard(): Observable<ApiResponse<DashboardClientModel>> {
    return Observable.create(observer => {
      this
        .fns
        .functions
        .httpsCallable('getUserClientDashboard')()
        .then(response => {
          observer.next(getQuickResponse(true, response.data));
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  getUserProfessionalDashboard(): Observable<ApiResponse<DashboardProfessionalModel>> {
    return Observable.create(observer => {
      this
        .fns
        .functions
        .httpsCallable('getUserProfessionalDashboard')()
        .then(response => {
          observer.next(getQuickResponse(true, response.data));
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }
}
