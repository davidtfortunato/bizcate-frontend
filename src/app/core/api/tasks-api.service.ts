import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {TaskModel, TaskReviewOrigin, TaskStatus} from '../../../../functions/src/data/task-model';
import {ApiResponse, FirestoreCollectionNames, getQuickResponse, UserProfileModel} from '../../../../functions/src/data';
import {Observable} from 'rxjs';
import {TaskOfferModel} from '../../../../functions/src/data/task-offer-model';
import {LanguageService} from '../language/language.service';
import {first} from 'rxjs/operators';
import {UserAPIService} from './user-api.service';
import {ProfessionalApiService} from './professional-api.service';
import {AngularFireFunctions} from '@angular/fire/functions';
import {AcceptTaskBody, GetMultipleTasksBody, PublishTaskReviewBody} from '../../../../functions/src/tasks';
import {ScheduleAvailabilityPeriod, ScheduleStatus} from '../../../../functions/src/data/calendar-model';
import {TaskLocationApiService} from './task-location-api.service';
import {response} from 'express';

@Injectable()
export class TasksApiService {

  constructor(private afs: AngularFirestore,
              private afAuth: AngularFireAuth,
              private profileAPI: UserAPIService,
              private professionalAPI: ProfessionalApiService,
              private langugage: LanguageService,
              private fns: AngularFireFunctions,
              private taskLocationAPI: TaskLocationApiService) {
  }

  /**
   * Make an offer
   */
  makeAnOffer(taskId: string, valueOffered: number, message: string): Observable<ApiResponse<TaskOfferModel>> {
    return Observable.create(observe => {
      this.professionalAPI
        .getProfessionalProfile()
        .pipe(first())
        .toPromise()
        .then(responseProfessioanl => {
          const professionalProfile = responseProfessioanl.responseData;
          console.log('makeAnOffer');

          // Get TaskData Updated
          this
            .getTaskDetails(taskId)
            .pipe(first())
            .toPromise()
            .then(response => {
              if (response.isSuccess) {
                // Check if the Task is open yet
                if (response.responseData.status === TaskStatus.OPEN) {
                  // Generate Data
                  const offer: TaskOfferModel = {
                    userInfo: {
                      uid: professionalProfile.uid,
                      name: professionalProfile.name,
                      photoUrl: professionalProfile.logoUrl
                    },
                    taskData: response.responseData,
                    taskId: taskId,
                    valueOffered: valueOffered,
                    message: message,
                    offerDate: Date.now(),
                    replies: [],
                    isAccepted: false
                  };

                  // Create Task Offer Document
                  this
                    .afs
                    .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
                    .add(offer)
                    .then(document => {
                      if (document.id) {
                        offer.id = document.id;
                      }
                      observe.next(getQuickResponse(true, offer));
                    }).catch(err => {
                    observe.next(getQuickResponse(false, err));
                  });
                } else {
                  observe.next(getQuickResponse(false, this.langugage.getLanguage().TASK_OFFER_ERROR_TASK_NOT_OPEN));
                }
              } else {
                observe.next(getQuickResponse(false, this.langugage.getLanguage().TASK_OFFER_ERROR_TASK_DOESNT_EXIST));
              }
            });
        });
    });
  }

  /**
   * Remove an offer made by the current user.
   * Should validate first if the current user is the owner of the taskOffer
   */
  removeUserOffer(taskOfferId: string): Observable<ApiResponse<TaskOfferModel>> {
    return Observable.create(observer => {

      this.profileAPI
        .getUserProfile()
        .pipe(first())
        .toPromise().then(user => {

        if (user) {
          // Get Task Offer
          this
            .afs
            .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
            .ref
            .doc(taskOfferId).get().then(taskOfferDoc => {
            const taskOfferData: TaskOfferModel = taskOfferDoc.data() as TaskOfferModel;
            taskOfferData.id = taskOfferId;

            if (taskOfferData && taskOfferData.userInfo.uid === user.uid) {
              // Remove task offer
              this
                .afs
                .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
                .doc(taskOfferId)
                .delete()
                .then(response => {
                  observer.next(getQuickResponse(true, taskOfferData));
                })
                .catch(err => observer.next(getQuickResponse(false, err)));
            } else {
              observer.next(getQuickResponse(false, 'Task offer not found'));
            }
          });
        } else {
          observer.next(getQuickResponse(false, 'Current user is not logged in'));
        }
      }).catch(err => observer.next(getQuickResponse(false, err)));

    });
  }

  /**
   * Accept an offer
   * Flow:
   *  - Get Task offer
   *  - Update task offer to be the accepted one
   *  - Get Task Data
   *  - Update Task Data with the accepted Offer and update the current status
   *  - Update Task Offer with the new data
   */
  acceptTaskOffer(body: AcceptTaskBody): Observable<ApiResponse<TaskModel>> {
    return Observable.create(observer => {
      this
        .fns
        .functions
        .httpsCallable('acceptTaskOffer')
        (body)
        .then(response => {
          observer.next(getQuickResponse(true, response));
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Publish the task review from the user. This method will execute a funciton on Firebase
   */
  publishTaskReview(taskData: TaskModel,
                    userProfile: UserProfileModel,
                    message: string,
                    level: number,
                    origin: TaskReviewOrigin): Observable<ApiResponse<any>> {
    return Observable.create(observer => {
      if (!taskData || !userProfile) {
        observer.next(getQuickResponse(false, 'Invalid data'));
        return;
      }

      // Generate Body request
      const body: PublishTaskReviewBody = {
        taskData: taskData,
        taskReview: {
          isVisible: false,
          date: new Date().getTime(),
          level: level,
          origin: origin,
          message: message,
          publisherUid: userProfile.uid,
          receivedUid: userProfile.uid === taskData.uid ? taskData.acceptedOffer.userInfo.uid : taskData.uid,
          taskTitle: taskData.details.title,
          taskId: taskData.id
        }
      };

      this
        .fns
        .functions
        .httpsCallable('publishTaskReview')(body).then(response => {
        observer.next(getQuickResponse(true, response));
      }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Reply offer with a comment
   */
  replyOfferMessage(taskOfferId: string, message: string): Observable<ApiResponse<TaskOfferModel>> {
    return Observable.create(observer => {
      this.profileAPI
        .getUserProfile()
        .pipe(first())
        .toPromise().then(user => {

        // Get Offer
        this
          .afs
          .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers).ref
          .doc(taskOfferId).get().then(taskOfferDoc => {
          if (taskOfferDoc && taskOfferDoc.id) {

            // Update with the new reply
            const taskOfferData: TaskOfferModel = taskOfferDoc.data() as TaskOfferModel;
            taskOfferData.id = taskOfferDoc.id;
            taskOfferData.replies.push({
              userInfo: {
                uid: user.uid,
                name: user.name,
                photoUrl: user.photoUrl
              },
              message: message,
              date: Date.now()
            });

            // Update Data
            this
              .afs
              .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
              .doc(taskOfferId)
              .update(taskOfferData)
              .then(response => {
                observer.next(getQuickResponse(true, taskOfferData));
              }).catch(err => getQuickResponse(false, err));
          }
        });

      });
    });
  }

  /**
   * Get Task Offers
   */
  getTaskOffers(taskId: string): Observable<ApiResponse<TaskOfferModel[]>> {
    const taskOffers: TaskOfferModel[] = [];

    return Observable.create(observer => {
      this.afs
        .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
        .ref
        .where('taskId', '==', taskId)
        .get()
        .then(queryResponse => {
          if (queryResponse) {
            queryResponse.forEach(result => {
              taskOffers.push({
                id: result.id,
                ...(result.data() as TaskOfferModel)
              });
            });
            observer.next(getQuickResponse(true, taskOffers));
          }
        }).catch(err => getQuickResponse(false, err));
    });
  }

  /**
   * Get the task details from the taskId
   */
  getTaskDetails(taskId: string): Observable<ApiResponse<TaskModel>> {
    return Observable.create(observer => {
      this.afs.collection<TaskModel>(FirestoreCollectionNames.tasksList).doc(taskId).ref.get().then(document => {
        if (document && document.data()) {
          observer.next(getQuickResponse(true, {
            ...document.data(),
            id: document.id
          }));
        } else {
          observer.next(getQuickResponse(false, 'Task details not found'));
        }
      }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Get multiple tasks by a group of ids
   * @param tasksIds
   */
  getListTasksDetails(tasksIds: string[]): Observable<ApiResponse<TaskModel[]>> {
    return Observable.create(observer => {
      const tasks: TaskModel[] = [];

      tasksIds.forEach(async taskId => {
        const taskDoc = await this.afs.collection<TaskModel>(FirestoreCollectionNames.tasksList).doc(taskId).ref.get();
        if (taskDoc && taskDoc.exists) {
          tasks.push({
            ...taskDoc.data() as TaskModel,
            id: taskDoc.id
          });
        }
      });

      if (tasks.length === tasksIds.length) {
        observer.next(getQuickResponse(true, tasks));
      }

      /*const body: GetMultipleTasksBody = {
        taskIds: tasksIds
      };
      this
        .fns
        .functions
        .httpsCallable('getMultipleTasksById')(body).then(response => {
        observer.next(getQuickResponse(true, response));
      }).catch(err => observer.next(getQuickResponse(false, err))); */

    });
  }

  /**
   * Create new task on Server Side
   */
  createNewTask(taskData: TaskModel, userProfile: UserProfileModel): Observable<ApiResponse<TaskModel>> {
    // Set UID
    taskData.uid = this.afAuth.auth.currentUser.uid;
    taskData.postedTime = Date.now();

    // User Infos
    taskData.userInfo = {
      name: userProfile.name,
      photoUrl: userProfile.photoUrl ? userProfile.photoUrl : ''
    };

    return Observable.create(observe => {
      // Create Task Document
      this
        .afs
        .collection<TaskModel>(FirestoreCollectionNames.tasksList)
        .add(taskData)
        .then(document => {
          if (document.id) {
            taskData.id = document.id;
          }

          // Task created with success. Should create the Geo Location as well
          this.taskLocationAPI.addTaskLocation(taskData);

          observe.next(getQuickResponse(true, taskData));
        }).catch(err => {
        observe.next(getQuickResponse(false, err));
      });
    });
  }

  /**
   * Get the professional tasks that he has an offer
   */
  getProfessionalOffers(): Observable<ApiResponse<TaskOfferModel[]>> {
    return Observable.create(observe => {
      this.profileAPI.getUser().then(user => {
        if (user) {
          this
            .afs
            .collection<TaskOfferModel>(FirestoreCollectionNames.taskOffers)
            .ref.where('userInfo.uid', '==', user.uid)
            .get()
            .then(taskOfferDocs => {
              const listOffers = [];
              if (taskOfferDocs && !taskOfferDocs.empty) {
                taskOfferDocs.forEach(taskOfferDoc => {
                  const taskOfferData: TaskOfferModel = taskOfferDoc.data() as TaskOfferModel;
                  taskOfferData.id = taskOfferDoc.id;
                  listOffers.push(taskOfferData);
                });
              }
              observe.next(getQuickResponse(true, listOffers));
            });
        } else {
          observe.next(getQuickResponse(false, 'User is not logged in'));
        }
      }).catch(err => observe.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Get the created tasks of the current user
   */
  getUserCreatedTasks(): Observable<ApiResponse<TaskModel[]>> {
    return Observable.create(observe => {
      this.profileAPI.getUser().then(user => {
        if (user) {
          this
            .afs
            .collection<TaskModel>(FirestoreCollectionNames.tasksList)
            .ref
            .where('uid', '==', user.uid)
            .get()
            .then(taskDocs => {
              const listTasks = [];
              if (taskDocs && !taskDocs.empty) {
                taskDocs.forEach(taskDoc => {
                  const task: TaskModel = taskDoc.data() as TaskModel;
                  task.id = taskDoc.id;
                  listTasks.push(task);
                });
              }
              observe.next(getQuickResponse(true, listTasks));
            });
        } else {
          observe.next(getQuickResponse(false, 'User is not logged in'));
        }
      }).catch(err => observe.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Get All tasks to display on the list
   */
  getAllTasks(): Observable<ApiResponse<TaskModel[]>> {
    return Observable.create(observe => {
      this
        .afs
        .collection<TaskModel>(FirestoreCollectionNames.tasksList)
        .ref
        .get()
        .then(taskDocs => {
          const listTasks = [];
          if (taskDocs && !taskDocs.empty) {
            taskDocs.forEach(taskDoc => {
              const task: TaskModel = taskDoc.data() as TaskModel;
              task.id = taskDoc.id;
              listTasks.push(task);
            });
          }
          observe.next(getQuickResponse(true, listTasks));
        });
    });
  }

  /**
   * Select the available periods for client to execute the task
   */
  selectClientAvailablePeriods(taskData: TaskModel, selectedPeriods: ScheduleAvailabilityPeriod[]): Observable<ApiResponse<TaskModel>> {
    return Observable.create(observe => {
      this.profileAPI.getUser().then(user => {
        // Check if is authorized
        if (user.uid === taskData.uid) {
          // Get task data updated
          this
            .afs
            .collection<TaskModel>(FirestoreCollectionNames.tasksList)
            .doc(taskData.id).ref.get().then(taskDoc => {
            // Update TaskData
            taskData = {
              ...taskDoc.data() as TaskModel,
              id: taskDoc.id
            };

            // Check if the update is not schedule yet
            if ((!taskData.schedule || !taskData.schedule.scheduledBlocks)
              && taskData.scheduledStatus !== ScheduleStatus.SCHEDULED_PHASE3) {

              // Not yet scheduled
              taskData.schedule = {
                availability: selectedPeriods
              };
              taskData.scheduledStatus = ScheduleStatus.SELECTING_TIME_PHASE2;

              // Update
              this
                .afs
                .collection<TaskModel>(FirestoreCollectionNames.tasksList)
                .doc(taskData.id)
                .update(taskData)
                .then(response => {
                  observe.next(getQuickResponse(true, taskData));
                }).catch(err => observe.next(getQuickResponse(false, err)));

            } else {
              observe.next(getQuickResponse(false, 'This task was already scheduled'));
            }

          });
        } else {
          observe.next(getQuickResponse(false, 'User not authorized to do this action'));
        }
      }).catch(err => observe.next(getQuickResponse(false, err)));
    });
  }


  /**
   * Schedule task execution (this action is done by the professional)
   */
  scheduleTaskExecution(taskData: TaskModel, selectedPeriods: ScheduleAvailabilityPeriod[]): Observable<ApiResponse<TaskModel>> {
    return Observable.create(observe => {
      this.profileAPI.getUser().then(user => {
        // Check if is authorized
        if (user.uid === taskData.acceptedOffer.userInfo.uid) {
          // Get task data updated
          this
            .afs
            .collection<TaskModel>(FirestoreCollectionNames.tasksList)
            .doc(taskData.id).ref.get().then(taskDoc => {
            // Update TaskData
            taskData = {
              ...taskDoc.data() as TaskModel,
              id: taskDoc.id
            };

            // Check if the update is not schedule yet
            if ((!taskData.schedule || !taskData.schedule.scheduledBlocks) && selectedPeriods.length > 0
              && taskData.scheduledStatus !== ScheduleStatus.SCHEDULED_PHASE3) {

              // Validate if the periods scheduled still available
              if (taskData.schedule.availability) {
                for (const selectedBlock of selectedPeriods) {
                  let isBlockValid = false;
                  taskData.schedule.availability.forEach(availablePeriod => {
                    if (selectedBlock.period === availablePeriod.period && selectedBlock.day === availablePeriod.day) {
                      isBlockValid = true;
                    }
                  });

                  if (!isBlockValid) {
                    // one of the blocks is not valid anymore, probably because the client updated his availability meanwhile
                    observe.next(getQuickResponse(false,
                      'One of the blocks is not valid anymore, probably because the client updated his availability meanwhile'));
                    return;
                  }
                }
              }

              // Get the earlier block date
              let earlierPeriod: ScheduleAvailabilityPeriod = null;
              selectedPeriods.forEach(period => {
                // is the first
                if (!earlierPeriod) {
                  earlierPeriod = period;
                } else {
                  if (period.day < earlierPeriod.day || (period.day === earlierPeriod.day && period.startHour < earlierPeriod.startHour)) {
                    earlierPeriod = period;
                  }
                }
              });

              // Get the last block date
              let latestPeriod: ScheduleAvailabilityPeriod = null;
              selectedPeriods.forEach(period => {
                // is the first
                if (!latestPeriod) {
                  latestPeriod = period;
                } else {
                  if (period.day > latestPeriod.day
                    || (period.day === latestPeriod.day && latestPeriod.startHour > earlierPeriod.startHour)) {
                    latestPeriod = period;
                  }
                }
              });

              const startDate = new Date(earlierPeriod.day);
              startDate.setHours(earlierPeriod.startHour);

              const endDate = new Date(latestPeriod.day);
              startDate.setHours(latestPeriod.endHour);

              // Not yet scheduled
              taskData.schedule = {
                scheduledBlocks: selectedPeriods,
                startDate: startDate.getTime(),
                endDate: endDate.getTime()
              };
              taskData.scheduledStatus = ScheduleStatus.SCHEDULED_PHASE3;

              // Update
              this
                .afs
                .collection<TaskModel>(FirestoreCollectionNames.tasksList)
                .doc(taskData.id)
                .update(taskData)
                .then(response => {
                  observe.next(getQuickResponse(true, taskData));
                }).catch(err => observe.next(getQuickResponse(false, err)));

            } else {
              observe.next(getQuickResponse(false, 'This task was already scheduled'));
            }

          });
        } else {
          observe.next(getQuickResponse(false, 'User not authorized to do this action'));
        }
      }).catch(err => observe.next(getQuickResponse(false, err)));
    });
  }
}
