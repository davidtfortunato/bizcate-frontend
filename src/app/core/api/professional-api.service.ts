import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {UserProfessionalProfile} from '../../../../functions/src/data/professional-profile-model';
import {ApiResponse, FirestoreCollectionNames, getQuickResponse} from '../../../../functions/src/data';
import {Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserAPIService} from './user-api.service';
import {first} from 'rxjs/operators';
import {UserProfileRoleType} from '../../../../functions/src/data/user-model';
import {UtilsService} from '../utils/utils.service';

@Injectable()
export class ProfessionalApiService {


  constructor(private afs: AngularFirestore,
              private afAuth: AngularFireAuth,
              private userAPIService: UserAPIService,
              private utils: UtilsService) {
  }

  getUser() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  getProfessionalProfileByUid(uid: string): Observable<ApiResponse<UserProfessionalProfile>> {
    return Observable.create(observer => {
      if (uid) {
        this.afs.collection(FirestoreCollectionNames.userProfessionalProfile)
          .ref
          .where('uid', '==', uid)
          .get()
          .then((queryResponse) => {
            if (queryResponse && !queryResponse.empty) {
              observer.next(getQuickResponse(true, queryResponse.docs[0].data()));
            } else {
              observer.next(getQuickResponse(false, 'Couldn\'t get the professional profile'));
            }
          });
      } else {
        // do something else
        observer.next(getQuickResponse(false, 'Couldn\'t get the user data'));
      }
    });
  }

  getProfessionalProfile(): Observable<ApiResponse<UserProfessionalProfile>> {
    return Observable.create(observer => {
      this.getUser().then(user => {
        if (user) {
          this.afs.collection(FirestoreCollectionNames.userProfessionalProfile)
            .ref
            .where('uid', '==', user.uid)
            .get()
            .then((queryResponse) => {
              if (queryResponse && !queryResponse.empty) {
                observer.next(getQuickResponse(true, queryResponse.docs[0].data()));
              } else {
                observer.next(getQuickResponse(false, 'Couldn\'t get the professional profile'));
              }
            });
        } else {
          // do something else
          observer.next(getQuickResponse(false, 'Couldn\'t get the user data'));
        }
      });
    });
  }

  /**
   * Create a new professional profile when the user select the option to be a Professional
   */
  createProfessionalProfile(professionalProfile: UserProfessionalProfile): Observable<ApiResponse<UserProfessionalProfile>> {
    console.log('Adding new professional: ' + professionalProfile);

    if (!professionalProfile.logoUrl) {
      professionalProfile.logoUrl = '';
    }

    return Observable.create(observer => {
      // Add data
      this.afs
        .collection<UserProfessionalProfile>(FirestoreCollectionNames.userProfessionalProfile)
        .add(professionalProfile)
        .then(document => {
          console.log('Professional added: ' + document);
          // Set Profile ID
          this.userAPIService.setProfessionalProfileID(document.id);
          if (document) {
            // Activate professional role type
            this.userAPIService.updateUserRoleType(UserProfileRoleType.CLIENT_PROFESSIONAL).toPromise().then(response => {
              if (response.isSuccess) {
                observer.next(getQuickResponse(true, professionalProfile));
              } else {
                observer.next(getQuickResponse(false, 'Couldn\'t add the professional profile'));
              }
            }).catch(err => observer.next(getQuickResponse(false, 'Couldn\'t add the professional profile')));

          } else {
            observer.next(getQuickResponse(false, 'Couldn\'t add the professional profile'));
          }
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

  /**
   * Update Professional Profile content
   * @param professionalProfile Data of the professional profile
   */
  updateProfessionalProfile(professionalProfile: UserProfessionalProfile): Observable<ApiResponse<UserProfessionalProfile>> {
    console.log('UpdateProfessionalProfile');
    if (!professionalProfile.logoUrl) {
      professionalProfile.logoUrl = '';
    }

    return Observable.create(observer => {

      const subs = this.userAPIService.updateUserRoleType(professionalProfile.isActive
        ? UserProfileRoleType.CLIENT_PROFESSIONAL : UserProfileRoleType.CLIENT).subscribe(res => {
          subs.unsubscribe();
          console.log('updateUserRoleType res = ' + res);
      });

      this.afs
        .collection<UserProfessionalProfile>(FirestoreCollectionNames.userProfessionalProfile)
        .ref
        .where('uid', '==', this.afAuth.auth.currentUser.uid)
        .get()
        .then(queryResponde => {
          if (!queryResponde.empty) {
            this.afs
              .collection(FirestoreCollectionNames.userProfessionalProfile)
              .doc(queryResponde.docs[0].id)
              .update(professionalProfile)
              .then(response => {
                observer.next(getQuickResponse(true, response));
              })
              .catch(err => observer.next(getQuickResponse(false, err)));
          }
        });
    });
  }

}
