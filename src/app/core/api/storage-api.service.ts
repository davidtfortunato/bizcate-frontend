import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {StorageFileModel} from '../../../../functions/src/data/storage-model';
import firebase from 'firebase';
import {FirestoreCollectionNames} from '../../../../functions/src/data';
import {logger} from 'codelyzer/util/logger';

@Injectable()
export class StorageApiService {
  constructor(private afs: AngularFirestore,
              private afStorage: AngularFireStorage,
              private afAuth: AngularFireAuth) {
  }

  /**
   * Upload photo of user
   */
  uploadUserFile(photoData: any,
                 fileName: string = '',
                 onComplete: (file: StorageFileModel) => void,
                 onError: (error: any) => void,
                 onUpdate?: (percentage: number) => void) {
    let storageFile: StorageFileModel;
    const filePath = this.afAuth.auth.currentUser.uid + '/' + fileName;

    const task = this
      .afStorage
      .upload(filePath, photoData).task;

    task.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: firebase.storage.UploadTaskSnapshot) => {
        const percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        if (onUpdate) {
          onUpdate(percentage);
        }
      },
      (error) => {
        if (onError) {
          onError(error);
        }
      },
      () => {
        // upload success
        task.snapshot.ref.getDownloadURL().then(value => {
          // Create a Storage File Entity
          storageFile = {
            uid: this.afAuth.auth.currentUser.uid,
            filePath: filePath,
            fileUrl: value,
            publishedDate: Date.now()
          };
          if (onComplete) {
            onComplete(storageFile);
          }

          // Add a registry in the DB
          this.addFileRegistry(storageFile);

        }).catch(err => {
          console.log(err);
          if (onError) {
            onError(err);
          }
        });
      });
  }

  /**
   * Remove a file by its file path
   */
  removeFile(filePath: string) {
    this.afStorage.ref(filePath).delete().subscribe((value: any) => {
      logger.debug('Deleting file response: ' + value);

      // Remove from the File Registries
      this
        .afs
        .collection(FirestoreCollectionNames.storageFilesRegistries)
        .ref
        .where('filePath', '==', filePath)
        .get()
        .then(queryResponse => {
          if (queryResponse && !queryResponse.empty) {
            this
              .afs
              .collection(FirestoreCollectionNames.storageFilesRegistries)
              .doc(queryResponse.docs[0].id).delete()
              .then(registryValue => {
                logger.debug(filePath + ' removed with success from the Storage and the Registries files');
              })
              .catch(err => {
                logger.debug('Something failed on removing ' + filePath + ' from registries');
              });
          }
        });
    }, err => {
      logger.debug('Deleting file ' + filePath + ' error: ' + err);
    });
  }

  /**
   * Add a registry for a new file
   */
  private addFileRegistry(storageFile: StorageFileModel) {
    // Check if already exist File Path
    this.afs
      .collection(FirestoreCollectionNames.storageFilesRegistries)
      .ref
      .where('filePath', '==', storageFile.filePath).get().then(queryResponse => {
      if (queryResponse && !queryResponse.empty) {
        // Already exists. So, update the file data
        this
          .afs
          .collection(FirestoreCollectionNames.storageFilesRegistries)
          .doc(queryResponse.docs[0].id).update(storageFile);
      } else {
        // Doesn't exist yet. So create the file entry
        this.afs.collection(FirestoreCollectionNames.storageFilesRegistries).add(storageFile);
      }
    });
  }

}
