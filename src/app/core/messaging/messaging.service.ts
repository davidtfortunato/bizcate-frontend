import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {take} from 'rxjs/operators';
import {LanguageService} from '../language/language.service';
import {FCMUserToken} from '../../../../functions/src/data/messaging-model';
import {PushNotificationBody} from '../../../../functions/src/messaging';
import {getQuickResponse} from '../../../../functions/src/data';
import {AngularFireFunctions} from '@angular/fire/functions';

@Injectable()
export class MessagingService {

  messaging = firebase.messaging();
  currentMessage = new BehaviorSubject(null);


  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth, private language: LanguageService,
              private fns: AngularFireFunctions) {
  }

  updateToken(token) {
    this.afAuth.authState.pipe(take(1)).subscribe(user => {
      if (!user) {
        return;
      }

      const data = {
        [user.uid]: {
          locale: this.language.locale,
          token: token
        } as FCMUserToken
      };
      this.db.object('fcmTokens/').update(data).then(value => {
        console.log('Token updated: ' + value);
      }).catch(err => console.log('Some error occurred on updating the token: ' + err));
    });
  }

  getPermission() {
    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        this.messaging.getToken().then((token) => {
          console.log('Token received: ' + token);
          this.updateToken(token);
        });
      } else {
        console.log('Unable to get permission to notify.');
      }
    });
    /*this.messaging.requestPermission()
      .then(() => {
        console.log('Notification permission granted.');
        return this.messaging.getToken();
      })
      .then(token => {
        console.log(token);
        this.updateToken(token);
      })
      .catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });*/
  }

  /*onPermissionGranted() {
    this.messaging.getToken().then((currentToken) => {
      if (currentToken) {
        sendTokenToServer(currentToken);
        updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
      setTokenSentToServer(false);
    });
  }*/

  receiveMessage() {
    this.messaging.onMessage((payload) => {
      console.log('Message received. ', payload);
      this.currentMessage.next(payload);
    });

  }

  sendPushNotification(notificationBody: PushNotificationBody) {
    return Observable.create(observer => {
      this
        .fns
        .functions
        .httpsCallable('acceptTaskOffer')
        (notificationBody)
        .then(response => {
          observer.next(getQuickResponse(true, response));
        }).catch(err => observer.next(getQuickResponse(false, err)));
    });
  }

}
