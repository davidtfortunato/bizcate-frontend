import {formatDate} from '@angular/common';
import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {LanguageService} from '../language/language.service';
import {AppMapLocation} from '../../../../functions/src/data';
import {ValidatorFn, Validators} from '@angular/forms';

const DAY_FORMAT = 'dd/MM/yyyy';

@Injectable()
export class UtilsService {

  constructor(@Inject(LOCALE_ID) private locale: string, private languages: LanguageService) {
  }

  convertMillisToFormattedDate(timeMillis: number) {
    return formatDate(timeMillis, DAY_FORMAT + ', h:mm:ss a', this.locale);
  }

  convertMillisToFormattedDateHours(timeMillis: number) {
    return formatDate(timeMillis, DAY_FORMAT + ', h:mm a', this.locale);
  }

  convertMillisToFormattedDateDay(timeMillis: number) {
    return formatDate(timeMillis, DAY_FORMAT, this.locale);
  }

  convertMillisToHours(timeMillis: number) {
    return new Date(timeMillis).getHours() + ' ' + this.languages.getLanguage().GENERIC_TIME_HOURS;
  }

  /**
   * This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
   */
  calcMapLocationsDistances(location1: AppMapLocation, location2: AppMapLocation) {
    const R = 6371; // km
    const dLat = this.toRad(location2.latitude - location1.latitude);
    const dLon = this.toRad(location2.longitude - location1.longitude);
    const lat1 = this.toRad(location1.latitude);
    const lat2 = this.toRad(location2.latitude);

    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
  }

  // Converts numeric degrees to radians
  toRad(Value) {
    return Value * Math.PI / 180;
  }

  getValidatorPhoneNumberPattern(): string {
    return '^(\\+\\d{1,3}[- ]?)?\\d{10}$';
  }

}

