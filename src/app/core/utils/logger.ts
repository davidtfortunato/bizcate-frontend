import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {environment} from '../../../environments/environment';

export const enum LOG_LEVEL {
  Error,
  Warning,
  Debug,
  Info
}

@Injectable()
export class Logger {
  private logLevel: LOG_LEVEL;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (!environment.production) {
      this.logLevel = LOG_LEVEL.Info;
    } else {
      this.logLevel = LOG_LEVEL.Error;
    }
  }

  debug(tag, message: string, data?: {}): void {
    if (this.logLevel >= LOG_LEVEL.Debug) {
      console.log(`Debug: [${tag}]: ${message}`, (data ? '\nDetails:' : ''), (data ? data : ''));
    }
  }

  info(tag, message: string, data?: {}): void {
    if (this.logLevel >= LOG_LEVEL.Info) {
      console.log(`Info: [${tag}]: ${message}`, (data ? '\nDetails:' : ''), (data ? data : ''));
    }
  }

  error(tag, message: string, data?: {}): void {
    if (this.logLevel >= LOG_LEVEL.Error) {
      console.error(`Error: [${tag}]: ${message}`, (data ? '\nDetails:' : ''), (data ? data : ''));
    }
  }

  warning(tag, message: string, data?: {}): void {
    if (this.logLevel >= LOG_LEVEL.Warning) {
      console.warn(`Warning: [${tag}]: ${message}`, (data ? '\nDetails:' : ''), (data ? data : ''));
    }
  }
}
