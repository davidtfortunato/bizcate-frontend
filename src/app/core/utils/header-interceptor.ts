import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import {Observable} from 'rxjs';

export class HeaderInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clone the request to add the new header
    req.headers.append('Content-Type', 'application/json');
    req.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    const clonedRequest = req.clone({headers: req.headers});

    // Pass the cloned request instead of the original request to the next handle
    return next.handle(clonedRequest);
  }
}
