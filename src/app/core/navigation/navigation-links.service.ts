import {Injectable} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {AppStoreService} from '../../store/app-store.service';

@Injectable()
export class NavigationLinksService {
  constructor(private router: Router, private appStoreService: AppStoreService) {}

  reloadPage() {
    window.location.reload();
  }

  openLink(link: string[], extras?: NavigationExtras) {
    this.router.navigate(link, extras);
    this.appStoreService.closeSideNavbar();
  }

  /** HOME */
  getHomePath() {
    return [''];
  }

  openHome() {
    this.openLink(this.getHomePath());
  }

  logoClick() {
    this.openHome();
  }

  /** AUTH MODULE */
  openRegister() {
    this.openLink(['auth', 'register']);
  }

  openLogin() {
    this.openLink(['auth', 'login']);
  }

  getEditProfile() {
    return ['auth', 'profile'];
  }

  openEditProfile() {
    this.openLink(this.getEditProfile());
  }

  getEditProfessionalProfile() {

    return ['professional', 'profile'];
  }

  openEditProfessionalProfile() {
    this.openLink(this.getEditProfessionalProfile());
  }

  /** MESSAGES MODULE */
  getMessagesPath() {
    return ['messages'];
  }

  openUserConversations() {
    this.openLink(this.getMessagesPath(), {preserveFragment: false});
  }

  openConversationDetails(conversationId: string, otherUserUID: string) {
    this.openLink(this.getMessagesPath(), {queryParams: {conversationId, otherUserUID}, preserveFragment: false});
  }

  openSendMessage(otherUserUID: string) {
    this.openLink(this.getMessagesPath(), {queryParams: {otherUserUID}, preserveFragment: false});
  }

  /** PUBLIC PROFILE */

  openUserProfile(uid: string) {
    this.openLink(this.getUserProfile(uid));
  }
  getUserProfile(uid: string) {
    return ['profile', 'user', uid];
  }


  openProfessionalProfile(uid: string) {
    this.openLink(this.getProfessionalProfile(uid));
  }
  getProfessionalProfile(uid: string) {
    return ['profile', 'pro', uid];
  }

  /** PROFESSIONAL REQUEST **/

  openRequestQuote(uid: string) {
    // TODO
    console.log('Open request quote user: ' + uid);
  }

  /** TASKS LINKS **/

  getTaskCreate() {
    return ['tasks', 'create'];
  }
  openTaskCreate() {
    this.openLink(this.getTaskCreate());
  }

  getTaskDetails(taskId: string) {
    return ['tasks', taskId];
  }

  openTaskDetails(taskId: string) {
    this.openLink(this.getTaskDetails(taskId));
  }

  getBrowseTasks() {
    return ['tasks', 'list'];
  }

  getMyTasksList() {
    return ['tasks', 'mytasks'];
  }

  getMyProfessionalTasksList() {
    return ['tasks', 'my-professional-tasks'];
  }

  openMyProfessionalTasks() {
    this.openLink(this.getMyProfessionalTasksList());
  }

  /** Initial Screen */
  getInitialScreenPath() {
    return ['create-task'];
  }

  openInitialScreen() {
    this.openLink(this.getInitialScreenPath());
  }

  /** Create Task **/
  getCreateTaskScreenPath() {
    return ['create-task'];
  }

  openCreateTaskScreen() {
    this.openLink(this.getCreateTaskScreenPath());
  }

  getCreateTaskStepsScreen() {
    return ['create-task', 'creation-steps'];
  }

  openCreateTaskStepsScreen() {
    this.openLink(this.getCreateTaskStepsScreen());
  }

  /** Utils **/
  parseArrayLinkToHref(urlArr: string[]) {
    return '#/' + urlArr.join('/');
  }


}
