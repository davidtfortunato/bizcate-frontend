import {Injectable} from '@angular/core';
import {HttpClient, HttpClientJsonpModule, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {until} from 'selenium-webdriver';
import urlContains = until.urlContains;
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';

@Injectable()
export class ApiRequestService {

  // Data
  private baseURL: string;

  // Token
  private token;

  constructor(private http: HttpClient,
              private store: State<fromStore.State>) {

    // Generate the url request
    this.baseURL = environment.api.protocol
      + environment.api.baseUrl
      + (environment.api.port ? ':' + environment.api.port : '')
      + environment.api.apiContext;

    /*this.token = this.store.pipe(select(getJWToken)).subscribe(token => {
      console.log('Received token: ' + token);
      this.token = token;
    });*/
  }


  /**
   * Execute a GET request
   */
  get<T>(url: string, headers?: HttpHeaders): Observable<T> {
    return this.request('GET', this.getFinalURL(url), headers);
  }

  /**
   * Execute a POST request
   */
  post<T>(url: string, headers?: HttpHeaders, body?): Observable<T> {
    return this.request('POST', this.getFinalURL(url), headers, body);
  }

  /**
   * Execute a DELETE request
   */
  delete<T>(url: string, headers?: HttpHeaders, body?): Observable<T> {
    return this.request('DELETE', this.getFinalURL(url), headers, body);
  }

  /**
   * Execute PUT request
   */
  put<T>(url: string, headers?: HttpHeaders, body?): Observable<T> {
    return this.request('PUT', this.getFinalURL(url), headers, body);
  }

  /**
   * Execute PATCH request
   */
  patch<T>(url: string, headers?: HttpHeaders, body?): Observable<T> {
    return this.request('PATCH', this.getFinalURL(url), headers, body);
  }


  private request<T>(method: string, url: string, headers, body = {}): Observable<T> {

    // Add Token
    headers = {
      'x-auth-token': this.token ? this.token : '',
      ...headers
    };
    return this.http.request<T>(method, url, {body: body, headers}).pipe(
      map(r => r),
      catchError((err: HttpErrorResponse) => throwError(err))
    );
  }

  /**
   * Generate the final url appending into the base URl
   * @param url
   */
  private getFinalURL(url) {
    return this.baseURL + url;
  }

}
