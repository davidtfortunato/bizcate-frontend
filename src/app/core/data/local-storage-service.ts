import {Injectable} from '@angular/core';

import {Logger} from '../utils/logger';

@Injectable()
export class LocalStorageService {
  constructor(private logger: Logger) {
  }

  getItem(keyName: string): string {
    try {
      return localStorage.getItem(keyName);
    } catch (e) {
      this.logger.warning('LocalStorageService', 'Failed to retrieve storage item', {key: keyName});
      return null;
    }
  }

  setItem(keyName: string, keyValue: string): void {
    try {
      localStorage.setItem(keyName, keyValue);
    } catch (e) {
      this.logger.warning('LocalStorageService', 'Failed to set item into storage', {key: keyName, value: keyValue});
    }
  }

  clear(): void {
    try {
      localStorage.clear();
    } catch (e) {
      this.logger.warning('LocalStorageService', 'Failed to clear storage');
    }
  }

  key(key: number): string {
    try {
      return localStorage.key(key);
    } catch (e) {
      this.logger.warning('LocalStorageService', 'Failed to get a specific key in storage at index' + key);
      return null;
    }
  }

  removeItem(keyName: string): void {
    try {
      localStorage.removeItem(keyName);
    } catch (e) {
      this.logger.warning('LocalStorageService', 'Failed to remove item from storage', {key: keyName});
    }
  }
}
