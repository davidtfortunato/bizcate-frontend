import {NgModule} from '@angular/core';
import {MapComponent} from './map-component/map.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {SharedModule} from '../shared/shared.module';
import {LeafletDrawModule} from '@asymmetrik/ngx-leaflet-draw';
import {CommonModule} from '@angular/common';

@NgModule({
  providers: [],
  declarations: [MapComponent],
  exports: [MapComponent],
  imports: [LeafletModule,
    SharedModule,
    CommonModule,
    LeafletDrawModule]
})
export class MapModule {
}
