import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {AppUIParams} from '../../shared/app.ui-params';
import {SearchLocationService} from '../../core/location/search-location.service';
import * as Leaflet from 'leaflet';
import {AppMapLocation, MapAreaDraw, UserWorkableArea} from '../../../../functions/src/data';
import {LatLng, LayerGroup, Map} from 'leaflet';
import {AppAssets} from '../../shared/app.assets';
import {GeolocationService} from '../../core/location/geolocation.service';
import {UtilsService} from '../../core/utils/utils.service';

export interface MapMarker {
  id?: string;
  isSelected?: boolean;
  location: AppMapLocation;
}

export interface MapUISettings {
  fillColor: string;
  iconColor: string;
}

export interface Layer {
  _leaflet_id?: number;
  _mRadius?: number;
  _latlng?: LatLng;
  _icon: Object;
}

/**
 * It is using the library https://github.com/Leaflet/Leaflet.draw to display open street maps
 */
@Component({
  selector: 'app-map-container',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {
  @Input() currentLocation: AppMapLocation;
  @Input() fallbackLocation: string;
  @Input() searchedLocation: AppMapLocation;
  @Input() mapDraws: UserWorkableArea[];
  @Input() mapHeight = 500;
  @Input() mapUISettings: MapUISettings = {
    fillColor: AppUIParams.mapFillColor,
    iconColor: AppUIParams.mapIconColor
  };
  @Input() isEditabled = true;
  @Input() drawCirle = true;
  @Input() drawMarker = false;
  @Input() mapStaticMarkers: MapMarker[];
  @Output() mapDrawsChanged = new EventEmitter<MapAreaDraw[]>();
  @Output() centerLocationChanged = new EventEmitter<AppMapLocation>();
  @Output() mapBoundsRadiusChanged = new EventEmitter<number>(); // in kilometers

  // Leaflet Options
  options = {
    layers: [
      Leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      })
    ],
    zoom: 12,
    center: Leaflet.latLng(46.879966, -121.726909) // Maybe persist locally last position to be the default
  };
  zoom = 12; // Default
  centerLocation;
  drawOptions;

  // Views
  mapContainer: any;
  featureGroup = Leaflet.featureGroup();
  layers: any[] = [this.featureGroup];
  drawableLayer: LayerGroup;

  // Data
  map: Map;

  // Calculated data
  mapBoundsDistance: number;
  mapCenterLocation: AppMapLocation;

  constructor(private searchLocation: SearchLocationService,
              private geoLocationService: GeolocationService,
              private utils: UtilsService) {
  }


  ngOnInit() {
    // Init params
    this.zoom = 12;

    // Get the default current location
    if (!this.currentLocation) {
      const sub = this.geoLocationService.getCurrentLocation().subscribe(location => {
        if (sub) {
          sub.unsubscribe();
        }

        if (!this.currentLocation) {
          this.currentLocation = {
            longitude: location.longitude,
            latitude: location.latitude
          };
          this.centerLocation = this.getCurrentLocation();
        }
      });
    }
  }

  ngOnChanges(changes) {
    if (this.searchedLocation) {
      this.currentLocation = {
        longitude: this.searchedLocation.longitude,
        latitude: this.searchedLocation.latitude
      };
      this.centerLocation = this.getCurrentLocation();
    } else if (!this.currentLocation && this.fallbackLocation) { // If current location is null, search from the fallback location
      this.searchLocation
        .searchLocation(this.fallbackLocation)
        .subscribe(value => {
          if (!this.currentLocation && value && value.length > 0) {
            this.currentLocation = {
              longitude: value[0].lon,
              latitude: value[0].lat
            };
            this.centerLocation = this.getCurrentLocation();
          }
        });
    } else if (this.currentLocation && changes['currentLocation']) {
      this.centerLocation = this.getCurrentLocation();
    }

    // If the changes was in the draws
    if (this.mapContainer) {
      this.drawUserWorkableAreas();
    }

    // Init Draw Options
    this.drawOptions = {
      position: 'topright',
      draw: {
        polyline: false,
        rectangle: false,
        marker: this.drawMarker ? {
          icon: this.getMarkerIcon(true)
        } : false,
        polygon: false,
        circlemarker: false,
        circle: this.drawCirle ? {
          shapeOptions: {
            color: this.mapUISettings.fillColor
          }
        } : false
      },
      edit: {
        featureGroup: this.featureGroup,
        allowIntersection: false
      }
    };

    if (!this.drawableLayer) {
      this.drawableLayer = Leaflet.layerGroup();
      this.layers.push(this.drawableLayer);
    } else {
      this.drawableLayer.clearLayers();
    }

    // Draw static markers
    if (this.mapStaticMarkers) {
      // First add the not selected markers
      const selectedMarkers = [];
      this.mapStaticMarkers.forEach(marker => {
        if (!marker.isSelected) {
          const markerDraw = Leaflet.marker({lat: marker.location.latitude, lng: marker.location.longitude}, {
            icon: this.getMarkerIcon(marker.isSelected),
          });
          this.drawableLayer.addLayer(markerDraw);
        } else {
          selectedMarkers.push(marker);
        }
      });

      // Draw the selected markers
      selectedMarkers.forEach(markerSelected => {
        const markerDraw = Leaflet.marker({lat: markerSelected.location.latitude, lng: markerSelected.location.longitude}, {
          icon: this.getMarkerIcon(markerSelected.isSelected)
        });
        this.drawableLayer.addLayer(markerDraw);
      });
    }

    if (this.mapDraws && !this.isEditabled) {
      Leaflet.layerGroup();
      this.mapDraws.forEach(marker => {
        this.drawableLayer.addLayer(Leaflet.circle({lat: marker.mapArea.location.latitude, lng: marker.mapArea.location.longitude},
          marker.mapArea.radius,
          {...this.drawOptions.draw.circle.shapeOptions}));
      });
    }
  }

  getCurrentLocation() {
    if (this.currentLocation) {
      return Leaflet.latLng(this.currentLocation.latitude, this.currentLocation.longitude);
    } else {
      return this.options.center;
    }
  }

  /**
   * Draw user workable areas
   */
  drawUserWorkableAreas() {
    console.log('drawUserWorkableAreas');
    this.featureGroup.clearLayers();
    if (this.mapDraws) {
      this.mapDraws.forEach(draw => {
        if (draw.mapArea.radius > 0) {
          this.addCircleArea(draw.mapArea);
        } else {
          this.addIconMarker(draw.mapArea);
        }
      });
    }
  }

  addCircleArea(draw: MapAreaDraw) {
    const drawArea = Leaflet.circle(Leaflet.latLng(draw.location.latitude, draw.location.longitude), draw.radius,
      {...this.drawOptions.draw.circle.shapeOptions});
    const responseAddDraw = drawArea.addTo(this.featureGroup);
    // const responseAddDraw = drawArea.addTo(this.mapContainer._map);
    console.log('Added response');
  }

  addIconMarker(draw: MapAreaDraw) {
    const drawArea = Leaflet.marker(Leaflet.latLng(draw.location.latitude, draw.location.longitude), {
      icon: this.getMarkerIcon(true)
    });
    const responseAddDraw = drawArea.addTo(this.featureGroup);
    // const responseAddDraw = drawArea.addTo(this.mapContainer._map);
    console.log('Added response');
  }

  onDrawCreated(event) {
    this.exportMapChanges();
  }

  onDrawEdited(event) {
    this.exportMapChanges();
  }

  onDrawRemoved(event) {
    this.exportMapChanges();
  }

  onDrawReady(data) {
    this.mapContainer = data;

    if (this.mapDraws) {
      this.mapDraws.forEach(draw => {
        this.addCircleArea(draw.mapArea);
      });
    }
  }

  exportMapChanges() {
    console.log('exportMapChanges');
    if (this.layers && this.layers.length > 0) {
      const drawLayer: any = this.layers[0];
      if (drawLayer && drawLayer._layers) {

        // Generate a new map of areas
        const mapAreas: MapAreaDraw[] = [];
        Object.keys(drawLayer._layers).forEach(layerId => {
          const layer: Layer = drawLayer._layers[layerId];
          // Check if is valid
          if (layer._mRadius && layer._latlng && this.drawCirle) {
            mapAreas.push({
              id: layer._leaflet_id as number,
              location: {
                longitude: layer._latlng.lng as number,
                latitude: layer._latlng.lat as number
              },
              radius: layer._mRadius as number
            });
          } else if (layer._icon && layer._latlng && this.drawMarker) {
            mapAreas.push({
              id: layer._leaflet_id as number,
              location: {
                longitude: layer._latlng.lng as number,
                latitude: layer._latlng.lat as number
              },
              radius: 0
            });
          }
        });
        console.log('exportMapChanges - mapAreas ' + JSON.stringify(mapAreas));
        this.mapDrawsChanged.emit(mapAreas);
      }
    }
  }


  getMarkerIcon(isSelected: boolean) {
    const iconSize = isSelected ? 50 : 30;
    return Leaflet.icon({
      iconSize: [iconSize, iconSize],
      iconAnchor: [iconSize / 2, iconSize],
      iconUrl: isSelected ? AppAssets.ic_map_marker_selected : AppAssets.ic_map_marker_empty
    });
  }

  onMapReady(map: Map) {
    this.map = map;

    // Init center
    this.mapCenterLocation = {
      latitude: this.map.getCenter().lat,
      longitude: this.map.getCenter().lng
    };
    this.centerLocationChanged.emit(this.mapCenterLocation);

    // Calculate distance
    this.mapBoundsDistance = this.utils.calcMapLocationsDistances({
      latitude: this.map.getCenter().lat,
      longitude: this.map.getCenter().lng
    }, {
      latitude: this.map.getBounds().getSouthWest().lat,
      longitude: this.map.getBounds().getSouthWest().lng
    });
    this.mapBoundsRadiusChanged.emit(this.mapBoundsDistance);
  }

  onMapMoved(data: any) {
    if (this.map) {
      this.mapCenterLocation = {
        latitude: this.map.getCenter().lat,
        longitude: this.map.getCenter().lng
      };
      this.centerLocationChanged.emit(this.mapCenterLocation);
    }
  }

  onMapZoomChanged(data: any) {
    if (this.map) {
      this.mapBoundsDistance = this.utils.calcMapLocationsDistances({
        latitude: this.map.getCenter().lat,
        longitude: this.map.getCenter().lng
      }, {
        latitude: this.map.getBounds().getSouthWest().lat,
        longitude: this.map.getBounds().getSouthWest().lng
      });
      this.mapBoundsRadiusChanged.emit(this.mapBoundsDistance);
    }
  }
}
