import {Component, Input} from '@angular/core';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

@Component({
  selector: 'app-dashboard-list-tasks',
  templateUrl: './dashboard-list-tasks.component.html',
  styleUrls: ['./dashboard-list-tasks.component.scss']
})
export class DashboardListTasksComponent {
  @Input() listTasks: TaskModel[];
  @Input() listOffers: TaskOfferModel[]; // The list of tasks can also be displayed from task offers
  @Input() title: string;
  @Input() subtitle: string;
  @Input() seeMoreLink: string; // TODO

}
