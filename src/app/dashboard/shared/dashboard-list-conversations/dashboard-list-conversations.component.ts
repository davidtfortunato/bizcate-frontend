import {Component, Input, OnInit} from '@angular/core';
import {UserConversationModel, UserProfileModel} from '../../../../../functions/src/data';
import {LanguageService} from '../../../core/language/language.service';

@Component({
  selector: 'app-dashboard-list-conversations',
  templateUrl: './dashboard-list-conversations.component.html',
  styleUrls: ['./dashboard-list-conversations.component.scss']
})
export class DashboardListConversationsComponent implements OnInit {
  @Input() listConversations: UserConversationModel[];
  @Input() userProfile: UserProfileModel;
  @Input() title: string;
  @Input() subtitle: string;

  // Labels
  labelEmptyList: string;

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    this.labelEmptyList = this.languages.getLanguage().USER_CONVERSATIONS_EMPTY;
  }

}
