import {Component, Input, OnInit} from '@angular/core';
import {DashboardClientModel} from '../../../../functions/src/data/dashboard-model';
import {LanguageService} from '../../core/language/language.service';
import {UserProfileModel} from '../../../../functions/src/data';
import {Observable} from 'rxjs';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';
import {select} from '@ngrx/store';
import {getClientMyDoneTasks, getClientMyOffers, getClientMyOpenedTasks} from '../../core/central-data/store/selectors';
import {TaskOfferModel} from '../../../../functions/src/data/task-offer-model';

@Component({
  selector: 'app-dashboard-client-container',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.scss']
})
export class ClientDashboardComponent implements OnInit {
  @Input() dashboardData: DashboardClientModel;
  @Input() userProfile: UserProfileModel;

  // Labels
  labelMyOpenTasks: string;
  labelCompletedTasks: string;
  labelReceivedOffers: string;
  labelRecentMessages: string;

  // Data
  myTasksOpened$: Observable<TaskModel[]>;
  myTasksDone$: Observable<TaskModel[]>;
  myReceivedOffers$: Observable<TaskOfferModel[]>;

  constructor(private languages: LanguageService, private centralDataStore: CentralUserDataStoreService) {
  }

  ngOnInit() {

    // Central Data
    this.myTasksOpened$ = this.centralDataStore.getStore().pipe(select(getClientMyOpenedTasks));
    this.myTasksDone$ = this.centralDataStore.getStore().pipe(select(getClientMyDoneTasks));
    this.myReceivedOffers$ = this.centralDataStore.getStore().pipe(select(getClientMyOffers));

    // Set labels
    this.labelMyOpenTasks = this.languages.getLanguage().DASHBOARD_TITLE_MY_OPEN_TASKS;
    this.labelCompletedTasks = this.languages.getLanguage().DASHBOARD_TITLE_MY_COMPLETED_TASKS;
    this.labelReceivedOffers = this.languages.getLanguage().DASHBOARD_TITLE_RECEIVED_OFFERS;
    this.labelRecentMessages = this.languages.getLanguage().DASHBOARD_TITLE_RECEIVED_MESSAGES;
  }
}
