import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../core/language/language.service';
import {HeaderActionButton} from '../shared/components/title-header/title-header.component';
import {UserProfileModel, UserProfileRoleType} from '../../../functions/src/data/user-model';
import {Observable} from 'rxjs';
import {select, State} from '@ngrx/store';
import * as fromStore from '../store';
import {getUserProfile} from '../auth/store/selectors';
import {DashboardClientModel, DashboardProfessionalModel} from '../../../functions/src/data/dashboard-model';

@Component({
  selector: 'app-dashboard-container',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // Labels
  headerTitle: { title?: string, subtitle?: string };

  // UI buttons
  headerActionButtons: HeaderActionButton[] = [];

  // Data
  userRoleType = UserProfileRoleType;
  userProfile$: Observable<UserProfileModel>;

  constructor(private languages: LanguageService,
              private store: State<fromStore.State>) {
  }

  ngOnInit() {

    // Load data
    this.userProfile$ = this.store.pipe(select(getUserProfile));

    const subUserProfile = this.userProfile$.subscribe(userProfile => {
      if (userProfile) {
        if (subUserProfile) {
          subUserProfile.unsubscribe();
        }
      }
    });

    // Init labels
    this.headerTitle = {
      title: this.languages.getLanguage().DASHBOARD_MODULE_TITLE,
      subtitle: this.languages.getLanguage().DASHBOARD_MODULE_SUBTITLE_CLIENT
    };

  }

  onHeaderButtonClick(actionButton: HeaderActionButton) {
    // Nothing for now
  }

}
