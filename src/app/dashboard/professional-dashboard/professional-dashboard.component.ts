import {Component, Input, OnInit} from '@angular/core';
import {DashboardProfessionalModel} from '../../../../functions/src/data/dashboard-model';
import {UserProfileModel} from '../../../../functions/src/data';
import {LanguageService} from '../../core/language/language.service';
import {Observable} from 'rxjs';
import {TaskOfferModel} from '../../../../functions/src/data/task-offer-model';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';
import {select} from '@ngrx/store';
import {getProfessionalAssignedTasks, getProfessionalOffersPending} from '../../core/central-data/store/selectors';

@Component({
  selector: 'app-dashboard-professional-container',
  templateUrl: './professional-dashboard.component.html',
  styleUrls: ['./professional-dashboard.component.scss']
})
export class ProfessionalDashboardComponent implements OnInit {
  @Input() dashboardData: DashboardProfessionalModel;
  @Input() userProfile: UserProfileModel;

  // Labels
  labelPendingOffers: string;
  labelAssignedTasks: string;

  // Central data
  myOffersPending$: Observable<TaskOfferModel[]>;
  myTasksAssigned$: Observable<TaskModel[]>;

  constructor(private languages: LanguageService, private centralDataStore: CentralUserDataStoreService) {
  }


  ngOnInit() {

    // Observe data
    this.myOffersPending$ = this.centralDataStore.getStore().pipe(select(getProfessionalOffersPending));
    this.myTasksAssigned$ = this.centralDataStore.getStore().pipe(select(getProfessionalAssignedTasks));

    // Set Labels
    this.labelPendingOffers = this.languages.getLanguage().DASHBOARD_TITLE_MY_PENDING_OFFERS_SENT;
    this.labelAssignedTasks = this.languages.getLanguage().DASHBOARD_TITLE_MY_ASSIGNED_TASKS;
  }
}
