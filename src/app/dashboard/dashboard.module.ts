import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {DashboardComponent} from './dashboard.component';
import {EffectsModule} from '@ngrx/effects';
import {ClientDashboardComponent} from './client-dashboard/client-dashboard.component';
import {ProfessionalDashboardComponent} from './professional-dashboard/professional-dashboard.component';
import {DashboardListTasksComponent} from './shared/dashboard-list-tasks/dashboard-list-tasks.component';
import {TasksSharedModule} from '../tasks/shared/tasks-shared.module';
import {DashboardListConversationsComponent} from './shared/dashboard-list-conversations/dashboard-list-conversations.component';
import {MessagesSharedModule} from '../messages/shared/messages-shared.module';

const ROUTES: Routes = [
  {path: '', component: DashboardComponent}
];

@NgModule({
  providers: [],
  declarations: [DashboardComponent,
    ClientDashboardComponent,
    ProfessionalDashboardComponent,
    DashboardListTasksComponent,
    DashboardListConversationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TasksSharedModule,
    MessagesSharedModule,
    RouterModule.forChild(ROUTES),
  ]
})
export class DashboardModule {
}
