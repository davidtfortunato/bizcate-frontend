import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // To use ngModel
import {CommonModule} from '@angular/common';
import {
  MatAutocompleteModule,
  MatCardModule,
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatRadioModule
} from '@angular/material';

// currently there is a bug while building the app with --prod
// - https://github.com/RaphaelJenni/FirebaseUI-Angular/issues/76
// the plugin exposes the two libraries as well. You can use those:
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import {AngularFireAuthModule} from '@angular/fire/auth';

import {AuthEffects} from './store/effects';
import {AuthStoreService} from './store/auth-store.service';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SharedModule} from '../shared/shared.module';
import {COMPONENTS} from './index';
import {RegisterComponent} from './register/register.component';

const AUTH_ROUTES: Routes = [
  {path: '', component: LoginComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'register', component: RegisterComponent, pathMatch: 'full'},
  {path: 'profile', component: RegisterComponent, pathMatch: 'full'}
];

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      scopes: [
        'public_profile',
        'email',
        'user_likes',
        'user_friends'
      ],
      customParameters: {
        'auth_type': 'reauthenticate'
      },
      provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID
    },
    // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    // firebase.auth.GithubAuthProvider.PROVIDER_ID,
    {
      requireDisplayName: true,
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
    }
    // firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    // firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  ],
  tosUrl: 'https://www.bizcate.com', // TODO
  privacyPolicyUrl: 'https://www.bizcate.com', // TODO
  credentialHelper: firebaseui.auth.CredentialHelper.NONE
};


@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    EffectsModule.forFeature([AuthEffects]),
    RouterModule.forChild(AUTH_ROUTES),
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCardModule,
    MatAutocompleteModule,
    SharedModule,
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig)
  ],
  declarations: [
    COMPONENTS
  ],
  providers: [AuthStoreService],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule {
}
