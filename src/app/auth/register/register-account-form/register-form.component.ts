import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';
import {AppConsts} from '../../../core/utils/constants';

@Component({
  selector: 'app-register-user-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  @Output() registerClick = new EventEmitter<{
    email, password, name
  }>();

  // Languages
  emailPlaceholder: string;
  passwordPlaceholder: string;
  confirmPasswordPlaceholder: string;
  usernamePlaceholder: string;
  namePlaceholder: string;
  registerBtnLabel: string;

  // Errors Messages
  errorPasswordMinLength: string;
  errorEmailInvalid: string;
  errorConfirmPasswordNotSame: string;
  errorFieldRequired: string;

  // Form Controllers
  form: FormGroup;
  passwordFormControl = new FormControl('', Validators.required);
  confirmPasswordFormControl = new FormControl('', Validators.required);
  emailFormControl = new FormControl('', Validators.required);
  nameFormControl = new FormControl('', Validators.required);
  isErrorForm = false;

  constructor(private formBuilder: FormBuilder, private language: LanguageService) {
  }

  ngOnInit() {

    // Get placeholders
    this.emailPlaceholder = this.language.getLanguage().AUTH_LOGIN_PLACEHOLDER;
    this.passwordPlaceholder = this.language.getLanguage().AUTH_PASSWORD_PLACEHOLDER;
    this.confirmPasswordPlaceholder = this.language.getLanguage().AUTH_REGISTER_PASSWORD_PLACEHOLDER;
    this.usernamePlaceholder = this.language.getLanguage().AUTH_REGISTER_USERNAME_PLACEHOLDER;
    this.namePlaceholder = this.language.getLanguage().AUTH_REGISTER_NAME_PLACEHOLDER;
    this.registerBtnLabel = this.language.getLanguage().AUTH_REGISTER_LABEL;
    this.errorPasswordMinLength = this.language.getLanguage().AUTH_ERROR_PASSWORD_MIN_LENGTH;
    this.errorEmailInvalid = this.language.getLanguage().AUTH_ERROR_EMAIL_INVALID;
    this.errorConfirmPasswordNotSame = this.language.getLanguage().AUTH_ERROR_CONFIRMPASSWORD_NOT_SAME;
    this.errorFieldRequired = this.language.getLanguage().AUTH_ERROR_FIELD_REQUIRED;

    this.initForm();
  }

  private initForm() {
    this.emailFormControl.setValidators(Validators.email);
    this.passwordFormControl.setValidators([Validators.required, Validators.minLength(8)]);
    this.confirmPasswordFormControl.setValidators([Validators.required, Validators.minLength(8)]);
    this.nameFormControl.setValidators([Validators.required]);

    // Init Form
    this.form = this.formBuilder.group({
      email: this.emailFormControl,
      password: this.passwordFormControl,
      confirmPassword: this.confirmPasswordFormControl,
      name: this.nameFormControl,
    }, {validators: this.checkPasswords});
  }


  /**
   * Validate if the password are the same
   */
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }


  resetErrorForm() {
    this.isErrorForm = false;
  }

  onRegisterClick() {
    if (this.form.valid) {
      console.log('Register valid');
      this.registerClick.emit({
        email: this.emailFormControl.value,
        password: this.passwordFormControl.value,
        name: this.nameFormControl.value
      });
    } else {
      console.log('Register not valid');
    }
  }

}
