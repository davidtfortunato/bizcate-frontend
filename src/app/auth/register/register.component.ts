import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {AuthStoreService} from '../store/auth-store.service';
import {Router} from '@angular/router';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getIsLoggedIn, getUser, getUserProfileForm, getUserRegisterState} from '../store/selectors';
import {Observable, Subscription} from 'rxjs';
import {AuthUserModel, UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';
import {LanguageService} from '../../core/language/language.service';
import {AppUIParams} from '../../shared/app.ui-params';
import {HeaderActionButton} from '../../shared/components/title-header/title-header.component';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {ProfessionalApiService} from '../../core/api';
import {map} from 'rxjs/operators';
import {UserRegisterState} from '../../../../functions/src/data/user-model';

@Component({
  selector: 'app-register-user',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnChanges, OnDestroy {

  constructor(private authService: AuthStoreService,
              private router: Router,
              private store: State<fromStore.State>,
              private language: LanguageService,
              private linkNavigation: NavigationLinksService,
              private prodessionalAPI: ProfessionalApiService) {
  }

  // Observable Data
  userRegisterState$: Observable<UserRegisterState>;
  isUserLoggedin$: Observable<boolean>;
  userData$: Observable<AuthUserModel>;
  userProfileForm$: Observable<UserProfileModel>;
  userProfessionalProfile$: Observable<UserProfessionalProfile>;

  // Data
  userProfessionalProfile: UserProfessionalProfile;
  userRegiserState: UserRegisterState;

  // UI
  headerTitle: {
    title: string;
    subtitle: string;
  };
  containerTitle: string;
  headerActionButtons: HeaderActionButton[];

  // Subscriptions
  subUserProfessionalProfile: Subscription;
  subUserRegisterState: Subscription;

  ngOnInit() {
    // Get Profile
    this.authService.getAccount();

    this.userRegisterState$ = this.store.pipe(select(getUserRegisterState));
    this.isUserLoggedin$ = this.store.pipe(select(getIsLoggedIn));
    this.userData$ = this.store.pipe(select(getUser));
    this.userProfileForm$ = this.store.pipe(select(getUserProfileForm));
    this.userProfessionalProfile$ = this.prodessionalAPI.getProfessionalProfile().pipe(map(response => response.responseData));
    this.subUserProfessionalProfile = this.userProfessionalProfile$.subscribe(professionalProfile => {
      this.userProfessionalProfile = professionalProfile;
      this.refreshHeaderActionButtons();
    });

    this.headerTitle = {
      title: this.language.getLanguage().USER_PROFILE_SCREEN_TITLE,
      subtitle: this.language.getLanguage().USER_PROFILE_SCREEN_SUBTITLE
    };
    this.containerTitle = this.language.getLanguage().USER_PROFILE_CONTAINER_TITLE;


    // Init Header Action Buttons
    this.refreshHeaderActionButtons();

    // If this component requires the user to be authenticated, then should check the auth state here
    this.subUserRegisterState = this.store.pipe(select(getUserRegisterState)).subscribe(userRegisterState => {
      this.userRegiserState = userRegisterState;
      if (userRegisterState !== UserRegisterState.REGISTERED_NO_PROFILE
        && userRegisterState !== UserRegisterState.REGISTER_COMPLETED) {
        // It's not validated yet. Will redirect to the login page
        this.linkNavigation.openLogin();
      }
    });
  }

  ngOnChanges() {
  }

  ngOnDestroy(): void {
    if (this.subUserRegisterState) {
      this.subUserRegisterState.unsubscribe();
      this.subUserRegisterState = null;
    }
  }

  /**
   * Refresh Header action buttons
   */
  private refreshHeaderActionButtons() {
    this.headerActionButtons = [
      {
        id: '1',
        label: this.language.getLanguage().GENERIC_SAVE_LABEL,
        backgroundColor: AppUIParams.actionButtonGreenColor,
        textColor: AppUIParams.actionButtonWhiteColor
      }
    ];

    // Edit Professional Profile
    if (this.userRegiserState && this.userRegiserState !== UserRegisterState.REGISTERED_NO_PROFILE) {
      if (this.userProfessionalProfile) {
        this.headerActionButtons.push({
          id: '2',
          label: this.language.getLanguage().AUTH_REGISTER_PROFESSIONAL_EDIT_PROFILE,
          backgroundColor: AppUIParams.actionButtonOrangeBackgroundColor,
          textColor: AppUIParams.actionButtonWhiteColor
        });
      }

      // Add Professional profile
      if (!this.userProfessionalProfile) {
        this.headerActionButtons.push({
          id: '2',
          label: this.language.getLanguage().AUTH_REGISTER_PROFESSIONAL_ADD_PROFILE,
          backgroundColor: AppUIParams.actionButtonBlueColor,
          textColor: AppUIParams.actionButtonWhiteColor
        });
      }
    }

  }

  getHeaderActionButtons(isLoggedin) {
    if (isLoggedin) {
      return this.headerActionButtons;
    } else {
      return [];
    }
  }

  onActionButtonClick(actionButton: HeaderActionButton) {
    if (actionButton.id === '1') {
      this.authService.saveProfile();
    } else if (actionButton.id === '2') {
      this.linkNavigation.openEditProfessionalProfile();
    }
  }

  onRegisterClick({email, password, username, name}) {
    this.authService.register(email, password, username, name);
  }

  onSaveProfile() {
    this.authService.saveProfile();
  }
}
