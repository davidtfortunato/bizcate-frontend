import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';
import {AuthUserModel, UserProfileModel, UserRegisterState} from '../../../../../functions/src/data/user-model';
import {AuthStoreService} from '../../store/auth-store.service';
import {AppAssets} from '../../../shared/app.assets';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {AppConsts} from '../../../core/utils/constants';
import {Observable} from 'rxjs';
import {SearchLocationService} from '../../../core/location/search-location.service';
import {MatAutocompleteSelectedEvent} from '@angular/material';
import {LocationResult} from '../../../../../functions/src/data/location-result';

@Component({
  selector: 'app-register-user-profile-form',
  templateUrl: './register-form-profile.component.html',
  styleUrls: ['./register-form-profile.component.scss']
})
export class RegisterFormProfileComponent implements OnInit, OnChanges {

  @Input() defaultNameValue: string;
  @Input() authUserData: AuthUserModel;
  @Input() userProfileForm: UserProfileModel;
  @Input() userRegisterState: UserRegisterState;
  @Output() saveProfileData = new EventEmitter<UserProfileModel>();

  // Types
  userRegisterStateTypes = UserRegisterState;

  // Data
  citySearchResults$: Observable<LocationResult[]>;

  // Error
  errorFieldRequired: string;
  labelProfileNotComplete: string;

  // Placeholders
  namePlaceholder: string;
  birthdayPlaceholder: string;
  genderPlaceholder: string;
  genderMalePlaceholder: string;
  genderFemalePlaceholder: string;
  usertypePlaceholder: string;
  addressPlaceholder: string;
  countryPlaceholder: string;
  cityPlaceholder: string;
  phonenumberPlaceholder: string;
  vatnumberPlaceholder: string;
  linkedinUrlPlaceholder: string;
  facebookUrlPlaceholder: string;

  // Form Controllers
  form: FormGroup;
  nameFormControl = new FormControl('', Validators.required);
  birthdayFormControl = new FormControl('', Validators.required);
  genderFormControl = new FormControl('', Validators.required);
  addressFormControl = new FormControl('', Validators.required);
  countryFormControl = new FormControl({value: '', disabled: true}, Validators.required);
  cityFormControl = new FormControl('', Validators.required);
  phonenumberFormControl = new FormControl({value: '', disabled: true}, Validators.required);
  vatnumberFormControl = new FormControl('', Validators.required);
  linkedinUrlFormControl = new FormControl('');
  facebookUrlFormControl = new FormControl('');

  userLocationSelected: LocationResult;

  // Limits
  birthdayMaxDate = new Date();

  // Values
  avatarFileName = AppConsts.FILENAME_PROFILE_AVATAR;

  constructor(private language: LanguageService,
              private formBuilder: FormBuilder,
              private authService: AuthStoreService,
              private linkNavigation: NavigationLinksService,
              private searchLocation: SearchLocationService,
              private authStoreService: AuthStoreService) {
  }

  ngOnInit() {
    // Get the current User Account
    this.authService.getAccount();

    this.labelProfileNotComplete = this.language.getLanguage().AUTH_ERROR_PROFILE_NOT_COMPLETED;
    this.errorFieldRequired = this.language.getLanguage().AUTH_ERROR_FIELD_REQUIRED;
    this.namePlaceholder = this.language.getLanguage().AUTH_REGISTER_NAME_PLACEHOLDER;
    this.birthdayPlaceholder = this.language.getLanguage().AUTH_REGISTER_BIRTHDAY_PLACEHOLDER;
    this.genderPlaceholder = this.language.getLanguage().AUTH_REGISTER_GENDER_PLACEHOLDER;
    this.genderMalePlaceholder = this.language.getLanguage().AUTH_REGISTER_GENDER_MALE_PLACEHOLDER;
    this.genderFemalePlaceholder = this.language.getLanguage().AUTH_REGISTER_GENDER_FEMALE_PLACEHOLDER;
    this.usertypePlaceholder = this.language.getLanguage().AUTH_REGISTER_USERTYPE_PLACEHOLDER;
    this.addressPlaceholder = this.language.getLanguage().AUTH_REGISTER_ADDRESS_PLACEHOLDER;
    this.countryPlaceholder = this.language.getLanguage().AUTH_REGISTER_COUNTRY_PLACEHOLDER;
    this.cityPlaceholder = this.language.getLanguage().AUTH_REGISTER_CITY_PLACEHOLDER;
    this.phonenumberPlaceholder = this.language.getLanguage().AUTH_REGISTER_PHONENUMBER_PLACEHOLDER;
    this.vatnumberPlaceholder = this.language.getLanguage().AUTH_REGISTER_VATNUMBER_PLACEHOLDER;
    this.linkedinUrlPlaceholder = this.language.getLanguage().AUTH_REGISTER_LINKEDINURL_PLACEHOLDER;
    this.facebookUrlPlaceholder = this.language.getLanguage().AUTH_REGISTER_FACEBOOKURL_PLACEHOLDER;

    // Disable Phone Number edition
    this.phonenumberFormControl.disable();
    this.countryFormControl.disable();

    this.initForm();

  }

  ngOnChanges() {

    if (this.userProfileForm) {
      this.userLocationSelected = this.userProfileForm.userLocation;

      this.nameFormControl.setValue(this.userProfileForm.name, {emitEvent: false});
      this.addressFormControl.setValue(this.userProfileForm.address, {emitEvent: false});
      this.birthdayFormControl.setValue(this.userProfileForm.birthday ? new Date(this.userProfileForm.birthday) : '',
        {emitEvent: false});
      this.genderFormControl.setValue(this.userProfileForm.gender ? this.userProfileForm.gender : 'M', {emitEvent: false});
      this.countryFormControl.setValue(this.userProfileForm.country, {emitEvent: false});
      this.cityFormControl.setValue(this.userProfileForm.city, {emitEvent: false});
      this.vatnumberFormControl.setValue(this.userProfileForm.vatNumber, {emitEvent: false});
      this.linkedinUrlFormControl.setValue(this.userProfileForm.linkedinUrl ? this.userProfileForm.linkedinUrl : '', {emitEvent: false});
      this.facebookUrlFormControl.setValue(this.userProfileForm.facebookUrl ? this.userProfileForm.facebookUrl : '', {emitEvent: false});
      this.phonenumberFormControl.setValue(this.authUserData.phoneNumber, {emitEvent: false});
    } else {
      this.nameFormControl.setValue(this.authUserData.displayName, {emitEvent: false});
      this.phonenumberFormControl.setValue(this.authUserData.phoneNumber, {emitEvent: false});
    }
  }

  private initForm() {
    // Init Form
    this.form = this.formBuilder.group({
      name: this.nameFormControl,
      birthday: this.birthdayFormControl,
      gender: this.genderFormControl,
      address: this.addressFormControl,
      country: this.countryFormControl,
      city: this.cityFormControl,
      phoneNumber: this.phonenumberFormControl,
      vatNumber: this.vatnumberFormControl,
      linkedinUrl: this.linkedinUrlFormControl,
      facebookUrl: this.facebookUrlFormControl
    });

    // Listen to city name changes
    this.cityFormControl.valueChanges.subscribe(value => {
      this.citySearchResults$ = this.searchLocation.searchLocation(value);
    });

    // Listen to the form changes
    this.form.valueChanges.subscribe(value => {
      this.onFormChanges();
    });
  }

  onFormChanges() {
    this.userProfileForm = {
      ...this.userProfileForm,
      birthday: this.birthdayFormControl.value ? this.birthdayFormControl.value.getTime() : '',
      gender: this.genderFormControl.value,
      name: this.nameFormControl.value,
      address: this.addressFormControl.value,
      country: this.countryFormControl.value,
      city: this.cityFormControl.value,
      phoneNumber: this.phonenumberFormControl.value,
      vatNumber: this.vatnumberFormControl.value,
      linkedinUrl: this.linkedinUrlFormControl.value,
      facebookUrl: this.facebookUrlFormControl.value,
      userLocation: this.userLocationSelected
    };
    this.authStoreService.updateProfileForm(this.userProfileForm);
  }

  getUserPhotoURL() {
    if (this.userProfileForm && this.userProfileForm.photoUrl) {
      return this.userProfileForm.photoUrl;
    } else if (this.authUserData.photoUrl) {
      return this.authUserData.photoUrl;
    } else {
      return AppAssets.ic_avatar;
    }
  }

  onPhotoUpload(photoUrl: string) {
    if (photoUrl) {
      this.userProfileForm.photoUrl = photoUrl;
      this.authStoreService.updateProfileForm(this.userProfileForm);
    }
  }

  onSelectedCity(selectedCity: MatAutocompleteSelectedEvent) {
    this.userLocationSelected = selectedCity.option.value;

    // Update forms
    if (this.userLocationSelected) {
      this.cityFormControl.setValue(this.userLocationSelected.name);
      this.countryFormControl.setValue(this.userLocationSelected.country);
    }
  }

}
