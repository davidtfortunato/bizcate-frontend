import {Action} from '@ngrx/store';
import {ApiResponse, UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';
import {AuthUserModel} from '../../../../functions/src/data';

export enum AuthActionTypes {
  Init = '[Auth] Init Auths',

  // Login
  Login = '[Auth] Login',
  LoginSuccess = '[Auth] LoginSuccess',
  LoginFailed = '[Auth] LoginFailed',

  // Get User Account
  GetAccount = '[Auth] GetAccount',
  GetAccountSuccess = '[Auth] GetAccountSuccess',
  GetAccountFailed = '[Auth] GetAccountFailed',

  // Get User Profile
  GetUserProfile = '[Auth] GetUserProfile',
  GetUserProfileResponse = '[Auth] GetUserProfileResponse',
  EditUserProfileForm = '[Auth] EditUserProfileForm',

  // Get User Professional Profile
  GetUserProfessionalProfile = '[Auth] GetUserProfessionalProfile',
  GetUserProfessionalProfileResponse = '[Auth] GetUserProfessionalProfileResponse',

  // Register
  Register = '[Auth] Register',
  RegisterSuccess = '[Auth] RegisterSuccess',
  RegisterFailed = '[Auth] RegisterFailed',

  // Save Profile
  SaveProfile = '[Auth] SaveProfile',
  SaveProfileSuccess = '[Auth] SaveProfileSuccess',
  SaveProfileFailed = '[Auth] SaveProfileFailed',

  // Logout
  Logout = '[Auth] Logout',
  LogoutSuccess = '[Auth] LogoutSuccess',
  LogoutFail = '[Auth] LogoutFail'
}

export class Init implements Action {
  readonly type = AuthActionTypes.Init;
}

/**
 * Get current user account by his JWToken
 */
export class GetAccount implements Action {
  readonly type = AuthActionTypes.GetAccount;
}

/**
 * Get current user account by his JWToken
 */
export class GetAccountSuccess implements Action {
  readonly type = AuthActionTypes.GetAccountSuccess;

  constructor(public payload: AuthUserModel) {
  }
}

/**
 * Get current user profile
 */
export class GetUserProfile implements Action {
  readonly type = AuthActionTypes.GetUserProfile;

  constructor(public uid: string) { }
}

/**
 * Get current user profile resposne
 */
export class GetUserProfileResponse implements Action {
  readonly type = AuthActionTypes.GetUserProfileResponse;

  constructor(public payload: UserProfileModel) { }
}

/**
 * Get current user profile professioanl
 */
export class GetUserProfessionalProfile implements Action {
  readonly type = AuthActionTypes.GetUserProfessionalProfile;

  constructor(public uid: string) { }
}

/**
 * Get current user profile professional response
 */
export class GetUserProfessionalProfileResponse implements Action {
  readonly type = AuthActionTypes.GetUserProfessionalProfileResponse;

  constructor(public payload: UserProfessionalProfile) { }
}

/**
 * Get current user profile resposne
 */
export class EditUserProfileForm implements Action {
  readonly type = AuthActionTypes.EditUserProfileForm;

  constructor(public payload: UserProfileModel) { }
}

/**
 * Get current user account by his JWToken
 */
export class GetAccountFailed implements Action {
  readonly type = AuthActionTypes.GetAccountFailed;

  constructor(public payload) {
  }
}

/**
 * Action to execute login
 */
export class Login implements Action {
  readonly type = AuthActionTypes.Login;

  constructor(public payload: { email: string, password: string }) {
  }
}

/**
 * Response after executing the login action
 */
export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: ApiResponse<any>) {
  }
}

/**
 * Response after executing the login action
 */
export class LoginFailed implements Action {
  readonly type = AuthActionTypes.LoginFailed;

  constructor(public payload: ApiResponse<any>) {
  }
}

/**
 * Execute logout action
 */
export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

/**
 * Execute logout action
 */
export class LogoutSuccess implements Action {
  readonly type = AuthActionTypes.LogoutSuccess;
}

/**
 * Logout call failed
 */
export class LogoutFail implements Action {
  readonly type = AuthActionTypes.LogoutFail;
}


/**
 * Action to execute Register
 */
export class Register implements Action {
  readonly type = AuthActionTypes.Register;

  constructor(public payload: { email: string, password: string, name: string, username: string }) {
  }
}

/**
 * Response after executing the login action
 */
export class RegisterSuccess implements Action {
  readonly type = AuthActionTypes.RegisterSuccess;

  constructor(public payload: { response: ApiResponse<any>, email, password, displayName}) {
  }
}

/**
 * Response after executing the login action
 */
export class RegisterFailed implements Action {
  readonly type = AuthActionTypes.RegisterFailed;

  constructor(public payload: ApiResponse<any>) {
  }
}

/**
 * Action to execute Save Profile
 */
export class SaveProfile implements Action {
  readonly type = AuthActionTypes.SaveProfile;
}

/**
 * Response after executing the login action
 */
export class SaveProfileSuccess implements Action {
  readonly type = AuthActionTypes.SaveProfileSuccess;

  constructor(public payload: ApiResponse<any>) {
  }
}

/**
 * Response after executing the login action
 */
export class SaveProfileFailed implements Action {
  readonly type = AuthActionTypes.SaveProfileFailed;

  constructor(public payload: ApiResponse<any>) {
  }
}

export type Actions = Init
  | Login
  | LoginSuccess
  | LoginFailed
  | Logout
  | LogoutSuccess
  | LogoutFail
  | Register
  | RegisterSuccess
  | RegisterFailed
  | SaveProfile
  | SaveProfileSuccess
  | SaveProfileFailed
  | GetAccount
  | GetAccountSuccess
  | GetAccountFailed
  | GetUserProfile
  | GetUserProfileResponse
  | EditUserProfileForm
  | GetUserProfessionalProfile
  | GetUserProfessionalProfileResponse;
