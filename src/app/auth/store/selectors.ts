import {createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromAuthStore from './reducer';
import {isUserProfileFinished, UserLoggedinState, UserRegisterState} from '../../../../functions/src/data/user-model';

export const selectAuthState = createFeatureSelector<fromAuthStore.State>('auth');

// Getters
export const getUser = createSelector(selectAuthState, fromAuthStore.getUser);
export const getUserProfile = createSelector(selectAuthState, fromAuthStore.getUserProfile);
export const getUserProfileForm = createSelector(selectAuthState, fromAuthStore.getUserProfileForm);
export const getUserProfessionalProfile = createSelector(selectAuthState, fromAuthStore.getUserProfessionalProfileForm);
export const isAuthInitialized = createSelector(selectAuthState, fromAuthStore.isAuthInitialized);
export const getIsLoggedIn = createSelector(getUser, isAuthInitialized, (user, isInitied) => {
  return user && !user.isAnonymous && user.uid != null && user.uid.length > 0 && isInitied;
});
export const getIsAnonymousUser = createSelector(getIsLoggedIn, isAuthInitialized, (isLoggedIn, isInitialized) => {
  return isInitialized && !isLoggedIn;
});
export const isUserAccountValidated = createSelector(getUser, getIsLoggedIn, (user, isLoggedIn) => {
  return isLoggedIn && user.emailVerified && user.phoneNumber;
});

export const getUserProfessionalProfileSkills = createSelector(getUserProfessionalProfile, (userPro => {
  let listSkills = [];
  if (userPro) {
    listSkills = userPro.professionalSkills;
  }
  return listSkills;
}));

export const getUserLoggedInState = createSelector(getIsLoggedIn, isAuthInitialized, (isLoggedIn, isAuthInitialized) => {
  if (!isAuthInitialized) {
    return UserLoggedinState.INITIALIZING_SESSION;
  } else if (isLoggedIn) {
    return UserLoggedinState.LOGGEDIN;
  } else {
    return UserLoggedinState.ANONYMOUS;
  }
});

export const getUserRegisterState = createSelector(getIsLoggedIn, getUser, getUserProfile, (isLoggedIn, user, userProfile) => {
  if (user) {
    if (isLoggedIn) {
      // Is logged in, check if is missing something
      if (!user.emailVerified) {
        return UserRegisterState.REGISTERED_NOT_VALIDATED_EMAIL;
      } else if (!user.phoneNumber) {
        return UserRegisterState.REGISTERED_NOT_VALIDATED_PHONE;
      } else if (!isUserProfileFinished(userProfile)) {
        return UserRegisterState.REGISTERED_NO_PROFILE;
      } else {
        return UserRegisterState.REGISTER_COMPLETED;
      }
    } else {
      return UserRegisterState.NOT_LOGGEDIN;
    }
  } else {
    return UserRegisterState.LOADING_USER_DATA;
  }
});

export const getUserPhotoUrl = createSelector(getUserProfile, getUser, (userProfile, userAuth) => {
  if (userProfile.photoUrl) {
    return userProfile.photoUrl;
  } else {
    return userAuth.photoUrl;
  }
});
