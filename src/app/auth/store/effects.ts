import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, concatMap, map, switchMap, withLatestFrom} from 'rxjs/operators';

import * as actions from './actions';
import {
  GetAccount,
  GetAccountFailed,
  GetAccountSuccess, GetUserProfessionalProfile,
  GetUserProfessionalProfileResponse,
  GetUserProfileResponse,
  Login,
  LoginFailed,
  LoginSuccess,
  Logout,
  LogoutFail,
  LogoutSuccess,
  RegisterFailed,
  RegisterSuccess,
  SaveProfileFailed,
  SaveProfileSuccess
} from './actions';
import {AuthAPIService} from '../../core/api/auth-api.service';
import {LocalStorageService} from '../../core/data/local-storage-service';
import {SnackBarService} from '../../shared/components/snack-bar/snack-bar.service';
import {LanguageService} from '../../core/language/language.service';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {UserAPIService} from '../../core/api/user-api.service';
import {select, Store} from '@ngrx/store';
import {State} from '../../store';
import {getUserProfileForm} from './selectors';
import {UserProfileRoleType} from '../../../../functions/src/data/user-model';
import {NoAction} from '../../store/app.actions';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {AppStoreService} from '../../store/app-store.service';


@Injectable()
export class AuthEffects {

  @Effect()
  initAuth$ = this.actions$.pipe(
    ofType(actions.AuthActionTypes.Init),
    map(action => {
      return new GetAccount();
    })
  );

  /** GET ACCOUNT **/
  @Effect()
  getAccount$ = this.actions$.pipe(
    ofType(actions.AuthActionTypes.GetAccount),
    switchMap(action => {
      return this.authAPIService.getUserAccount().pipe(map(response => {
        if (response && response.uid) {
          return new GetAccountSuccess({
            uid: response.uid,
            displayName: response.displayName,
            photoUrl: response.photoURL,
            email: response.email,
            emailVerified: response.emailVerified,
            isAnonymous: response.isAnonymous,
            phoneNumber: response.phoneNumber
          });
        } else {
          return new GetAccountFailed(null);
        }
      }), catchError((err: HttpErrorResponse) => {
        return of(new GetAccountFailed(err.error));
      }));
    })
  );

  /*@Effect()
  getAccountFailed$ = this.actions$.pipe(
    ofType(actions.AuthActionTypes.GetAccountFailed),
    map(action => {
      return new Logout();
    })
  );*/

  @Effect()
  getAccountSuccess$ = this.actions$.pipe(
    ofType<actions.GetAccountSuccess>(actions.AuthActionTypes.GetAccountSuccess),
    // map(action => new actions.GetUserProfile(action.payload.uid))
    switchMap(action => {
      return this.userAPIService.getProfileData(action.payload.uid).pipe(map(response => {
        return new GetUserProfileResponse(response);
      }), catchError(err => of(new GetUserProfileResponse(null))));
    })
  );

  @Effect()
  getUserProfile$ = this.actions$.pipe(
    ofType<actions.GetUserProfile>(actions.AuthActionTypes.GetUserProfile),
    switchMap(action => {
      return this.userAPIService.getProfileData(action.uid).pipe(map(response => {
        return new GetUserProfileResponse(response);
      }), catchError(err => of(new GetUserProfileResponse(null))));
    })
  );

  @Effect()
  getUserProfileResponse$ = this.actions$.pipe(
    ofType<actions.GetUserProfileResponse>(actions.AuthActionTypes.GetUserProfileResponse),
    map(action => {
      return action.payload
      && action.payload.role === UserProfileRoleType.CLIENT_PROFESSIONAL
        ? new GetUserProfessionalProfile(action.payload.uid) : new NoAction();
    })
  );

  /** END GET ACCOUNT **/

  /** GET USER PROFESSIONAL PROFILE **/

  @Effect()
  getUserProfileProfessional$ = this.actions$.pipe(
    ofType<actions.GetUserProfessionalProfile>(actions.AuthActionTypes.GetUserProfessionalProfile),
    switchMap(action => {
      return this.userAPIService.getProfessionalProfileData(action.uid).pipe(map(response => {
        return new GetUserProfessionalProfileResponse(response);
      }), catchError(err => of(new GetUserProfessionalProfileResponse(null))));
    })
  );

  /** END GET USER PROFESSIONAL PROFILE **/

  /*** LOGIN **/

  @Effect()
  login$ = this.actions$.pipe(
    ofType<actions.Login>(actions.AuthActionTypes.Login),
    switchMap(action => {
      this.appService.displayProgressBarById('login');
      return this.authAPIService.doLogin(action.payload.email, action.payload.password)
        .pipe(map(response => {
          this.appService.hideProgressBar('login');
          if (response.isSuccess) {
            return new LoginSuccess(response);
          } else {
            return new LoginFailed(response);
          }
        }), catchError((err: HttpErrorResponse) => {
          return of(new LoginFailed(err.error));
        }));
    })
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType<actions.LoginSuccess>(actions.AuthActionTypes.LoginSuccess),
    map(action => {
        let nameToDisplay = action.payload.responseData.user.displayName;
        if (!nameToDisplay) {
          nameToDisplay = action.payload.responseData.user.email;
        }

        this.snackBarService.displayMessage(this.languageService
          .getLanguage().AUTH_LOGIN_SUCCESS.replace('[X]', nameToDisplay));
        return new GetAccount();
      }
    )
  );


  @Effect({dispatch: false})
  loginFailed$ = this.actions$.pipe(
    ofType<actions.LoginFailed>(actions.AuthActionTypes.LoginFailed),
    map(action => {
        if (action.payload && action.payload.displayMessage) {
          this.snackBarService.displayMessage(action.payload.message, 'error');
        }
      }
    )
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType<actions.LoginSuccess>(actions.AuthActionTypes.Logout),
    switchMap(action => {
      return this.authAPIService.doLogout().pipe(map(response => {
        if (response.isSuccess) {
          this.navigation.openHome();
          return new LogoutSuccess();
        } else {
          return new LogoutFail();
        }
      }));
    }));

  /*** END LOGIN **/

  /** REGISTER **/

  @Effect()
  register$ = this.actions$.pipe(
    ofType<actions.Register>(actions.AuthActionTypes.Register),
    switchMap(action => {
      this.appService.displayProgressBarById('register');
      return this.authAPIService.doRegisterUser(action.payload.email, action.payload.password, action.payload.name)
        .pipe(map(response => {
          this.appService.hideProgressBar('register');
          if (response.isSuccess) {
            return new RegisterSuccess({
              response, email: action.payload.email, password: action.payload.password,
              displayName: action.payload.name
            });
          } else {
            return new RegisterFailed(response);
          }

        }), catchError((err: HttpErrorResponse) => {
          return of(new RegisterFailed(err.error));
        }));
    })
  );

  @Effect()
  registerSuccess$ = this.actions$.pipe(
    ofType<actions.RegisterSuccess>(actions.AuthActionTypes.RegisterSuccess),
    map(action => action.payload),
    concatMap(payload => {
      this.snackBarService.displayMessage(this.languageService.getLanguage().AUTH_REGISTERED_SUCCESS);
      return [new Login({email: payload.email, password: payload.password})];
    })
  );

  /** END REGISTER **/

  /** Save Profile **/

  @Effect()
  saveProfile$ = this.actions$.pipe(
    ofType<actions.SaveProfile>(actions.AuthActionTypes.SaveProfile),
    withLatestFrom(this.store.pipe(select(getUserProfileForm))),
    switchMap(([action, userProfileForm]) => {
      this.appService.displayProgressBarById('saveProfile');
      return this.userAPIService.patchSaveProfile(userProfileForm).pipe(map(response => {
        this.appService.hideProgressBar('saveProfile');
        if (response.isSuccess) {
          this.snackBarService.displayMessage(this.languageService.getLanguage().USER_PROFILE_UPDATED_SUCCESS);
          return new SaveProfileSuccess(response);
        } else {
          return new SaveProfileFailed(response);
        }
      }), catchError((err: HttpErrorResponse) => {
        this.snackBarService.displayMessage(this.languageService.getLanguage().GENERIC_ERROR_SAVING, 'error');
        return of(new SaveProfileFailed(err.error));
      }));
    })
  );

  /** END Save Profile **/

  constructor(private actions$: Actions,
              private authAPIService: AuthAPIService,
              private userAPIService: UserAPIService,
              private localStorage: LocalStorageService,
              private snackBarService: SnackBarService,
              private languageService: LanguageService,
              private store: Store<State>,
              private navigation: NavigationLinksService,
              private appService: AppStoreService) {
  }
}
