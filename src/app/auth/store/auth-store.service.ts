import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';

import * as actions from './actions';
import * as fromReducer from './reducer';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable()
export class AuthStoreService {
  constructor(private store: Store<fromReducer.State>, private afAuth: AngularFireAuth) {
  }

  init() {
    this.store.dispatch(new actions.Init());
  }

  login(email: string, password: string) {
    this.store.dispatch(new actions.Login({email, password}));
  }

  logout() {
    this.store.dispatch(new actions.Logout());
  }

  register(email: string, password: string, username: string, name: string) {
    this.store.dispatch(new actions.Register({email, password, username, name}));
  }

  saveProfile() {
    this.store.dispatch(new actions.SaveProfile());
  }

  updateProfileForm(profileData) {
    this.store.dispatch(new actions.EditUserProfileForm(profileData));
  }

  getAccount() {
    this.store.dispatch(new actions.GetAccount());
  }


  /**
   * Get User UID
   */
  getUserUID() {
    if (this.afAuth.auth.currentUser) {
      return this.afAuth.auth.currentUser.uid;
    } else {
      return null;
    }
  }
}
