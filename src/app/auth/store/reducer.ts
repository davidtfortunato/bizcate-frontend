import * as actions from './actions';
import {AuthUserModel, UserProfessionalProfile, UserProfileModel} from '../../../../functions/src/data';

export interface State {
  user?: AuthUserModel;
  userProfile?: UserProfileModel;
  userProfessionalProfile?: UserProfessionalProfile;
  formUserProfile?: UserProfileModel;
  isInitialized: boolean;
}

export const initialState: State = {
  user: null,
  userProfile: null,
  userProfessionalProfile: null,
  formUserProfile: null,
  isInitialized: false
};

export function reducer(state = initialState, action: actions.Actions): State {
  switch (action.type) {
    case actions.AuthActionTypes.LogoutSuccess:
      return {
        ...state,
        user: {
          isAnonymous: true
        },
        userProfile: null,
        userProfessionalProfile: null
      };
    case actions.AuthActionTypes.SaveProfileSuccess:
      return {
        ...state,
        userProfile: action.payload.responseData
      };
    case actions.AuthActionTypes.GetAccountSuccess:
      return {
        ...state,
        user: action.payload
      };
    case actions.AuthActionTypes.GetAccountFailed:
      return {
        ...state,
        user: {
          isAnonymous: true
        },
        isInitialized: true
      };

    case actions.AuthActionTypes.GetUserProfileResponse:
      if (action.payload) {
        return {
          ...state,
          user: {
            ...state.user,
            photoUrl: action.payload.photoUrl
          },
          userProfile: action.payload,
          formUserProfile: action.payload,
          isInitialized: true
        };
      }
      break;

    case actions.AuthActionTypes.EditUserProfileForm:
      if (action.payload) {
        return {
          ...state,
          formUserProfile: action.payload
        };
      }
      break;

    case actions.AuthActionTypes.GetUserProfessionalProfileResponse:
      if (action.payload) {
        return {
          ...state,
          userProfessionalProfile: action.payload
        };
      }
      break;
  }

  return state;
}

// Pure Functions
export const getUser = (state: State) => state.user;
export const getUserProfile = (state: State) => state.userProfile;
export const getUserProfileForm = (state: State) => state.formUserProfile;
export const getUserProfessionalProfileForm = (state: State) => state.userProfessionalProfile;
export const isAuthInitialized = (state: State) => state.isInitialized;
