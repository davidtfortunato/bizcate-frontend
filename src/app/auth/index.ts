import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {RegisterFormProfileComponent} from './register/register-profile-form/register-form-profile.component';
import {RegisterFormComponent} from './register/register-account-form/register-form.component';
import {LoginFirebaseComponent} from './login/login-firebase/login-firebase.component';
import {ValidateAccountComponent} from './login/validate-account/validate-account.component';
import {ValidatePhoneComponent} from './login/validate-phone/validate-phone.component';

export const COMPONENTS = [LoginComponent,
  LoginFirebaseComponent,
  ValidateAccountComponent,
  ValidatePhoneComponent,
  RegisterComponent,
  RegisterFormComponent,
  RegisterFormProfileComponent];
