import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {select, State} from '@ngrx/store';

import {LanguageService} from '../../core/language/language.service';
import {AuthStoreService} from '../store/auth-store.service';
import {getUserRegisterState} from '../store/selectors';
import * as fromStore from '../../store/index';
import {Observable, Subscription} from 'rxjs';
import {UserRegisterState} from '../../../../functions/src/data/user-model';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // Labels
  titleLoginContainer: string;
  titleValidateAccountContainer: string;
  titleValidatePhoneContainer: string;

  // Data
  userStateType = UserRegisterState;
  userState$: Observable<UserRegisterState>;
  userState: UserRegisterState;

  // Subscription
  subUserState: Subscription;

  constructor(private language: LanguageService,
              private formBuilder: FormBuilder,
              private authService: AuthStoreService,
              private router: Router,
              private store: State<fromStore.State>,
              private navigation: NavigationLinksService) {
  }

  ngOnInit() {

    this.titleLoginContainer = this.language.getLanguage().AUTH_LOGIN_LABEL;
    this.titleValidateAccountContainer = this.language.getLanguage().AUTH_LOGIN_VALIDATE_ACCOUNT_LABEL;
    this.titleValidatePhoneContainer = this.language.getLanguage().AUTH_LOGIN_VALIDATE_PHONENUMBER_LABEL;

    this.authService.init();
    this.userState$ = this.store.pipe(select(getUserRegisterState));
    this.userState$.subscribe(userState => {
      console.log('UserState: ' + userState);
      this.userState = userState;
      if (this.userState === this.userStateType.REGISTER_COMPLETED) {
        // Redirect to home if the user is already logged in
        this.router.navigate(['/']);
      } else if (this.userState === this.userStateType.REGISTERED_NO_PROFILE) {
        this.navigation.openEditProfile();
      };
    });
  }

  ngOnDestroy() {
    if (this.subUserState) {
      this.subUserState.unsubscribe();
      this.subUserState = null;
    }
  }

}
