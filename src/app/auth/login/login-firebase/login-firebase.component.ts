import {Component, OnInit} from '@angular/core';
import {FirebaseuiAngularLibraryService, FirebaseUISignInFailure, FirebaseUISignInSuccessWithAuthResult} from 'firebaseui-angular';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-login-firebase',
  templateUrl: './login-firebase.component.html',
  styleUrls: ['./login-firebase.component.scss']
})
export class LoginFirebaseComponent implements OnInit {

  constructor(private firebaseuiAngularLibraryService: FirebaseuiAngularLibraryService, private afAuth: AngularFireAuth) {
    firebaseuiAngularLibraryService.firebaseUiInstance.disableAutoSignIn();
  }


  ngOnInit(): void {
  }

  successCallback(signInSuccessData: FirebaseUISignInSuccessWithAuthResult) {
    if (!this.afAuth.auth.currentUser.emailVerified) {
      // Send confirmation email
      this.afAuth.auth.currentUser.sendEmailVerification();
    }
  }

  errorCallback(errorData: FirebaseUISignInFailure) {
    console.log('errorCallback - ' + errorData.code);
  }

}
