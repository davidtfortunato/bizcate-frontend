import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {AppAssets} from '../../../shared/app.assets';
import * as firebase from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-validate-account-container',
  templateUrl: './validate-account.component.html',
  styleUrls: ['./validate-account.component.scss']
})
export class ValidateAccountComponent implements OnInit {

  // Labels
  validateEmailLabel: string;
  labelResendEmail: string;

  // Icons
  notValidatedIconUrl: string = AppAssets.ic_validate_account;

  constructor(private languages: LanguageService, public afAuth: AngularFireAuth) {
  }

  ngOnInit(): void {
    this.validateEmailLabel = this.languages.getLanguage().AUTH_VALIDATE_EMAIL;
    this.labelResendEmail = this.languages.getLanguage().AUTH_VALIDATE_EMAIL_RESEND_VALIDATION;

  }

  onResendEmailClick() {
    this.afAuth.auth.currentUser.sendEmailVerification().then(() => {
      console.log('Email successfuly sent');
    }).catch(err => {
      console.log('Error sending email: ' + err.toString());
    });
  }
}
