import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {WindowRefService} from '../../../shared/platform/window.service';
import {LanguageService} from '../../../core/language/language.service';
import {AppAssets} from '../../../shared/app.assets';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../../core/utils/utils.service';
import {AuthStoreService} from '../../store/auth-store.service';
import {AppStoreService} from '../../../store/app-store.service';

@Component({
  selector: 'app-validate-phone',
  templateUrl: './validate-phone.component.html',
  styleUrls: ['./validate-phone.component.scss']
})
export class ValidatePhoneComponent implements OnInit {

  // Labels
  labelValidatePhoneNumber: string;
  labelSendSMSCode: string;
  labelEnterValidationCode: string;
  validatePhoneNumberIconUrl: string;
  phoneNumberPlaceholder = '+351 9XXXXXXXX';
  validateCodePlaceholder = '+351 9XXXXXXXX';
  labelBtnValidateSMSCode: string;
  // Error
  errorFieldRequired: string;
  errorCodeInvalid: string;

  // Form
  phoneForm: FormGroup;
  phoneNumberForm: FormControl;
  validationCodeFormGroup: FormGroup;
  validateCodeForm = new FormControl('', Validators.required);
  isInvalidCode: boolean = false;

  windowRef: any;
  user: any;

  // Data
  confirmationResult: any;

  constructor(private windowService: WindowRefService,
              private languages: LanguageService,
              private formBuilder: FormBuilder,
              private utils: UtilsService,
              private authStoreService: AuthStoreService,
              private appStoreService: AppStoreService) {
  }

  ngOnInit() {

    // Init form group
    this.phoneNumberForm = new FormControl('', [Validators.required,
      Validators.pattern(this.utils.getValidatorPhoneNumberPattern())]);
    this.phoneForm = this.formBuilder.group({
      phoneNumber: this.phoneNumberForm
    });
    this.validationCodeFormGroup = this.formBuilder.group({
      validateCodeForm: this.validateCodeForm
    });

    // Init labels
    this.labelValidatePhoneNumber = this.languages.getLanguage().AUTH_VALIDATE_PHONE_NUMBER;
    this.labelSendSMSCode = this.languages.getLanguage().AUTH_VALIDATE_PHONE_SEND_SMSCODE;
    this.validatePhoneNumberIconUrl = AppAssets.ic_validate_phone_number;
    this.errorFieldRequired = this.languages.getLanguage().AUTH_ERROR_FIELD_REQUIRED;
    this.labelEnterValidationCode = this.languages.getLanguage().AUTH_VALIDATE_PHONE_SET_SMS_CODE;
    this.errorCodeInvalid = this.languages.getLanguage().AUTH_VALIDATE_PHONE_ERROR_INVALID_CODE;
    this.validateCodePlaceholder = this.languages.getLanguage().AUTH_VALIDATE_PHONE_SET_SMS_CODE_PLACEHOLDER;
    this.labelBtnValidateSMSCode = this.languages.getLanguage().AUTH_VALIDATE_PHONE_SET_VALIDATE_CODE_BTN;

    this.windowRef = this.windowService.nativeWindow;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')

    this.windowRef.recaptchaVerifier.render();
  }

  sendLoginCode() {
    this.isInvalidCode = false;
    const appVerifier = this.windowRef.recaptchaVerifier;

    const num = this.phoneNumberForm.value;
    firebase.auth().currentUser.linkWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.confirmationResult = result;
      })
      .catch( error => console.log(error) );
  }

  verifyLoginCode() {
    this.appStoreService.displayProgressBarById('verifyCode');
    const confirmCode = this.validateCodeForm.value;
    console.log('Validate code: ' + confirmCode);

    this.isInvalidCode = false;
    this.confirmationResult
      .confirm(confirmCode)
      .then( result => {
        this.appStoreService.hideProgressBar();
        this.authStoreService.init();
      })
      .catch( error => {
        this.appStoreService.hideProgressBar();
        this.isInvalidCode = true;
        console.log(error, 'Incorrect code entered?');
      });
  }

}
