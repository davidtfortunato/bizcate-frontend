import {ProfessionalComponent} from './professional.component';
import {EditProfessionalProfileComponent} from './edit-professional-profile/edit-professional-profile.component';
import {PortfolioFormComponent} from './edit-professional-profile/portfolio-form/portfolio-form.component';
import {ProfessionalProfileFormComponent} from './edit-professional-profile/profile-form/professional-profile-form.component';
import {SkillsFormComponent} from './edit-professional-profile/skills-form/skills-form.component';
import {AddPortfolioComponent} from './edit-professional-profile/portfolio-form/add-portfolio/add-portfolio.component';
import {LocationFormComponent} from './edit-professional-profile/locations-form/location-form.component';
import {ProfessionalServiceItemComponent} from './shared/professional-service-item/professional-service-item.component';
import {HourlyRatePriceComponent} from './shared/hourly-rate-price/hourly-rate-price.component';

export const COMPONENTS = [ProfessionalComponent,
  EditProfessionalProfileComponent,
  LocationFormComponent,
  PortfolioFormComponent,
  ProfessionalProfileFormComponent,
  SkillsFormComponent,
  AddPortfolioComponent,
  HourlyRatePriceComponent,
  ProfessionalServiceItemComponent];
