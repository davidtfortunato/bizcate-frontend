import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProfessionalServiceAvailable, ServiceSkill} from '../../../../../functions/src/data/index';
import {DEFAULT_CURRENCY} from '../../../../../functions/src/data/currency-list';

@Component({
  selector: 'app-professional-service-item',
  templateUrl: './professional-service-item.component.html',
  styleUrls: ['./professional-service-item.component.scss']
})
/**
 * Component to display the professional service the Bizcater provides and the hourly ratee
 */
export class ProfessionalServiceItemComponent {
  @Input() professionalServiceItem: ProfessionalServiceAvailable;
  @Input() editable: boolean;
  @Input() currency = DEFAULT_CURRENCY;
  @Output() professionalServiceItemChanged = new EventEmitter<ProfessionalServiceAvailable>();
  @Output() skillRemoved = new EventEmitter<ServiceSkill>();


  constructor() {
  }

  onSkillRemoved(skill) {
    this.skillRemoved.emit(skill);
  }
}
