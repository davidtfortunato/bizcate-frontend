import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ProfileFormData} from '../../edit-professional-profile/profile-form/professional-profile-form.component';
import {BizcaterPriceHourlyRate} from '../../../../../functions/src/data';
import {FormControl, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';
import {DEFAULT_CURRENCY} from '../../../../../functions/src/data/currency-list';

@Component({
  selector: 'app-professional-hourly-rate',
  templateUrl: './hourly-rate-price.component.html',
  styleUrls: ['./hourly-rate-price.component.scss']
})
export class HourlyRatePriceComponent implements OnInit, OnChanges {
  @Input() priceHourlyRate: BizcaterPriceHourlyRate = {
    priceHourly: 10,
    currency: DEFAULT_CURRENCY
  };
  @Output() priceChanged = new EventEmitter<BizcaterPriceHourlyRate>();

  // Labels
  labelPriceHourly: string;
  labelErrorRequired: string;
  labelPlaceholder: string;

  // Form Control
  priceFormControl = new FormControl('', Validators.required);


  constructor(private language: LanguageService) {
    this.labelPriceHourly = this.language.getLanguage().PROFESSIONAL_PROFILE_PRICE_HOURLY;
    this.labelErrorRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;
    this.labelPlaceholder = this.labelPriceHourly;
  }

  ngOnInit(): void {

    this.priceFormControl.valueChanges.subscribe(priceChanged => {
      if (this.priceHourlyRate.priceHourly !== priceChanged) {
        this.priceHourlyRate.priceHourly = priceChanged;
        this.priceChanged.emit(this.priceHourlyRate);
      }
    });
  }

  ngOnChanges() {
    if (!this.priceHourlyRate) {
      this.priceHourlyRate = {
        currency: DEFAULT_CURRENCY,
        priceHourly: 10
      };
    }

    this.labelPlaceholder = this.priceHourlyRate.currency.symbol + ' / ' + this.language.getLanguage().GENERIC_TIME_HOUR;
    this.priceFormControl.setValue(this.priceHourlyRate.priceHourly);
  }

}
