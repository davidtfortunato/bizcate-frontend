import {Component, OnInit} from '@angular/core';
import {ProfessionalStoreService} from './store/professional-store.service';

@Component({
  selector: 'app-professional-container',
  templateUrl: './professional.component.html',
  styleUrls: ['./professional.component.scss']
})
export class ProfessionalComponent implements OnInit {

  constructor(private professionalStoreService: ProfessionalStoreService) {

  }

  ngOnInit() {
    this.professionalStoreService.init();
  }
}
