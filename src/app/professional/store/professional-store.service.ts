import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../store';
import {
  AddProfessionalSkill, DeletePortfolioEntry,
  Init, LoadPortfolioList, RemoveProfessionalSkill,
  SaveProfessionalProfile,
  SaveUserWorkableAreas,
  UpdateProfessionalProfileForm,
  UpdateProfessionalWorkAreasForm
} from './actions';
import {MapAreaDraw, ProfessionalServiceAvailable, ServiceSkill, UserWorkableArea} from '../../../../functions/src/data';
import {ProfileFormData} from '../edit-professional-profile/profile-form/professional-profile-form.component';

@Injectable()
export class ProfessionalStoreService {

  constructor(private store: Store<State>) { }

  init() {
    this.store.dispatch(new Init());
  }

  updateProfileForm(formData: ProfileFormData) {
    this.store.dispatch(new UpdateProfessionalProfileForm(formData));
  }

  updateWorkableAreas(mapAreaDraws: MapAreaDraw[]) {
    this.store.dispatch(new UpdateProfessionalWorkAreasForm(mapAreaDraws));
  }

  saveProfessionalProfile() {
    this.store.dispatch(new SaveProfessionalProfile());
  }

  saveUserWorkableAreas(oldWorkableAreas: UserWorkableArea[], newWorkableAreas: UserWorkableArea[]) {
    this.store.dispatch(new SaveUserWorkableAreas({oldWorkableAreas, newWorkableAreas}));
  }

  addProfessionalSkill(bizcaterService: ProfessionalServiceAvailable) {
    this.store.dispatch(new AddProfessionalSkill(bizcaterService));
  }

  removeProfessionalSkill(skill: ServiceSkill) {
    this.store.dispatch(new RemoveProfessionalSkill({categoryId: skill.category.id, skillId: skill.serviceName.id}));
  }

  loadPortfolioList() {
    this.store.dispatch(new LoadPortfolioList());
  }

  deletePortfolioEntry(docId) {
    this.store.dispatch(new DeletePortfolioEntry({docId}));
  }
}
