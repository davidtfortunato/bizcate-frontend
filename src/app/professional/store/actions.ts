import {Action} from '@ngrx/store';
import {
  ApiResponse, MapAreaDraw,
  ProfessionalDocument, ProfessionalServiceAvailable,
  ServiceSkill,
  UserProfessionalProfile,
  UserProfileModel, UserWorkableArea
} from '../../../../functions/src/data';
import {ProfileFormData} from '../edit-professional-profile/profile-form/professional-profile-form.component';
import {ProfessionalPortfolioModel} from '../../../../functions/src/data/portfolio-model';

export enum ActionTypes {
  Init = '[Pro] Init Professional',

  // Professional Actions
  CreateProfessionalProfile = '[Pro] Create professional profile',
  UpdateProfessionalProfileForm = '[Pro] Update professional profile',
  UpdateProfessionalWorkAreasForm = '[Pro] Update professional workable areas',
  UpdateProfessionalWorkAreasFormResult = '[Pro] Update professional workable areas result',

  LoadProfessionalProfile = '[Pro] Load Professional profile',
  LoadProfessionalProfileResult = '[Pro] Load Professional profile result',

  SaveProfessionalProfile = '[Pro] Save Professional profile',
  SaveProfessionalProfileResult = '[Pro] Save Professional profile result',

  SaveUserWorkableAreas = '[Pro] Save User Workable Areas',

  AddPortfolioDocument = '[Pro] Add Portfolio document',
  AddPortfolioDocumentResult = '[Pro] Add Portfolio document result',

  AddProfessionalSkill = '[Pro] Add Skill',
  RemoveProfessionalSkill = '[Pro] Remove Skill',

  LoadPortfolioList = '[Pro] Load portfolio list',
  LoadPortfolioListResult = '[Pro] Load portfolio list result',
  DeletePortfolioEntry = '[Pro] Delete Portfolio entry'
}

export class Init implements Action {
  readonly type = ActionTypes.Init;
}

// Create Professional profile local
export class CreateProfessionalProfile {
  readonly type = ActionTypes.CreateProfessionalProfile;

  constructor(public payload: UserProfileModel) {
  }
}

// Update Professional profile form
export class UpdateProfessionalProfileForm {
  readonly type = ActionTypes.UpdateProfessionalProfileForm;

  constructor(public payload: ProfileFormData) {
  }
}

// Update Professional workable areas
export class UpdateProfessionalWorkAreasForm {
  readonly type = ActionTypes.UpdateProfessionalWorkAreasForm;

  constructor(public payload: MapAreaDraw[]) {
  }
}

// Update Professional workable areas result
export class UpdateProfessionalWorkAreasFormResult {
  readonly type = ActionTypes.UpdateProfessionalWorkAreasFormResult;

  constructor(public payload: UserWorkableArea[]) {
  }
}


// Load Professional profile
export class LoadProfessionalProfile {
  readonly type = ActionTypes.LoadProfessionalProfile;
}
export class LoadProfessionalProfileResult {
  readonly type = ActionTypes.LoadProfessionalProfileResult;

  constructor(public payload: ApiResponse<UserProfessionalProfile>) {
  }
}

// Save Professional profile
export class SaveProfessionalProfile {
  readonly type = ActionTypes.SaveProfessionalProfile;
}
export class SaveProfessionalProfileResult {
  readonly type = ActionTypes.SaveProfessionalProfileResult;

  constructor(public payload: ApiResponse<UserProfessionalProfile>) {
  }
}

// Save User Workable areas
export class SaveUserWorkableAreas {
  readonly type = ActionTypes.SaveUserWorkableAreas;
  constructor(public payload: {oldWorkableAreas: UserWorkableArea[], newWorkableAreas: UserWorkableArea[]}) {
  }
}

// Add Portfolio document
export class AddPortfolioDocument {
  readonly type = ActionTypes.AddPortfolioDocument;

  constructor(public payload: ProfessionalDocument) {
  }
}
export class AddPortfolioDocumentResult {
  readonly type = ActionTypes.AddPortfolioDocumentResult;

  constructor(public payload: ApiResponse<ProfessionalDocument>) {
  }
}

// Add Professional Skill
export class AddProfessionalSkill {
  readonly type = ActionTypes.AddProfessionalSkill;

  constructor(public payload: ProfessionalServiceAvailable) {
  }
}
// Remove profesisonal serviceName
export class RemoveProfessionalSkill {
  readonly type = ActionTypes.RemoveProfessionalSkill;

  constructor(public payload: {categoryId: string, skillId: string}) {
  }
}

export class LoadPortfolioList {
  readonly type = ActionTypes.LoadPortfolioList;
}
export class LoadPortfolioListResult {
  readonly type = ActionTypes.LoadPortfolioListResult;

  constructor(public payload: {list: ProfessionalPortfolioModel[]}) {
  }
}
export class DeletePortfolioEntry {
  readonly type = ActionTypes.DeletePortfolioEntry;

  constructor(public payload: {docId: string}) {
  }
}


export type Actions = Init
  | CreateProfessionalProfile
  | SaveProfessionalProfile
  | SaveProfessionalProfileResult
  | AddPortfolioDocument
  | AddPortfolioDocumentResult
  | AddProfessionalSkill
  | RemoveProfessionalSkill
  | LoadProfessionalProfile
  | LoadProfessionalProfileResult
  | UpdateProfessionalProfileForm
  | UpdateProfessionalWorkAreasForm
  | UpdateProfessionalWorkAreasFormResult
  | SaveUserWorkableAreas
  | LoadPortfolioList
  | LoadPortfolioListResult
  | DeletePortfolioEntry;
