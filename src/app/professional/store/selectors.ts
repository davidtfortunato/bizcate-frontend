import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromStore from './reducer';
import {ServiceModel} from '../../../../functions/src/data';
import {DEFAULT_CURRENCY} from '../../../../functions/src/data/currency-list';

export const selectProfessionalState = createFeatureSelector<fromStore.State>('professional');

// Getters
export const getProfessionalProfile = createSelector(selectProfessionalState, fromStore.getProfessionalProfile);
export const getFormData = createSelector(selectProfessionalState, fromStore.getFormData);
export const getPortfolioList = createSelector(selectProfessionalState, fromStore.getPortfolioList);
export const getWorkableAreas = createSelector(getProfessionalProfile, (userProfile => userProfile.workableAreas));
export const isFormDataValid = createSelector(selectProfessionalState, fromStore.getIsFormValid);
export const hasFormChanges = createSelector(selectProfessionalState, fromStore.hasFormChanges);
export const getProfessionalSkills = createSelector(getProfessionalProfile,
  professionalProfile => professionalProfile ? professionalProfile.professionalSkills : []);
export const getProfessionalSkillsModels = createSelector(getProfessionalSkills, proSkills => {
  const skillsModels: ServiceModel[] = [];

  if (proSkills) {
    proSkills.forEach(proSkill => {
      skillsModels.push(proSkill.serviceName);
    });
  }

  return skillsModels;
});

export const getProfessionalCurrency = createSelector(getProfessionalProfile, profile => {
  // Right now the professional doesn't have any configuration for currency, so just use the Default one
  return DEFAULT_CURRENCY;
});
