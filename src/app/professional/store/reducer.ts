import {UserProfessionalProfile, UserWorkableArea} from '../../../../functions/src/data';
import {Actions, ActionTypes} from './actions';
import {ProfessionalPortfolioModel} from '../../../../functions/src/data/portfolio-model';
import {DEFAULT_CURRENCY} from '../../../../functions/src/data/currency-list';


export interface State {
  formData?: {
    data?: UserProfessionalProfile;
    isValid: boolean;
    isNewProfile: boolean;
    hasChanges: boolean;
    oldWorkableAreas: UserWorkableArea[];
  };
  portfolioList?: ProfessionalPortfolioModel[];
}

export const initialState: State = {
  formData: null,
  portfolioList: []
};

export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.Init:
      return state;

    case ActionTypes.CreateProfessionalProfile:
      return {
        ...state,
        formData: {
          data: {
            uid: action.payload.uid,
            isActive: true,
            logoUrl: action.payload.photoUrl,
            name: action.payload.name,
            description: '',
            address: action.payload.address,
            country: action.payload.country,
            city: action.payload.city,
            vatNumber: action.payload.vatNumber,
            phoneNumber: {
              number: action.payload.phoneNumber,
              validated: false
            },
            documents: [],
            remoteWork: false,
            availableServices: [],
            professionalSkills: [],
            websiteUrls: [],
            workableAreas: [],
            priceHourlyRate: {
              currency: DEFAULT_CURRENCY,
              priceHourly: 0
            }
          },
          isValid: false,
          isNewProfile: true,
          hasChanges: true,
          oldWorkableAreas: []
        }
      };
    case ActionTypes.LoadProfessionalProfileResult:
      if (action.payload.isSuccess) {
        return {
          ...state,
          formData: {
            ...state.formData,
            data: action.payload.responseData,
            isValid: true,
            isNewProfile: false,
            hasChanges: false,
            oldWorkableAreas: action.payload.responseData.workableAreas
          }
        };
      }
      break;
    case ActionTypes.SaveProfessionalProfileResult:
      return {
        ...state,
        formData: {
          ...state.formData,
          hasChanges: false
        }
      };
    case ActionTypes.UpdateProfessionalProfileForm:
      return {
        ...state,
        formData: {
          ...state.formData,
          data: action.payload.data,
          isValid: action.payload.isValid,
          hasChanges: true
        }
      };
    case ActionTypes.UpdateProfessionalWorkAreasFormResult:
      return {
        ...state,
        formData: {
          ...state.formData,
          data: {
            ...state.formData.data,
            workableAreas: action.payload
          },
          hasChanges: true
        }
      };
    case ActionTypes.AddProfessionalSkill:
      if (!state.formData.data.availableServices) {
        state.formData.data.availableServices = [];
      }

      // Create a new ProfessionalAvailableService
      if (state.formData.data.availableServices && action.payload)
      {
        for (const professionalService of state.formData.data.availableServices) {
          const skill = professionalService.service;
          if (skill.category.id === action.payload.service.category.id && skill.serviceName.id === action.payload.service.serviceName.id) {
            // Not added
            return state;
          }
        }
      }

      // If it reached here, means that doesn't exists yet
      state.formData.data.availableServices.push(action.payload);
      state.formData.hasChanges = true;

      return state;

    case ActionTypes.RemoveProfessionalSkill:
      if (action.payload.skillId && action.payload.skillId) {
        state.formData.data.availableServices = state.formData.data.availableServices.filter(value => {
          return value.service.serviceName.id !== action.payload.skillId && value.service.category.id !== action.payload.categoryId;
        });
        state.formData.hasChanges = true;
      }
      return state;
    case ActionTypes.LoadPortfolioListResult:
      return {
        ...state,
        portfolioList: action.payload.list
      };
  }
  return state;
}

// Pure Functions
export const getFormData = (state: State) => state.formData;
export const getPortfolioList = (state: State) => state.portfolioList;
export const getProfessionalProfile = (state: State) => state.formData.data;
export const getIsFormValid = (state: State) => state.formData.isValid;
export const hasFormChanges = (state: State) => state && state.formData ? state.formData.hasChanges : false;
