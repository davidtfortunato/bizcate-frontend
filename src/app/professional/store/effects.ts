import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap, switchMap, withLatestFrom} from 'rxjs/operators';
import {
  ActionTypes,
  CreateProfessionalProfile, DeletePortfolioEntry,
  Init, LoadPortfolioList, LoadPortfolioListResult,
  LoadProfessionalProfile,
  LoadProfessionalProfileResult,
  SaveProfessionalProfile,
  SaveProfessionalProfileResult,
  SaveUserWorkableAreas,
  UpdateProfessionalWorkAreasForm,
  UpdateProfessionalWorkAreasFormResult
} from './actions';
import {select, Store} from '@ngrx/store';
import {State} from '../../store';
import {getUser, getUserProfile} from '../../auth/store/selectors';
import {NoAction} from '../../store/app.actions';
import {ProfessionalApiService, UserAPIService} from '../../core/api';
import {of} from 'rxjs';
import {getQuickResponse, getResponse, MapAreaDraw, UserWorkableArea} from '../../../../functions/src/data';
import {getFormData} from './selectors';
import {SnackBarService} from '../../shared/components/snack-bar/snack-bar.service';
import {LanguageService} from '../../core/language/language.service';
import {WorkableAreasApiService} from '../../core/api/workable-areas-api.service';
import {ProfessionalStoreService} from './professional-store.service';
import {PortfolioApiService} from '../../core/api/portfolio-api.service';
import {GetUserProfessionalProfile} from '../../auth/store/actions';
import {AppStoreService} from '../../store/app-store.service';

@Injectable()
export class Effects {

  @Effect()
  init$ = this.actions$.pipe(
    ofType<Init>(ActionTypes.Init),
    mergeMap((action) => {
      return [new LoadProfessionalProfile(), new LoadPortfolioList()];
    })
  );

  @Effect()
  loadProfessionalProfile$ = this.actions$.pipe(
    ofType<LoadProfessionalProfile | LoadProfessionalProfileResult>(ActionTypes.LoadProfessionalProfile),
    switchMap(action => {
      return this.professionalAPI
        .getProfessionalProfile()
        .pipe(map(response => {
          return new LoadProfessionalProfileResult(response);
        }), catchError(err => of(new LoadProfessionalProfileResult(getQuickResponse(false, err)))));
    })
  );

  @Effect()
  loadProfessionalProfileResult$ = this.actions$.pipe(
    ofType<LoadProfessionalProfileResult>(ActionTypes.LoadProfessionalProfileResult),
    switchMap((action) => {
      return this.userAPI.getUserProfile().pipe(map(userProfile => {
        console.log('User profile: ' + userProfile);

        if (!action.payload.isSuccess) {
          // If doesn't exist, should create a new Professional Profile
          return new CreateProfessionalProfile(userProfile);
        } else {
          return new NoAction();
        }
      }));
    })
  );

  @Effect()
  saveProfessionalProfile$ = this.actions$.pipe(
    ofType<SaveProfessionalProfile>(ActionTypes.SaveProfessionalProfile),
    withLatestFrom(this.store.pipe(select(getFormData))),
    switchMap(([action, formData]) => {
      const oldWorkableAreas: UserWorkableArea[] = formData.oldWorkableAreas;
      this.appStoreService.displayProgressBarById('saveProfile');
      console.log('Saving Professional Profile');

      if (formData.isValid) {
        if (formData.isNewProfile) {
          return this.professionalAPI.createProfessionalProfile(formData.data)
            .pipe(map(response => {
                // Save workable areas
                this.professionalStoreService.saveUserWorkableAreas(oldWorkableAreas, formData.data.workableAreas);

                // Profile correctly saved
                return new SaveProfessionalProfileResult(response);
              }),
              catchError(err => of(new SaveProfessionalProfileResult(getQuickResponse(false, err)))));
        } else {
          // Save workable areas
          this.professionalStoreService.saveUserWorkableAreas(oldWorkableAreas, formData.data.workableAreas);

          return this.professionalAPI.updateProfessionalProfile(formData.data)
            .pipe(map(response => {
              // Profile correctly saved. Save Workable areas
              return new SaveProfessionalProfileResult(response);
            }), catchError(err => of(new SaveProfessionalProfileResult(getQuickResponse(false, err)))));
        }
      } else {
        this.snackBar.displayMessage(this.language.getLanguage().ERROR_FORM_INVALID, 'error');
        return of(new SaveProfessionalProfileResult(getQuickResponse(false, 'Form invalid to save Professional Profile')));
      }
    })
  );

  @Effect()
  saveProfessionalProfileResult$ = this.actions$.pipe(
    ofType<SaveProfessionalProfileResult>(ActionTypes.SaveProfessionalProfileResult),
    withLatestFrom(this.store.pipe(select(getUser))),
    map(([action, user]) => {
      this.appStoreService.hideProgressBar('saveProfile');
      if (action.payload.isSuccess) {
        this.snackBar.displayMessage(this.language.getLanguage().TOAST_MESSAGE_SAVED_SUCCESS, 'success');
        return new GetUserProfessionalProfile(user.uid);
      } else {
        this.snackBar.displayMessage(this.language.getLanguage().ERROR_GENERIC_REQUEST, 'error');
        return new NoAction();
      }
    })
  );

  @Effect()
  updateProfessionalWorkAreasForm$ = this.actions$.pipe(
    ofType<UpdateProfessionalWorkAreasForm>(ActionTypes.UpdateProfessionalWorkAreasForm),
    withLatestFrom(this.store.pipe(select(getUser))),
    map(([action, user]) => {
      // Convert draws in User Workable Areas
      const workableAreas: UserWorkableArea[] = [];
      action.payload.forEach((mapDraw: MapAreaDraw) => {
        workableAreas.push({
          uid: user.uid,
          mapArea: mapDraw
        });
      });

      return new UpdateProfessionalWorkAreasFormResult(workableAreas);
    })
  );

  @Effect({dispatch: false})
  saveUserWorkableAreas$ = this.actions$.pipe(
    ofType<SaveUserWorkableAreas>(ActionTypes.SaveUserWorkableAreas),
    map(action => {
      const locationsRemove: UserWorkableArea[] = [];

      if (action.payload.oldWorkableAreas && action.payload.newWorkableAreas) {
        action.payload.oldWorkableAreas.forEach(oldArea => {
          // Check if still exists in the new areas
          let exist = false;
          for (const newArea of action.payload.newWorkableAreas) {
            if (newArea.mapArea.id === oldArea.mapArea.id) {
              exist = true;
              break;
            }
          }
          if (!exist) {
            locationsRemove.push(oldArea);
          }
        });
      }

      // Remove old areas
      if (locationsRemove && locationsRemove.length > 0) {
        this.workableAreasService.removeWorkableAreas(locationsRemove);
      }

      // Update Workable Areas
      if (action.payload.newWorkableAreas) {
        this.workableAreasService.addUserWorkableAreas(action.payload.newWorkableAreas);
      }
    })
  );

  @Effect()
  loadPortfolioList$ = this.actions$.pipe(
    ofType<LoadPortfolioList>(ActionTypes.LoadPortfolioList),
    switchMap(action => {
      return this.portfolioAPI.getProfessionalPortfolio().pipe(map(response => {
        if (response.isSuccess) {
          return new LoadPortfolioListResult({list: response.responseData});
        } else {
          return new NoAction();
        }
      }));
    })
  );

  @Effect()
  deletePortfolioEntry$ = this.actions$.pipe(
    ofType<DeletePortfolioEntry>(ActionTypes.DeletePortfolioEntry),
    switchMap(action => {
      this.appStoreService.displayProgressBarById('deletePortfolio');
      return this.portfolioAPI.deletePortfolio(action.payload.docId)
        .pipe(map(response => {
          this.appStoreService.hideProgressBar('deletePortfolio');
          if (response.isSuccess) {
            this.snackBar.displayMessage(this.language.getLanguage().GENERIC_DELETED_SUCCESS);
            return new LoadPortfolioList();
          } else {
            this.snackBar.displayMessage(this.language.getLanguage().ERROR_GENERIC_DELETED_FAILED);
            return new NoAction();
          }
        }));
    })
  );

  constructor(private actions$: Actions,
              private store: Store<State>,
              private professionalAPI: ProfessionalApiService,
              private portfolioAPI: PortfolioApiService,
              private userAPI: UserAPIService,
              private workableAreasService: WorkableAreasApiService,
              private snackBar: SnackBarService,
              private language: LanguageService,
              private professionalStoreService: ProfessionalStoreService,
              private appStoreService: AppStoreService) {
  }
}
