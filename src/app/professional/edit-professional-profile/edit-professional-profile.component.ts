import {Component, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {
  BizcaterPriceHourlyRate,
  MapAreaDraw,
  ProfessionalServiceAvailable,
  ServiceSkill,
  UserProfessionalProfile,
  UserProfileModel
} from '../../../../functions/src/data/index';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getUser, getUserProfile, isAuthInitialized} from '../../auth/store/selectors';
import {getFormData, getProfessionalProfile, hasFormChanges, isFormDataValid} from '../store/selectors';
import {LanguageService} from '../../core/language/language.service';
import {ProfessionalStoreService} from '../store/professional-store.service';
import {HeaderActionButton} from '../../shared/components/title-header/title-header.component';
import {AppUIParams} from '../../shared/app.ui-params';
import {ProfileFormData} from './profile-form/professional-profile-form.component';
import {SnackBarService} from '../../shared/components/snack-bar/snack-bar.service';
import {DEFAULT_CURRENCY} from '../../../../functions/src/data/currency-list';

@Component({
  selector: 'app-edit-professional-profile',
  templateUrl: './edit-professional-profile.component.html',
  styleUrls: ['./edit-professional-profile.component.scss']
})
export class EditProfessionalProfileComponent implements OnInit {

  // Data
  isAuthInitialized$: Observable<Boolean>;
  userProfile$: Observable<UserProfileModel>;
  userProfessionalProfile$: Observable<UserProfessionalProfile>;
  formData$: Observable<ProfileFormData>;
  hasFormChanges$: Observable<Boolean>;

  // UI
  headerTitle: { title: string, subtitle: string };
  headerActionButtons: HeaderActionButton[];

  constructor(private store: State<fromStore.State>,
              private language: LanguageService,
              private professionalStoreService: ProfessionalStoreService,
              private snackbar: SnackBarService) {

    this.userProfile$ = this.store.pipe(select(getUserProfile));
    this.userProfessionalProfile$ = this.store.pipe(select(getProfessionalProfile));
    this.formData$ = this.store.pipe(select(getFormData));
    this.isAuthInitialized$ = this.store.pipe(select(isAuthInitialized));
    this.hasFormChanges$ = this.store.pipe(select(hasFormChanges));
    this.hasFormChanges$.subscribe(value => {
      // The form has changed
      this.refreshActionButtons(value.valueOf());
    });

    // Init when receives the user profile
    const subsUserAuth = this.store.pipe(select(getUser)).subscribe((userAuth) => {
      if (subsUserAuth) {
        subsUserAuth.unsubscribe();
      }
      if (userAuth) {
        this.professionalStoreService.init();
      }
    });
  }

  ngOnInit() {
    this.headerTitle = {
      title: this.language.getLanguage().PROFESSIONAL_PROFILE_SCREEN_TITLE,
      subtitle: this.language.getLanguage().PROFESSIONAL_PROFILE_SCREEN_SUBTITLE,
    };

    // Init Header Action Buttons
    this.refreshActionButtons(false);
  }

  onActionButtonClick(actionButton: HeaderActionButton) {
    if (actionButton.id === '1') {
      this.professionalStoreService.saveProfessionalProfile();
    }
  }

  onProfileChanged(formResponse: ProfileFormData) {
    this.professionalStoreService.updateProfileForm(formResponse);
  }

  onSkillAdded(skill: ServiceSkill) {
    const professionalService: ProfessionalServiceAvailable = {
      service: skill,
      currency: DEFAULT_CURRENCY,
      priceHour: 10
    };
    if (skill) {
      this.professionalStoreService.addProfessionalSkill(professionalService);
    }
  }

  onSkillRemoved(skill: ServiceSkill) {
    if (skill) {
      this.professionalStoreService.removeProfessionalSkill(skill);
    }
  }

  private refreshActionButtons(hasFormChanged: boolean) {
    if (hasFormChanged) {
      this.headerActionButtons = [
        {
          id: '1',
          label: this.language.getLanguage().GENERIC_SAVE_LABEL,
          backgroundColor: AppUIParams.actionButtonOrangeBackgroundColor,
          textColor: AppUIParams.actionButtonWhiteColor
        }
      ];
    } else {
      this.headerActionButtons = [
        {
          id: '1',
          label: this.language.getLanguage().GENERIC_SAVE_LABEL,
          backgroundColor: AppUIParams.actionButtonGreenColor,
          textColor: AppUIParams.actionButtonWhiteColor
        }
      ];
    }
  }
}
