import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {BizcaterPriceHourlyRate, ServiceSkill} from '../../../../../functions/src/data';
import {ProfileFormData} from '../profile-form/professional-profile-form.component';

@Component({
  selector: 'app-professional-skill-form',
  templateUrl: './skills-form.component.html',
  styleUrls: ['./skills-form.component.scss']
})
export class SkillsFormComponent implements OnInit {
  @Input() formData: ProfileFormData;
  @Output() skillAdded = new EventEmitter<ServiceSkill>();
  @Output() skillRemoved = new EventEmitter<ServiceSkill>();
  @Output() profileFormDataChanged = new EventEmitter<ProfileFormData>();

  // Labels
  headerTitle: string;
  headerSubtitle: string;
  placholderEmptySkills: string;

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    // Header title
    this.headerTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_SKILLS_TITLE;
    this.headerSubtitle = this.language.getLanguage().PROFESSIONAL_PROFILE_SKILLS_SUBTITLE;
    this.placholderEmptySkills = this.language.getLanguage().PROFESSIONAL_PROFILE_SKILLS_EMPTY;
  }

  onSkillAdded(skill) {
    this.skillAdded.emit(skill);
  }

  onSkillRemoved(skill) {
    this.skillRemoved.emit(skill);
  }

  onPriceChanged(priceHourly: BizcaterPriceHourlyRate) {
    this.formData.data.priceHourlyRate = priceHourly;
    this.profileFormDataChanged.emit(this.formData);
  }
}
