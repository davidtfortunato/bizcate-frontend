import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {UserProfessionalProfile, UserProfileModel} from '../../../../../functions/src/data';
import {AppAssets} from '../../../shared/app.assets';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageService} from '../../../core/language/language.service';
import {AppConsts} from '../../../core/utils/constants';
import {SnackBarService} from '../../../shared/components/snack-bar/snack-bar.service';
import {SearchLocationService} from '../../../core/location/search-location.service';
import {Observable} from 'rxjs';
import {MatOptionSelectionChange} from '@angular/material';
import {LocationResult} from '../../../../../functions/src/data/location-result';

export interface ProfileFormData {
  data?: UserProfessionalProfile;
  isValid: boolean;
}

@Component({
  selector: 'app-professional-profile-form',
  templateUrl: './professional-profile-form.component.html',
  styleUrls: ['./professional-profile-form.component.scss']
})
export class ProfessionalProfileFormComponent implements OnInit, OnChanges {
  @Input() userProfile: UserProfileModel;
  @Input() formData: ProfileFormData;
  @Output() profileChanged = new EventEmitter<ProfileFormData>();

  // Data
  citySearchResults$: Observable<LocationResult[]>;

  // Values
  avatarFileName = AppConsts.FILENAME_PROFILE_PRO_AVATAR;

  // Placeholders
  namePlaceholder: string;
  phonenumberPlaceholder: string;
  vatnumberPlaceholder: string;
  descriptionPlaceholder: string;

  // Form controllers
  form: FormGroup;
  nameFormControl = new FormControl('', Validators.required);
  phonenumberFormControl = new FormControl('', Validators.required);
  vatnumberFormControl = new FormControl('', Validators.required);
  descriptionFormControl = new FormControl('', Validators.required);

  // Labels
  labelTitle: string;
  labelSubtitle: string;
  errorFieldRequired: string;

  constructor(private language: LanguageService,
              private formBuilder: FormBuilder,
              private snackBar: SnackBarService,
              private searchLocation: SearchLocationService) {
  }

  ngOnInit() {
    this.namePlaceholder = this.language.getLanguage().AUTH_REGISTER_NAME_PLACEHOLDER;
    this.phonenumberPlaceholder = this.language.getLanguage().AUTH_REGISTER_PHONENUMBER_PLACEHOLDER;
    this.vatnumberPlaceholder = this.language.getLanguage().AUTH_REGISTER_VATNUMBER_PLACEHOLDER;
    this.descriptionPlaceholder = this.language.getLanguage().AUTH_REGISTER_DESCRIPTION_PLACEHOLDER;
    this.labelTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_FORM_TITLE;
    this.labelSubtitle = this.language.getLanguage().PROFESSIONAL_PROFILE_FORM_SUBTITLE;
    this.errorFieldRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;

    this.initForm();
  }

  ngOnChanges() {
    this.initForm();
  }

  private initForm() {

    if (this.formData && this.formData.data) {
      this.nameFormControl.setValue(this.formData.data.name, {emitEvent: false});
      this.phonenumberFormControl.setValue(this.formData.data.phoneNumber.number, {emitEvent: false});
      this.vatnumberFormControl.setValue(this.formData.data.vatNumber, {emitEvent: false});
      this.descriptionFormControl.setValue(this.formData.data.description, {emitEvent: false});
    }

    // Init Form
    this.form = this.formBuilder.group({
      name: this.nameFormControl,
      phoneNumber: this.phonenumberFormControl,
      vatNumber: this.vatnumberFormControl,
      description: this.descriptionFormControl
    });
    this.form.valueChanges.subscribe(val => {
      this.onFormChanges();
    });
  }

  getUserPhotoURL() {
    if (this.formData && this.formData.data.logoUrl) {
      return this.formData.data.logoUrl;
    } else if (this.userProfile && this.userProfile.photoUrl) {
      return this.userProfile.photoUrl;
    } else {
      return AppAssets.ic_avatar;
    }
  }


  onPhotoUpload(photoUrl: string) {
    if (photoUrl) {
      this.formData.data.logoUrl = photoUrl;
      this.profileChanged.emit(this.formData);
    }
  }

  onFormChanges() {
    if (this.formData && this.formData.data) {
      this.formData.isValid = this.form.valid;
      this.formData.data.name = this.nameFormControl.value;
      this.formData.data.description = this.descriptionFormControl.value;
      this.formData.data.vatNumber = this.vatnumberFormControl.value;
      this.formData.data.phoneNumber.number = this.phonenumberFormControl.value;
      this.profileChanged.emit(this.formData);
    }
  }
}
