import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LocationResult} from '../../../../../functions/src/data/location-result';
import {LanguageService} from '../../../core/language/language.service';
import {ProfileFormData} from '../profile-form/professional-profile-form.component';
import {MatCheckboxChange} from '@angular/material/typings/esm5/checkbox';

@Component({
  selector: 'app-professional-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss']
})
export class LocationFormComponent implements OnInit {
  @Input() formData: ProfileFormData;
  @Input() currentListLocations: LocationResult[];
  @Output() profileChanged = new EventEmitter<ProfileFormData>();

  // Labels
  headerTitle: string;
  headerSubtitle: string;
  labelRemoteWork: string;

  // Data
  selectedCityToAdd: LocationResult;

  constructor(private language: LanguageService) {

  }

  ngOnInit(): void {
    // Header title
    this.headerTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_LOCATION_TITLE;
    this.headerSubtitle = this.language.getLanguage().PROFESSIONAL_PROFILE_LOCATION_SUBTITLE;

    // Remote work
    this.labelRemoteWork = this.language.getLanguage().PROFESSIONAL_PROFILE_REMOTEWORK_LABEL;

  }

  onRemoteWorkChange(check: MatCheckboxChange) {
    this.formData.data.remoteWork = check.checked;
    this.profileChanged.emit(this.formData);
  }

  onCitySelected(selectedCity: LocationResult) {
    if (this.formData && (!this.formData.data.location || this.formData.data.location.id !== selectedCity.id)) {
      this.selectedCityToAdd = selectedCity;
      this.formData.data.location = this.selectedCityToAdd;

      this.profileChanged.emit(this.formData);
    }
  }

}
