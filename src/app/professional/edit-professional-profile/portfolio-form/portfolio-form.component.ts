import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {AddPortfolioComponent} from './add-portfolio/add-portfolio.component';
import {MatDialog} from '@angular/material';
import {PlatformService} from '../../../shared/platform';
import {ProfessionalStoreService} from '../../store/professional-store.service';
import {Observable} from 'rxjs';
import {ProfessionalPortfolioModel} from '../../../../../functions/src/data/portfolio-model';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../../store';
import {getPortfolioList} from '../../store/selectors';

@Component({
  selector: 'app-professional-portfolio-form',
  templateUrl: './portfolio-form.component.html',
  styleUrls: ['./portfolio-form.component.scss']
})
export class PortfolioFormComponent implements OnInit {

  // Data
  portfolioList$: Observable<ProfessionalPortfolioModel[]>;

  // Labels
  headerTitle: string;
  headerSubtitle: string;
  labelAddPortfolio: string;

  constructor(private store: State<fromStore.State>,
              private language: LanguageService,
              private dialog: MatDialog,
              private platform: PlatformService,
              private professionalStore: ProfessionalStoreService,
              private professionalService: ProfessionalStoreService) {
  }

  ngOnInit() {
    // Data
    this.portfolioList$ = this.store.pipe(select(getPortfolioList));

    // Header title
    this.headerTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_PORTFOLIO_TITLE;
    this.headerSubtitle = this.language.getLanguage().PROFESSIONAL_PROFILE_PORTFOLIO_SUBTITLE;
    this.labelAddPortfolio = this.language.getLanguage().PROFESSIONAL_PROFILE_PORTFOLIO_ADD_PORTFOLIO;

  }

  onAddPortfolio() {
    const dialogRef = this.dialog.open(AddPortfolioComponent, {
      width: this.platform.isMobileSize() ? '90%' : '40%'
    });

    const closePortfolioSub = dialogRef.componentInstance.onCloseDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    const closeDialog = dialogRef.afterClosed().subscribe(result => {
      // Load Portfolio list
      this.professionalStore.loadPortfolioList();

      closePortfolioSub.unsubscribe();
      closeDialog.unsubscribe();
    });
  }

  onPortfolioItemRemove(id: string) {
    this.professionalService.deletePortfolioEntry(id);
  }
}
