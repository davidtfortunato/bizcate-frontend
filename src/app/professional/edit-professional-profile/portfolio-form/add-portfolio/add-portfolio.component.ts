import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {PlatformService} from '../../../../shared/platform';
import {LanguageService} from '../../../../core/language/language.service';
import {ProfessionalPortfolioModel} from '../../../../../../functions/src/data/portfolio-model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppUIParams} from '../../../../shared/app.ui-params';
import {StorageApiService} from '../../../../core/api/storage-api.service';
import {StorageFileModel} from '../../../../../../functions/src/data/storage-model';
import {ProgressBarInterface} from '../../../../shared/components/progress-bar/progress-bar.component';
import {SnackBarService} from '../../../../shared/components/snack-bar/snack-bar.service';
import {PortfolioApiService} from '../../../../core/api/portfolio-api.service';
import {AppStoreService} from '../../../../store/app-store.service';

const PREFIX_FILENAME = 'portfolio_';

@Component({
  selector: 'app-add-portfolio',
  templateUrl: './add-portfolio.component.html',
  styleUrls: ['./add-portfolio.component.scss']
})
export class AddPortfolioComponent implements OnInit {

  @Output() onCloseDialog = new EventEmitter();

  // Data
  isMobile$: Observable<boolean>;
  progressBarData: ProgressBarInterface = {
    display: false,
    percentage: 0,
    type: 'determine'
  };

  // Header title
  labelHeaderTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_PORTFOLIO_TITLE;

  // Placeholders
  labelTitle: string;
  labelDescription: string;
  labelLink: string;
  labelPubDate: string;
  labelSaveBtn: string;
  labelCancelBtn: string;
  // Error
  errorFieldRequired: string;

  // Form
  form: FormGroup;
  formControlTitle: FormControl = new FormControl('', Validators.required);
  formControlDescription: FormControl = new FormControl('', Validators.required);
  formControlLink: FormControl = new FormControl('');
  formControlPubDate: FormControl = new FormControl('');

  // Limits
  pubdateMaxDate = new Date();

  // UI Params
  uiSaveBtnBgcolor = AppUIParams.actionButtonGreenColor;
  uiCancelBtnBgcolor = AppUIParams.actionButtonOrangeBackgroundColor;

  // Data to save
  fileThumbnail: any;

  constructor(
    private formBuilder: FormBuilder,
    private platform: PlatformService,
    private language: LanguageService,
    private firebaseUtils: StorageApiService,
    private snackbar: SnackBarService,
    private portfolioAPI: PortfolioApiService,
    private appStoreService: AppStoreService) {
  }

  ngOnInit() {
    this.isMobile$ = this.platform.isMobileSize(true);

    // Labels
    this.labelHeaderTitle = this.language.getLanguage().PROFESSIONAL_PROFILE_PORTFOLIO_ADD_PORTFOLIO;
    this.labelTitle = this.language.getLanguage().GENERIC_FORM_TITLE;
    this.labelDescription = this.language.getLanguage().GENERIC_FORM_DESCRIPTION;
    this.labelLink = this.language.getLanguage().GENERIC_FORM_LINK;
    this.labelPubDate = this.language.getLanguage().GENERIC_FORM_PUBLISHED_DATE;
    this.errorFieldRequired = this.language.getLanguage().GENERIC_FORM_ERROR_FIELD_REQUIRED;
    this.labelSaveBtn = this.language.getLanguage().GENERIC_SAVE_LABEL;
    this.labelCancelBtn = this.language.getLanguage().GENERIC_CANCEL_LABEL;

    this.initForm();

  }

  private initForm() {
    // Init Form
    this.form = this.formBuilder.group({
      title: this.formControlTitle,
      description: this.formControlDescription,
      link: this.formControlLink,
      pubDate: this.formControlPubDate
    });
  }

  onSavePortfolio() {

    // First validate if form is valid
    if (this.form.valid) {

      const portfolioData: ProfessionalPortfolioModel = {
        uid: '',
        title: this.formControlTitle.value,
        description: this.formControlDescription.value,
        url: this.formControlLink.value ? this.formControlLink.value : '',
        publishedDate: this.formControlPubDate.value ? this.formControlPubDate.value.getTime() : ''
      };

      // Check if should save the Thumbnail first
      if (this.fileThumbnail) {
        this.firebaseUtils
          .uploadUserFile(this.fileThumbnail,
            PREFIX_FILENAME + this.formControlTitle.value,

            (storageFileModel: StorageFileModel) => {
              this.progressBarData.type = 'indetermine';
              this.progressBarData.percentage = 100;
              this.appStoreService.hideProgressBar(this.progressBarData.id);
              portfolioData.thumbnailImageUrl = storageFileModel.fileUrl;
              portfolioData.thumbnailFile = storageFileModel;

              // Emit save portfolio data
              this.savePortfolioData(portfolioData);
            },
            (error: any) => {
              // upload failed
              this.progressBarData.display = false;
              this.progressBarData.percentage = 0;
              this.appStoreService.hideProgressBar();
              this.snackbar.displayMessage(this.language.getLanguage().ERROR_GENERIC_UPLOAD_PHOTO, 'error');
            },
            (percentage: number) => {
              this.progressBarData.display = true;
              this.progressBarData.percentage = percentage;
              this.appStoreService.displayProgressBarByData(this.progressBarData);
            });
      }
    } else {
      this.form.markAsTouched();
    }
  }

  /**
   * Save portfolio data
   * @param portfolioData
   */
  private savePortfolioData(portfolioData: ProfessionalPortfolioModel) {
    const subs = this.portfolioAPI.addProfessionalPortfolio(portfolioData).subscribe((response) => {
      if (response && response.isSuccess) {
        this.snackbar.displayMessage(this.language.getLanguage().GENERIC_SAVED_SUCCESS, 'success');
        this.onCloseDialog.emit();
      } else {
        this.progressBarData.display = false;
        this.progressBarData.percentage = 0;
        this.appStoreService.hideProgressBar();
        this.snackbar.displayMessage(this.language.getLanguage().ERROR_GENERIC_SAVING_FAILED, 'error');
      }

      subs.unsubscribe();
    });
  }

  onCancelClick() {
    this.onCloseDialog.emit();
  }

  onFileChange(file) {
    this.fileThumbnail = file;
  }
}
