import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatCardModule,
  MatCheckboxModule, MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatRadioModule,
  MatSelectModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {EffectsModule} from '@ngrx/effects';
import {Effects} from './store/effects';
import {RouterModule, Routes} from '@angular/router';
import {EditProfessionalProfileComponent} from './edit-professional-profile/edit-professional-profile.component';
import {ProfessionalComponent} from './professional.component';
import {COMPONENTS} from './index';
import {ProfessionalStoreService} from './store/professional-store.service';
import {MapModule} from '../map/map.module';
import {AddPortfolioComponent} from './edit-professional-profile/portfolio-form/add-portfolio/add-portfolio.component';

const ROUTES: Routes = [
  {path: '', component: ProfessionalComponent, pathMatch: 'full'},
  {path: 'profile', component: EditProfessionalProfileComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCardModule,
    CommonModule,
    SharedModule,
    EffectsModule.forFeature([Effects]),
    RouterModule.forChild(ROUTES),
    MapModule,
    MatDialogModule
  ],
  declarations: [
    COMPONENTS
  ],
  providers: [ProfessionalStoreService],
  entryComponents: [AddPortfolioComponent]
})
export class ProfessionalModule {
}
