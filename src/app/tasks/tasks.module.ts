import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {COMPONENTS} from './index';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDatepickerModule, MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule
} from '@angular/material';
import {MapModule} from '../map/map.module';
import {EffectsModule} from '@ngrx/effects';
import {Effects} from './create/store/effects';
import {TaskCreateStoreService} from './create/store/task-create-store.service';
import {TaskCreateContainerComponent} from './create/task-create-container.component';
import {TaskDetailsContainerComponent} from './details/task-details-container.component';
import {AppRoutes} from '../core/navigation/routes';
import {MakeOfferDialogComponent} from './details/task-offers/make-offer-dialog/make-offer-dialog.component';
import {DisplayMakeOfferService} from './details/services/display-make-offer.service';
import {TaskDetailsEffects} from './details/store/effects';
import {TaskDetailsStoreService} from './details/store/task-details-store.service';
import {TaskListContainerComponent} from './mytasks/task-list-container.component';
import {TaskBrowseContainerComponent} from './browsetasks/task-browse-container.component';
import {CalendarEventsComponent} from '../shared/components/calendar-events/calendar-events.component';
import {DisplaySelectAvailabilityService} from './details/services/display-select-availability.service';
import {TaskCalendarDialogComponent} from './details/task-schedule/calendar-dialog/calendar-dialog.component';
import {PostReviewDialogComponent} from './details/task-reviews/post-dialog/post-review-dialog.component';
import {BrowseTasksStoreService} from './browsetasks/store/browse-tasks-store.service';
import {BrowseTasksEffects} from './browsetasks/store/effects';
import {TaskBrowseFilterContainerComponent} from './browsetasks/filters/task-browse-filter-container.component';
import {TasksSharedModule} from './shared/tasks-shared.module';
import {TaskProfessionalListContainerComponent} from './my-pro-tasks/task-professional-list-container.component';

const ROUTES: Routes = [
  {path: '', component: TaskCreateContainerComponent, pathMatch: 'full'},
  {path: 'create', component: TaskCreateContainerComponent, pathMatch: 'full'},
  {path: 'list', component: TaskBrowseContainerComponent, pathMatch: 'full'},
  {path: 'mytasks', component: TaskListContainerComponent, pathMatch: 'full'},
  {path: 'my-professional-tasks', component: TaskProfessionalListContainerComponent, pathMatch: 'full'},
  {path: ':' + AppRoutes.TASKID, component: TaskDetailsContainerComponent, pathMatch: 'full'}
];

@NgModule({
  providers: [
    TaskCreateStoreService,
    DisplayMakeOfferService,
    TaskDetailsStoreService,
    DisplaySelectAvailabilityService,
    BrowseTasksStoreService
  ],
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    TasksSharedModule,
    EffectsModule.forFeature([Effects, TaskDetailsEffects, BrowseTasksEffects]),
    RouterModule.forChild(ROUTES),
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MapModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule
  ],
  entryComponents: [
    MakeOfferDialogComponent,
    CalendarEventsComponent,
    TaskCalendarDialogComponent,
    PostReviewDialogComponent,
    TaskBrowseFilterContainerComponent
  ]
})
export class TasksModule {
}
