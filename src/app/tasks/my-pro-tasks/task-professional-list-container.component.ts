import {Component, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../core/language/language.service';
import {TasksApiService} from '../../core/api/tasks-api.service';
import {Observable, Subscription} from 'rxjs';
import {TaskOfferModel} from '../../../../functions/src/data/task-offer-model';
import {ApiResponse} from '../../../../functions/src/data';
import {SwitchItemData} from '../../shared/components/switch/switch-container.component';
import {TaskModel, TaskStatus} from '../../../../functions/src/data/task-model';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';
import {select} from '@ngrx/store';
import {
  getAcceptedScheduledTasks,
  getAcceptedWaitingScheduleTasks, getProfessionalFinishedOffers,
  getProfessionalOffersPending
} from '../../core/central-data/store/selectors';

enum MyProOffersTypes {
  ACCEPTED_OFFERS_PENDING_SCHEDULE = 'ACCEPTED_OFFERS_PENDING_SCHEDULE',
  PENDING_OFFERS = 'PENDING_OFFERS',
  SCHEDULED_TASKS = 'SCHEDULED_TASKS',
  COMPLETED_TASKS = 'COMPLETED_TASKS'
}

@Component({
  selector: 'app-task-professional-list-container',
  templateUrl: './task-professional-list-container.component.html',
  styleUrls: ['./task-professional-list-container.component.scss']
})
export class TaskProfessionalListContainerComponent implements OnInit {

  // Types
  myProOffersTypes = MyProOffersTypes;

  // Labels
  headerTitle: { title?: string, subtitle?: string } = {};
  labelSelectTaskStatus: string;
  labelEmptyMessage: string;

  // Data
  statusSwitchData: SwitchItemData[];
  currentTaskStatusSelected: SwitchItemData;

  // Observed data
  myAcceptedWaitingScheduleTasks$: Observable<TaskOfferModel[]>;
  myAcceptedScheduleTasks$: Observable<TaskOfferModel[]>;
  myFinishedTasks$: Observable<TaskOfferModel[]>;
  myPendingOffers$: Observable<TaskOfferModel[]>;


  constructor(private languages: LanguageService,
              private centralDataStore: CentralUserDataStoreService) {
  }

  ngOnInit(): void {

    // Set Labels
    this.headerTitle.title = this.languages.getLanguage().MY_TASKS_MODULE_TITLE;
    this.headerTitle.subtitle = this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SUBTITLE;
    this.labelSelectTaskStatus = this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SWITCH_TITLE_CHANGE_STATUS;
    this.labelEmptyMessage = this.languages.getLanguage().TASK_BROWSE_EMPTY_LIST;

    // Get observable data
    this.myPendingOffers$ = this.centralDataStore.getStore().pipe(select(getProfessionalOffersPending));
    this.myAcceptedWaitingScheduleTasks$ = this.centralDataStore.getStore().pipe(select(getAcceptedWaitingScheduleTasks));
    this.myAcceptedScheduleTasks$ = this.centralDataStore.getStore().pipe(select(getAcceptedScheduledTasks));
    this.myFinishedTasks$ = this.centralDataStore.getStore().pipe(select(getProfessionalFinishedOffers));

    // Generate Task Status switcher
    // Set Switch Data

    // Accepted offer and waiting to schedule
    this.statusSwitchData = [];
    this.currentTaskStatusSelected = {
      id: MyProOffersTypes.ACCEPTED_OFFERS_PENDING_SCHEDULE,
      label: this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SWITCH_ACCEPTED_OFFERS_PENDING_SCHEDULE
    };
    this.statusSwitchData.push(this.currentTaskStatusSelected);

    // Accepted and scheduled tasks
    this.statusSwitchData.push({
      id: MyProOffersTypes.SCHEDULED_TASKS,
      label: this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SWITCH_SCHEDULED_TASKS
    });

    // Completed task
    this.statusSwitchData.push({
      id: MyProOffersTypes.COMPLETED_TASKS,
      label: this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SWITCH_COMPLETED_TASKS
    });

    // Pending offers
    this.statusSwitchData.push({
      id: MyProOffersTypes.PENDING_OFFERS,
      label: this.languages.getLanguage().MY_PROFESSIONAL_TASKS_SWITCH_PENDING_OFFERS
    });
  }

  onSwitchSelectionChanged(switchItem: SwitchItemData) {
    this.currentTaskStatusSelected = switchItem;
  }

}
