import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {MakeOfferDialogComponent} from '../task-offers/make-offer-dialog/make-offer-dialog.component';
import {PlatformService} from '../../../shared/platform';
import {UserProfileModel} from '../../../../../functions/src/data';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';
import {UserProfileRoleType} from '../../../../../functions/src/data/user-model';

@Injectable()
export class DisplayMakeOfferService {

  constructor(private dialog: MatDialog, private platform: PlatformService) {
  }

  displayMakeOfferDialog(taskData: TaskModel) {
    const dialogRef = this.dialog.open(MakeOfferDialogComponent, {
      width: this.platform.isMobileSize() ? '90%' : '40%',
      data: {
        taskData: taskData
      }
    });

    const closePortfolioSub = dialogRef.componentInstance.closeDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    dialogRef.afterClosed().toPromise().then(value => {
      if (closePortfolioSub) {
        closePortfolioSub.unsubscribe();
      }
    });
  }

  hasMakeOfferButton(userProfile: UserProfileModel, taskData: TaskModel) {
    return userProfile
      && userProfile.role === UserProfileRoleType.CLIENT_PROFESSIONAL
      && taskData
      && taskData.status === TaskStatus.OPEN
      && taskData.uid !== userProfile.uid;
  }

  isMakeOfferButtonEnabled(userProfile: UserProfileModel, taskData: TaskModel, taskOffers: TaskOfferModel[]) {
    let isButtonEnabled = false;

    if (this.hasMakeOfferButton(userProfile, taskData) && taskOffers) {
      isButtonEnabled = true;
      // Check if User already sent an offer
       taskOffers.forEach(offer => {
         if (offer.userInfo.uid === userProfile.uid) {
           isButtonEnabled = false;
         }
       });
    }
    return isButtonEnabled;
  }
}
