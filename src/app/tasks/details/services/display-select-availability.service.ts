import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {PlatformService} from '../../../shared/platform';
import {TaskCalendarDialogComponent} from '../task-schedule/calendar-dialog/calendar-dialog.component';
import {TaskDetailsStoreService} from '../store/task-details-store.service';
import {UserProfileModel} from '../../../../../functions/src/data';

@Injectable()
export class DisplaySelectAvailabilityService {

  constructor(private dialog: MatDialog, private platform: PlatformService, private storeService: TaskDetailsStoreService) {
  }

  displaySelectAvailabilityDialog(taskData: TaskModel, userProfile: UserProfileModel) {
    const dialogRef = this.dialog.open(TaskCalendarDialogComponent, {
      width: this.platform.isMobileSize() ? '90%' : '90%',
      data: {
        taskData: taskData,
        userProfile: userProfile,
        isMobile: this.platform.isMobileSize()
      }
    });

    const closeDialogSub = dialogRef.componentInstance.closeDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    dialogRef.afterClosed().toPromise().then(value => {
      this.storeService.init(taskData.id);

      if (closeDialogSub) {
        closeDialogSub.unsubscribe();
      }
    });
  }

}
