import {Component, Input, OnInit} from '@angular/core';
import {TaskExtraStatus, TaskStatus} from '../../../../../../functions/src/data/task-model';
import {LanguageService} from '../../../../core/language/language.service';
import {AppUIParams} from '../../../../shared/app.ui-params';

interface TaskStatusUI {
  textColor: string;
  textSelectedColor: string;
  backgroundSelectedColor: string;
}

@Component({
  selector: 'app-task-details-status',
  templateUrl: './task-status.component.html',
  styleUrls: ['./task-status.component.scss']
})
export class TaskDetailsStatusComponent implements OnInit {

  @Input() taskStatus: TaskStatus;
  @Input() extraStatus: TaskExtraStatus;

  // UI Params
  uiStatusParams = {
    textColor: AppUIParams.actionButtonGrayColor,
    textSelectedColor: AppUIParams.actionButtonGreenColor,
    backgroundSelectedColor: AppUIParams.actionButtonGreenColorOpacity
  };

  // Labels
  labelOpen: string;
  labelAssigned: string;
  labelCompleted: string;

  // Types
  taskStatusTypes = TaskStatus;

  // flags
  isExpired: boolean = false;

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    this.labelOpen = this.languages.getLanguage().TASK_DETAILS_STATUS_OPEN;
    this.labelAssigned = this.languages.getLanguage().TASK_DETAILS_STATUS_ASSIGNED;
    if (this.taskStatus === TaskStatus.ASSIGNED && this.extraStatus === TaskExtraStatus.IN_PROGRESS) {
      this.labelAssigned = this.languages.getLanguage().TASK_DETAILS_STATUS_IN_PROGRESS;
      this.uiStatusParams.backgroundSelectedColor = AppUIParams.actionButtonOrangeBackgroundColorOpacity;
      this.uiStatusParams.textSelectedColor = AppUIParams.actionButtonOrangeBackgroundColor;
    }
    this.labelCompleted = this.languages.getLanguage().TASK_DETAILS_STATUS_DONE;
    if (this.taskStatus === TaskStatus.DONE && this.extraStatus === TaskExtraStatus.EXPIRED) {
      this.labelCompleted = this.languages.getLanguage().TASK_DETAILS_STATUS_EXPIRED;
      this.uiStatusParams.backgroundSelectedColor = AppUIParams.actionButtonRedColorOpacity;
      this.uiStatusParams.textSelectedColor = AppUIParams.actionButtonRedColor;
      this.isExpired = true;
    }
  }

}
