import {Component, Input, OnInit} from '@angular/core';
import {LanguageService} from '../../../../core/language/language.service';
import {TaskModel} from '../../../../../../functions/src/data/task-model';

@Component({
  selector: 'app-task-details-photos',
  templateUrl: './task-photos.component.html',
  styleUrls: ['./task-photos.component.scss']
})
export class TaskPhotosComponent implements OnInit {
  @Input() taskData: TaskModel;

  // Label
  labelTaskPhotosTitle: string;

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    this.labelTaskPhotosTitle = this.languages.getLanguage().TASK_DETAILS_PHOTOS_TITLE;
  }
}
