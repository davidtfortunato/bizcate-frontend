import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {LanguageService} from '../../../../core/language/language.service';
import {TaskBudgetType, TaskModel} from '../../../../../../functions/src/data/task-model';
import {AppUIParams} from '../../../../shared/app.ui-params';
import {DisplayMakeOfferService} from '../../services/display-make-offer.service';
import {TaskOfferModel} from '../../../../../../functions/src/data/task-offer-model';
import {UserProfileModel} from '../../../../../../functions/src/data';
import {TasksApiService} from '../../../../core/api/tasks-api.service';

@Component({
  selector: 'app-task-details-budget',
  templateUrl: './task-budget.component.html',
  styleUrls: ['./task-budget.component.scss']
})
export class TaskDetailsBudgetComponent implements OnInit, OnChanges {
  @Input() taskData: TaskModel;
  @Input() taskOffers: TaskOfferModel[];
  @Input() userProfile: UserProfileModel;

  // Labels
  labelTaskBudget: string;
  labelMakeOfferButton: string;

  // Data
  budgetTotalText: string;
  isButtonEnabled: boolean;

  // UI Params
  uiActionButtonBackgroundcolor: string;
  uiActionButtonTextcolor: string;

  constructor(private languages: LanguageService,
              private displayMakeOffer: DisplayMakeOfferService,
              private tasksAPI: TasksApiService) {
  }

  ngOnInit() {
    this.labelTaskBudget = this.languages.getLanguage().TASK_DETAILS_BUDGET_TITLE;
    this.labelMakeOfferButton = this.languages.getLanguage().TASK_DETAILS_BUDGET_MAKE_OFFER;

  }

  ngOnChanges() {
    this.budgetTotalText = this.getTaskBudget();

    this.isButtonEnabled = this.displayMakeOffer.isMakeOfferButtonEnabled(this.userProfile, this.taskData, this.taskOffers);
    this.uiActionButtonBackgroundcolor = this.isButtonEnabled
      ? AppUIParams.actionButtonGreenColor : AppUIParams.actionButtonOrangeBackgroundColor;
    this.uiActionButtonTextcolor = AppUIParams.actionButtonWhiteColor;
  }

  getTaskBudget() {
    console.log('getTaskBudget');
    if (this.taskData) {
      this.budgetTotalText = this.taskData.budget.expectedBudget;

      // Set currency
      this.budgetTotalText += this.taskData.budget.currency ? this.taskData.budget.currency : '€';

      if (this.taskData.budget.budgetType === TaskBudgetType.HOURLY) {
        this.budgetTotalText += '/' + this.languages.getLanguage().TASK_DETAILS_BUDGET_PER_HOUR.toLocaleUpperCase();
      }
    }
    return this.budgetTotalText;
  }

  hasMakeOfferButton() {
    return this.displayMakeOffer.hasMakeOfferButton(this.userProfile, this.taskData);
  }

  onMakeOfferClick() {
    this.displayMakeOffer.displayMakeOfferDialog(this.taskData);
  }

}
