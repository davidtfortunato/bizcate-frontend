import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../../functions/src/data/task-model';
import {AppAssets} from '../../../../shared/app.assets';
import {LanguageService} from '../../../../core/language/language.service';
import {NavigationLinksService} from '../../../../core/navigation/navigation-links.service';
import {UtilsService} from '../../../../core/utils/utils.service';
import {AppUIParams} from '../../../../shared/app.ui-params';

@Component({
  selector: 'app-task-details-description',
  templateUrl: './task-description.component.html',
  styleUrls: ['./task-description.component.scss']
})
export class TaskDetailsDescriptionComponent implements OnInit, OnChanges {
  @Input() taskData: TaskModel;

  // Labels
  labelPostedBy: string;
  timePostedText: string;
  labelDuedate: string;
  duedateText: string;
  labelListSkills: string;
  labelDescription: string;

  constructor(private languages: LanguageService, private navigation: NavigationLinksService, private utils: UtilsService) {
  }

  ngOnInit() {
    this.labelDuedate = this.languages.getLanguage().TASK_DETAILS_DUEDATE;
    this.labelPostedBy = this.languages.getLanguage().TASK_DETAILS_POSTEDBY;
    this.labelListSkills = this.languages.getLanguage().TASK_DETAILS_LISTSKILLS;
    this.labelDescription = this.languages.getLanguage().TASK_DETAILS_DESCRIPTION;
  }

  ngOnChanges() {
    if (this.taskData && this.taskData.postedTime) {
      this.timePostedText = this.languages.getLanguage().TASK_DETAILS_POSTED_AT
        + this.utils.convertMillisToFormattedDate(this.taskData.postedTime);
    }

    if (this.taskData && this.taskData.budget && this.taskData.budget.dueDate) {
      this.duedateText = this.utils.convertMillisToFormattedDateDay(Number(this.taskData.budget.dueDate));
    } else {
      this.duedateText = this.languages.getLanguage().TASK_DETAILS_DUEDATE_NOT_DEFINED;
    }
  }

  getUserPhoto() {
    return this.taskData
    && this.taskData.userInfo
    && this.taskData.userInfo.photoUrl ? this.taskData.userInfo.photoUrl : AppAssets.ic_avatar;
  }

  onUserClick() {
    this.navigation.openUserProfile(this.taskData.uid);
  }
}
