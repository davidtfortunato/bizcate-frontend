import {Component, Input, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {ApiResponse, UserProfileModel} from '../../../../../functions/src/data';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

@Component({
  selector: 'app-task-details-header',
  templateUrl: './task-details-header.component.html',
  styleUrls: ['./task-details-header.component.scss']
})
export class TaskDetailsHeaderComponent implements OnInit {
  @Input() taskData: TaskModel;
  @Input() taskOffers: TaskOfferModel[];
  @Input() userProfile: UserProfileModel;

  constructor() {
  }

  ngOnInit() {
    console.log(JSON.stringify(this.taskData));
  }

}
