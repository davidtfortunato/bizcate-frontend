import {Component, Inject, Input, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../../functions/src/data/task-model';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../../core/navigation/routes';

@Component({
  selector: 'app-task-details-share',
  templateUrl: './task-share.component.html',
  styleUrls: ['./task-share.component.scss']
})
export class TaskDetailsShareComponent implements OnInit {
  @Input() taskData: TaskModel;

  // Shareable Link
  shareableLink: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.shareableLink = AppRoutes.DOMAIN + this.router.url;
  }

}
