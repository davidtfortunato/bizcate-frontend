import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../../functions/src/data/task-model';
import {AppMapLocation} from '../../../../../../functions/src/data';
import {LanguageService} from '../../../../core/language/language.service';
import {MapMarker} from '../../../../map/map-component/map.component';

@Component({
  selector: 'app-task-details-location',
  templateUrl: './task-location.component.html',
  styleUrls: ['./task-location.component.scss']
})
export class TaskDetailsLocationComponent implements OnInit, OnChanges {
  @Input() taskData: TaskModel;


  labelTitle: string;
  labelIsRemoteAllowed: string;
  mapMarkers: MapMarker[];

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    this.labelTitle = this.languages.getLanguage().TASK_DETAILS_LOCATION_TITLE;
    this.labelIsRemoteAllowed = this.languages.getLanguage().TASK_DETAILS_LOCATION_REMOTE_WORK;
  }


  ngOnChanges() {
    this.mapMarkers = this.getLocationAreas();
    console.log('Map Markers: ' + this.mapMarkers);
  }

  hasLocation() {
    return this.taskData && this.taskData.location && this.taskData.location.mapLocation;
  }
  getLocation(): AppMapLocation {
    return this.taskData.location.mapLocation;
  }

  getLocationAreas(): MapMarker[] {
    return this.hasLocation() ? [{
      isSelected: true,
      location: this.getLocation()
    }] : [];
  }
}
