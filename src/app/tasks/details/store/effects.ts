import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {TasksApiService} from '../../../core/api/tasks-api.service';
import {Store} from '@ngrx/store';
import {State} from '../../../store';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {LanguageService} from '../../../core/language/language.service';
import {SnackBarService} from '../../../shared/components/snack-bar/snack-bar.service';
import {map, mergeMap, switchMap} from 'rxjs/operators';
import {Init, ListTaskOffers, ListTaskOffersResponse, LoadTaskDetails, LoadTaskDetailsResponse} from './actions';
import {ActionTypes} from './actions';
import {AppStoreService} from '../../../store/app-store.service';

@Injectable()
export class TaskDetailsEffects {

  @Effect()
  init$ = this.actions$.pipe(
    ofType<Init>(ActionTypes.Init),
    mergeMap((action) => {
        return [new LoadTaskDetails({taskId: action.payload.taskId}),
          new ListTaskOffers({taskId: action.payload.taskId})];
      }
    ));

  @Effect()
  loadDetails$ = this.actions$.pipe(
    ofType<LoadTaskDetails>(ActionTypes.LoadTaskDetails),
    switchMap((action) => {
      return this.tasksAPI
        .getTaskDetails(action.payload.taskId)
        .pipe(map(response => {
          return new LoadTaskDetailsResponse({response});
        }));
    }));

  @Effect({dispatch: false})
  loadDetailsResponse$ = this.actions$.pipe(
    ofType<LoadTaskDetailsResponse>(ActionTypes.LoadTaskDetailsResponse),
    map(action => {
      if (!action.payload.response.isSuccess) {
        this.snackbarService.displayMessage(this.language.getLanguage().ERROR_GENERIC_OPENING_PAGE);
      }
    })
  );

  @Effect()
  loadTaskOffers$ = this.actions$.pipe(
    ofType<ListTaskOffers>(ActionTypes.ListTaskOffers),
    switchMap(action => {
      return this.tasksAPI.getTaskOffers(action.payload.taskId).pipe(
        map(response => {
          return new ListTaskOffersResponse({response});
        })
      );
    })
  );

  constructor(private actions$: Actions,
              private tasksAPI: TasksApiService,
              private store: Store<State>,
              private navigation: NavigationLinksService,
              private language: LanguageService,
              private snackbarService: SnackBarService) {
  }

}
