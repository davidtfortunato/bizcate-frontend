import {TaskBudgetType, TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';
import {Actions, ActionTypes} from './actions';

export interface State {
  taskId: string;
  task: TaskModel;
  taskOffers: TaskOfferModel[];
}


export const initialState: State = getInitState();

export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.Init:
      return {
        ...state,
        taskId: action.payload.taskId
      };

    case ActionTypes.LoadTaskDetailsResponse:
      return {
        ...state,
        task: action.payload.response.isSuccess ? action.payload.response.responseData : state.task
      };

    case ActionTypes.ListTaskOffersResponse:
      return {
        ...state,
        taskOffers: action.payload.response.isSuccess ? action.payload.response.responseData : state.taskOffers,
      };
  }

  return state;
}


function getInitState() {
  return {
    taskId: '',
    task: null,
    taskOffers: []
  };
}

// Pure Functions
export const getTaskDetails = (state: State) => state.task;
export const getTaskOffers = (state: State) => state.taskOffers;
