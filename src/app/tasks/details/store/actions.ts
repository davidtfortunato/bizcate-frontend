import {Action} from '@ngrx/store';
import {ApiResponse} from '../../../../../functions/src/data';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

export enum ActionTypes {
  Init = '[Task] Init Task Details',

  LoadTaskDetails = '[Task] LoadTaskDetails',
  LoadTaskDetailsResponse = '[Task] LoadTaskDetailsResponse',

  // Task Offers
  ListTaskOffers = '[Task] ListTaskOffers',
  ListTaskOffersResponse = '[Task] ListTaskOffersResponse'

}

export class Init implements Action {
  readonly type = ActionTypes.Init;

  constructor(public payload: {taskId: string}) {}
}

/*** Load Task Details **/
export class LoadTaskDetails implements Action {
  readonly type = ActionTypes.LoadTaskDetails;

  constructor(public payload: {taskId: string}) {}
}
export class LoadTaskDetailsResponse implements Action {
  readonly type = ActionTypes.LoadTaskDetailsResponse;

  constructor(public payload: {response: ApiResponse<TaskModel>}) {
  }
}

/*** Load Task Offers **/
export class ListTaskOffers implements Action {
  readonly type = ActionTypes.ListTaskOffers;

  constructor(public payload: {taskId: string}) {}
}
export class ListTaskOffersResponse implements Action {
  readonly type = ActionTypes.ListTaskOffersResponse;

  constructor(public payload: {response: ApiResponse<TaskOfferModel[]>}) {
  }
}

export type Actions = Init
  | LoadTaskDetails
  | LoadTaskDetailsResponse
  | ListTaskOffers
  | ListTaskOffersResponse;
