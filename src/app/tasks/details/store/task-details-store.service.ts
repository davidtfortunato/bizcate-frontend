import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Init, ListTaskOffers, LoadTaskDetails} from './actions';
import {State} from './reducer';

@Injectable()
export class TaskDetailsStoreService {

  constructor(private store: Store<State>) {
  }

  init(taskId: string) {
    this.store.dispatch(new Init({taskId}));
  }

  loadTaskDetails(taskId: string) {
    this.store.dispatch(new LoadTaskDetails({taskId}));
  }

  loadListTaskOffers(taskId: string) {
    this.store.dispatch(new ListTaskOffers({taskId}));
  }
}
