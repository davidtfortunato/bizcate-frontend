import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromStore from './reducer';


export const selectDetailsTaskState = createFeatureSelector<fromStore.State>('taskDetails');

// Getters
export const getTaskDetails = createSelector(selectDetailsTaskState, fromStore.getTaskDetails);
export const getTaskOffers = createSelector(selectDetailsTaskState, fromStore.getTaskOffers);
