import {Component, OnDestroy, OnInit} from '@angular/core';
import {TasksApiService} from '../../core/api/tasks-api.service';
import {ActivatedRoute} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {TaskModel, TaskStatus} from '../../../../functions/src/data/task-model';
import {AppRoutes} from '../../core/navigation/routes';
import {ApiResponse, UserProfileModel} from '../../../../functions/src/data';
import {LanguageService} from '../../core/language/language.service';
import {HeaderActionButton} from '../../shared/components/title-header/title-header.component';
import {AppUIParams} from '../../shared/app.ui-params';
import {DisplayMakeOfferService} from './services/display-make-offer.service';
import {TaskOfferModel} from '../../../../functions/src/data/task-offer-model';
import {UserAPIService} from '../../core/api';
import {TaskDetailsStoreService} from './store/task-details-store.service';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getTaskDetails, getTaskOffers} from './store/selectors';

@Component({
  selector: 'app-task-details-container',
  templateUrl: './task-details-container.component.html'
})
export class TaskDetailsContainerComponent implements OnInit, OnDestroy {

  // Labels
  headerTitle: { title?: string, subtitle?: string } = {};

  // Header Buttons
  headerActionButtons: HeaderActionButton[] = [];

  // Observable Data
  userProfile$: Observable<UserProfileModel>;
  taskDetails$: Observable<TaskModel>;
  taskOffers$: Observable<TaskOfferModel[]>;

  // Data
  taskData: TaskModel;
  taskOffersData: TaskOfferModel[];
  userProfile: UserProfileModel;

  // params
  paramTaskId: string;

  // Subscriptions
  subsParams: Subscription;
  subsTaskDetails: Subscription;
  subsTaskOffers: Subscription;
  subsUserProfile: Subscription;

  constructor(private tasksAPI: TasksApiService,
              private route: ActivatedRoute,
              private languages: LanguageService,
              private displayMakeOffer: DisplayMakeOfferService,
              private userAPI: UserAPIService,
              private taskStoreService: TaskDetailsStoreService,
              private store: State<fromStore.State>) {
  }

  ngOnInit() {

    // Languages
    this.headerTitle.title = this.languages.getLanguage().TASKS_MODULE_TITLE;
    this.headerTitle.subtitle = this.languages.getLanguage().TASK_DETAILS_TITLE;

    // Load Task Details
    this.taskDetails$ = this.store.pipe(select(getTaskDetails));
    this.taskOffers$ = this.store.pipe(select(getTaskOffers));

    // Get Task id
    this.subsParams = this.route.params.subscribe(params => {
      if (params[AppRoutes.TASKID]) {
        this.paramTaskId = params[AppRoutes.TASKID];
        this.taskStoreService.init(this.paramTaskId);
        this.subscribeData();
      }
    });

    // Get User Profile
    this.userProfile$ = this.userAPI.getUserProfile();
    this.subsUserProfile = this.userProfile$.subscribe(user => {
      this.userProfile = user;
      this.generateHeaderActionButtons();
    });

    // Make offer button
    this.generateHeaderActionButtons();
  }

  subscribeData() {
    this.subsTaskDetails = this.taskDetails$.subscribe(data => {
      if (data) {
        this.taskData = data;
        this.generateHeaderActionButtons();
      }
    });

    this.subsTaskOffers = this.taskOffers$.subscribe(data => {
      if (data) {
        this.taskOffersData = data;
        this.generateHeaderActionButtons();
      }
    });
  }

  generateHeaderActionButtons() {
    this.headerActionButtons = [];

    if (this.displayMakeOffer.hasMakeOfferButton(this.userProfile, this.taskData)) {
      this.headerActionButtons.push({
        id: '1',
        label: this.languages.getLanguage().TASK_DETAILS_BUDGET_MAKE_OFFER,
        state: this.displayMakeOffer
          .isMakeOfferButtonEnabled(this.userProfile, this.taskData, this.taskOffersData) ? 'enabled' : 'disabled',
        backgroundColor: this.displayMakeOffer
          .isMakeOfferButtonEnabled(this.userProfile, this.taskData, this.taskOffersData)
          ? AppUIParams.actionButtonGreenColor : AppUIParams.actionButtonOrangeBackgroundColor,
        textColor: AppUIParams.actionButtonWhiteColor
      });
    }
  }

  onActionButtonClick(button: HeaderActionButton) {
    console.log('Click on: ' + button.id);
    if (button.state === 'enabled') {
      this.displayMakeAnOfferDialog();
    }
  }

  /**
   * Display Make an offer dialog
   */
  displayMakeAnOfferDialog() {
    this.displayMakeOffer.displayMakeOfferDialog(this.taskData);
  }

  ngOnDestroy() {
    if (this.subsParams) {
      this.subsParams.unsubscribe();
    }

    if (this.subsTaskOffers) {
      this.subsTaskOffers.unsubscribe();
    }

    if (this.subsTaskDetails) {
      this.subsTaskDetails.unsubscribe();
    }

    if (this.subsUserProfile) {
      this.subsUserProfile.unsubscribe();
    }
  }
}
