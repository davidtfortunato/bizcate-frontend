import {Component, Input, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {UserProfileModel} from '../../../../../functions/src/data';
import {LanguageService} from '../../../core/language/language.service';
import {UtilsService} from '../../../core/utils/utils.service';
import {DisplaySelectAvailabilityService} from '../services/display-select-availability.service';
import {ScheduleStatus} from '../../../../../functions/src/data/calendar-model';

@Component({
  selector: 'app-task-schedule-container',
  templateUrl: './task-schedule-container.component.html',
  styleUrls: ['./task-schedule-container.component.scss']
})
export class TaskScheduleContainerComponent implements OnInit {
  @Input() taskData: TaskModel;
  @Input() userProfile: UserProfileModel;

  // Types
  scheduleStatusType = ScheduleStatus;

  // Label
  labelTitle: string;
  labelDescription: string;
  labelSetAvailabilityBtn: string; // For client, to define the period available
  labelSelectPeriodBtn: string; // For professional select, inside the availability the date and time to execute the task
  labelShowCalendar: string;
  labelScheduledDate: string;
  labelScheduledDuration: string;

  constructor(private languages: LanguageService,
              private utils: UtilsService,
              private displaySelectAvailability: DisplaySelectAvailabilityService) {
  }

  ngOnInit() {
    this.labelTitle = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_TITLE;

    if (this.taskData && this.taskData.scheduledStatus) {
      switch (this.buttonScheduleType) {
        case ScheduleStatus.SELECTING_AVAILABILITY_PHASE1:
          this.labelSetAvailabilityBtn = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_AVAILABILITY_PHASE1;
          if (this.isClient()) {
            this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE1_CLIENT_DESCRIPTION;
          } else if (this.isProfessional()) {
            this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE1_PROFESSIONAL_DESCRIPTION;
          }
          break;
        case ScheduleStatus.SELECTING_TIME_PHASE2:
          this.labelSelectPeriodBtn = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_SCHEDULE_DAY_PHASE2;
          if (this.isClient()) {
            this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE2_CLIENT_DESCRIPTION;
          } else if (this.isProfessional()) {
            this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE2_PROFESSIONAL_DESCRIPTION;
          }
          break;
        case ScheduleStatus.SCHEDULED_PHASE3:
          this.labelTitle = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_SCHEDULED_PHASE3;

          this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE3_DESCRIPTION;
          this.labelShowCalendar = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_SHOW_CALENDAR;

          // Set Label for scheduled date and time
          this.labelScheduledDate = this.utils.convertMillisToFormattedDateHours(this.taskData.schedule.startDate);

          if (this.taskData.schedule.endDate) {
            this.labelScheduledDate = this.labelScheduledDate
              + ' - '
              + this.utils.convertMillisToFormattedDateHours(this.taskData.schedule.endDate);
          }

          this.labelScheduledDuration = this.languages
            .getLanguage().TASK_DETAILS_SCHEDULE_DURATION
            .replace('[HOURS]', String(this.taskData.schedule.scheduledBlocks.length));

          if (this.isClient() || this.isProfessional()) {
            this.labelDescription = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_PHASE3_DESCRIPTION;
          }

          break;
      }
    }
  }

  get buttonScheduleType(): ScheduleStatus {
    if (this.taskData && this.taskData.scheduledStatus) {
      switch (this.taskData.scheduledStatus) {
        case ScheduleStatus.SELECTING_AVAILABILITY_PHASE1:
          return this.taskData.scheduledStatus;
        case ScheduleStatus.SELECTING_TIME_PHASE2:
          if (this.isClient()) {
            // The client can still edit is selections
            return ScheduleStatus.SELECTING_AVAILABILITY_PHASE1;
          } else {
            return ScheduleStatus.SELECTING_TIME_PHASE2;
          }
        case ScheduleStatus.SCHEDULED_PHASE3:
          return ScheduleStatus.SCHEDULED_PHASE3;
      }
    }
  }

  isClient() {
    return this.taskData && this.userProfile && this.taskData.uid === this.userProfile.uid;
  }

  isProfessional() {
    return this.taskData
      && this.userProfile
      && this.taskData.acceptedOffer
      && this.taskData.acceptedOffer.userInfo.uid === this.userProfile.uid;
  }

  onSelectDate() {
    this.displaySelectAvailability.displaySelectAvailabilityDialog(this.taskData, this.userProfile);
  }

  onSelectAvailability() {
    this.displaySelectAvailability.displaySelectAvailabilityDialog(this.taskData, this.userProfile);
  }

  onShowScheduleCalendar() {
    this.displaySelectAvailability.displaySelectAvailabilityDialog(this.taskData, this.userProfile);
  }

}
