import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {TaskModel} from '../../../../../../functions/src/data/task-model';
import {AppCalendarPeriod, ViewType} from '../../../../shared/components/calendar-events/calendar-events.component';
import {CalendarHelperService} from '../../../../shared/components/calendar-events/calendar-helper.service';
import {PlatformService} from '../../../../shared/platform';
import {LanguageService} from '../../../../core/language/language.service';
import {TasksApiService} from '../../../../core/api/tasks-api.service';
import {SnackBarService} from '../../../../shared/components/snack-bar/snack-bar.service';
import {UserProfileModel} from '../../../../../../functions/src/data';
import {ScheduleStatus} from '../../../../../../functions/src/data/calendar-model';

@Component({
  selector: 'app-task-calendar-dialog',
  templateUrl: './calendar-dialog.component.html',
  styleUrls: ['./calendar-dialog.component.scss']
})
export class TaskCalendarDialogComponent implements OnInit {
  @Output() closeDialog = new EventEmitter();

  // Data
  userProfile: UserProfileModel;
  taskData: TaskModel;
  listEvents: AppCalendarPeriod[];
  calendarViewType: ViewType = ViewType.WEEK;
  editable: boolean;

  // Labels
  labelSave: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private calendarHelper: CalendarHelperService,
              private platformService: PlatformService,
              private language: LanguageService,
              private taskAPI: TasksApiService,
              private snackBar: SnackBarService) {
    this.calendarViewType = this.platformService.isMobileSize() ? ViewType.DAY : ViewType.WEEK;
  }

  ngOnInit() {
    if (this.data.taskData) {
      this.userProfile = this.data.userProfile;
      this.taskData = this.data.taskData;
      this.editable = true;
      switch (this.taskData.scheduledStatus) {
        case ScheduleStatus.SELECTING_AVAILABILITY_PHASE1:
          if (this.isClient()) {
            this.listEvents = this.calendarHelper.generateAvailablePeriods(new Date(), new Date(this.taskData.budget.dueDate));
          } else {
            this.listEvents = [];
            this.editable = false;
          }
          break;
        case ScheduleStatus.SELECTING_TIME_PHASE2:
          if (this.isClient()) {
            this.listEvents = this.calendarHelper.generateAvailablePeriods(new Date(), new Date(this.taskData.budget.dueDate));
          } else if (this.isProfessional()) {
            this.listEvents = this.calendarHelper.generateSelectableHours(this.taskData.schedule.availability);
          } else {
            this.editable = false;
          }
          break;
        case ScheduleStatus.SCHEDULED_PHASE3:
          this.listEvents = this.calendarHelper.generateScheduledEvents(this.taskData.schedule.scheduledBlocks);

          console.log('ListEvents: ' + this.listEvents.length);
          this.editable = false;
          break;
      }

      // Update the selected periods
      if (this.taskData.schedule && this.taskData.schedule.availability) {
        this.listEvents.forEach(event => {
          // Check if is selected
          this.taskData.schedule.availability.forEach(selectedEvent => {
            if (event.period.id === selectedEvent.id) {
              event.isSelected = true;
            }
          });
        });
      }
    }
    this.labelSave = this.isClient() ? this.language.getLanguage().GENERIC_SAVE_LABEL
      : this.language.getLanguage().GENERIC_SCHEDULE_TASK_EXECUTION;
  }

  updateListEvents(listEvents: AppCalendarPeriod[]) {
    this.listEvents = listEvents;
  }

  hasAnyPeriodSelected() {
    if (this.listEvents) {
      for (const event of this.listEvents) {
        if (event.isSelected) {
          return true;
        }
      }
    }
    return false;
  }

  isClient() {
    return this.taskData && this.userProfile && this.taskData.uid === this.userProfile.uid;
  }

  isProfessional() {
    return this.taskData
      && this.userProfile
      && this.taskData.acceptedOffer
      && this.taskData.acceptedOffer.userInfo.uid === this.userProfile.uid;
  }

  onSaveClick() {
    const selectedPeriods = [];
    this.listEvents.forEach(event => {
      if (event.isSelected) {
        selectedPeriods.push(event.period);
      }
    });

    if (this.isClient()) {
      const sub = this.taskAPI.selectClientAvailablePeriods(this.taskData, selectedPeriods).subscribe(response => {

        if (response.isSuccess) {
          this.snackBar.displayMessage(this.language.getLanguage().GENERIC_SAVED_SUCCESS, 'success');
          this.closeDialog.emit(true);
        } else {
          this.snackBar.displayMessage('Error: ' + response.message, 'error');
        }

        if (sub) {
          sub.unsubscribe();
        }
      });
    } else {
      const sub = this.taskAPI.scheduleTaskExecution(this.taskData, selectedPeriods).subscribe(response => {
        if (response.isSuccess) {
          this.snackBar.displayMessage(this.language.getLanguage().GENERIC_SAVED_SUCCESS, 'success');
          this.closeDialog.emit(true);
        } else {
          this.snackBar.displayMessage('Error: ' + response.message, 'error');
        }

        if (sub) {
          sub.unsubscribe();
        }
      });
    }

  }
}
