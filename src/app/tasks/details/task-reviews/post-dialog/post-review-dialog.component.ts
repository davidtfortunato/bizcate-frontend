import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material';
import {LanguageService} from '../../../../core/language/language.service';
import {TaskModel, TaskReviewOrigin} from '../../../../../../functions/src/data/task-model';
import {UserProfileModel} from '../../../../../../functions/src/data';
import {TasksApiService} from '../../../../core/api/tasks-api.service';
import {SnackBarService} from '../../../../shared/components/snack-bar/snack-bar.service';
import {TaskDetailsStoreService} from '../../store/task-details-store.service';

@Component({
  selector: 'app-post-review-dialog',
  templateUrl: './post-review-dialog.component.html',
  styleUrls: ['./post-review-dialog.component.scss']
})
export class PostReviewDialogComponent {
  @Output() closeDialog = new EventEmitter();

  // Data
  taskData: TaskModel;
  userProfile: UserProfileModel;

  // Labels
  labelHeaderTitle: string;
  labelReviewMessage: string;
  labelErrorFieldRequired: string;
  labelEvaluation: string;
  labelPostReviewBtn: string;

  // Form
  formControlReviewMessage: FormControl = new FormControl('', Validators.required);
  reviewLevel: number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private language: LanguageService,
              private taskAPI: TasksApiService,
              private snack: SnackBarService,
              private taskStore: TaskDetailsStoreService) {

    this.labelHeaderTitle = this.language.getLanguage().TASK_DETAILS_REVIEW_BTN_POST_REVIEW;
    this.labelReviewMessage = this.language.getLanguage().TASK_DETAILS_REVIEW_FORM_MESSAGE;
    this.labelErrorFieldRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;
    this.labelEvaluation = this.language.getLanguage().TASK_DETAILS_REVIEW_LEVEL_EVALUATION;
    this.labelPostReviewBtn = this.language.getLanguage().GENERIC_PUBLISH;

    this.taskData = this.data.taskData;
    this.userProfile = this.data.userProfile;
  }

  onLevelSelected(level: number) {
    this.reviewLevel = level;
  }

  isReadyToPost() {
    return this.reviewLevel > 0 && this.formControlReviewMessage.valid;
  }

  onPublishCLick() {

    const sub = this.taskAPI.publishTaskReview(this.taskData,
      this.userProfile,
      this.formControlReviewMessage.value,
      this.reviewLevel,
      this.userProfile.uid === this.taskData.uid ? TaskReviewOrigin.FROM_CLIENT : TaskReviewOrigin.FROM_PROFESSIONAL)
      .subscribe(response => {
        if (sub) {
          sub.unsubscribe();
        }

        if (response && response.isSuccess) {
          this.snack.displayMessage(this.language.getLanguage().GENERIC_PUBLISH_SUCCESS, 'success');
          this.closeDialog.emit();
          this.taskStore.loadTaskDetails(this.taskData.id);
        } else {
          this.snack.displayMessage('Error: ' + response.message);
        }
      });
  }
}
