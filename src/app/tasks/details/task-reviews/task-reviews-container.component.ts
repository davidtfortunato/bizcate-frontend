import {Component, Input, OnInit} from '@angular/core';
import {TaskExtraStatus, TaskModel} from '../../../../../functions/src/data/task-model';
import {LanguageService} from '../../../core/language/language.service';
import {UserProfileModel} from '../../../../../functions/src/data';
import {PostReviewDialogComponent} from './post-dialog/post-review-dialog.component';
import {MatDialog} from '@angular/material';
import {PlatformService} from '../../../shared/platform';

@Component({
  selector: 'app-task-reviews-container',
  templateUrl: './task-reviews-container.component.html',
  styleUrls: ['./task-reviews-container.component.scss']
})
export class TaskReviewsContainerComponent implements OnInit {
  @Input() taskData: TaskModel;
  @Input() userProfile: UserProfileModel;

  // Labels
  labelTitle: string;
  labelHintText: string;
  labelBtnPostReview: string;

  // Types
  extraStatusType;

  constructor(private languages: LanguageService,
              private dialog: MatDialog,
              private platform: PlatformService) {
    this.extraStatusType = TaskExtraStatus;
  }

  ngOnInit(): void {
    // Get label
    this.labelTitle = this.languages.getLanguage().TASK_DETAILS_TASK_REVIEWS;
    this.labelBtnPostReview = this.languages.getLanguage().TASK_DETAILS_REVIEW_BTN_POST_REVIEW;

    // Check if both already have reviewed
    if (this.taskData.reviews && this.taskData.reviews.clientReview && this.taskData.reviews.professionalReview) {
      this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_ALL_REVIEWED;
    } else {
      // Set the right hint depending on client/professional and if one of other already published a review
      if (this.isClient()) {
        // Is the client

        // Check if is waiting for the professional
        if (this.taskData.reviews && this.taskData.reviews.clientReview) {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_CLIENT_WAITING_PROFESSIONAL;
        } else if (this.taskData.reviews && this.taskData.reviews.professionalReview) {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_CLIENT_WITH_PROFESSIONAL;
        } else {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_CLIENT_WITHOUT_PROFESSIONAL;
        }

      } else if (this.isProfessional()) {
        // Is the professional

        // Check if is waiting for the professional
        if (this.taskData.reviews && this.taskData.reviews.professionalReview) {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WAITING_CLIENT;
        } else if (this.taskData.reviews && this.taskData.reviews.clientReview) {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WITH_CLIENT;
        } else {
          this.labelHintText = this.languages.getLanguage().TASK_DETAILS_REVIEW_HINT_PROFESSIONAL_WITHOUT_CLIENT;
        }
      }
    }
  }

  isPostReviewEnabled() {
    return (this.isClient() && (!this.taskData.reviews || !this.taskData.reviews.clientReview))
      || (this.isProfessional() && (!this.taskData.reviews || !this.taskData.reviews.professionalReview));
  }

  isClient() {
    return this.taskData && this.userProfile && this.taskData.uid === this.userProfile.uid;
  }

  isProfessional() {
    return this.taskData
      && this.userProfile
      && this.taskData.acceptedOffer
      && this.taskData.acceptedOffer.userInfo.uid === this.userProfile.uid;
  }

  isPublic() {
    return this.isClient()
      || this.isProfessional()
      || (this.taskData.reviews && this.taskData.reviews.clientReview && this.taskData.reviews.professionalReview);
  }

  onPostReviewClick() {
    this.displayPostReviewDialog(this.taskData);
  }

  displayPostReviewDialog(taskData: TaskModel) {
    const dialogRef = this.dialog.open(PostReviewDialogComponent, {
      width: this.platform.isMobileSize() ? '90%' : '40%',
      data: {
        taskData: taskData,
        userProfile: this.userProfile
      }
    });

    const closePortfolioSub = dialogRef.componentInstance.closeDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    dialogRef.afterClosed().toPromise().then(value => {
      if (closePortfolioSub) {
        closePortfolioSub.unsubscribe();
      }
    });
  }

}
