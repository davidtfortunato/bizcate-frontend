import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ApiResponse, UserProfileModel} from '../../../../../functions/src/data';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {LanguageService} from '../../../core/language/language.service';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

@Component({
  selector: 'app-task-details-content',
  templateUrl: './task-details-content.component.html'
})
export class TaskDetailsContentComponent implements OnInit, OnChanges {
  @Input() taskAPIResponse: TaskModel;
  @Input() taskOffersResponse: TaskOfferModel[];
  @Input() userProfile: UserProfileModel;

  // Labels
  labelTaskNotFound: string;

  constructor(private languages: LanguageService) {
  }

  ngOnInit() {
    this.labelTaskNotFound = this.languages.getLanguage().TASK_DETAILS_NOT_FOUND;
  }

  ngOnChanges() {
    console.log('Task API Response: ' + this.taskAPIResponse);
  }


  isNotFound() {
    return !this.taskAPIResponse;
  }
}
