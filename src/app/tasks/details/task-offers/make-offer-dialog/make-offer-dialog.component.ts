import {Component, EventEmitter, Inject, OnDestroy, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {LanguageService} from '../../../../core/language/language.service';
import {FormControl, Validators} from '@angular/forms';
import {TaskModel} from '../../../../../../functions/src/data/task-model';
import {AppUIParams} from '../../../../shared/app.ui-params';
import {Subscription} from 'rxjs';
import {TasksApiService} from '../../../../core/api/tasks-api.service';
import {SnackBarService} from '../../../../shared/components/snack-bar/snack-bar.service';
import {TaskDetailsStoreService} from '../../store/task-details-store.service';

@Component({
  selector: 'app-task-make-offer',
  templateUrl: './make-offer-dialog.component.html',
  styleUrls: ['./make-offer-dialog.component.scss']
})
export class MakeOfferDialogComponent implements OnInit, OnDestroy {
  @Output() closeDialog = new EventEmitter();

  // Data
  taskData: TaskModel;

  // Labels
  labelHeaderTitle: string;
  labelHeaderSubtitle: string;
  labelTaskOffer: string;
  labelMakeOfferButton: string;
  offerPlaceholder: string;
  messagePlaceholder: string;
  messagePlaceholderHint: string;
  errorFieldRequired: string;

  // Form
  offerFormControl: FormControl = new FormControl('', Validators.required);
  messafeFormControl: FormControl = new FormControl('', Validators.required);

  // UI Params
  uiActionButtonBackgroundcolor: string;
  uiActionButtonTextcolor: string;

  // Subs
  subOfferFormChanges: Subscription;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private language: LanguageService,
              private tasksAPI: TasksApiService,
              private snack: SnackBarService,
              private taskStoreService: TaskDetailsStoreService) {
  }


  ngOnInit() {
    if (this.data.taskData) {
      this.taskData = this.data.taskData;

      // Set Default value
      this.offerFormControl.setValue(Number(this.taskData.budget.expectedBudget));
    }

    // Labels
    this.labelHeaderTitle = this.language.getLanguage().TASK_OFFER_MAKE_AN_OFFER_TITLE;
    this.offerPlaceholder = this.language.getLanguage().TASK_OFFER_OFFER_VALUE;
    this.labelTaskOffer = this.language.getLanguage().TASK_OFFER_OFFER_VALUE;
    this.labelMakeOfferButton = this.language.getLanguage().TASK_DETAILS_BUDGET_MAKE_OFFER;
    this.labelHeaderSubtitle = this.taskData.details.title;
    this.errorFieldRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;
    this.messagePlaceholder = this.language.getLanguage().TASK_OFFER_WRITE_A_MESSAGE;
    this.messagePlaceholderHint = this.language.getLanguage().TASK_OFFER_WRITE_A_MESSAGE_HINT;

    this.uiActionButtonBackgroundcolor = AppUIParams.actionButtonGreenColor;
    this.uiActionButtonTextcolor = AppUIParams.actionButtonWhiteColor;

    this.subOfferFormChanges = this.offerFormControl.valueChanges.subscribe(value => {
      if (value < 1) {
        this.offerFormControl.setValue(1);
      }
    });
  }

  onMakeOfferClick() {
    // Check if the form is valid
    if (this.offerFormControl.valid && this.messafeFormControl.valid) {
      const subMakeofferAPI = this.tasksAPI
        .makeAnOffer(this.taskData.id, this.offerFormControl.value, this.messafeFormControl.value)
        .subscribe(response => {
          if (subMakeofferAPI) {
            subMakeofferAPI.unsubscribe();
          }

          if (response.isSuccess) {
            // Refresh list offers
            this.taskStoreService.loadListTaskOffers(this.taskData.id);

            this.snack.displayMessage(this.language.getLanguage().TASK_OFFER_SUCCESS_SENT);
            this.closeDialog.emit();
          } else {
            this.snack.displayMessage(response.errorData);
          }
        });
    } else {
      this.offerFormControl.markAsTouched();
      this.messafeFormControl.markAsTouched();
    }
  }

  ngOnDestroy() {
    if (this.subOfferFormChanges) {
      this.subOfferFormChanges.unsubscribe();
    }
  }
}
