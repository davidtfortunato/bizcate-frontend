import {TaskCreateComponent} from './create/content/task-create.component';
import {TaskDetailsComponent} from './create/details/task-details.component';
import {TaskLocationComponent} from './create/location/task-location.component';
import {TaskUploadFilesComponent} from './create/upload-files/task-upload-files.component';
import {TaskBudgetComponent} from './create/budget/task-budget.component';
import {TaskUploadItemComponent} from './create/upload-files/upload-item/upload-item.component';
import {TaskCreateContainerComponent} from './create/task-create-container.component';
import {TaskDetailsContentComponent} from './details/content/task-details-content.component';
import {TaskDetailsContainerComponent} from './details/task-details-container.component';
import {TaskDetailsDescriptionComponent} from './details/task-details-header/task-description/task-description.component';
import {TaskDetailsShareComponent} from './details/task-details-header/task-share/task-share.component';
import {TaskDetailsStatusComponent} from './details/task-details-header/task-status/task-status.component';
import {TaskDetailsHeaderComponent} from './details/task-details-header/task-details-header.component';
import {TaskDetailsBudgetComponent} from './details/task-details-header/task-budget/task-budget.component';
import {TaskDetailsLocationComponent} from './details/task-details-header/task-location/task-location.component';
import {TaskPhotosComponent} from './details/task-details-header/task-photos/task-photos.component';
import {MakeOfferDialogComponent} from './details/task-offers/make-offer-dialog/make-offer-dialog.component';
import {TaskListContainerComponent} from './mytasks/task-list-container.component';
import {TaskBrowseContainerComponent} from './browsetasks/task-browse-container.component';
import {TaskScheduleContainerComponent} from './details/task-schedule/task-schedule-container.component';
import {TaskCalendarDialogComponent} from './details/task-schedule/calendar-dialog/calendar-dialog.component';
import {TaskReviewsContainerComponent} from './details/task-reviews/task-reviews-container.component';
import {PostReviewDialogComponent} from './details/task-reviews/post-dialog/post-review-dialog.component';
import {TaskBrowseFilterContainerComponent} from './browsetasks/filters/task-browse-filter-container.component';
import {TaskProfessionalListContainerComponent} from './my-pro-tasks/task-professional-list-container.component';

export const COMPONENTS = [TaskCreateComponent,
  TaskDetailsComponent,
  TaskLocationComponent,
  TaskUploadFilesComponent,
  TaskBudgetComponent,
  TaskUploadItemComponent,
  TaskCreateContainerComponent,
  TaskDetailsContainerComponent,
  TaskDetailsContentComponent,
  TaskDetailsBudgetComponent, // Details
  TaskDetailsDescriptionComponent,
  TaskDetailsShareComponent,
  TaskDetailsStatusComponent,
  TaskDetailsHeaderComponent,
  TaskDetailsLocationComponent,
  TaskPhotosComponent,
  MakeOfferDialogComponent,
  TaskListContainerComponent,
  TaskBrowseContainerComponent,
  TaskScheduleContainerComponent,
  TaskCalendarDialogComponent,
  TaskReviewsContainerComponent,
  PostReviewDialogComponent,
  TaskBrowseFilterContainerComponent,
  TaskProfessionalListContainerComponent];
