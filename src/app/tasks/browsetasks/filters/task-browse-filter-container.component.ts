import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {ToggleButtonData} from '../../../shared/components/filters/filter-button-toggle/filter-button-toggle.component';
import {TaskBrowseFilters, TaskLocationType} from '../../../../../functions/src/data/task-browse';
import {MAT_DIALOG_DATA, MatCheckboxChange} from '@angular/material';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../../store';
import {BrowseTasksStoreService} from '../store/browse-tasks-store.service';
import {TaskStatus} from '../../../../../functions/src/data/task-model';

@Component({
  selector: 'app-task-browse-filter-container',
  templateUrl: './task-browse-filter-container.component.html',
  styleUrls: ['./task-browse-filter-container.component.scss']
})
export class TaskBrowseFilterContainerComponent implements OnInit {
  @Input() browseFilterData: TaskBrowseFilters;
  @Input() isProfessional: boolean;
  @Output() closeDialog = new EventEmitter<TaskBrowseFilters>();

  // Labels
  labelHeaderTitle: string;
  labelTitleLocation: string;
  labelTitlePriceRange: string;
  labelTitleTaskStatus: string;
  labelTitleMyWorkableAreas: string;
  labelTitleTaskSkills: string;
  labelCheckboxOnlyMySkills: string;
  labelSaveBtn: string;
  labelCancelBtn: string;

  labelTitleSize: number;

  // Filter Location
  filterLocationButtons: ToggleButtonData[];
  filterStatusButtons: ToggleButtonData[];


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private languages: LanguageService,
              private browseStoreService: BrowseTasksStoreService) {

    this.browseFilterData = this.data.browseFilterData;
    this.isProfessional = this.data.isProfessional;
  }

  ngOnInit() {
    // Init labels
    this.labelHeaderTitle = this.languages.getLanguage().TASK_BROWSE_FILTER_DIALOG;
    this.labelTitleLocation = this.languages.getLanguage().TASK_BROWSE_FILTER_TITLE_LOCATION;
    this.labelTitlePriceRange = this.languages.getLanguage().TASK_BROWSE_FILTER_TITLE_PRICE_RANGE;
    this.labelTitleTaskStatus = this.languages.getLanguage().TASK_BROWSE_FILTER_TITLE_STATUS;
    this.labelTitleMyWorkableAreas = this.languages.getLanguage().TASK_BROWSE_FILTER_TITLE_WORKABLE_AREAS;
    this.labelTitleTaskSkills = this.languages.getLanguage().TASK_BROWSE_FILTER_TITLE_TASK_SKILLS;
    this.labelCheckboxOnlyMySkills = this.languages.getLanguage().TASK_BROWSE_FILTER_ONLY_MY_SKILLS;
    this.labelSaveBtn = this.languages.getLanguage().GENERIC_SAVE_LABEL;
    this.labelCancelBtn = this.languages.getLanguage().GENERIC_CANCEL_LABEL;
    this.labelTitleSize = 14;

    // Init Location buttons
    this.filterLocationButtons = [
      {
        id: TaskLocationType.ALL,
        label: this.languages.getLanguage().TASK_BROWSE_FILTER_LOCATION_ALL,
        isDefault: this.browseFilterData.locationType === TaskLocationType.ALL
      },
      {
        id: TaskLocationType.REMOTE,
        label: this.languages.getLanguage().TASK_BROWSE_FILTER_LOCATION_REMOTE,
        isDefault: this.browseFilterData.locationType === TaskLocationType.REMOTE
      },
      {
        id: TaskLocationType.PERSON,
        label: this.languages.getLanguage().TASK_BROWSE_FILTER_LOCATION_PERSON,
        isDefault: this.browseFilterData.locationType === TaskLocationType.PERSON
      }];

    // Init Task statys buttons
    this.filterStatusButtons = [
      {
        id: TaskStatus.OPEN,
        label: this.languages.getLanguage().TASK_DETAILS_STATUS_OPEN,
        isDefault: this.browseFilterData.taskStatus === TaskStatus.OPEN
      },
      {
        id: TaskStatus.ASSIGNED,
        label: this.languages.getLanguage().TASK_DETAILS_STATUS_ASSIGNED,
        isDefault: this.browseFilterData.taskStatus === TaskStatus.ASSIGNED
      },
      {
        id: TaskStatus.DONE,
        label: this.languages.getLanguage().TASK_DETAILS_STATUS_DONE,
        isDefault: this.browseFilterData.taskStatus === TaskStatus.DONE
      }];
  }

  onFilterLocationToggle(buttonId: string) {
    this.browseFilterData = {
      ...this.browseFilterData,
      locationType: buttonId as TaskLocationType
    };
  }
  onFilterTaskStatusToggle(buttonId: string) {
    this.browseFilterData = {
      ...this.browseFilterData,
      taskStatus: buttonId as TaskStatus
    };
  }

  onPriceRangeChanged(maxPriceLimit: number) {
    this.browseFilterData = {
      ...this.browseFilterData,
      price: {
        ...this.browseFilterData.price,
        maxPriceLimit: maxPriceLimit
      }
    };
  }

  onOnlyMySkillsChecked(isChecked: MatCheckboxChange) {
    this.browseFilterData = {
      ...this.browseFilterData,
      onlyMySkills: isChecked.checked
    };
  }

  onMyWorkableAreas(isChecked: MatCheckboxChange) {
    this.browseFilterData = {
      ...this.browseFilterData,
      onlyMyWorkAreas: isChecked.checked
    };
  }


  onSaveFiltersClick() {
    this.browseStoreService.saveFilters(this.browseFilterData);
    this.closeDialog.emit();
  }

  onCancelClick() {
    this.closeDialog.emit();
  }

}
