import {TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {AppMapLocation} from '../../../../../functions/src/data';
import {Actions, ActionTypes} from './actions';
import {TaskBrowseFilters, TaskLocationType} from '../../../../../functions/src/data/task-browse';

export interface State {
  mapChanged: boolean;
  centerLocation: AppMapLocation;
  mapRadius: number;
  listTaskIds: string[];
  tasks: {
    [taskId: string]: TaskModel
  };
  browseFilters: TaskBrowseFilters;
}

export const initialState: State = getInitState();

export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {

    case ActionTypes.Init:
      return {
        ...state
      };
    case ActionTypes.SetMapChanged:
      return {
        ...state,
        mapChanged: action.payload.hasChanged
      };

    case ActionTypes.SetMapCenterLocation:
      return {
        ...state,
        centerLocation: action.payload.centerLocation,
        mapChanged: true
      };

    case ActionTypes.SetMapRadius:
      return {
        ...state,
        mapRadius: action.payload.radius,
        mapChanged: true
      };
    case ActionTypes.AddTaskIdFound:
      let exist = false;
      state.listTaskIds.forEach(value => {
        if (value === action.payload.taskId) {
          exist = true;
        }
      });
      return {
        ...state,
        listTaskIds: exist ? state.listTaskIds : state.listTaskIds.concat(action.payload.taskId)
      };
    case ActionTypes.RemoveTaskIdFound:
      return {
        ...state,
        listTaskIds: state.listTaskIds.filter((value => value !== action.payload.taskId))
      };

    case ActionTypes.LoadTaskFoundResult:
      if (action.payload.response.isSuccess && action.payload.response.responseData) {
        return {
          ...state,
          tasks: {
            ...state.tasks,
            [action.payload.response.responseData.id]: action.payload.response.responseData
          }
        };
      }
      break;

    case ActionTypes.FiltersUpdatePriceRangeResult:
      return {
        ...state,
        browseFilters: {
          ...state.browseFilters,
          price: action.payload.priceRange
        }
      };

    case ActionTypes.FiltersUpdateFiltersData:
      return {
        ...state,
        browseFilters: {
          ...action.payload.filtersData,
          updatedAt: new Date().getTime()
        }
      };

    case ActionTypes.LoadListTasksFoundResult:

      if (action.payload.response.isSuccess) {

        // Update Price range on filters
        let minPrice = 0;
        let maxPrice = 0;

        let tasksUpdated: { [taskId: string]: TaskModel } = {};
        action.payload.response.responseData.forEach(task => {
          tasksUpdated = {
            ...tasksUpdated,
            [task.id]: task
          };

          const taskPrice = Number(task.budget.expectedBudget);
          if (minPrice === 0 || minPrice > taskPrice) {
            minPrice = taskPrice;
          }

          if (maxPrice < taskPrice) {
            maxPrice = taskPrice;
          }
        });

        return {
          ...state,
          tasks: {
            ...state.tasks,
            ...tasksUpdated
          },
          browseFilters: {
            ...state.browseFilters,
            price: {
              ...state.browseFilters.price,
              minPrice: minPrice,
              maxPrice: maxPrice
            }
          }
        };
      }
  }

  return state;
}


function getInitState(): State {
  return {
    mapChanged: true,
    centerLocation: null,
    mapRadius: 0,
    listTaskIds: [],
    tasks: {},
    browseFilters: {
      locationType: TaskLocationType.ALL,
      onlyMyWorkAreas: false,
      price: {
        minPrice: -1,
        maxPrice: -1,
        maxPriceLimit: -1
      },
      onlyMySkills: false,
      taskStatus: TaskStatus.OPEN
    }
  };
}

// Pure Functions
export const getMapChanged = (state: State) => state.mapChanged;
export const getTaskIds = (state: State) => state.listTaskIds;
export const getMapCenterLocation = (state: State) => state.centerLocation;
export const getMapRadius = (state: State) => state.mapRadius;
export const getTasks = (state: State) => state.tasks;
export const getBrowseFilters = (state: State) => state.browseFilters;
