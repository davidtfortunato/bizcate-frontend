import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromStore from './reducer';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {isTaskFitsFilters} from '../../../../../functions/src/data/task-browse';
import {getUserProfessionalProfileSkills} from '../../../auth/store/selectors';

export const selectBrowseTasksState = createFeatureSelector<fromStore.State>('browseTasks');

// Getters
export const getMapChanged = createSelector(selectBrowseTasksState, fromStore.getMapChanged);
export const getTaskIds = createSelector(selectBrowseTasksState, fromStore.getTaskIds);
export const getTasks = createSelector(selectBrowseTasksState, fromStore.getTasks);
export const getTaskIdsToLoad = createSelector(getTasks, getTaskIds, (tasks, taskIds) => {
  const taskIdsToLoad: string[] = [];
  if (taskIds && tasks) {
    taskIds.forEach(taskId => {
      if (!tasks[taskId]) {
        taskIdsToLoad.push(taskId);
      }
    });
  }
  return taskIdsToLoad;
});

export const getBrowseFilters = createSelector(selectBrowseTasksState, fromStore.getBrowseFilters);

export const getTasksToDisplay = createSelector(getTasks,
  getTaskIds,
  (tasksData: { [taskId: string]: TaskModel }, taskIds) => {
    let tasksArr: TaskModel[] = [];
    taskIds.forEach(taskId => {
      const taskData = tasksData[taskId as string];
      if (taskData) {
        tasksArr.push(taskData);
      }
    });

    tasksArr = tasksArr.sort((taskA, taskB) => {
      return taskA.postedTime - taskB.postedTime;
    });
    return tasksArr;
  });


export const getBrowseFiltersPriceRange = createSelector(getBrowseFilters, getTasksToDisplay, (filters, tasks) => {
  let minPrice = -1;
  let maxPrice = -1;

  // Look for the max and min price
  if (tasks) {
    tasks.forEach(task => {
      const taskBudget = Number(task.budget.expectedBudget);
      if (minPrice < 0 || taskBudget < minPrice) {
        minPrice = taskBudget;
      }
      if (maxPrice < 0 || maxPrice < taskBudget) {
        maxPrice = taskBudget;
      }
    });
  }

  return {
    minPrice: minPrice,
    maxPrice: maxPrice,
    maxPriceLimit: filters.price.maxPriceLimit > maxPrice ? maxPrice : filters.price.maxPriceLimit
  };
});
export const getBrowseFiltersUpdated = createSelector(getBrowseFilters,
  getBrowseFiltersPriceRange,
  (filters, priceRange) => {
    filters.price = priceRange;
    return filters;
  });



/**
 * Get tasks to display after filters
 */
export const getTasksToDisplayFiltered = createSelector(getTasksToDisplay,
  getBrowseFilters, getUserProfessionalProfileSkills,
  (listTasks, filters, proSkills) => {
    return listTasks.filter(task => isTaskFitsFilters(task, filters, proSkills));
  });

