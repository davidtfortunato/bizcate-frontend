import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {State} from '../../../store';
import {filter, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {
  ActionTypes,
  AddTaskIdFound,
  FiltersUpdatePriceRange, FiltersUpdatePriceRangeResult,
  LoadListTasksFound,
  LoadListTasksFoundResult,
  LoadTaskFound,
  LoadTaskFoundResult
} from './actions';
import {TasksApiService} from '../../../core/api/tasks-api.service';
import {getBrowseFiltersPriceRange, getTaskIds, getTaskIdsToLoad} from './selectors';
import {PriceRange} from '../../../../../functions/src/data/task-browse';

@Injectable()
export class BrowseTasksEffects {

  @Effect()
  addTaskIdFound$ = this.actions$.pipe(
    ofType<AddTaskIdFound>(ActionTypes.AddTaskIdFound),
    withLatestFrom(this.store.pipe(select(getTaskIds)),
      (action: AddTaskIdFound, taskIds: string[]) => {
        return {action, taskIds};
      }),
    filter((payload, index) => {
      // Check if the taskId already exist
      const setTasks = new Set(payload.taskIds);
      return setTasks.has(payload.action.payload.taskId);
    }),
    map((payload) => {
      // Check if has the taskId
      // return new LoadTaskFound({taskId: payload.action.payload.taskId});
      return new LoadListTasksFound();
    })
  );

  @Effect()
  loadListTasksFound$ = this.actions$.pipe(
    ofType<LoadListTasksFound>(ActionTypes.LoadListTasksFound),
    withLatestFrom(this.store.pipe(select(getTaskIdsToLoad)),
      (action: LoadListTasksFound, taskIds: string[]) => {
        return {action, taskIds};
      }),
    switchMap((payload) => {
      return this.tasksAPI
        .getListTasksDetails(payload.taskIds)
        .pipe(map(response => {
          return new LoadListTasksFoundResult({response});
        }));
    }));

  @Effect()
  filtersUpdatePriceRange$ = this.actions$.pipe(
    ofType<FiltersUpdatePriceRange>(ActionTypes.FiltersUpdatePriceRange),
    withLatestFrom(this.store.pipe(select(getBrowseFiltersPriceRange)), (action, priceRange: PriceRange) => {
      return {
        action: action,
        priceRange: priceRange
      };
    }),
    map((payload) => {
      return new FiltersUpdatePriceRangeResult({priceRange: payload.priceRange});
    })
  );

  constructor(private actions$: Actions,
              private store: Store<State>,
              private tasksAPI: TasksApiService) {
  }

}
