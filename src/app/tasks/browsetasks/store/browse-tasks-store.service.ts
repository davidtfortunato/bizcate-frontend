import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from './reducer';
import {
  AddTaskIdFound, FiltersUpdateFiltersData, FiltersUpdatePriceRange,
  Init,
  LoadListTasksFoundResult, LoadTaskFoundResult,
  RemoveTaskIdFound,
  SetMapCenterLocation,
  SetMapChanged,
  SetMapRadius
} from './actions';
import {ApiResponse, AppMapLocation} from '../../../../../functions/src/data';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {TaskBrowseFilters} from '../../../../../functions/src/data/task-browse';

@Injectable()
export class BrowseTasksStoreService {

  constructor(private store: Store<State>) {
  }

  init() {
    this.store.dispatch(new Init());
  }

  setMapCenterLocation(location: AppMapLocation) {
    this.store.dispatch(new SetMapCenterLocation({centerLocation: location}));
  }

  setMapRadius(mapRadius: number) {
    this.store.dispatch(new SetMapRadius({radius: mapRadius}));
  }

  addTaskIdFound(taskId: string) {
    this.store.dispatch(new AddTaskIdFound({taskId: taskId}));
  }

  removeTaskIdFound(taskId: string) {
    this.store.dispatch(new RemoveTaskIdFound({taskId: taskId}));
  }

  setMapChanged(hasChanged: boolean) {
    this.store.dispatch(new SetMapChanged({hasChanged: hasChanged}));
  }

  setTaskData(taskDataResponse: ApiResponse<TaskModel>) {
    this.store.dispatch(new LoadTaskFoundResult({response: taskDataResponse}));
  }

  updateFiltersPriceRange() {
    this.store.dispatch(new FiltersUpdatePriceRange());
  }

  saveFilters(browseFilters: TaskBrowseFilters) {
    this.store.dispatch(new FiltersUpdateFiltersData({filtersData: browseFilters}));
  }
}
