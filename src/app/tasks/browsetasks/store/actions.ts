import {Action} from '@ngrx/store';
import {ApiResponse, AppMapLocation} from '../../../../../functions/src/data';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {PriceRange, TaskBrowseFilters} from '../../../../../functions/src/data/task-browse';

export enum ActionTypes {
  Init = '[BrowseTask] Init Browser tasks',

  // Map Update
  SetMapChanged = '[BrowseTask] Set map changed',
  SetMapCenterLocation = '[BrowseTask] Set new location',
  SetMapRadius = '[BroswerTask] Set map radius',
  AddTaskIdFound = '[BrowserTask] Add taskId found',
  RemoveTaskIdFound = '[BrowserTask] Remove taskId found',

  // Load new tasks
  LoadListTasksFound = '[BrowserTasks] Load tasks found',
  LoadListTasksFoundResult = '[BrowserTasks] Load tasks found result',
  LoadTaskFound = '[BrowserTasks] Load founded task',
  LoadTaskFoundResult = '[BrowserTasks] Load founded task result',

  // Filters Actions
  FiltersUpdatePriceRange = '[BrowserTasksFilters] Update price range',
  FiltersUpdatePriceRangeResult = '[BrowserTasksFilters] Update price range result',
  FiltersUpdateFiltersData = '[BrowserTasksFilters] Update filters data'
}

export class Init implements Action {
  readonly type = ActionTypes.Init;

  constructor() {
  }
}

export class SetMapChanged implements Action {
  readonly type = ActionTypes.SetMapChanged;

  constructor(public payload: { hasChanged: boolean }) {
  }
}

export class SetMapCenterLocation implements Action {
  readonly type = ActionTypes.SetMapCenterLocation;

  constructor(public payload: { centerLocation: AppMapLocation }) {
  }
}

export class SetMapRadius implements Action {
  readonly type = ActionTypes.SetMapRadius;

  constructor(public payload: { radius: number }) {
  }
}

export class AddTaskIdFound implements Action {
  readonly type = ActionTypes.AddTaskIdFound;

  constructor(public payload: { taskId: string }) {
  }
}

export class RemoveTaskIdFound implements Action {
  readonly type = ActionTypes.RemoveTaskIdFound;

  constructor(public payload: { taskId: string }) {
  }
}

export class LoadListTasksFound implements Action {
  readonly type = ActionTypes.LoadListTasksFound;
}

export class LoadListTasksFoundResult implements Action {
  readonly type = ActionTypes.LoadListTasksFoundResult;

  constructor(public payload: { response: ApiResponse<TaskModel[]> }) {
  }
}

export class LoadTaskFound implements Action {
  readonly type = ActionTypes.LoadTaskFound;

  constructor(public payload: { taskId: string }) {
  }
}

export class LoadTaskFoundResult implements Action {
  readonly type = ActionTypes.LoadTaskFoundResult;

  constructor(public payload: { response: ApiResponse<TaskModel> }) {
  }
}

export class FiltersUpdatePriceRange implements Action {
  readonly type = ActionTypes.FiltersUpdatePriceRange;
}

export class FiltersUpdatePriceRangeResult implements Action {
  readonly type = ActionTypes.FiltersUpdatePriceRangeResult;

  constructor(public payload: { priceRange: PriceRange }) {
  }
}

export class FiltersUpdateFiltersData implements Action {
  readonly type = ActionTypes.FiltersUpdateFiltersData;

  constructor(public payload: { filtersData: TaskBrowseFilters }) {
  }
}

export type Actions = Init
  | SetMapChanged
  | SetMapCenterLocation
  | SetMapRadius
  | AddTaskIdFound
  | RemoveTaskIdFound
  | LoadListTasksFound
  | LoadListTasksFoundResult
  | LoadTaskFound
  | LoadTaskFoundResult
  | FiltersUpdatePriceRange
  | FiltersUpdatePriceRangeResult
  | FiltersUpdateFiltersData;
