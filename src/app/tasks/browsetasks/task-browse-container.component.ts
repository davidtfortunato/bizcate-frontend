import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {TasksApiService} from '../../core/api/tasks-api.service';
import {AppMapLocation, UserProfileModel, UserWorkableArea} from '../../../../functions/src/data';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {LanguageService} from '../../core/language/language.service';
import {TaskLocationApiService} from '../../core/api/task-location-api.service';
import {BrowseTasksStoreService} from './store/browse-tasks-store.service';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getBrowseFiltersUpdated, getMapChanged, getTasksToDisplay, getTasksToDisplayFiltered} from './store/selectors';
import {GeoQuery} from 'geofire';
import {animate, style, transition, trigger} from '@angular/animations';
import {getUserProfile} from '../../auth/store/selectors';
import {MapMarker} from '../../map/map-component/map.component';
import {HeaderActionButton} from '../../shared/components/title-header/title-header.component';
import {AppUIParams} from '../../shared/app.ui-params';
import {MatDialog} from '@angular/material';
import {PlatformService} from '../../shared/platform';
import {TaskBrowseFilters} from '../../../../functions/src/data/task-browse';
import {TaskBrowseFilterContainerComponent} from './filters/task-browse-filter-container.component';
import {UserProfileRoleType} from '../../../../functions/src/data/user-model';

@Component({
  selector: 'app-task-browse-container',
  templateUrl: './task-browse-container.component.html',
  styleUrls: ['./task-browse-container.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})
export class TaskBrowseContainerComponent implements OnInit, OnDestroy {

  // Labels
  headerTitle: { title?: string, subtitle?: string } = {};

  // Stream Data
  listTasks$: Observable<TaskModel[]>;
  userProfile$: Observable<UserProfileModel>;
  mapHasChanged$: Observable<Boolean>;
  browseTasksFilters$: Observable<TaskBrowseFilters>;

  // Data
  listTasksData: TaskModel[] = [];
  listMapMarkers: MapMarker[] = [];
  listAreasDraws: UserWorkableArea[] = [];
  centerLocation: AppMapLocation;
  mapRadiusDistance: number;
  userProfile: UserProfileModel;
  browseFilters: TaskBrowseFilters;

  // Header buttons
  headerActionButtons: HeaderActionButton[] = [];

  // Labels
  labelSearchAreaBtn: string;
  labelEmptyList: string;

  // Geo Query
  geoQuery: GeoQuery;

  // Subscriptions
  subListTasks: Subscription;
  subBrowseFilters: Subscription;
  geoQuerySubs: Subscription;

  constructor(private tasksAPI: TasksApiService,
              private linkNavigation: NavigationLinksService,
              private languages: LanguageService,
              private taskLocationsAPI: TaskLocationApiService,
              private browseTasksService: BrowseTasksStoreService,
              private store: State<fromStore.State>,
              private dialog: MatDialog,
              private platform: PlatformService) {

  }

  ngOnInit() {

    // Init store
    this.browseTasksService.init();

    // Header buttons
    this.headerActionButtons = [{
      id: '1',
      state: 'enabled',
      backgroundColor: AppUIParams.actionButtonGreenColor,
      textColor: AppUIParams.actionButtonWhiteColor,
      label: this.languages.getLanguage().TASK_BROWSE_FILTER_BTN
    }];

    // Languages
    this.headerTitle.title = this.languages.getLanguage().TASKS_MODULE_TITLE;
    this.headerTitle.subtitle = this.languages.getLanguage().TASK_LIST_TITLE;
    this.labelSearchAreaBtn = this.languages.getLanguage().TASK_BROWSE_SEARCH_AREA_BTN;
    this.labelEmptyList = this.languages.getLanguage().TASK_BROWSE_EMPTY_LIST;

    this.mapHasChanged$ = this.store.pipe(select(getMapChanged));
    this.userProfile$ = this.store.pipe(select(getUserProfile));
    const sub = this.userProfile$.subscribe(userProfile => {
      if (sub) {
        sub.unsubscribe();
      }
      this.userProfile = userProfile;
    });
    this.listTasks$ = this.store.pipe(select(getTasksToDisplayFiltered));
    // this.listTasks$ = this.tasksAPI.getAllTasks();
    this.subListTasks = this.listTasks$.subscribe(tasks => {
      this.listTasksData = tasks;
      this.updateMapMarkers();
    });


    // Subscribe to browse tasks filters
    this.browseTasksFilters$ = this.store.pipe(select(getBrowseFiltersUpdated));
    this.subBrowseFilters = this.browseTasksFilters$.subscribe(data => {
      this.browseFilters = data;
    });
  }

  ngOnDestroy() {
    if (this.geoQuerySubs) {
      this.geoQuerySubs.unsubscribe();
    }
    if (this.geoQuery) {
      this.geoQuery.cancel();
    }

    if (this.subListTasks) {
      this.subListTasks.unsubscribe();
    }

    if (this.subBrowseFilters) {
      this.subBrowseFilters.unsubscribe();
    }
  }

  onHeaderButtonClick(headerButton: HeaderActionButton) {
    if (headerButton.id === '1') {
      this.displayFilterDialog();
    }
  }

  onTaskOpened(taskData: TaskModel) {
    this.linkNavigation.openTaskDetails(taskData.id);
  }

  onMapCenterChanged(centerLocation: AppMapLocation) {
    this.centerLocation = centerLocation;
    if (this.mapRadiusDistance > 0 && this.centerLocation) {
      this.browseTasksService.setMapCenterLocation(centerLocation);

      // check if is the first time
      if (!this.geoQuery) {
        this.updateGeoQuery();
      }
    }
  }

  onMapRadiusChanged(radiusDistance: number) {
    this.mapRadiusDistance = radiusDistance;
    this.browseTasksService.setMapRadius(radiusDistance);

  }

  onSearchOnArea() {
    this.updateGeoQuery();
    this.browseTasksService.setMapChanged(false);
  }

  updateMapMarkers() {
    this.listMapMarkers = [];

    this.listTasksData.forEach(task => {
      if (task.location && task.location.mapLocation) {
        this.listMapMarkers.push({
          id: task.id,
          isSelected: false,
          location: task.location.mapLocation
        });
      }
    });
  }

  /**
   * Callback when the mouse pointer is hover the task
   */
  onTaskListHover(task: TaskModel) {
    this.listMapMarkers.forEach(marker => {
      if (marker.id !== task.id && marker.isSelected) {
        marker.isSelected = false;
      } else if (marker.id === task.id) {
        marker.isSelected = true;
      }
    });
    this.listMapMarkers = Array.from(this.listMapMarkers);
  }

  /**
   * Update geo query
   */
  updateGeoQuery() {
    if (this.centerLocation && this.mapRadiusDistance > 0) {
      this.updateDrawAreas();

      // Check if should initialize
      if (!this.geoQuery) {
        this.geoQuery = this.taskLocationsAPI.getGeoQueryTaskArea(this.mapRadiusDistance, this.centerLocation);

        // Setup callbacks
        this.geoQuery.on('key_entered', (key, location, distance) => {
          this.browseTasksService.addTaskIdFound(key);
          this.loadTaskData(key);
        });
        this.geoQuery.on('key_exited', (key, location, distance) => {
          this.browseTasksService.removeTaskIdFound(key);
        });

      } else {
        this.geoQuery.updateCriteria({
          center: [this.centerLocation.latitude, this.centerLocation.longitude],
          radius: this.mapRadiusDistance
        });
      }
    }
  }

  /**
   * Load task data
   */
  loadTaskData(taskId: string) {
    // Validate if was already loaded
    let exists = false;
    this.listTasksData.forEach(task => {
      if (task.id === taskId) {
        // already exist
        exists = true;
      }
    });

    if (!exists) {
      // Load task data
      const sub = this.tasksAPI.getTaskDetails(taskId).subscribe(response => {
        if (sub) {
          sub.unsubscribe();
        }
        this.browseTasksService.setTaskData(response);
      });
    }
  }

  updateDrawAreas() {
    this.listAreasDraws = [];

    if (this.centerLocation && this.mapRadiusDistance > 0) {
      this.listAreasDraws.push({
        uid: this.userProfile ? this.userProfile.uid : '',
        mapArea: {
          id: 1,
          radius: this.mapRadiusDistance * 1000,
          location: this.centerLocation
        }
      });
    }
  }

  displayFilterDialog() {
    const dialogRef = this.dialog.open(TaskBrowseFilterContainerComponent, {
      width: this.platform.isMobileSize() ? '90%' : '60%',
      data: {
        browseFilterData: this.browseFilters,
        isProfessional: this.userProfile.role === UserProfileRoleType.CLIENT_PROFESSIONAL
      }
    });

    const closePortfolioSub = dialogRef.componentInstance.closeDialog.subscribe(() => {
      // Close dialog
      dialogRef.close();
    });

    dialogRef.afterClosed().toPromise().then(value => {
      if (closePortfolioSub) {
        closePortfolioSub.unsubscribe();
      }
    });
  }
}
