import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {Observable, Subscription} from 'rxjs';
import {ServiceSkill, ServiceModel, UserProfileModel} from '../../../../../functions/src/data/index';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../../store/index';
import {getUserProfile} from '../../../auth/store/selectors';
import {TaskCreateStoreService} from '../store/task-create-store.service';
import {TaskBudget, TaskDetails, TaskLocation, TaskModel} from '../../../../../functions/src/data/task-model';
import {TaskFileData} from '../upload-files/upload-item/upload-item.component';
import {HeaderActionButton} from '../../../shared/components/title-header/title-header.component';
import {AppUIParams} from '../../../shared/app.ui-params';
import {SnackBarService} from '../../../shared/components/snack-bar/snack-bar.service';
import {StorageApiService} from '../../../core/api/storage-api.service';
import {StorageFileModel} from '../../../../../functions/src/data/storage-model';
import {ProgressBarInterface} from '../../../shared/components/progress-bar/progress-bar.component';
import {AppStoreService} from '../../../store/app-store.service';

const PREFIX_FILENAME = 'task_';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss']
})
export class TaskCreateComponent implements OnInit, OnChanges {
  @Input() taskDetailsData: TaskModel;
  @Input() taskFiles: TaskFileData[];
  @Input() isTaskReady: boolean;

  // Data
  userProfileData$: Observable<UserProfileModel>;
  progressBarData: ProgressBarInterface = {
    id: 'taskCreate',
    display: false,
    percentage: 0,
    type: 'determine'
  };

  // UI
  headerTitle: { title: string, subtitle: string };
  headerActionButtons: HeaderActionButton[] = [];


  constructor(private language: LanguageService,
              private store: State<fromStore.State>,
              private taskCreateStore: TaskCreateStoreService,
              private snack: SnackBarService,
              private storageAPI: StorageApiService,
              private appStoreService: AppStoreService) {

  }

  ngOnInit() {
    // Init component
    this.taskCreateStore.initTaskCreation();

    this.userProfileData$ = this.store.pipe(select(getUserProfile));

    this.headerTitle = {
      title: this.language.getLanguage().TASKS_MODULE_TITLE,
      subtitle: this.language.getLanguage().TASK_MODULE_CREATE_SUBTITLE
    };

    this.updateHeaderButtons();
  }

  ngOnChanges() {
    this.updateHeaderButtons();

  }

  updateHeaderButtons() {
    this.headerActionButtons = [{
      id: '1',
      label: this.language.getLanguage().TASKS_CREATE_HEADER_BUTTON_CREATE_TASK,
      backgroundColor: this.isTaskReady ? AppUIParams.actionButtonGreenColor
        : AppUIParams.actionButtonOrangeBackgroundColor,
      textColor: '#FFFFFF',
      state: this.isTaskReady ? 'enabled' : 'disabled'
    }];
  }

  updateStateTaskData() {
    this.taskCreateStore.updateTaskData(this.taskDetailsData);
  }


  onActionButtonClick(actionButton: HeaderActionButton) {
    switch (actionButton.id) {
      case '1':
        this.executeTaskCreation();
        break;
    }
  }

  executeTaskCreation() {
    if (this.taskFiles && this.taskFiles.length > 0 && this.taskFiles[0].file) {
      this.executeUploadFile(this.taskFiles[0]);
    } else {
      this.taskCreateStore.executeCreateTask();
    }
  }

  /**
   * Upload file
   */
  executeUploadFile(fileData: TaskFileData) {
    if (fileData) {
      this.storageAPI.uploadUserFile(fileData.file, PREFIX_FILENAME
        + '_'
        + Date.now() + '_'
        + fileData.index,
        (storageFileModel: StorageFileModel) => {
          this.progressBarData.type = 'indetermine';
          this.progressBarData.percentage = 100;
          this.appStoreService.hideProgressBar(this.progressBarData.id);
          this.taskDetailsData.uploadedFiles.push(storageFileModel);

          // Remove the file from the list
          this.taskFiles = this.taskFiles.filter(file => file.index !== fileData.index);
          this.taskCreateStore.updateTaskFiles(this.taskFiles);
          this.executeTaskCreation();
        },
        (error: any) => {
          // upload failed
          this.progressBarData.display = false;
          this.progressBarData.percentage = 0;
          this.appStoreService.hideProgressBar(this.progressBarData.id);
          this.snack.displayMessage(this.language.getLanguage().ERROR_GENERIC_UPLOAD_PHOTO, 'error');
        },
        (percentage: number) => {
          this.progressBarData.display = true;
          this.progressBarData.percentage = percentage;
          this.appStoreService.displayProgressBarByData(this.progressBarData);
        });
    }
  }

  /*** DETAILS **/

  onListSkillsChanged(listSkills: ServiceSkill[]) {
    this.taskDetailsData.skills = listSkills;
    this.updateStateTaskData();
  }

  onTaskDetailsChanged(taskDetails: TaskDetails) {
    this.taskDetailsData.details = taskDetails;
    this.updateStateTaskData();
  }

  /** Location **/

  onTaskLocationChanged(taskLocation: TaskLocation) {
    this.taskDetailsData.location = taskLocation;
    this.updateStateTaskData();
  }

  /** Budget **/

  onTaskBudgetChanged(taskBudget: TaskBudget) {
    this.taskDetailsData.budget = taskBudget;
    this.updateStateTaskData();
  }

  /** Files **/

  onTaskFilesChanged(listFiles: TaskFileData[]) {
    this.taskCreateStore.updateTaskFiles(listFiles);
  }

}
