import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from './reducer';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {CreateNewTask, Init, UpdateTaskDetails, UploadedFilesChanged} from './actions';
import {TaskFileData} from '../upload-files/upload-item/upload-item.component';

@Injectable()
export class TaskCreateStoreService {

  constructor(private store: Store<State>) {
  }

  initTaskCreation() {
    this.store.dispatch(new Init());
  }

  updateTaskData(taskData: TaskModel) {
    this.store.dispatch(new UpdateTaskDetails({taskData}));
  }

  updateTaskFiles(taskFiles: TaskFileData[]) {
    this.store.dispatch(new UploadedFilesChanged({files: taskFiles}));
  }

  executeCreateTask() {
    this.store.dispatch(new CreateNewTask());
  }

}
