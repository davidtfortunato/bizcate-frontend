import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {ActionTypes, CreateNewTask, CreateNewTaskResult} from './actions';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {TasksApiService} from '../../../core/api/tasks-api.service';
import {select, Store} from '@ngrx/store';
import {getTaskDetails} from './selectors';
import {State} from '../../../store';
import {of} from 'rxjs';
import {getQuickResponse} from '../../../../../functions/src/data';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {LanguageService} from '../../../core/language/language.service';
import {SnackBarService} from '../../../shared/components/snack-bar/snack-bar.service';
import {getUserProfile} from '../../../auth/store/selectors';
import {AppStoreService} from '../../../store/app-store.service';

@Injectable()
export class Effects {


  @Effect()
  createNewTask$ = this.actions$.pipe(
    ofType<CreateNewTask>(ActionTypes.CreateNewTask),
    withLatestFrom(this.store.pipe(select(getTaskDetails)), this.store.pipe(select(getUserProfile))),
    switchMap(([action, taskData, userProfile]) => {
      this.appStoreService.displayProgressBarById('createTask');
      return this.tasksAPI
        .createNewTask(taskData, userProfile)
        .pipe(map(response => {
          return new CreateNewTaskResult({response});
        }), catchError(err => of(new CreateNewTaskResult({response: getQuickResponse(false, err)}))));
    })
  );

  @Effect({dispatch: false})
  createNewTaskResult$ = this.actions$.pipe(
    ofType<CreateNewTaskResult>(ActionTypes.CreateNewTaskResult),
    map((action) => {
      this.appStoreService.hideProgressBar('createTask');
      console.log('Create Task Response: ' + JSON.stringify(action.payload));
      if (action.payload.response.isSuccess) {
        this.snackbarService.displayMessage(this.language.getLanguage().GENERIC_SAVED_SUCCESS, 'success');
        this.navigation.openTaskDetails(action.payload.response.responseData.id);
      } else {
        this.snackbarService.displayMessage(this.language.getLanguage().ERROR_GENERIC_SAVING_FAILED, 'error');
      }
    })
  );

  constructor(private actions$: Actions,
              private tasksAPI: TasksApiService,
              private store: Store<State>,
              private navigation: NavigationLinksService,
              private language: LanguageService,
              private snackbarService: SnackBarService,
              private appStoreService: AppStoreService) {
  }

}
