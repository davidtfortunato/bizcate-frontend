import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromStore from './reducer';

export const selectCreateTaskState = createFeatureSelector<fromStore.State>('createTask');


// Getters
export const getTaskDetails = createSelector(selectCreateTaskState, fromStore.getTaskDetails);
export const getUploadedFiles = createSelector(selectCreateTaskState, fromStore.getTaskFilesUploaded);
export const isTaskReadyToCreate = createSelector(getTaskDetails, (taskData => {
  return taskData.details.title
    && taskData.details.description
    && (taskData.location.mapLocation || taskData.location.isRemote)
    && taskData.budget.expectedBudget && taskData.budget.dueDate && taskData.skills.length > 0;
}));
