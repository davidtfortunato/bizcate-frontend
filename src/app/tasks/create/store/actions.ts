import {Action} from '@ngrx/store';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {TaskFileData} from '../upload-files/upload-item/upload-item.component';
import {ApiResponse} from '../../../../../functions/src/data';

export enum ActionTypes {
  Init = '[Task] Init Create Task',

  UpdateTaskDetails = '[Task] Update Task details',

  // Files changes
  UploadedFilesChanged = '[Task] Uploaded Files Changed',

  // Create New Task
  CreateNewTask = '[Task] Create new task',
  CreateNewTaskResult = '[Task] Create new task Result'
}

export class Init implements Action {
  readonly type = ActionTypes.Init;
}

export class UpdateTaskDetails implements Action {
  readonly type = ActionTypes.UpdateTaskDetails;

  constructor(public payload: {taskData: TaskModel}) {
  }
}

export class UploadedFilesChanged implements Action {
  readonly type = ActionTypes.UploadedFilesChanged;

  constructor(public payload: {files: TaskFileData[]}) {
  }
}


export class CreateNewTask implements Action {
  readonly type = ActionTypes.CreateNewTask;
}

export class CreateNewTaskResult implements Action {
  readonly type = ActionTypes.CreateNewTaskResult;

  constructor(public payload: {response: ApiResponse<TaskModel>}) {
  }
}


export type Actions = Init
  | UpdateTaskDetails
  | UploadedFilesChanged
  | CreateNewTask
  | CreateNewTaskResult;
