import {Actions, ActionTypes} from './actions';
import {TaskBudgetType, TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {TaskFileData} from '../upload-files/upload-item/upload-item.component';

export interface State {
  task: TaskModel;
  filesUploaded: TaskFileData[];
}

export const initialState: State = getInitState();

export function reducer(state = initialState, action: Actions): State {

  switch (action.type) {

    case ActionTypes.Init:
      return getInitState();

    case ActionTypes.UpdateTaskDetails:
      return {
        ...state,
        task: {
          ...action.payload.taskData
        }
      };

    case ActionTypes.UploadedFilesChanged:
      return {
        ...state,
        filesUploaded: Object.assign([], action.payload.files)
      };
  }

  return state;
}

function getInitState() {
  return {
    task: {
      uid: '',
      details: {
        title: '',
        description: ''
      },
      skills: [],
      location: {
        mapLocation: null,
        isRemote: false
      },
      budget: {
        expectedBudget: '',
        budgetType: TaskBudgetType.TOTAL,
        dueDate: ''
      },
      uploadedFiles: [],
      status: TaskStatus.OPEN
    },
    filesUploaded: []
  };
}

// Pure Functions
export const getTaskDetails = (state: State) => state.task;
export const getTaskFilesUploaded = (state: State) => state.filesUploaded;
