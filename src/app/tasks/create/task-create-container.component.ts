import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TaskModel} from '../../../../functions/src/data/task-model';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getTaskDetails, getUploadedFiles, isTaskReadyToCreate} from './store/selectors';
import {TaskFileData} from './upload-files/upload-item/upload-item.component';

@Component({
  selector: 'app-task-create-container',
  templateUrl: './task-create-container.component.html'
})
export class TaskCreateContainerComponent implements OnInit {

  // Data
  taskData$: Observable<TaskModel>;
  isTaskReady$: Observable<boolean>;
  taskFiles$: Observable<TaskFileData[]>;

  constructor(private store: State<fromStore.State>) {
  }

  ngOnInit() {
    this.taskData$ = this.store.pipe(select(getTaskDetails));
    this.isTaskReady$ = this.store.pipe(select(isTaskReadyToCreate));
    this.taskFiles$ = this.store.pipe(select(getUploadedFiles));
  }

}
