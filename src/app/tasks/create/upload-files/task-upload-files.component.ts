import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {TaskFileData} from './upload-item/upload-item.component';

@Component({
  selector: 'app-tasks-create-upload-files',
  templateUrl: './task-upload-files.component.html',
  styleUrls: ['./task-upload-files.component.scss']
})
export class TaskUploadFilesComponent implements OnInit {
  @Output() taskFilesChanged = new EventEmitter<TaskFileData[]>();

  // Data
  listUploadItems: TaskFileData[] = [];

  // Labels
  labelTitle: string;
  labelSubtitle: string;

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    // Title labels
    this.labelTitle = this.language.getLanguage().TASK_CREATE_UPLOAD_FILES_TITLE;
    this.labelSubtitle = this.language.getLanguage().TASK_CREATE_UPLOAD_FILES_SUBTITLE;

    // Init array
    this.listUploadItems.push({
      index: 0
    });
  }

  onRemoveUploadItem(index: number) {
    console.log('Removed index: ' + index);
    this.listUploadItems = this.listUploadItems.filter((value) => {
      return value.index !== index;
    });

    // Update indexes
    this.listUploadItems.forEach((value, arrIndex) => {
      value.index = arrIndex;
    });
    this.emitListFilesChanged();
  }

  onUploadItemChanged(fileData: TaskFileData) {
    // Check if is a new upload
    if (fileData.index <= this.listUploadItems.length - 1) {
      // It's an update
      this.listUploadItems[fileData.index] = fileData;
    } else {
      this.onAddedNewFile(fileData.index);
    }
  }

  onAddedNewFile(index: number) {
    // It's a new file data
    this.listUploadItems.push({
      index: index + 1
    });
    this.emitListFilesChanged();
  }

  emitListFilesChanged() {
    this.taskFilesChanged.emit(this.listUploadItems);
  }

}
