import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

export interface TaskFileData {
  index: number;
  imgUrl?: string;
  file?: any;
}

@Component({
  selector: 'app-tasks-create-upload-item',
  templateUrl: './upload-item.component.html',
  styleUrls: ['./upload-item.component.scss']
})
export class TaskUploadItemComponent implements OnInit {
  @Input() fileData: TaskFileData;
  @Output() fileChanged = new EventEmitter<TaskFileData>();
  @Output() removeItem = new EventEmitter<number>(); // Index
  @Output() addedNewFile = new EventEmitter<number>();


  constructor() {
  }

  ngOnInit() {
  }

  onFileToUpade(file) {
    if (!this.fileData.file) {
      this.emitAddedFileUploaded();
    }
    this.fileData.file = file;
    this.emitFileChanges();
  }

  onSrcImage(url) {
    this.fileData.imgUrl = url;
    this.emitFileChanges();
  }

  onFileRemove() {
    this.removeItem.emit(this.fileData.index);
  }

  emitFileChanges() {
    if (this.fileData.imgUrl && this.fileData.file) {
      this.fileChanged.emit(this.fileData);
    }
  }

  emitAddedFileUploaded() {
    this.addedNewFile.emit(this.fileData.index);
  }

}
