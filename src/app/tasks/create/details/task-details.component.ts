import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ServiceSkill} from '../../../../../functions/src/data';
import {TaskDetails} from '../../../../../functions/src/data/task-model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tasks-create-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent implements OnInit, OnDestroy {
  @Output() taskDetailsChanged = new EventEmitter<TaskDetails>();
  @Output() listSkillsChanged = new EventEmitter<ServiceSkill[]>();

  // Placeholders
  titlePlaceholder: string;
  descriptionPlaceholder: string;

  // Labels
  labelTitle: string;
  labelSubtitle: string;
  labelSkillsMiddleTitle: string;
  errorFieldRequired: string;

  // Form controllers
  form: FormGroup;
  titleFormControl = new FormControl('', Validators.required);
  descriptionFormControl = new FormControl('', Validators.required);

  // Data
  listSkills: ServiceSkill[];

  // Subscription
  subsFormChanges: Subscription;

  constructor(private language: LanguageService,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    // Init data
    this.listSkills = [];

    // Title labels
    this.labelTitle = this.language.getLanguage().TASK_CREATE_DETAILS_TITLE;
    this.labelSubtitle = this.language.getLanguage().TASK_CREATE_DETAILS_SUBTITLE;
    this.titlePlaceholder = this.language.getLanguage().TASK_CREATE_DETAILS_TASK_TITLE;
    this.descriptionPlaceholder = this.language.getLanguage().TASK_CREATE_DETAILS_TASK_DESCRIPTION;
    this.errorFieldRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;
    this.labelSkillsMiddleTitle = this.language.getLanguage().TASK_CREATE_DETAILS_TASK_SKILLS_TITLE;

    // Initialize Form
    this.initForm();

    this.subsFormChanges = this.form.valueChanges.subscribe(value => {
      this.taskDetailsChanged.emit({
        title: this.titleFormControl.value,
        description: this.descriptionFormControl.value
      });
    });
  }

  ngOnDestroy() {
    if (this.subsFormChanges) {
      this.subsFormChanges.unsubscribe();
    }
  }

  private initForm() {

    // Init Form
    this.form = this.formBuilder.group({
      name: this.titleFormControl,
      description: this.descriptionFormControl
    });
  }

  onSkillAdded(skill: ServiceSkill) {
    this.listSkills.push(skill);
    this.onListSkillsChanged();
  }

  onRemovedSkill(skill: ServiceSkill) {
    // Remove from the list
    this.listSkills = this.listSkills.filter(value => {
      return value.category.id !== skill.category.id && value.serviceName.id !== skill.serviceName.id;
    });

    this.onListSkillsChanged();
  }

  onListSkillsChanged() {
    this.listSkillsChanged.emit(this.listSkills);
  }
}
