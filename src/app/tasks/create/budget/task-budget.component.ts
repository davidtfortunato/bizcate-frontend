import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TaskBudget, TaskBudgetType, TaskModel} from '../../../../../functions/src/data/task-model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tasks-create-budget',
  templateUrl: './task-budget.component.html',
  styleUrls: ['./task-budget.component.scss']
})
export class TaskBudgetComponent implements OnInit, OnDestroy {
  @Output() budgetDataChanged = new EventEmitter<TaskBudget>();

  // Placeholders
  budgetPlaceholder: string;
  duedatePlaceholder: string;

  // Limits
  duedateMin = new Date();

  // Labels
  labelTitle: string;
  labelSubtitle: string;
  errorFieldRequired: string;
  labelBudgetTypeHourly: string;
  labelBudgetTypeTotal: string;

  // Form
  form: FormGroup;
  budgetFormControl: FormControl = new FormControl('', Validators.required);
  budgetTypeFormControl: FormControl = new FormControl('', Validators.required);
  duedateFormControl: FormControl = new FormControl('', Validators.required);

  // Types
  budgetTypes = TaskBudgetType;

  // Subscriptions
  subsFormChanges: Subscription;

  constructor(private language: LanguageService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    // Title labels
    this.labelTitle = this.language.getLanguage().TASK_CREATE_BUDGET_TITLE;
    this.labelSubtitle = this.language.getLanguage().TASK_CREATE_BUDGET_SUBTITLE;
    this.errorFieldRequired = this.language.getLanguage().ERROR_GENERIC_FIELD_REQUIRED;
    this.labelBudgetTypeHourly = this.language.getLanguage().TASK_CREATE_BUDGET_TYPE_HOURLY;
    this.labelBudgetTypeTotal = this.language.getLanguage().TASK_CREATE_BUDGET_TYPE_TOTAL;

    // Init Form
    this.budgetPlaceholder = this.language.getLanguage().TASK_CREATE_PLACEHOLDER_BUDGET_VALUE;
    this.duedatePlaceholder = this.language.getLanguage().TASK_CREATE_PLACEHOLDER_DUEDATE;
    this.initForm();

    // Listening to form changes
    this.subsFormChanges = this.form.valueChanges.subscribe((value => {
      this.budgetDataChanged.emit({
        expectedBudget: this.budgetFormControl.value,
        budgetType: this.budgetTypeFormControl.value,
        dueDate: this.duedateFormControl.value ? this.duedateFormControl.value.getTime() : ''
      });
    }));
    this.form.markAsTouched();
  }

  ngOnDestroy() {
    if (this.subsFormChanges) {
      this.subsFormChanges.unsubscribe();
    }
  }

  private initForm() {

    if (!this.form) {
      this.budgetFormControl = new FormControl('', Validators.required);
      this.budgetTypeFormControl = new FormControl('', Validators.required);
      this.budgetTypeFormControl.setValue(this.budgetTypes.TOTAL);
      this.duedateFormControl = new FormControl('', Validators.required);
      this.duedateFormControl.setValue(new Date());

      // Init Form
      this.form = this.formBuilder.group({
        budget: this.budgetFormControl,
        budgetType: this.budgetTypeFormControl,
        duedate: this.duedateFormControl
      });
    }
  }

}
