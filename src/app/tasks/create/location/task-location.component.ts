import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../../core/language/language.service';
import {Observable} from 'rxjs';
import {AppMapLocation, MapAreaDraw, UserProfileModel, UserWorkableArea} from '../../../../../functions/src/data';
import {GeolocationService} from '../../../core/location/geolocation.service';
import {FormControl} from '@angular/forms';
import {SearchLocationService} from '../../../core/location/search-location.service';
import {TaskLocation} from '../../../../../functions/src/data/task-model';
import {MatCheckboxChange} from '@angular/material/typings/esm5/checkbox';
import {LocationResult} from '../../../../../functions/src/data/location-result';

@Component({
  selector: 'app-tasks-create-location',
  templateUrl: './task-location.component.html',
  styleUrls: ['./task-location.component.scss']
})
export class TaskLocationComponent implements OnInit {

  @Input() userProfile: UserProfileModel;
  @Output() taskLocationChanged = new EventEmitter<TaskLocation>();

  // Data
  currentLocation$: Observable<AppMapLocation>;
  searchLocationResult$: Observable<LocationResult[]>;
  searchLocationResultSelected: AppMapLocation;
  markersDraw: UserWorkableArea[] = [];
  isRemoteTask = false;

  // Labels
  labelTitle: string;
  labelSubtitle: string;
  labelRemoteWork: string;
  labelSearchLocation: string;

  // Form
  searchLocationFormControl: FormControl = new FormControl('');

  constructor(private language: LanguageService, private geoLocation: GeolocationService, private searchLocation: SearchLocationService) {
  }

  ngOnInit() {
    // Title labels
    this.labelTitle = this.language.getLanguage().TASK_CREATE_LOCATION_TITLE;
    this.labelSubtitle = this.language.getLanguage().TASK_CREATE_LOCATION_SUBTITLE;
    this.labelRemoteWork = this.language.getLanguage().TASK_CREATE_REMOTE_WORK;
    this.labelSearchLocation = this.language.getLanguage().TASK_CREATE_SEARCH_LOCATION;

    // Current location
    this.currentLocation$ = this.geoLocation.getCurrentLocation();

    // Listening to search location Changes
    // Listen to city name changes
    this.searchLocationFormControl.valueChanges.subscribe(value => {
      this.searchLocationResult$ = this.searchLocation.searchLocation(value);
    });
  }


  onSearchLocationSelected(locationSelected) {
    const location: LocationResult = locationSelected.option.id;
    this.searchLocationResultSelected = {
      longitude: location.lon,
      latitude: location.lat
    };
  }

  onMapDrawsChanged(markersDraw: MapAreaDraw[]) {
    if (markersDraw && markersDraw.length > 0) {
      // Only add the new marker
      this.markersDraw = [{uid: this.userProfile.uid, mapArea: markersDraw[markersDraw.length - 1]}];
    }
    this.emitDataChanges();
  }

  onRemoteWorkChange(isRemote: MatCheckboxChange) {
    this.isRemoteTask = isRemote.checked;
    this.emitDataChanges();
  }

  emitDataChanges() {
    this.taskLocationChanged.emit({
      mapLocation: this.markersDraw.length > 0 ? this.markersDraw[0].mapArea.location : null,
      isRemote: this.isRemoteTask
    });
  }
}
