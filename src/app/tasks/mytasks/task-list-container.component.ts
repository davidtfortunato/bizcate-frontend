import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TaskModel, TaskStatus} from '../../../../functions/src/data/task-model';
import {TasksApiService} from '../../core/api/tasks-api.service';
import {ApiResponse, UserProfileModel} from '../../../../functions/src/data';
import {NavigationLinksService} from '../../core/navigation/navigation-links.service';
import {LanguageService} from '../../core/language/language.service';
import {UserAPIService} from '../../core/api';
import {SwitchItemData} from '../../shared/components/switch/switch-container.component';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../store';
import {getUserProfile} from '../../auth/store/selectors';
import {CentralUserDataStoreService} from '../../core/central-data/store/central-user-data-store.service';
import {
  getClientMyAssignedTasks,
  getClientMyDoneTasks,
  getClientMyPendingTasks
} from '../../core/central-data/store/selectors';

@Component({
  selector: 'app-task-list-container',
  templateUrl: './task-list-container.component.html',
  styleUrls: ['./task-list-container.component.scss']
})
export class TaskListContainerComponent implements OnInit {

  // Types
  taskStatsTypes = TaskStatus;

  // Labels
  headerTitle: { title?: string, subtitle?: string } = {};
  labelSelectTaskStatus: string;
  labelEmptyMessage: string;

  // Stream Data
  myOpenTasks$: Observable<TaskModel[]>;
  myAssignedTasks$: Observable<TaskModel[]>;
  myDoneTasks$: Observable<TaskModel[]>;

  // Data
  statusSwitchData: SwitchItemData[];
  currentTaskStatusSelected: SwitchItemData;

  constructor(
    private store: State<fromStore.State>,
    private linkNavigation: NavigationLinksService,
    private languages: LanguageService,
    private centralDataStore: CentralUserDataStoreService) {

  }

  ngOnInit() {

    // Observe data
    this.myOpenTasks$ = this.centralDataStore.getStore().pipe(select(getClientMyPendingTasks));
    this.myAssignedTasks$ = this.centralDataStore.getStore().pipe(select(getClientMyAssignedTasks));
    this.myDoneTasks$ = this.centralDataStore.getStore().pipe(select(getClientMyDoneTasks));

    // Languages
    this.headerTitle.title = this.languages.getLanguage().MY_TASKS_MODULE_TITLE;
    this.headerTitle.subtitle = this.languages.getLanguage().TASK_LIST_TITLE;
    this.labelSelectTaskStatus = this.languages.getLanguage().MY_TASKS_SWITCH_TITLE_CHANGE_STATUS;
    this.labelEmptyMessage = this.languages.getLanguage().TASK_BROWSE_EMPTY_LIST;

    // Generate Task Status switcher
    // Set Switch Data
    this.statusSwitchData = [];
    this.currentTaskStatusSelected = {
      id: TaskStatus.OPEN,
      label: this.languages.getLanguage().TASK_DETAILS_STATUS_OPEN
    };
    // Open Task
    this.statusSwitchData.push(this.currentTaskStatusSelected);
    // Assigned Task
    this.statusSwitchData.push({
      id: TaskStatus.ASSIGNED,
      label: this.languages.getLanguage().TASK_DETAILS_STATUS_ASSIGNED
    });
    // Done Task
    this.statusSwitchData.push({
      id: TaskStatus.DONE,
      label: this.languages.getLanguage().TASK_DETAILS_STATUS_DONE
    });

  }

  onSwitchSelectionChanged(switchItem: SwitchItemData) {
    this.currentTaskStatusSelected = switchItem;
  }
}
