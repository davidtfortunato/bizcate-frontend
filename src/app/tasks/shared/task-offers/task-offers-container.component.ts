import {Component, Input, OnChanges} from '@angular/core';
import {TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';
import {LanguageService} from '../../../core/language/language.service';
import {UserProfileModel} from '../../../../../functions/src/data/index';

@Component({
  selector: 'app-task-details-offers',
  templateUrl: './task-offers-container.component.html',
  styleUrls: ['./task-offers-container.component.scss']
})
export class TaskOffersContainerComponent implements OnChanges {
  @Input() taskData: TaskModel;
  @Input() userProfile: UserProfileModel;
  @Input() listOffers: TaskOfferModel[];

  // Labels
  labelTitle: string;

  constructor(private language: LanguageService) {
  }

  ngOnChanges() {
    this.labelTitle = this.taskData.status === TaskStatus.OPEN
      ? this.language.getLanguage().TASK_OFFER_TITLE : this.language.getLanguage().TASK_OFFER_ASSIGNEE_TITLE;
  }

}
