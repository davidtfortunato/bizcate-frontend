import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {TaskModel} from '../../../../../functions/src/data/task-model';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {LanguageService} from '../../../core/language/language.service';
import {Observable} from 'rxjs';
import {UserProfileModel} from '../../../../../functions/src/data';
import {select, State} from '@ngrx/store';
import * as fromStore from '../../../store';
import {getUserProfile} from '../../../auth/store/selectors';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, OnChanges {
  @Input() listTasks: TaskModel[];
  @Input() listOffers: TaskOfferModel[]; // Since the list of tasks can be shown
  @Input() displayOffers = false;
  @Input() displayGrid = false;

  // Data
  userProfile$: Observable<UserProfileModel>;

  // Labels
  labelEmptyList: string;

  // List tasks to display merged with the task offers received
  listTasksMerged: TaskModel[];

  constructor(private linkNavigation: NavigationLinksService,
              private languages: LanguageService,
              private store: State<fromStore.State>) {

  }

  ngOnInit() {
    this.labelEmptyList = this.languages.getLanguage().TASK_BROWSE_EMPTY_LIST;

    // Get user profile
    this.userProfile$ = this.store.pipe(select(getUserProfile));
  }

  ngOnChanges() {
    if (this.listTasks) {
      this.listTasksMerged = this.listTasks;
    } else {
      this.listTasksMerged = [];
    }

    // Add list offers to the list tasks
    if (this.listOffers) {
      this.listOffers.forEach(taskOffer => {
        taskOffer.taskData.taskOffersData = [taskOffer];
        this.listTasksMerged.push(taskOffer.taskData);
      });
    }
  }

  onTaskOpened(taskData: TaskModel) {
    this.linkNavigation.openTaskDetails(taskData.id);
  }

  getListItemsClass() {
    return {
      'displayGrid': this.displayGrid
    };
  }
}
