import {Component, Input, OnChanges} from '@angular/core';
import {TaskBudgetType, TaskExtraStatus, TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {LanguageService} from '../../../core/language/language.service';
import {UtilsService} from '../../../core/utils/utils.service';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {UserProfileModel} from '../../../../../functions/src/data/index';
import {AppUIParams} from '../../../shared/app.ui-params';
import {ScheduleStatus} from '../../../../../functions/src/data/calendar-model';

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.scss']
})
export class TaskListItemComponent implements OnChanges {
  @Input() taskData: TaskModel;
  @Input() currentUser: UserProfileModel;

  // labels
  labelDueDateText: string;
  labelBudgetText: string;
  labelStatusText: string;
  labelTaskLocation: string;
  label: string;
  labelSchedulePhase: string;

  // UI
  uiStatusColor: string = AppUIParams.actionButtonGreenColor;
  uiStatusBackgroundColor: string = AppUIParams.actionButtonGreenColorOpacity;
  uiScheduleColor: string = AppUIParams.actionButtonOrangeBackgroundColor;
  uiScheduleBackgroundColor: string = AppUIParams.actionButtonOrangeBackgroundColorOpacity;

  constructor(private languages: LanguageService, private utils: UtilsService, private navigationLinks: NavigationLinksService) {
  }


  ngOnChanges() {
    if (this.taskData && this.taskData.budget && this.taskData.budget.dueDate) {
      this.labelDueDateText = this.languages.getLanguage().TASK_DETAILS_DUEDATE
        + ': '
        + this.utils.convertMillisToFormattedDateDay(Number(this.taskData.budget.dueDate));
    } else {
      this.labelDueDateText = this.languages.getLanguage().TASK_DETAILS_DUEDATE_NOT_DEFINED;
    }

    // set budget text
    this.labelBudgetText = `${this.taskData.budget.expectedBudget} ${this.taskData.budget.currency ? this.taskData.budget.currency : '€'}`;
    if (this.taskData.budget.budgetType === TaskBudgetType.HOURLY) {
      this.labelBudgetText += '/' + this.languages.getLanguage().TASK_DETAILS_BUDGET_PER_HOUR;
    }

    // Set task status label
    switch (this.taskData.status) {
      case TaskStatus.OPEN:
        this.labelStatusText = this.languages.getLanguage().TASK_DETAILS_STATUS_OPEN;
        break;
      case TaskStatus.ASSIGNED:
        this.labelStatusText = this.languages.getLanguage().TASK_DETAILS_STATUS_ASSIGNED;

        // Check if should set some label to schedule phase
        if (this.currentUser
          && (this.currentUser.uid === this.taskData.uid || this.currentUser.uid === this.taskData.acceptedOffer.userInfo.uid)
          && this.taskData.scheduledStatus) {
          // Should display the Schedule Phase
          switch (this.taskData.scheduledStatus) {
            case ScheduleStatus.SELECTING_AVAILABILITY_PHASE1:
              this.labelSchedulePhase = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_AVAILABILITY_PHASE1;
              break;
            case ScheduleStatus.SELECTING_TIME_PHASE2:
              this.labelSchedulePhase = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_SCHEDULE_DAY_PHASE2;
              break;
            case ScheduleStatus.SCHEDULED_PHASE3:
              // Check if is in progress
              if (this.taskData.extraStatus === TaskExtraStatus.IN_PROGRESS) {
                this.labelSchedulePhase = this.languages.getLanguage().TASK_DETAILS_STATUS_IN_PROGRESS;
              } else {
                this.labelSchedulePhase = this.languages.getLanguage().TASK_DETAILS_SCHEDULE_SCHEDULED_PHASE3;
              }
              break;
          }
        }
        break;
      case TaskStatus.DONE:
        // Check if is expired
        if (this.taskData.extraStatus && this.taskData.extraStatus === TaskExtraStatus.EXPIRED) {
          this.labelStatusText = this.languages.getLanguage().TASK_DETAILS_STATUS_EXPIRED;
          this.uiStatusBackgroundColor = AppUIParams.actionButtonRedColorOpacity;
          this.uiStatusColor = AppUIParams.actionButtonRedColor;
        } else {
          this.labelStatusText = this.languages.getLanguage().TASK_DETAILS_STATUS_DONE;
        }
        break;
      default:
        this.labelStatusText = this.languages.getLanguage().TASK_DETAILS_STATUS_EXPIRED;
        this.uiStatusBackgroundColor = AppUIParams.actionButtonRedColorOpacity;
        this.uiStatusColor = AppUIParams.actionButtonRedColor;
        break;
    }

    // set task location label
    this.labelTaskLocation = this.taskData.location.addressLocation ? this.taskData.location.addressLocation : '';
    this.labelTaskLocation += this.taskData.location.isRemote ? ' (' + this.languages.getLanguage().TASK_DETAILS_LOCATION_REMOTE_WORK + ')'
      : '';
  }

  /**
   * Navigate to the username screen
   */
  onUserNameClick() {
    this.navigationLinks.openUserProfile(this.taskData.uid);
  }

}
