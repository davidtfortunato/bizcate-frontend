import {NgModule} from '@angular/core';
import {COMPONENTS} from './index';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {MatIconModule} from '@angular/material';
import {TaskDetailsStoreService} from '../details/store/task-details-store.service';

@NgModule({
  providers: [TaskDetailsStoreService],
  declarations: [COMPONENTS],
  exports: [COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule],
  entryComponents: [
  ]
})
export class TasksSharedModule {
}
