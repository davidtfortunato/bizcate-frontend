import {TaskOffersContainerComponent} from './task-offers/task-offers-container.component';
import {TaskOfferItemComponent} from './offer-item/offer-item.component';
import {TaskListItemComponent} from './task-item/task-list-item.component';
import {TaskListComponent} from './list-tasks/task-list.component';

export const COMPONENTS = [
  TaskOffersContainerComponent,
  TaskOfferItemComponent,
  TaskListItemComponent,
  TaskListComponent];
