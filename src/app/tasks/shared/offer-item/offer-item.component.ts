import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {TaskOfferModel} from '../../../../../functions/src/data/task-offer-model';
import {UserProfileModel} from '../../../../../functions/src/data/index';
import {AppUIParams} from '../../../shared/app.ui-params';
import {LanguageService} from '../../../core/language/language.service';
import {TaskModel, TaskStatus} from '../../../../../functions/src/data/task-model';
import {TasksApiService} from '../../../core/api/tasks-api.service';
import {TaskDetailsStoreService} from '../../details/store/task-details-store.service';
import {NavigationLinksService} from '../../../core/navigation/navigation-links.service';
import {AppStoreService} from '../../../store/app-store.service';

@Component({
  selector: 'app-task-offer-item',
  templateUrl: './offer-item.component.html',
  styleUrls: ['./offer-item.component.scss']
})
export class TaskOfferItemComponent implements OnInit, OnChanges {
  @Input() taskDetails: TaskModel;
  @Input() taskOffer: TaskOfferModel;
  @Input() userProfile: UserProfileModel;

  // Data
  isTaskOwner: boolean;
  isOfferOwner: boolean;
  isTaskOpen: boolean;
  hasRejectButton: boolean;
  offerValue: string;

  // UI
  uiButtonBackgroundGreen: string;
  uiButtonBackgroundRed: string;
  uiButtonTextColor: string;
  btnFontSize = 12;
  btnTopBottomPadding = 0;

  // Labels
  labelButtonAcceptOffer: string;
  labelButtonRejectOffer: string;
  labelButtonRemoveOffer: string;

  labelStatusRejected: string;
  labelStatusAssignee: string;
  labelStatusPending: string;

  constructor(private language: LanguageService,
              private taskAPIService: TasksApiService,
              private taskDetailsStore: TaskDetailsStoreService,
              private appStoreService: AppStoreService,
              private navigationLink: NavigationLinksService) {
  }

  ngOnInit() {
    this.hasRejectButton = false; // Not available yet

    this.uiButtonBackgroundGreen = AppUIParams.actionButtonGreenColor;
    this.uiButtonBackgroundRed = AppUIParams.actionButtonRedColor;
    this.uiButtonTextColor = AppUIParams.actionButtonWhiteColor;

    // Set labels
    this.labelButtonAcceptOffer = this.language.getLanguage().TASK_OFFER_BTN_ACCEPT_LABEL;
    this.labelButtonRemoveOffer = this.language.getLanguage().TASK_OFFER_BTN_REMOVE_LABEL;
    this.labelButtonRejectOffer = this.language.getLanguage().TASK_OFFER_BTN_REJECT_LABEL;
  }

  ngOnChanges() {
    if (this.taskOffer && this.userProfile) {
      this.isTaskOpen = this.taskDetails.status === TaskStatus.OPEN;
      this.isTaskOwner = this.taskOffer.taskData.uid === this.userProfile.uid;
      this.isOfferOwner = this.taskOffer.userInfo.uid === this.userProfile.uid;
      this.offerValue = String(this.taskOffer.valueOffered);
      this.offerValue += this.taskOffer.taskData.budget.currency ? this.taskOffer.taskData.budget.currency : '€';
    }
  }

  onAcceptOffer() {
    this.appStoreService.displayProgressBarById('acceptOffer');
    const subsAPI = this
      .taskAPIService
      .acceptTaskOffer({taskId: this.taskDetails.id, taskOfferId: this.taskOffer.id})
      .subscribe(response => {
        // Unsubscrive
        if (subsAPI) {
          subsAPI.unsubscribe();
        }

        // Re-init task
        this.appStoreService.hideProgressBar('acceptOffer');
        this.taskDetailsStore.init(this.taskDetails.id);
        this.appStoreService.reloadPage();
      });
  }

  onRejectOffer() {
    /*const subsAPI = this.taskAPIService.acceptAnOffer(this.taskOffer.id)
      .subscribe(response => {
        // Unsubscrive
        if (subsAPI) {
          subsAPI.unsubscribe();
        }

        // Re-init task
        this.taskDetailsStore.init(this.taskDetails.id);
      });*/
  }

  onRemoveOffer() {
    this.appStoreService.displayProgressBarById('removeOffer');
    const subsAPI = this.taskAPIService.removeUserOffer(this.taskOffer.id)
      .subscribe(response => {
        // Unsubscrive
        if (subsAPI) {
          subsAPI.unsubscribe();
        }

        // Re-init task
        this.appStoreService.hideProgressBar('removeOffer');
        this.taskDetailsStore.init(this.taskDetails.id);
        this.appStoreService.reloadPage();
      });
  }

  onProfileClick() {
    this.navigationLink.openProfessionalProfile(this.taskOffer.userInfo.uid);
  }

}
