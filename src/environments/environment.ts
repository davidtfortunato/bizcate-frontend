// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Prod
/*export const environment = {
  production: true,
  api: {
    protocol: 'https',
    baseUrl: 'bizcate.herokuapp.com',
    port: null, // Don't set the port
    apiContext: 'api'
  },
  firebaseConfig: {
    apiKey: 'AIzaSyDeVxFcULnyfA5O8CHBqeylkhbCrHcjJM0',
    authDomain: 'bizcate-app.firebaseapp.com',
    databaseURL: 'https://bizcate-app.firebaseio.com',
    projectId: 'bizcate-app',
    storageBucket: 'bizcate-app.appspot.com',
    messagingSenderId: '501408078160',
    appId: "1:501408078160:web:31b80ca9d5ebc8c8a93455"
  }
};*/


// Staging
export const environment = {
  production: false,
  api: {
    protocol: 'http://',
    baseUrl: 'localhost',
    port: '8080',
    apiContext: '/api'
  },
  firebaseConfig: {
    apiKey: 'AIzaSyALGZCYNa51DldQQW6T77NMGn_dJnYn0c4',
    authDomain: 'bizcate-app-dev.firebaseapp.com',
    databaseURL: 'https://bizcate-app-dev.firebaseio.com',
    projectId: 'bizcate-app-dev',
    storageBucket: 'bizcate-app-dev.appspot.com',
    messagingSenderId: '633155659895',
    appId: '1:633155659895:web:43a35aa445c4570bcabe72'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
