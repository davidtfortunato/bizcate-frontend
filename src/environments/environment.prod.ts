export const environment = {
  production: true,
  api: {
    protocol: 'https',
    baseUrl: 'bizcate.herokuapp.com',
    port: null, // Don't set the port
    apiContext: 'api'
  },
  firebaseConfig: {
    apiKey: 'AIzaSyDeVxFcULnyfA5O8CHBqeylkhbCrHcjJM0',
    authDomain: 'bizcate-app.firebaseapp.com',
    databaseURL: 'https://bizcate-app.firebaseio.com',
    projectId: 'bizcate-app',
    storageBucket: 'bizcate-app.appspot.com',
    messagingSenderId: '501408078160'
  }
};
